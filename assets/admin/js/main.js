$(function () {
									
  'use strict'
  $('.nav-tabs li.disabledTab > a').on('click',function (e) {
  	e.preventDefault();
  	e.stopImmediatePropagation();
  });



  $('.select').select2();
  
  // Settings Periode
  $('#status-periode').select2({
  	placeholder:'Silahkan Pilih Status',
  	allowClear:true
  })

  $('.my-colorpicker2').colorpicker()

  /*NEW PEGAWAI*/
	  // Tab Penempatan Baru Placeholder
	  $('#branchEmp').select2({placeholder: 'Silahkan Pilih Cabang',allowClear:true});
	  $('#mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
	  $('#deptEmp').select2({placeholder: 'Silahkan Pilih Departments',allowClear:true});
	  $('#subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments'});
	  $('#jabatanEmp').select2({placeholder: 'Silahkan Pilih Jabatan'});
	  $('#statskarEmp').select2({placeholder: 'Silahkan Pilih Status Karyawan'});
	  // Tab Pribadi Placeholder
	  $('#jk').select2({placeholder: 'Silahkan Pilih Jenis Kelamin',allowClear:true});
	  $('#agamaEmp').select2({placeholder: 'Silahkan Pilih Agama',allowClear:true});
	  $('#provktp').select2({placeholder: 'Silahkan Pilih Provinsi',allowClear:true});
	  $('#kabkota').select2({placeholder: 'Silahkan Pilih Kabupaten/Kota',allowClear:true});
	  $('#kec').select2({placeholder: 'Silahkan Pilih Kecamatan',allowClear:true});
	  $('#kel').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	  $('#provdomisili').select2({placeholder: 'Silahkan Pilih Provinsi',allowClear:true});
	  $('#kabkotadomisili').select2({placeholder: 'Silahkan Pilih Kabupaten/Kota',allowClear:true});
	  $('#kecdomisili').select2({placeholder: 'Silahkan Pilih Kecamatan',allowClear:true});
	  $('#keldomisili').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	  // Tab Orang Tua
	  $('#provOrtu').select2({placeholder: 'Silahkan Pilih Provinsi',allowClear:true});
	  $('#kabkotaOrtu').select2({placeholder: 'Silahkan Pilih Kabupaten/Kota',allowClear:true});
	  $('#kecOrtu').select2({placeholder: 'Silahkan Pilih Kecamatan',allowClear:true});
	  $('#kelOrtu').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	  //Tab Keluarga
	  $('#statusMenikah').select2({placeholder: 'Silahkan Pilih Status Pernikahan',allowClear:true});
	  $('#statusAnak1').select2({placeholder: 'Silahkan Pilih Status Pernikahan Anak Ke 1',allowClear: true});
	  $('#statusAnak2').select2({placeholder: 'Silahkan Pilih Status Pernikahan Anak Ke 2',allowClear: true});
	  $('#statusAnak3').select2({placeholder: 'Silahkan Pilih Status Pernikahan Anak Ke 3',allowClear: true});
	  $('#provPasutri').select2({placeholder: 'Silahkan Pilih Provinsi',allowClear:true});
	  $('#kabkotaPasutri').select2({placeholder: 'Silahkan Pilih Kabupaten/Kota',allowClear:true});
	  $('#kecpasutri').select2({placeholder: 'Silahkan Pilih Kecamatan',allowClear:true});
	  $('#kelPasutri').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	  //Tab Bank
	  $('#bankEmp').select2({placeholder: 'Silahkan Pilih Bank',allowClear:true});
	  //Tab Pendidikan
	  $('#pendidikanEmp').select2({placeholder: 'Silahkan Pilih Pendidikan Terakhir',allowClear: true});

	/*Edit Pegawai*/
	  // Tab Penempatan Baru Placeholder
	  $('#e-branchEmp').select2({placeholder: 'Silahkan Pilih Cabang',allowClear:true});
	  $('#e-mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
	  $('#e-deptEmp').select2({placeholder: 'Silahkan Pilih Departments',allowClear:true});
	  $('#e-subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments'});
	  $('#e-jabatanEmp').select2({placeholder: 'Silahkan Pilih Jabatan'});
	  $('#e-statskarEmp').select2({placeholder: 'Silahkan Pilih Status Karyawan',allowClear:true});
	  // Tab Pribadi Placeholder
	  $('#e-jk').select2({placeholder: 'Silahkan Pilih Jenis Kelamin',allowClear:true});
	  $('#e-agamaEmp').select2({placeholder: 'Silahkan Pilih Agama',allowClear:true});
	  $('#e-provktp').select2({placeholder: 'Silahkan Pilih Provinsi',allowClear:true});
	  $('#e-kabkota').select2({placeholder: 'Silahkan Pilih Kabupaten/Kota',allowClear:true});
	  $('#e-kec').select2({placeholder: 'Silahkan Pilih Kecamatan',allowClear:true});
	  $('#e-kel').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	  $('#e-provdomisili').select2({placeholder: 'Silahkan Pilih Provinsi',allowClear:true});
	  $('#e-kabkotadomisili').select2({placeholder: 'Silahkan Pilih Kabupaten/Kota',allowClear:true});
	  $('#e-kecdomisili').select2({placeholder: 'Silahkan Pilih Kecamatan',allowClear:true});
	  $('#e-keldomisili').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	  // Tab Orang Tua
	  $('#e-provOrtu').select2({placeholder: 'Silahkan Pilih Provinsi',allowClear:true});
	  $('#e-kabkotaOrtu').select2({placeholder: 'Silahkan Pilih Kabupaten/Kota',allowClear:true});
	  $('#e-kecOrtu').select2({placeholder: 'Silahkan Pilih Kecamatan',allowClear:true});
	  $('#e-kelOrtu').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	  //Tab Keluarga
	  $('#e-statusMenikah').select2({placeholder: 'Silahkan Pilih Status Pernikahan',allowClear:true});
	  $('#e-statusAnak1').select2({placeholder: 'Silahkan Pilih Status Pernikahan Anak Ke 1',allowClear: true});
	  $('#e-statusAnak2').select2({placeholder: 'Silahkan Pilih Status Pernikahan Anak Ke 2',allowClear: true});
	  $('#e-statusAnak3').select2({placeholder: 'Silahkan Pilih Status Pernikahan Anak Ke 3',allowClear: true});
	  $('#e-provPasutri').select2({placeholder: 'Silahkan Pilih Provinsi',allowClear:true});
	  $('#e-kabkotaPasutri').select2({placeholder: 'Silahkan Pilih Kabupaten/Kota',allowClear:true});
	  $('#e-kecpasutri').select2({placeholder: 'Silahkan Pilih Kecamatan',allowClear:true});
	  $('#e-kelPasutri').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	  //Tab Bank
	  $('#e-bankEmp').select2({placeholder: 'Silahkan Pilih Bank',allowClear:true});
	  //Tab Pendidikan
	  $('#e-pendidikanEmp').select2({placeholder: 'Silahkan Pilih Pendidikan Terakhir',allowClear: true});
  

  // $('#parent	id').select2();
  	
    $(document).on('change','.deptcombo',function () {
	    var deptid = $(this).val();
	    var div       = $('#subdepartments');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findsubdept',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled>--Select Sub Departments--</option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
	            $('.subdepartments').select2();
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  
    $(document).on('change','.dept-jabatan',function () {
	    var deptid = $(this).val();
	    var div       = $('#subdeptid');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findsubdept',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled>--Select Sub Departments--</option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
	            $('.subdeptid').select2();
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
 /*New Pegawai*/
  	// Tab Penempatan Baru
  	// 
  	// Cabang
    $(document).on('change','#branchEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#mrEmp');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/branch/findmr',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  	// Departments
    $(document).on('change','#deptEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#subdeptEmp');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findsubdept',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  	// Sub Departments
    $(document).on('change','#subdeptEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#jabatanEmp');
	    var op        = " ";
	   	
	   	var subdeptText = $("#subdeptEmp option:selected").text();


	   	if (subdeptText == "SALES") {
	   		$("#kodesales").removeAttr('style');
	   		$("#kodesales").prop("required",true);
	   	}else{
	   		$("#kodesales").css('display','none');
	   		$("#kodesales").prop("required",false);
	   	}

	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findjabatan',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].jabatan+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#jabatanEmp').select2({placeholder: 'Silahkan Pilih Jabatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  	// Jabatan
    $(document).on('change','#jabatanEmp',function () {
	    var deptid = $(this).val();
	    var level       = $('#level');
	    var grade       = $('#grade');
	    var op = "";
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/jabatan/findjabatanchild',
	        data:{'id':deptid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	          data = data[0];
	          level.val(data.level);
	          level.trigger('change');
	          grade.val(data.grade);
	            
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});

	/*// Level
	$(document).on('change','#level',function (e) {
		e.preventDefault();
		console.log($(this).val());
	})
*/

	/*Kontrak awal*/
	 $("#statskarEmp").change(function () {
    	$('#statskarEmp option:selected').each(function () {
    		var id = $(this).val(),
    			text = $(this).text();

			if (id == 5) {
				$('#nkonawal,#nkonakhir').each(function(index){
		    		$(this).find('small').css('display','none');
		    	})
				$('#kontrak-awal,#kontrak-akhir').removeAttr('required');
				$('#kontrak-awal,#kontrak-akhir').prop('readonly', true);		
			}else{
				$('#nkonawal,#nkonakhir').each(function(index){
		    		$(this).find('small').css('display','');
		    	})
				$('#kontrak-awal,#kontrak-akhir').attr('required');
				$('#kontrak-awal,#kontrak-akhir').prop('readonly', false);		
			}
    		// console.log(`id : ${id}, text = ${text}`);
    	})	
    })
	/*Kontrak awal*/

    var tableAtasan ;
    var cabangTitle;
	// Atasan Pegawai
	$(document).on('click','#searchAtasan', function (e) {
		e.preventDefault();
		var cabang = $("#branchEmp");
		
		
		// Data Table
		$.ajax({
            type  : 'post',
            url   : base_url + "kepeg/pegawai/empfilter",
            async : false,
        	data:{'id':cabang.val()},
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                            '<td>'+data[i].nama+'</td>'+
                            '<td>'+data[i].nip+'</td>'+
                            '<td>'+data[i].cabang+'</td>'+
                            '<td>'+data[i].deptname+'</td>'+
                            '<td>'+data[i].jabatan+'</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
                cabangTitle = $("#branchEmp option:selected").text();
                if ( ! $.fn.DataTable.isDataTable( '#matasan-table' ) ) {
					tableAtasan = $('#matasan-table').DataTable({
						'lengthChange': false,
						'ordering'    : true,
						'info'        : true,
						'autoWidth'   : false,
				        "ordering":false,
				        // "paging":false,
				        // scrollY:        "300px",
				        // scrollX:        true,
				        // scrollCollapse: true,
						
					});
				}
            }
        });

		$("#matasan-table").dataTable().fnDraw();
		$("#modal-atasan-pegawai").modal('show');
	    $('#matasan-table tbody').on('click', 'tr', function () {
            var matasanTable = $('#matasan-table').DataTable();
            var data = matasanTable.row(this).data();
            var nama = data[0],
            	nik = data[1];
            var nama_nik = nama + " - " + nik;

            $("#atasan").val(nama_nik);
            $("#modal-atasan-pegawai").modal('hide');
        });
	});


	// Tab Pribadi
	//KTP
	// Provinsi
	$(document).on('change','#provktp',function (e) {
		e.preventDefault();
		var provid = $(this).val(),
		    div = $('#kabkota'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kabupaten',
	        data:{'id':provid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kabkota').select2({placeholder: 'Silahkan Pilih Kabupaten'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kabupaten
	$(document).on('change','#kabkota',function (e) {
		e.preventDefault();
		var kabkota = $(this).val(),
		    div = $('#kec'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kecamatan',
	        data:{'id':kabkota},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kec').select2({placeholder: 'Silahkan Pilih Kecamatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kecamatan
	$(document).on('change','#kec',function (e) {
		e.preventDefault();
		var kec = $(this).val(),
		    div = $('#kel'),
		    op;

		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/desa',
	        data:{'id':kec},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kel').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// SDA Domisili
	// $('#')
	// Domisili
	// Provinsi
	$(document).on('change','#provdomisili',function (e) {
		console.log(e);
		e.preventDefault();
		var provid = $(this).val(),
		    div = $('#kabkotadomisili'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kabupaten',
	        data:{'id':provid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kabkotadomisili').select2({placeholder: 'Silahkan Pilih Kabupaten'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kabupaten
	$(document).on('change','#kabkotadomisili',function (e) {
		e.preventDefault();
		var kabkota = $(this).val(),
		    div = $('#kecdomisili'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kecamatan',
	        data:{'id':kabkota},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kecdomisili').select2({placeholder: 'Silahkan Pilih Kecamatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kecamatan
	$(document).on('change','#kecdomisili',function (e) {
		e.preventDefault();
		var kec = $(this).val(),
		    div = $('#keldomisili'),
		    op;

		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/desa',
	        data:{'id':kec},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#keldomisili').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Tab Orang Tua
	// Orang Tua
	// Provinsi
	$(document).on('change','#provOrtu',function (e) {
		console.log(e);
		e.preventDefault();
		var provid = $(this).val(),
		    div = $('#kabkotaOrtu'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kabupaten',
	        data:{'id':provid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kabkotaOrtu').select2({placeholder: 'Silahkan Pilih Kabupaten'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kabupaten
	$(document).on('change','#kabkotaOrtu',function (e) {
		e.preventDefault();
		var kabkota = $(this).val(),
		    div = $('#kecOrtu'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kecamatan',
	        data:{'id':kabkota},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kecOrtu').select2({placeholder: 'Silahkan Pilih Kecamatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kecamatan
	$(document).on('change','#kecOrtu',function (e) {
		e.preventDefault();
		var kec = $(this).val(),
		    div = $('#kelOrtu'),
		    op;

		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/desa',
	        data:{'id':kec},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kelOrtu').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Tab Keluarga
	// Keluarga
	// Provinsi
	$(document).on('change','#provPasutri',function (e) {
		console.log(e);
		e.preventDefault();
		var provid = $(this).val(),
		    div = $('#kabkotaPasutri'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kabupaten',
	        data:{'id':provid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kabkotaPasutri').select2({placeholder: 'Silahkan Pilih Kabupaten'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kabupaten
	$(document).on('change','#kabkotaPasutri',function (e) {
		e.preventDefault();
		var kabkota = $(this).val(),
		    div = $('#kecpasutri'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kecamatan',
	        data:{'id':kabkota},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kecpasutri').select2({placeholder: 'Silahkan Pilih Kecamatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kecamatan
	$(document).on('change','#kecpasutri',function (e) {
		e.preventDefault();
		var kec = $(this).val(),
		    div = $('#kelPasutri'),
		    op;

		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/desa',
	        data:{'id':kec},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#kelPasutri').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})



	// ID Finger Ger From NIP
	var $nipEmp = $('input[name="nipEmp"]');

	$nipEmp.on('keydown',function() {
		setTimeout(function() {
			var l = $.trim($nipEmp.val()).length,
				fid;
			if (l == 11) {
				fid = $.trim($nipEmp.val()).slice(-6);
				$('#fid').val(fid);
			}else{
				$('#fid').val('');
				return false;
			}
		},0);
	})
	// add or remove attribute readonly anak
	$(document).on('change','#statusMenikah',function (e) {
		e.preventDefault();
		var text;
		text = $('#statusMenikah option:selected').text();
		if (text.search("0") > 0){
			$('#anak1').prop("readonly", true);
			$('#tglanak1').prop("readonly", true);
			$('#statusAnak1').attr("readonly", true);
			$('#statusAnak1').parent().eq('0').css('cursor','not-allowed');
			
			$('#anak2').prop("readonly", true);
			$('#tglanak2').prop("readonly", true);
			$('#statusAnak2').attr("readonly", true);
			$('#statusAnak2').parent().eq('0').css('cursor','not-allowed');

			$('#anak3').prop("readonly", true);
			$('#tglanak3').prop("readonly", true);
			$('#statusAnak3').attr("readonly", true);
			$('#statusAnak3').parent().eq('0').css('cursor','not-allowed');
		}else if(text.search("1") > 0){
			$('#anak1').prop("readonly", false);
			$('#tglanak1').prop("readonly", false);
			$('#statusAnak1').attr("readonly", false);
			$('#statusAnak1').parent().eq('0').css('cursor','not-allowed');
			
			$('#anak2').prop("readonly", true);
			$('#tglanak2').prop("readonly", true);
			$('#statusAnak2').attr("readonly", true);
			$('#statusAnak2').parent().eq('0').css('cursor','not-allowed');

			$('#anak3').prop("readonly", true);
			$('#tglanak3').prop("readonly", true);
			$('#statusAnak3').attr("readonly", true);
			$('#statusAnak3').parent().eq('0').css('cursor','not-allowed');
		}else if(text.search("2") > 0){
			$('#anak1').prop("readonly", false);
			$('#tglanak1').prop("readonly", false);
			$('#statusAnak1').attr("readonly", false);
			$('#statusAnak1').parent().eq('0').css('cursor','not-allowed');
			
			$('#anak2').prop("readonly", false);
			$('#tglanak2').prop("readonly", false);
			$('#statusAnak2').attr("readonly", false);
			$('#statusAnak2').parent().eq('0').css('cursor','not-allowed');

			$('#anak3').prop("readonly", true);
			$('#tglanak3').prop("readonly", true);
			$('#statusAnak3').attr("readonly", true);
			$('#statusAnak3').parent().eq('0').css('cursor','not-allowed');
		}else{
			$('#anak1').prop("readonly", false);
			$('#tglanak1').prop("readonly", false);
			$('#statusAnak1').attr("readonly", false);
			$('#statusAnak1').parent().eq('0').css('cursor','not-allowed');
			
			$('#anak2').prop("readonly", false);
			$('#tglanak2').prop("readonly", false);
			$('#statusAnak2').attr("readonly", false);
			$('#statusAnak2').parent().eq('0').css('cursor','not-allowed');

			$('#anak3').prop("readonly", false);
			$('#tglanak3').prop("readonly", false);
			$('#statusAnak3').attr("readonly", false);
			$('#statusAnak3').parent().eq('0').css('cursor','not-allowed');
		}
	})
/*End New Pegawai*/
	
/*Edit Pegawai*/
	// add or remove attribute anak from status menikah on database
	var editStatusPasangan = $('#e-statusMenikah option:selected').text();
	if (editStatusPasangan.search("0") > 0){
		$('#e-anak1').prop("readonly", true);
		$('#e-tglanak1').prop("readonly", true);
		$('#e-statusAnak1').attr("readonly", true);
		$('#e-statusAnak1').parent().eq('0').css('cursor','not-allowed');
		
		$('#e-anak2').prop("readonly", true);
		$('#e-tglanak2').prop("readonly", true);
		$('#e-statusAnak2').attr("readonly", true);
		$('#e-statusAnak2').parent().eq('0').css('cursor','not-allowed');

		$('#e-anak3').prop("readonly", true);
		$('#e-tglanak3').prop("readonly", true);
		$('#e-statusAnak3').attr("readonly", true);
		$('#e-statusAnak3').parent().eq('0').css('cursor','not-allowed');
	}else if (editStatusPasangan.search("1") > 0){
		$('#e-anak1').prop("readonly", false);
		$('#e-tglanak1').prop("readonly", false);
		$('#e-statusAnak1').attr("readonly", false);
		$('#e-statusAnak1').parent().eq('0').css('cursor','not-allowed');
		
		$('#e-anak2').prop("readonly", true);
		$('#e-tglanak2').prop("readonly", true);
		$('#e-statusAnak2').attr("readonly", true);
		$('#e-statusAnak2').parent().eq('0').css('cursor','not-allowed');

		$('#e-anak3').prop("readonly", true);
		$('#e-tglanak3').prop("readonly", true);
		$('#e-statusAnak3').attr("readonly", true);
		$('#e-statusAnak3').parent().eq('0').css('cursor','not-allowed');
	}else if (editStatusPasangan.search("2") > 0){
		$('#e-anak1').prop("readonly", false);
		$('#e-tglanak1').prop("readonly", false);
		$('#e-statusAnak1').attr("readonly", false);
		$('#e-statusAnak1').parent().eq('0').css('cursor','not-allowed');
		
		$('#e-anak2').prop("readonly", false);
		$('#e-tglanak2').prop("readonly", false);
		$('#e-statusAnak2').attr("readonly", false);
		$('#e-statusAnak2').parent().eq('0').css('cursor','not-allowed');

		$('#e-anak3').prop("readonly", true);
		$('#e-tglanak3').prop("readonly", true);
		$('#e-statusAnak3').attr("readonly", true);
		$('#e-statusAnak3').parent().eq('0').css('cursor','not-allowed');
	}
	// Tab Penempatan Baru
  	// 
  	// Cabang
    $(document).on('change','#e-branchEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#e-mrEmp');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/branch/findmr',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#e-mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
	// add or remove attribute anak for edit
	$(document).on('change','#e-statusMenikah',function (e) {
		e.preventDefault();
		var text;
		text = $('#e-statusMenikah option:selected').text();
		if (text.search("0") > 0){
			$('#e-anak1').prop("readonly", true);
			$('#e-tglanak1').prop("readonly", true);
			$('#e-statusAnak1').attr("readonly", true);
			$('#e-statusAnak1').parent().eq('0').css('cursor','not-allowed');
			
			$('#e-anak2').prop("readonly", true);
			$('#e-tglanak2').prop("readonly", true);
			$('#e-statusAnak2').attr("readonly", true);
			$('#e-statusAnak2').parent().eq('0').css('cursor','not-allowed');

			$('#e-anak3').prop("readonly", true);
			$('#e-tglanak3').prop("readonly", true);
			$('#e-statusAnak3').attr("readonly", true);
			$('#e-statusAnak3').parent().eq('0').css('cursor','not-allowed');
		}else if (text.search("1") > 0){
			$('#e-anak1').prop("readonly", false);
			$('#e-tglanak1').prop("readonly", false);
			$('#e-statusAnak1').attr("readonly", false);
			$('#e-statusAnak1').parent().eq('0').css('cursor','not-allowed');
			
			$('#e-anak2').prop("readonly", true);
			$('#e-tglanak2').prop("readonly", true);
			$('#e-statusAnak2').attr("readonly", true);
			$('#e-statusAnak2').parent().eq('0').css('cursor','not-allowed');

			$('#e-anak3').prop("readonly", true);
			$('#e-tglanak3').prop("readonly", true);
			$('#e-statusAnak3').attr("readonly", true);
			$('#e-statusAnak3').parent().eq('0').css('cursor','not-allowed');
		}else if (text.search("2") > 0){
			$('#e-anak1').prop("readonly", false);
			$('#e-tglanak1').prop("readonly", false);
			$('#e-statusAnak1').attr("readonly", false);
			$('#e-statusAnak1').parent().eq('0').css('cursor','not-allowed');
			
			$('#e-anak2').prop("readonly", false);
			$('#e-tglanak2').prop("readonly", false);
			$('#e-statusAnak2').attr("readonly", false);
			$('#e-statusAnak2').parent().eq('0').css('cursor','not-allowed');

			$('#e-anak3').prop("readonly", true);
			$('#e-tglanak3').prop("readonly", true);
			$('#e-statusAnak3').attr("readonly", true);
			$('#e-statusAnak3').parent().eq('0').css('cursor','not-allowed');
		}else{
			$('#e-anak1').prop("readonly", false);
			$('#e-tglanak1').prop("readonly", false);
			$('#e-statusAnak1').attr("readonly", false);
			$('#e-statusAnak1').parent().eq('0').css('cursor','');
			
			$('#e-anak2').prop("readonly", false);
			$('#e-tglanak2').prop("readonly", false);
			$('#e-statusAnak2').attr("readonly", false);
			$('#e-statusAnak2').parent().eq('0').css('cursor','');

			$('#e-anak3').prop("readonly", false);
			$('#e-tglanak3').prop("readonly", false);
			$('#e-statusAnak3').attr("readonly", false);
			$('#e-statusAnak3').parent().eq('0').css('cursor','');
		}
	});

  	// Departments
    $(document).on('change','#e-deptEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#e-subdeptEmp');
	    var op        = " ";
	   	

	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findsubdept',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  	// Sub Departments
    $(document).on('change','#e-subdeptEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#e-jabatanEmp');
	    var op        = " ";
	   
	   	var subdeptText = $("#e-subdeptEmp option:selected").text();


	   	if (subdeptText == "SALES") {
	   		$("#e-kodesales").removeAttr('style');
	   		$("#e-kodesales").prop("required",true);
	   	}else{
	   		$("#e-kodesales").css('display','none');
	   		$("#e-kodesales").prop("required",false);
	   	}

	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findjabatan',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].jabatan+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-jabatanEmp').select2({placeholder: 'Silahkan Pilih Jabatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  	// Jabatan
    $(document).on('change','#e-jabatanEmp',function () {
	    var deptid = $(this).val();
	    var level       = $('#e-level');
	    var grade       = $('#e-grade');
	    var op = "";
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/jabatan/findjabatanchild',
	        data:{'id':deptid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	          data = data[0];
	          level.val(data.level);
	          grade.val(data.grade);
	            
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
    var tableAtasan ;
    var cabangTitle;
    // Status Karyawan
    var editStatusKaryawan = $('#e-statskarEmp option:selected').val();
    
    $("#e-statskarEmp").change(function () {
    	$('#e-statskarEmp option:selected').each(function () {
    		var id = $(this).val(),
    			text = $(this).text();

			if (id == 5) {
				$('#konawal,#konakhir').each(function(index){
		    		$(this).find('small').css('display','none');
		    	})
				$('#e-kontrak-awal,#e-kontrak-akhir').removeAttr('required');
				$('#e-kontrak-awal,#e-kontrak-akhir').prop('readonly', true);		
			}else{
				$('#konawal,#konakhir').each(function(index){
		    		$(this).find('small').css('display','');
		    	})
				$('#e-kontrak-awal,#e-kontrak-akhir').attr('required');
				$('#e-kontrak-awal,#e-kontrak-akhir').prop('readonly', false);		
			}
    		// console.log(`id : ${id}, text = ${text}`);
    	})	
    })
    if (editStatusKaryawan == 5) {
    	$('#konawal,#konakhir').each(function(index){
    		$(this).find('small').css('display','none');
    	})
		$('#e-kontrak-awal,#e-kontrak-akhir').removeAttr('required');
		$('#e-kontrak-awal,#e-kontrak-akhir').prop('readonly', true);
    	
    }else{
		$('#konawal,#konakhir').each(function(index){
    		$(this).find('small').css('display','');
    	})
		$('#e-kontrak-awal,#e-kontrak-akhir').attr('required');
		$('#e-kontrak-awal,#e-kontrak-akhir').prop('readonly', false);		
	}
	// Atasan Pegawai
	$(document).on('click','#e-searchAtasan', function (e) {
		e.preventDefault();
		var cabang = $('#e-branchEmp');
		// Data Table
		$.ajax({
            type  : 'post',
            url   : base_url + "kepeg/pegawai/empfilter",
            async : false,
        	data:{'id':cabang.val()},
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                            '<td>'+data[i].nama+'</td>'+
                            '<td>'+data[i].nip+'</td>'+
                            '<td>'+data[i].cabang+'</td>'+
                            '<td>'+data[i].deptname+'</td>'+
                            '<td>'+data[i].jabatan+'</td>'+
                            '</tr>';
                }
                $('#e-show_data').html(html);
                cabangTitle = $("#e-branchEmp option:selected").text();
                if ( ! $.fn.DataTable.isDataTable( '#e-matasan-table' ) ) {
					tableAtasan = $('#e-matasan-table').DataTable({
						'lengthChange': false,
						'ordering'    : true,
						'info'        : true,
						'autoWidth'   : false,
				        "ordering":false,
				        // "paging":false,
				        // scrollY:        "300px",
				        // scrollX:        true,
				        // scrollCollapse: true,
						
					});
				}
            }

        });

		$('#e-matasan-table').dataTable().fnDraw();

		// $('#e-titleAtasan').text('Daftar Karyawan '+$("#e-branchEmp option:selected").text());
		$("#e-modal-atasan-pegawai").modal('show');
		$('#e-matasan-table tbody').on('click', 'tr', function () {
            var ematasanTable = $('#e-matasan-table').DataTable();
            var data = ematasanTable.row(this).data();
            var nama = data[0],
            	nik = data[1];
            var nama_nik = nama + " - " + nik;

            $("#e-atasan").val(nama_nik);
            $("#e-modal-atasan-pegawai").modal('hide');
        });
	});

/*Edit Pegawai*/
	// Eprov KTP
	$(document).on('change','#e-provktp',function (e) {
		e.preventDefault();
		var provid = $(this).val(),
		    div = $('#e-kabkota'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kabupaten',
	        data:{'id':provid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kabkota').select2({placeholder: 'Silahkan Pilih Kabupaten'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kabupaten
	$(document).on('change','#e-kabkota',function (e) {
		e.preventDefault();
		var kabkota = $(this).val(),
		    div = $('#e-kec'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kecamatan',
	        data:{'id':kabkota},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kec').select2({placeholder: 'Silahkan Pilih Kecamatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kecamatan
	$(document).on('change','#e-kec',function (e) {
		e.preventDefault();
		var kec = $(this).val(),
		    div = $('#e-kel'),
		    op;

		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/desa',
	        data:{'id':kec},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kel').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Domisili
	    
	// Provinsi
	$(document).on('change','#e-provdomisili',function (e) {
		
		e.preventDefault();
		var provid = $(this).val(),
		    div = $('#e-kabkotadomisili'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kabupaten',
	        data:{'id':provid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kabkotadomisili').select2({placeholder: 'Silahkan Pilih Kabupaten'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kabupaten
	$(document).on('change','#e-kabkotadomisili',function (e) {
		e.preventDefault();
		var kabkota = $(this).val(),
		    div = $('#e-kecdomisili'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kecamatan',
	        data:{'id':kabkota},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kecdomisili').select2({placeholder: 'Silahkan Pilih Kecamatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kecamatan
	$(document).on('change','#e-kecdomisili',function (e) {
		e.preventDefault();
		var kec = $(this).val(),
		    div = $('#e-keldomisili'),
		    op;

		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/desa',
	        data:{'id':kec},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-keldomisili').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});

		// Tab Orang Tua
	// Orang Tua
	// Provinsi
	$(document).on('change','#e-provOrtu',function (e) {
		console.log(e);
		e.preventDefault();
		var provid = $(this).val(),
		    div = $('#e-kabkotaOrtu'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kabupaten',
	        data:{'id':provid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kabkotaOrtu').select2({placeholder: 'Silahkan Pilih Kabupaten'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kabupaten
	$(document).on('change','#e-kabkotaOrtu',function (e) {
		e.preventDefault();
		var kabkota = $(this).val(),
		    div = $('#e-kecOrtu'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kecamatan',
	        data:{'id':kabkota},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kecOrtu').select2({placeholder: 'Silahkan Pilih Kecamatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kecamatan
	$(document).on('change','#e-kecOrtu',function (e) {
		e.preventDefault();
		var kec = $(this).val(),
		    div = $('#e-kelOrtu'),
		    op;

		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/desa',
	        data:{'id':kec},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kelOrtu').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Tab Keluarga
	// Keluarga
	// Provinsi
	$(document).on('change','#e-provPasutri',function (e) {
		console.log(e);
		e.preventDefault();
		var provid = $(this).val(),
		    div = $('#e-kabkotaPasutri'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kabupaten',
	        data:{'id':provid},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kabkotaPasutri').select2({placeholder: 'Silahkan Pilih Kabupaten'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kabupaten
	$(document).on('change','#e-kabkotaPasutri',function (e) {
		e.preventDefault();
		var kabkota = $(this).val(),
		    div = $('#e-kecpasutri'),
		    op;
		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/kecamatan',
	        data:{'id':kabkota},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kecpasutri').select2({placeholder: 'Silahkan Pilih Kecamatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})

	// Kecamatan
	$(document).on('change','#e-kecpasutri',function (e) {
		e.preventDefault();
		var kec = $(this).val(),
		    div = $('#e-kelPasutri'),
		    op;

		$.ajax({
	        type:'post',
	        url: base_url+'kepegawaian/pegawai/desa',
	        data:{'id':kec},
	        success:function (data) {
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-kelPasutri').select2({placeholder: 'Silahkan Pilih Kelurahan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	})



	
});






$(function () {
	
	'use strict';

	var table = 'inventaris';

    var cabang  = $("#inv-branchEmp"),
        mr      = $("#inv-mrEmp");

    var kinv = $("#kinv").clone();
    var 
        htmlDept     = '',
        htmlSubDept  = '',
        htmlJabatan  = '',
        htmlLevel    = '',
        htmlGrade    = '',
        htmlAtasan    = '',
        htmlTglMasuk = '';

    // Placeholder select2
    // $('#inv-category').select2({placeholder: '-- Pilih Surat Category --',allowClear:true});
    $('#inv-branchEmp').select2({placeholder: 'Silahkan Pilih Cabang',allowClear:true});
    $('#inv-mrEmp').select2({placeholder: 'Silahkan Pilih MR',allowClear:true});
    $('#inv-deptEmp').select2({placeholder: 'Silahkan Pilih Departments',allowClear:true});
    $('#inv-subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments',allowClear:true});
    // $('#inv-status').select2({placeholder: 'Silahkan Pilih Status Barang',allowClear:true});

    var invTable = $("#"+table+"-table").DataTable({
     	'lengthChange': true,
		'autoWidth'   : false,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 5,
        /*"drawCallback": function( settings ) {

            $("#load-sp").css("display","none");
            $("#load-sp").remove();
            $('#spt-table').removeAttr("style");
        }*/
	});

    $("#"+table+"-table").dataTable().fnDraw();

	$("#btnAddInventaris").click(function (e) {
		e.preventDefault();

		location.replace(base_url + "inv/inventaris/add/");

	});

    // Add New Element
    var i = 1;
	$("#btnAddTotalInventaris").click(function (e) {
		e.preventDefault();

        // get the last DIV which ID starts with ^= "cmitem"
        var $div = $('div[id^="cmitem0"]:last');

        // Read the Number from that DIV's ID (i.e: 3 from "cmitem3")
        // And increment that number by 1
        // var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
        var num = i++;
        // Clone it and assign the new ID (i.e: from num 4 to ID "cmitem4")
        var $cmitem = $div.clone().prop('id', 'cmitem'+num );
        // initialize id for dropdown
        $cmitem.children().eq(1).children().attr('data-sid',num);
        // Initialize id for input
        $cmitem.children().eq(2).children().prop('id','inputv'+num);
        $cmitem.children().eq(2).children().attr('placeholder','');
        $cmitem.children().eq(2).children().val('');
        // Add id for button remove
        var kids = $($cmitem).children().eq(5).children().prop('id',num);
        // Transform btn info to btn danger
        kids.toggleClass("btn-info btn-danger");
        // add Class btn remove for event remove element
        kids.addClass("btn_remove");
        // change icon for remove button
        kids.children().toggleClass('fa-plus fa-times');
        // add Element/
        $cmitem.appendTo('#kinv');
        // console.log($cmitem);

	});

    // Remove Element
    $(document).on('click','.btn_remove',function () {
        var buttonId = $(this).attr("id");
        if (buttonId > 0) {
            $("#cmitem"+buttonId+"").remove();
        }
    })


    // Get Attribute
    $(document).on('change','.inc',function () {
        var id = $(this).attr("data-sid");
        var selected = $(this).children("option:selected").val();
        $.get( base_url + "inv/inventaris/attr?s="+selected, function( data ) {
            var res = JSON.parse(data);
            $("#inputv"+id).attr('placeholder',res.attr);
        });
    });


	// Cabang 
    $(document).on('change','#inv-branchEmp',function () {
        var deptid = $(this).val();
        var div       = $('#inv-mrEmp');
        var op        = " ";
       
        
        $.ajax({
            type:'post',
            url: base_url+'settings/branch/findmr',
            data:{'id':deptid},
            success:function (data) {
              
              data =  $.parseJSON(data);
                op+='<option value="" selected disabled></option>';
                for (var i = 0; i < data.length; i++) {
                    
                    op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
                }

                div.html(" ");
                div.append(op);
                $('#inv-mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
            },
            error:function () {
                console.log('Something wrong !!!');
            }
        });
    });

    // Search Pegawai
    $(document).on('click','#inv-searchNama',function (e) {
        e.preventDefault();
        var mrTitle = '';
        var valCabang = cabang.val(),
            valMr = mr.val();
        if (valCabang == null || valCabang == '' || valCabang == undefined) {
            swal('Silahkan Pilih Cabang');
        }else{
            if (valMr == null || valMr == '' || valMr == undefined) {
                swal('Silahkan Pilih MR');
            }else{
                if ($('#inv-mrEmp option:selected').text() !== mrTitle) {
                    mrTitle = $("#inv-mrEmp option:selected").text();
                    $("#invm-table").DataTable({
                        "destroy" : true,
                        "ajax" : {
                            "url"  : base_url + "kepeg/pegawai/empfilter",
                            "type" : 'POST',
                            "data" : {
                                "id"    : cabang.val(),
                                "subid" : mr.val(),
                                "dt"    : 1,
                                "type"  : 1
                            },

                        },
                        "columns": [
                            { "data": "nama" },
                            { "data": "nip" },
                            { "data": "cabang" },
                            { "data": "mr" },
                            { "data": "deptname" },
                            { "data": "jabatan" }
                        ],
                        'ordering'    : true,
                        'info'       : true,
                        'lengthChange': true,
                        'autoWidth'   : false,
                        scrollX:        true,
                        scrollCollapse: true,
                        "pageLength": 5,
                        fixedHeader: true,
                    });
                }
                
                $("#invm-table").dataTable().fnDraw();

                $('#invtitle').text('Daftar Karyawan '+$("#inv-branchEmp option:selected").text()+ ' di MR ' + $("#inv-mrEmp option:selected").text());
                $("#invmodal-pegawai").modal('show');

                    $("#invm-table tbody").on('click', 'tr', function () {
                    
                        var invAtasanTable = $("#invm-table").DataTable();
                        var data = invAtasanTable.row( this ).data();
                        var nama = data.nama,
                            nip  = data.nip;
                        
                       $('#inv-namaEmp').val(nama);
                       $('#inv-nipEmp').val(nip);

                       // Ajax menampilkan status yang lama
                       $.ajax({
                            url: base_url + 'kepeg/pegawai/search',
                            type: 'POST',
                            dataType: 'json',
                            data: {nip: nip},
                       })
                       .done(function(data) {
                        
                            htmlDept    = '<option value='+data['pegawai']['deptid'] +' selected>' + data['pegawai']['deptname']+ '</option>';
                            htmlSubDept = '<option value='+data['pegawai']['subdeptid'] +' selected>' + data['pegawai']['subdept']+ '</option>';
                            htmlJabatan = '<option value='+data['pegawai']['jabatan_id'] +' selected>' + data['pegawai']['jabatan']+ '</option>';
                            htmlLevel   = data['pegawai']['level'];
                            htmlGrade   = data['pegawai']['grade'];
                            htmlAtasan   = data['pegawai']['nama_atasan'];
                            $("#inv-deptEmp").append(htmlDept);
                            $("#inv-subdeptEmp").append(htmlSubDept);
                            $("#inv-jabatanEmp").append(htmlJabatan);
                            $("#inv-level").val(htmlLevel);
                            $("#inv-grade").val(htmlGrade);
                            $("#inv-atasan").val(htmlAtasan);
                            
                            return false;
                       });
                       
                       $('#invmodal-pegawai').modal('hide');
                       // $("#invm-table").DataTable().destroy();
                    });
            }
        }

    })

    // Add Inventaris
    var frmregister_inv = $("#register_inv");
    var validator = frmregister_inv.validate({
      rules: {
        'inv-no': {
          required: true,
          remote: base_url + "inv/inventaris/exist"
        },
      },
     messages: {
          'inv-no'   : {remote: "Nomor sudah di gunakan!"},
      }
    });


    $.validator.addMethod("cAttrInv", $.validator.methods.remote, "Nomor sudah di gunakan !");
    jQuery.validator.addClassRules("attrinv", {
      cAttrInv: base_url + "inv/inventaris/exattr",
    });


    /*$(".attrinv").on('keypress', function () {
        
        var value = $(this).val();

        if (value != '' || value != undefined) {
            $.get( base_url + "inv/inventaris/exattr?a="+value, function( data ) {
                // var res = JSON.parse(data);
                if (data == false) {
                    frmregister_inv.valid(false);
                }
                // $("#e-inputv"+id).attr('placeholder',res.attr);
            });
        }

    });*/
    frmregister_inv.validate();

    frmregister_inv.submit(function (e) {
        e.preventDefault();

        if (frmregister_inv.valid()) {
            swal({
                  title: "Apakah anda yakin?",
                  text: "Ingin Menambah Data ?!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
            })
            .then((willSend) => {
                  if (willSend) {
                    $.ajax({
                        url: base_url + 'inv/inventaris/store',
                        method: 'POST',
                        data: new FormData($('#register_inv')[0]),
                        cache:false,
                        processData:false,
                        contentType:false,
                    })
                    .done(function(data) {
                          
                          var obj = JSON.parse(data);
                          
                          if (obj.results == "success") {

                            swal(obj.reason, {
                              icon: "success",
                            }).then((value) => {
                              location.reload(true);
                            });
                          }else{
                            swal(obj.reason, {
                              icon: "error",
                            })
                          }

                    })
                    .fail(function() {
                      swal("Ooops terjadi masalah, coba hubungi administrator");
                    })
                    /*.always(function() {
                    });*/
                    
                  }else {
                    swal("Form tidak di kirim!");
                  }
            })
        }
    })

    /*Edit*/
    
    $("#e-btnAddTotalInventaris").click(function (e) {
        e.preventDefault();

        var IDs = [];
        $(".cmitem").each(function(){ IDs.push(this.id); });

        var id = IDs.pop();
        var i = id.substr(id.length - 1);
        i++;
        
        // get the last DIV which ID starts with ^= "cmitem"
        var $div = $('div[id^="e-cmitem0"]:last');
        // Read the Number from that DIV's ID (i.e: 3 from "cmitem3")
        // And increment that number by 1
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
        var num = i;
        // Clone it and assign the new ID (i.e: from num 4 to ID "cmitem4")
        var $cmitem = $div.clone().prop('id', 'e-cmitem'+num );
        // $($cmitem).children().eq(4).children().empty();
        console.log($cmitem);
        // console.log($cmitem);
        // initialize id for dropdown
        $cmitem.children().eq(1).children().attr('data-esid',num);
        $cmitem.children().eq(1).children().val(0);
        $cmitem.children().eq(1).children().removeAttr('disabled');

        // Initialize id for input
        $cmitem.children().eq(2).children().prop('id','e-inputv'+num);
        $cmitem.children().eq(2).children().attr('placeholder','');
        $cmitem.children().eq(2).children().val('');
        $cmitem.children().eq(2).children().removeAttr('readonly');

        $cmitem.children().eq(3).children().removeAttr('disabled');
        $cmitem.children().eq(4).children().removeAttr('readonly');
        // Add id for button remove
        var kids = $($cmitem).children().eq(5).children().prop('id',num);
        // Transform btn info to btn danger
        kids.toggleClass("btn-info btn-danger");
        // kids.children().toggleClass("btn-info btn-danger");
        // add Class btn remove for event remove element
        kids.addClass("ebtn_remove");
        // change icon for remove button
        kids.children().toggleClass('fa-plus fa-times');
        // add Element
        $cmitem.appendTo('#e-kinv');

    });
    
    // Remove Element Edit
    $(document).on('click','.ebtn_remove',function () {
        var buttonId = $(this).attr("id");
        var buttontid = $(this).attr("data-attr");
        if (buttonId > 0) {
            $("#e-cmitem"+buttonId+"").remove();
            $.ajax({
                type:'post',
                url: base_url+'inv/inventaris/del',
                data:{'id':buttontid},
            });
        }
    })

    // Edit Category
    $("select.e-inc-edit").change(function(){
        // id t_inventaris
        var idtin = $(this).attr("data-attr"); 
        // id new
        var idn = $(this).children("option:selected").val();
        // text new
        var txtn = $(this).children("option:selected").text();
        $.ajax({
            type:'post',
            url: base_url+'inv/inventaris/e-cat',
            data:{'idn':idn,'idtin':idtin,'txtn':txtn},
        });
    });

     // Get Attribute
    $(document).on('change','.e-inc',function () {
        var id = $(this).attr("data-esid");
        console.log(id)
        var selected = $(this).children("option:selected").val();
        $.get( base_url + "inv/inventaris/attr?s="+selected, function( data ) {
            var res = JSON.parse(data);
            $("#e-inputv"+id).attr('placeholder',res.attr);
        });
    });

    // Edit Inventaris
    var frmedit_inv = $("#edit_inv");

    frmedit_inv.validate();

    frmedit_inv.submit(function (e) {
        e.preventDefault();

        if (frmedit_inv.valid()) {
            swal({
                  title: "Apakah anda yakin?",
                  text: "Ingin Mengubah Data ?!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
            })
            .then((willSend) => {
                  if (willSend) {
                    $.ajax({
                        url: base_url + 'inv/inventaris/update',
                        method: 'POST',
                        data: new FormData($('#edit_inv')[0]),
                        cache:false,
                        processData:false,
                        contentType:false,
                    })
                    .done(function(data) {
                          
                          var obj = JSON.parse(data);
                          
                          if (obj.results == "success") {

                            swal(obj.reason, {
                              icon: "success",
                            }).then((value) => {
                              location.reload(true);
                            });
                          }else{
                            swal(obj.reason, {
                              icon: "error",
                            })
                          }

                    })
                    .fail(function() {
                      swal("Ooops terjadi masalah, coba hubungi administrator");
                    })
                    /*.always(function() {
                    });*/
                    
                  }else {
                    swal("Form tidak di kirim!");
                  }
            })
        }
    })

    /*Edit*/

    /*Delete Inventaris*/
    var frmdel_inv = $("#del_inv");

    frmdel_inv.validate();

    frmdel_inv.submit(function (e) {
        e.preventDefault();

        if (frmdel_inv.valid()) {
            swal({
                  title: "Apakah anda yakin menonaktifkan inventaris?",
                  text: "Data tidak dapat di rubah setelah di hapus, pastikan data benar !!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
            })
            .then((willSend) => {
                  if (willSend) {
                    $.ajax({
                        url: base_url + 'inv/inventaris/remove',
                        method: 'POST',
                        data: new FormData($('#del_inv')[0]),
                        cache:false,
                        processData:false,
                        contentType:false,
                    })
                    .done(function(data) {
                          
                          var obj = JSON.parse(data);
                          
                          if (obj.results == "success") {

                            swal(obj.reason, {
                              icon: "success",
                            }).then((value) => {
                              location.reload(true);
                            });
                          }else{
                            swal(obj.reason, {
                              icon: "error",
                            })
                          }

                    })
                    .fail(function() {
                      swal("Ooops terjadi masalah, coba hubungi administrator");
                    })
                    /*.always(function() {
                    });*/
                    
                  }else {
                    swal("Form tidak di kirim!");
                  }
            })
        }
    })
    /*Delete Inventaris*/
})
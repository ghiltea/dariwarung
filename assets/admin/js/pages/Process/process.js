$(function () {
	// body...

	'use strict';

	var arr;
	var pages = 'process';
	var processTable = $("#"+pages+"-table");

	$.ajax({
		async:false,
		url: base_url + 'dropros',
	})
	.done(function(data) {
		arr = data;
		arr = JSON.parse(data);
	});

	var comboTree;

	comboTree = $('#inputCmbProcess').comboTree({
				source : arr,
				isMultiple: false,
				page:pages
	});

	/*Initialize DataTable*/
	$("#"+pages+"-table").DataTable({
		'destroy' : true,
		'searching' : false,
		'ordering' : false,
		'filtering' : false,
		'autoWidth' : false,
		'lengthChange' : false,
		scrollX:        true,
		scrollCollapse: true,
		"drawCallback": function( settings ) {
        	$("#load-process").css("display","none");
        	$("#load-process").remove();
	        $("#pros-table").removeAttr("style");
	    }

	});

	$("#"+ pages +"-table").dataTable().fnDraw();


	/*Fill DataTable*/
	$('.comboTreeItemTitle-'+pages).on('click',function () {
		$("#loadProcessModal").modal('show');
		load_data();
		$("#loadProcessModal").modal('hide');
			
	})

	// Search
	$("#searchProc").keyup(function () {
		var id = comboTree.getSelectedItemsId();
		if (!id || id == '') {
			swal('Silahkan Pilih Area');
		}else{
			load_data(this.value);
		}
	})

	// Toggle Class Selected DataTables
	var data = [];
    $('#'+ pages + '-table tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        data = processTable.DataTable().row( this ).data();
    } );

    // Process By User Click
    $("#pUser").click(function (e) {
    	var fromdate,todate;
		var id = comboTree.getSelectedItemsId();

		var arr_user = getValues();

    	fromdate = $("#fromsdate").val();
    	todate = $("#fromedate").val();

    	console.log(id);

    	if (fromdate == '' || todate == '') {
    		swal('Please Select Date ');
    	}else if ( id == '' || id == undefined) {
    		swal('Please Select Area');
    	}else{
    		if (arr_user.length) {
    			arr_user = arr_user.join();
    		}else{
    			arr_user = 0;
    		}
    		var url = base_url + 'process/p?fromdate=' + fromdate + '&todate=' + todate + '&area=' + id +'&user=' + arr_user + '&a=0';
			window.open(url);	
    	}
    	// swal();
    })




    /*=======================================================================*/

	// Load Data
	function load_data(query) {
		var a = comboTree.getSelectedItemsId();
 		$.ajax({
			url: base_url+ 'process/u',
			type: 'POST',
			data: {
				a : a,
				q : query
			},
			beforeSend: function(){
		     // Handle the beforeSend event
		   },
			success: function (data) {
				var dataSet = JSON.parse(data);
				// console.log(dataSet.data);
				$("#"+ pages +"-table").DataTable({
					'destroy' : true,
		    		'data' : dataSet.data,
					'searching' : false,
					'ordering' : false,
					'filtering' : false,
					'autoWidth' : false,
					'lengthChange' : false,
					scrollX:        true,
					scrollCollapse: true,
					"drawCallback": function( settings ) {

			            $("#loadProcessModal").modal('hide');
			        }

				});
			}
		})
 	}

 	function getValues(){
	  var ids = $.map(processTable.DataTable().rows('.selected').data(), function (item) {
	     return item[0]
	  });
	  return ids
	}

})
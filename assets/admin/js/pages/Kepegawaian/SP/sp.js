$(function () {
    var table = 'sp';

    var cabang  = $("#sp-branchEmp"),
        mr      = $("#sp-mrEmp");

    var 
        htmlDept     = '',
        htmlSubDept  = '',
        htmlJabatan  = '',
        htmlLevel    = '',
        htmlGrade    = '',
        htmlAtasan    = '',
        htmlTglMasuk = '';

    // Placeholder select2
    $('#sp-category').select2({placeholder: '-- Pilih Surat Peringatan --',allowClear:true});
    $('#sp-branchEmp').select2({placeholder: 'Silahkan Pilih Cabang',allowClear:true});
    $('#sp-mrEmp').select2({placeholder: 'Silahkan Pilih MR',allowClear:true});
    $('#sp-deptEmp').select2({placeholder: 'Silahkan Pilih Departments',allowClear:true});
    $('#sp-subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments',allowClear:true});

	var spTable = $("#"+table+"-table").DataTable({
     	'lengthChange': true,
		'autoWidth'   : false,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 5,
        "drawCallback": function( settings ) {

            $("#load-sp").css("display","none");
            $("#load-sp").remove();
            $('#spt-table').removeAttr("style");
        }
	});
    $("#"+table+"-table").dataTable().fnDraw();

    // Button Add
    $(document).on('click','#btnAddSp',function (e) {
        location.replace(base_url + "kepeg/sp/add");
    })
    //End Button Add

    // Cabang 
    $(document).on('change','#sp-branchEmp',function () {
        var deptid = $(this).val();
        var div       = $('#sp-mrEmp');
        var op        = " ";
       
        
        $.ajax({
            type:'post',
            url: base_url+'settings/branch/findmr',
            data:{'id':deptid},
            success:function (data) {
              
              data =  $.parseJSON(data);
                op+='<option value="" selected disabled></option>';
                for (var i = 0; i < data.length; i++) {
                    
                    op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
                }

                div.html(" ");
                div.append(op);
                $('#sp-mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
            },
            error:function () {
                console.log('Something wrong !!!');
            }
        });
    });
    
    // Search Pegawai
    $(document).on('click','#sp-searchNama',function (e) {
        e.preventDefault();
        var mrTitle = '';
        var valCabang = cabang.val(),
            valMr = mr.val();
        if (valCabang == null || valCabang == '' || valCabang == undefined) {
            swal('Silahkan Pilih Cabang');
        }else{
            if (valMr == null || valMr == '' || valMr == undefined) {
                swal('Silahkan Pilih MR');
            }else{
                var mrEmp = $('#sp-mrEmp option:selected').text();
                if ($('#sp-mrEmp option:selected').text() !== mrTitle) {
                    console.log(mr.val());
                    mrTitle = $("#sp-mrEmp option:selected").text();
                    $("#spm-table").DataTable({
                        "destroy" : true,
                        "ajax" : {
                            "url"  : base_url + "kepeg/pegawai/empfilter",
                            "type" : 'POST',
                            "data" : {
                                "id"    : cabang.val(),
                                "subid" : mr.val(),
                                "dt"    : 1,
                                "type"  : 1
                            },

                        },
                        "columns": [
                            { "data": "nama" },
                            { "data": "nip" },
                            { "data": "cabang" },
                            { "data": "mr" },
                            { "data": "deptname" },
                            { "data": "jabatan" }
                        ],
                        'ordering'    : true,
                        'info'       : true,
                        'lengthChange': true,
                        'autoWidth'   : false,
                        scrollX:        true,
                        scrollCollapse: true,
                        "pageLength": 5,
                        fixedHeader: true,
                    });
                }
                
                $("#spm-table").dataTable().fnDraw();

                $('#sptitle').text('Daftar Karyawan '+$("#sp-branchEmp option:selected").text()+ ' di MR ' + $("#sp-mrEmp option:selected").text());
                $("#spmodal-pegawai").modal('show');

                    $("#spm-table tbody").on('click', 'tr', function () {
                    
                        var rSpAtasanTable = $("#spm-table").DataTable();
                        var data = rSpAtasanTable.row( this ).data();
                        var nama = data.nama,
                            nip  = data.nip;
                        
                       $('#sp-namaEmp').val(nama);
                       $('#sp-nipEmp').val(nip);

                       // Ajax menampilkan status yang lama
                       $.ajax({
                            url: base_url + 'kepeg/pegawai/search',
                            type: 'POST',
                            dataType: 'json',
                            data: {nip: nip},
                       })
                       .done(function(data) {
                        
                            htmlDept    = '<option value='+data['pegawai']['deptid'] +' selected>' + data['pegawai']['deptname']+ '</option>';
                            htmlSubDept = '<option value='+data['pegawai']['subdeptid'] +' selected>' + data['pegawai']['subdept']+ '</option>';
                            htmlJabatan = '<option value='+data['pegawai']['jabatan_id'] +' selected>' + data['pegawai']['jabatan']+ '</option>';
                            htmlLevel   = data['pegawai']['level'];
                            htmlGrade   = data['pegawai']['grade'];
                            htmlAtasan   = data['pegawai']['nama_atasan'];
                            console.log(htmlDept)
                            $("#sp-deptEmp").append(htmlDept);
                            $("#sp-subdeptEmp").append(htmlSubDept);
                            $("#sp-jabatanEmp").append(htmlJabatan);
                            $("#sp-level").val(htmlLevel);
                            $("#sp-grade").val(htmlGrade);
                            $("#sp-atasan").val(htmlAtasan);
                            
                            return false;
                       });
                       
                       $('#spmodal-pegawai').modal('hide');
                       // $("#spm-table").DataTable().destroy();
                    });
            }
        }

    })
    // End Search Pegawai

    // Submit

    // Validate Form
    var frm_register_sp = $("#register_sp");

    frm_register_sp.validate();

    frm_register_sp.submit(function (e) {
        e.preventDefault();
        if (frm_register_sp.valid()) {
            var newForm =  new FormData($('#register_sp')[0]);

            swal({
                title: "Apakah anda yakin?",
                text: "Pastikan tidak ada data yang keliru!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willSend) => {
                if (willSend) {
                    $.ajax({
                      method :'post',
                      url  : base_url + 'kepeg/sp/store',
                      data : new FormData($('#register_sp')[0]),
                      cache:false,
                      processData:false,
                      contentType:false,              
                      success : function (data) {
                        // body...
                        var obj = jQuery.parseJSON(data);
                        if (obj.results == "success") {
                          swal("Form berhasil di kirim!", {
                            icon: "success",
                          })
                          .then((value) => {
                            location.reload(true);
                          });
                        }else{
                          swal("Form gagal kirim!", {
                            icon: "error",
                          })
                        }
                      },
                      error : function(){
                         swal("Ooops tidak bisa tersambung dengan server");
                      }

                    });
                } else {
                  swal("Form tidak di kirim!");
                }
            })  
        }
    })

    /*Update*/
     // Validate Form
    var frm_update_sp = $("#update_sp");

    frm_update_sp.validate();

    frm_update_sp.submit(function (e) {
        e.preventDefault();
        if (frm_update_sp.valid()) {
            var newForm =  new FormData($('#update_sp')[0]);

            swal({
                title: "Apakah anda yakin?",
                text: "Pastikan tidak ada data yang keliru!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willSend) => {
                if (willSend) {
                    $.ajax({
                      method :'post',
                      url  : base_url + 'kepeg/sp/update',
                      data : new FormData($('#update_sp')[0]),
                      cache:false,
                      processData:false,
                      contentType:false,              
                      success : function (data) {
                        // body...
                        var obj = jQuery.parseJSON(data);
                        if (obj.results == "success") {
                          swal("Form berhasil di kirim!", {
                            icon: "success",
                          })
                          .then((value) => {
                            location.reload(true);
                          });
                        }else{
                          swal("Form gagal kirim!", {
                            icon: "error",
                          })
                        }
                      },
                      error : function(){
                         swal("Ooops tidak bisa tersambung dengan server");
                      }

                    });
                } else {
                  swal("Form tidak di kirim!");
                }
            })  
        }
    })
    

    /*Delete Role*/
    $(document).on('click','.del-sp',function (e) {
        e.preventDefault();

        var id = $(this).attr('name')

        swal({
          title: "Apakah anda yakin?",
          text: "Ingin menghapus data ini ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSend) => {
          if (willSend) {
             return fetch(base_url + `kepeg/sp/delete?id=${id}`);
          } else {
            throw null;
            
            swal("Oops Data gagal di hapus!");
          }
        })
        .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di hapus!");
            }

            swal({
              title: "Success",
              text: 'Data berhasil di hapus',
              icon:'success',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
   
    })
    /*Delete Role*/
})
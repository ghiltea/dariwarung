$(function () {
    var cabang  = $("#r-branchEmp"),
        mr      = $("#r-mrEmp");

    var 
        htmlDept     = '',
        htmlSubDept  = '',
        htmlJabatan  = '',
        htmlLevel    = '',
        htmlGrade    = '',
        htmlTglMasuk = '';

    // Placeholder select2
    $('#r-category').select2({placeholder: 'Silahkan Pilih Category',allowClear:true});
    $('#r-branchEmp').select2({placeholder: 'Silahkan Pilih Cabang',allowClear:true});
    $('#r-mrEmp').select2({placeholder: 'Silahkan Pilih MR',allowClear:true});
    $('#r-deptEmp').select2({placeholder: 'Silahkan Pilih Departments',allowClear:true});
    $('#r-subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments',allowClear:true});

	var resignTable = $("#resign-table").DataTable({
		dom: 'Bfrtip',
        buttons: [
            {
                text: 'Add Pegawai Keluar',
                action: function ( e, dt, node, config ) {
                    location.replace(base_url+'kepeg/pegawai-keluar/add')
                },
                "className": 'btn btn-default'
            }
        ],
		'lengthChange': true,
		'autoWidth'   : false,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 5,
        "drawCallback": function( settings ) {

            $("#load-resign").css("display","none");
            $("#load-resign").remove();
            $('#r-table').removeAttr("style");
        }
	});
    $("#resign-table").dataTable().fnDraw();

    // Cabang 
    $(document).on('change','#r-branchEmp',function () {
        var deptid = $(this).val();
        var div       = $('#r-mrEmp');
        var op        = " ";
       
        
        $.ajax({
            type:'post',
            url: base_url+'settings/branch/findmr',
            data:{'id':deptid},
            success:function (data) {
              
              data =  $.parseJSON(data);
                op+='<option value="" selected disabled></option>';
                for (var i = 0; i < data.length; i++) {
                    
                    op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
                }

                div.html(" ");
                div.append(op);
                $('#r-mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
            },
            error:function () {
                console.log('Something wrong !!!');
            }
        });
    });
    // Departments
    $(document).on('change','#r-deptEmp',function () {
        var deptid = $(this).val();
        var div       = $('#r-subdeptEmp');
        var op        = " ";
       
        $.ajax({
            type:'post',
            url: base_url+'settings/departments/findsubdept',
            data:{'id':deptid},
            success:function (data) {
              
              data =  $.parseJSON(data);
                op+='<option value="" selected disabled></option>';
                for (var i = 0; i < data.length; i++) {
                    
                    op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
                }

                div.html(" ");
                div.append(op);
                $('#r-subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments'});
            },
            error:function () {
                console.log('Something wrong !!!');
            }
        });
    });
    // Sub Departments
    $(document).on('change','#r-subdeptEmp',function () {
        var deptid = $(this).val();
        var div       = $('#r-jabatanEmp');
        var op        = " ";


        $.ajax({
            type:'post',
            url: base_url+'settings/departments/findjabatan',
            data:{'id':deptid},
            success:function (data) {
              
              data =  $.parseJSON(data);
                op+='<option value="" selected disabled></option>';
                for (var i = 0; i < data.length; i++) {
                    
                    op+='<option value="'+data[i].id+'">'+data[i].jabatan+'</option>'
                }

                div.html(" ");
                div.append(op);
                $('#r-jabatanEmp').select2({placeholder: 'Silahkan Pilih Jabatan'});
            },
            error:function () {
                console.log('Something wrong !!!');
            }
        });
    });
    // Jabatan
    $(document).on('change','#r-jabatanEmp',function () {
        var deptid = $(this).val();
        var level       = $('#r-level');
        var grade       = $('#r-grade');
        var op = "";
        $.ajax({
            type:'post',
            url: base_url+'settings/jabatan/findjabatanchild',
            data:{'id':deptid},
            success:function (data) {
                  data =  $.parseJSON(data);
                  data = data[0];
                if (data !== undefined) {
                  level.val(data.level);
                  grade.val(data.grade);
                }
                
            },
            error:function () {
                console.log('Something wrong !!!');
            }
        });
    });

    // Search Pegawai
    $(document).on('click','#r-searchNama',function (e) {
        e.preventDefault();
        var mrTitle = '';
        var valCabang = cabang.val(),
            valMr = mr.val();
        if (valCabang == null || valCabang == '' || valCabang == undefined) {
            swal('Silahkan Pilih Cabang');
        }else{
            if (valMr == null || valMr == '' || valMr == undefined) {
                swal('Silahkan Pilih MR');
            }else{
                if ($('#r-mrEmp option:selected').text() !== mrTitle) {
                    mrTitle = $("#r-mrEmp option:selected").text();
                    $('#r-pegawai-table').DataTable({
                        "destroy" : true,
                        "ajax" : {
                            "url"  : base_url + "kepeg/pegawai/empfilter",
                            "type" : 'POST',
                            "data" : {
                                "id"    : cabang.val(),
                                "subid" : mr.val(),
                                "dt"    : 1,
                                "type"  : 1
                            },

                        },
                        "columns": [
                            { "data": "nama" },
                            { "data": "nip" },
                            { "data": "cabang" },
                            { "data": "mr" },
                            { "data": "deptname" },
                            { "data": "jabatan" }
                        ],
                        'ordering'    : true,
                        'info'       : true,
                        'lengthChange': true,
                        'autoWidth'   : false,
                        scrollX:        true,
                        scrollCollapse: true,
                        "pageLength": 5,
                        fixedHeader: true,
                    });
                }
                
                $("#r-pegawai-table").dataTable().fnDraw();

                $('#r-titlePegawai').text('Daftar Karyawan '+$("#r-branchEmp option:selected").text()+ ' di MR ' + $("#r-mrEmp option:selected").text());
                $("#r-modal-pegawai").modal('show');

                    $('#r-pegawai-table tbody').on('click', 'tr', function () {
                    
                    var rPegawaiTable = $('#r-pegawai-table').DataTable();
                    var data = rPegawaiTable.row( this ).data();
                    var nama = data.nama,
                        nip  = data.nip;
                    
                   $('#r-namaEmp').val(nama);
                   $('#r-nipEmp').val(nip);

                   // Ajax menampilkan status yang lama
                   $.ajax({
                        url: base_url + 'kepeg/pegawai/search',
                        type: 'POST',
                        dataType: 'json',
                        data: {nip: nip},
                   })
                   .done(function(data) {
                    
                        htmlDept    = '<option value='+data['pegawai']['deptid'] +' selected>' + data['pegawai']['deptname']+ '</option>';
                        htmlSubDept = '<option value='+data['pegawai']['subdeptid'] +' selected>' + data['pegawai']['subdept']+ '</option>';
                        htmlJabatan = '<option value='+data['pegawai']['jabatan_id'] +' selected>' + data['pegawai']['jabatan']+ '</option>';
                        htmlLevel   = data['pegawai']['level'];
                        htmlGrade   = data['pegawai']['grade'];
                        $("#r-deptEmp").append(htmlDept);
                        $("#r-subdeptEmp").append(htmlSubDept);
                        $("#r-jabatanEmp").append(htmlJabatan);
                        $("#r-level").val(htmlLevel);
                        $("#r-grade").val(htmlGrade);
                        
                        return false;
                   });
                   
                   $('#r-modal-pegawai').modal('hide');
                });
            }
        }

    })
    // End Search Pegawai

    // Submit

    // Validate Form
    var frm_register_pegawai_keluar = $("#register_pegawai_keluar");
    
    frm_register_pegawai_keluar.validate();

    $(document).on("click","#btn_tambah_PegawaiKeluar",function (e) {
        e.preventDefault();
        if (frm_register_pegawai_keluar.valid()) {
            
            var newForm =  new FormData($('#register_pegawai_keluar')[0]);

            swal({
                title: "Apakah anda yakin?",
                text: "Pastikan tidak ada data yang keliru!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willSend) => {
                if (willSend) {
                    $.ajax({
                      method :'post',
                      url  : base_url + 'kepeg/pegawai-keluar/store',
                      data : new FormData($('#register_pegawai_keluar')[0]),
                      cache:false,
                      processData:false,
                      contentType:false,              
                      success : function (data) {
                        // body...
                        var obj = jQuery.parseJSON(data);
                        if (obj.results == "success") {
                          swal("Form berhasil di kirim!", {
                            icon: "success",
                          })
                          .then((value) => {
                            location.reload(true);
                          });
                        }else{
                          swal("Form gagal kirim!", {
                            icon: "error",
                          })
                        }
                      },
                      error : function(){
                         swal("Ooops tidak bisa tersambung dengan server");
                      }

                    });
                } else {
                  swal("Form tidak di kirim!");
                }
            })  
        }
    })
    // End Submit

    // Edit Pegawai Keluar

    // Validate Form
    var frm_edit_pegawai_keluar = $("#edit_pegawai_keluar");
    
    frm_edit_pegawai_keluar.validate();

    $(document).on("click","#btn_Edit_PegawaiKeluar",function (e) {
        e.preventDefault();
        if (frm_edit_pegawai_keluar.valid()) {
            
            var newForm =  new FormData($('#edit_pegawai_keluar')[0]);

            swal({
                title: "Apakah anda yakin?",
                text: "Pastikan tidak ada data yang keliru!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willSend) => {
                if (willSend) {
                    $.ajax({
                      method :'post',
                      url  : base_url + 'kepeg/pegawai-keluar/update',
                      data : new FormData($('#edit_pegawai_keluar')[0]),
                      cache:false,
                      processData:false,
                      contentType:false,              
                      success : function (data) {
                        // body...
                        var obj = jQuery.parseJSON(data);
                        if (obj.results == "success") {
                          swal("Form berhasil di kirim!", {
                            icon: "success",
                          })
                        }else{
                          swal("Form gagal kirim!", {
                            icon: "error",
                          })
                        }
                      },
                      error : function(){
                         swal("Ooops tidak bisa tersambung dengan server");
                      }

                    });
                } else {
                  swal("Form tidak di kirim!");
                }
            })  
        }
    })
})
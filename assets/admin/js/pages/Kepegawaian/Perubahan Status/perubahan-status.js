$(function () {
/*New Perubahan Status Pegawai*/
	// Data Table Perubahan Status Pegawai
        $('#pstatus-table').DataTable({
		dom: 'Bfrtip',
        buttons: [
            {
                text: 'Add Perubahan Status MDRP',
                action: function ( e, dt, node, config ) {
                    location.replace(base_url+'kepeg/perubahan-status-pegawai/add')
                },
                "className": 'btn btn-default'
            }
        ],
		'lengthChange': true,
		'autoWidth'   : true,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 5,
        // responsive : true,
        // fixedHeader: true,
        "drawCallback": function( settings ) {

        	$("#load-pstatus").css("display","none");
        	$("#load-pstatus").remove();
	        $('#pst-table').removeAttr("style");
	    }
	});
	$("#pstatus-table").dataTable().fnDraw();
        //kekdua tablel kontrak
	$('#pstatus-table2').DataTable({
		dom: 'Bfrtip',
        buttons: [
            {
                text: 'Add Perubahan Status KONTRAK',
                action: function ( e, dt, node, config ) {
                    location.replace(base_url+'kepeg/perubahan-status-pegawai/add')
                },
                "className": 'btn btn-default'
            }
        ],
		'lengthChange': true,
		'autoWidth'   : true,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 5,
        // responsive : true,
        // fixedHeader: true,
        "drawCallback": function( settings ) {

        	$("#load-pstatus").css("display","none");
        	$("#load-pstatus").remove();
	        $('#pst-table2').removeAttr("style");
	    }
	});
	$("#pstatus-table2").dataTable().fnDraw();
  	// 
  	// Cabang Lama
    $(document).on('change','#pst-branchEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#pst-mrEmp');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/branch/findmr',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#pst-mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});// 
  	// Cabang Baru
    $(document).on('change','#pst-baru-branchEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#pst-baru-mrEmp');
	    var op        = " ";
	   	$("#pst-baru-atasan").val('');
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/branch/findmr',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#pst-baru-mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
	// Departments Baru
    $(document).on('change','#pst-baru-deptEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#pst-baru-subdeptEmp');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findsubdept',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#pst-baru-subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  	// Sub Departments
    $(document).on('change','#pst-baru-subdeptEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#pst-baru-jabatanEmp');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findjabatan',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].jabatan+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#pst-baru-jabatanEmp').select2({placeholder: 'Silahkan Pilih Jabatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  	// Jabatan
    $(document).on('change','#pst-baru-jabatanEmp',function () {
	    var deptid = $(this).val();
	    var level       = $('#pst-baru-level');
	    var grade       = $('#pst-baru-grade');
	    var op = "";
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/jabatan/findjabatanchild',
	        data:{'id':deptid},
	        success:function (data) {
		          data =  $.parseJSON(data);
		          data = data[0];
	        	if (data !== undefined) {
		          level.val(data.level);
		          grade.val(data.grade);
	        	}
	            
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
	
    var tablePegawai,
    	mrTitle;
    	
	// Search Pegawai
	$(document).on('click','#pst-searchNama', function (e) {
		e.preventDefault();

		var cabang  = $("#pst-branchEmp"),
			mr  	= $("#pst-mrEmp");
		
		if (cabang.val() == null) {
			alert('silahkan isi cabang terlebih dahulu');
			// swal('silahkan isi cabang terlebih dahulu');
		}else{
			if (mr.val() == null) {
				alert('silahkan isi mr terlebih dahulu');
			}else{
				if ($('#pst-mrEmp option:selected').text() !== mrTitle) {

					$('#pst-pegawai-table').DataTable({
						"destroy" : true,
                		"ajax" : {
                			"url"  : base_url + "kepeg/pegawai/empfilter",
                			"type" : 'POST',
                			"data" : {
                				"id" : cabang.val(),
                				"subid" : mr.val(),
                				"dt" : 1
                			},

                		},
                		"columns": [
				            { "data": "nama" },
				            { "data": "nip" },
				            { "data": "cabang" },
				            { "data": "mr" },
				            { "data": "deptname" },
				            { "data": "jabatan" }
				        ],
						'ordering'    : true,
						'info'       : true,
						'lengthChange': true,
						'autoWidth'   : true,
						scrollX:        true,
				        scrollCollapse: true,
				        "pageLength": 5,
				        fixedHeader: true,
                	});
		            mrTitle = $("#pst-mrEmp option:selected").text();
				}

				$('#pst-titlePegawai').text('Daftar Karyawan '+$("#pst-branchEmp option:selected").text()+ ' di MR ' + $("#pst-mrEmp option:selected").text());
				$("#pst-modal-pegawai").modal('show');

			var htmlDept = '',
		   		htmlSubDept = '',
		   		htmlJabatan = '',
		   		htmlLevel = '',
		   		htmlGrade = '',
		   		htmlTglMasuk = '',
		   		htmlKontrakAwal = '',
		   		htmlKontrakAkhir = '',
		   		htmlStatsKar = '';
		   		


		   		$('#pst-pegawai-table tbody').on('click', 'tr', function () {
		   			
		   			var pstPegawaiTable = $('#pst-pegawai-table').DataTable();
			        var data = pstPegawaiTable.row( this ).data();
			        var nama = data.nama,
			        	nip  = data.nip;
			        
				   $('#pst-namaEmp').val(nama);
				   $('#pst-nipEmp').val(nip);

				   // Ajax menampilkan status yang lama
				   $.ajax({
				   	url: base_url + 'kepeg/pegawai/search',
				   	type: 'POST',
				   	dataType: 'json',
				   	data: {nip: nip},
				   })
				   .done(function(data) {
				   	
				   	htmlDept 	= '<option value='+data['pegawai']['deptid'] +' selected>' + data['pegawai']['deptname']+ '</option>';
				   	htmlSubDept = '<option value='+data['pegawai']['subdeptid'] +' selected>' + data['pegawai']['subdept']+ '</option>';
				   	htmlJabatan = '<option value='+data['pegawai']['jabatan_id'] +' selected>' + data['pegawai']['jabatan']+ '</option>';
				   	htmlLevel   = data['pegawai']['level'];
				   	htmlGrade   = data['pegawai']['grade'];
				   	htmlTglMasuk = data['pegawai']['tgl_masuk'];
				   	htmlKontrakAwal = data['pegawai']['kontrak_awal'];
				   	htmlKontrakAkhir = data['pegawai']['kontrak_akhir'];
				   	htmlStatsKar = '<option value='+data['pegawai']['status_karyawan_id'] +' selected>' + data['pegawai']['status_karyawan']+ '</option>';
				   	$("#pst-deptEmp").append(htmlDept);
				   	$("#pst-subdeptEmp").append(htmlSubDept);
				   	$("#pst-jabatanEmp").append(htmlJabatan);
				   	$("#pst-level").val(htmlLevel);
				   	$("#pst-grade").val(htmlGrade);
				   	$("#pst-tanggal-masuk").val(htmlTglMasuk);
				   	$("#pst-kontrak-awal").val(htmlKontrakAwal);
				   	$("#pst-kontrak-akhir").val(htmlKontrakAkhir);
				   	$("#pst-statskarEmp").append(htmlStatsKar);

				   	var today  = new Date().toISOString().slice(0,10),
				   		tgl2 = $("#pst-tanggal-masuk").val();
				   	var lama_kerja = getAge(new Date(tgl2), new Date(today));

				   	$("#pst-lamakerja").val(lama_kerja)

				   	
				   	return false;
				   });
				   
				   $('#pst-modal-pegawai').modal('hide');
			    });
			}
		}
	});
	
	var cabangTitle;
	// Search Atasan Perubahan Status Baru
	$(document).on('click','#pst-baru-searchAtasan', function (e) {
		e.preventDefault();
		var cabang  = $("#pst-baru-branchEmp"),
			mr  	= $("#pst-baru-mrEmp");
		
		if (cabang.val() == null) {
			alert('silahkan isi cabang terlebih dahulu');
			// swal('silahkan isi cabang terlebih dahulu');
		}else{
			if ($('#pst-baru-branchEmp option:selected').text() !== cabangTitle) {

				$('#pst-atasan-table').DataTable({
					"destroy" : true,
					"processing" : true,
					"serverside" : true,
            		"ajax" : {
            			"url"  : base_url + "kepeg/pegawai/empfilter",
            			"type" : 'POST',
            			"data" : {
            				"id" : cabang.val(),
            				"dt" : 1
            			},

            		},
            		"columns": [
			            { "data": "nama" },
			            { "data": "nip" },
			            { "data": "cabang" },
			            { "data": "mr" },
			            { "data": "deptname" },
			            { "data": "jabatan" }
			        ],
					'info'       : true,
					'lengthChange': true,
					'autoWidth'   : true,
					scrollX:        true,
			        scrollCollapse: true,
			        "pageLength": 5,
			        fixedHeader: true,
            	});
	            cabangTitle = $("#pst-baru-branchEmp option:selected").text();
			}

			$('#pst-titleAtasan').text('Daftar Karyawan '+$("#pst-baru-branchEmp option:selected").text());
			$("#pst-atasan-pegawai").modal('show');


	   		$('#pst-atasan-table tbody').on('click', 'tr', function () {
	   			
	   			var pstAtasanTable = $('#pst-atasan-table').DataTable();
		        var data = pstAtasanTable.row( this ).data();
		        var nama = data.nama,
		        	nip  = data.nip,
		        	nama_nip = nama + " - " + nip;
		       	
		       $("#pst-baru-atasan").val(nama_nip); 
			   
			   $('#pst-atasan-pegawai').modal('hide');
		    });
		}
	});

	/*Send Data*/

	// Validate Form
	var frmRegister_perubahan_employee = $("#register_perubahan_employee");
	var validator = frmRegister_perubahan_employee.validate({
		rules: {
			'pst-branchEmp'     : {required: true},
			'pst-mrEmp'         : {required: true},
			'pst-namaEmp'       : {required: true},
			'pst-nipEmp'        : {required: true},
			'pst-deptEmp'       : {required: true},
			'pst-subdeptEmp'    : {required: true},
			'pst-jabatanEmp'    : {required: true},
			'pst-level'         : {required: true},
			'pst-tanggal-masuk' : {required: true},
			'pst-kontrak-awal'  : {required: true},
			'pst-kontrak-akhir' : {required: true},
		    'pst-statskarEmp'   : {required: true},
		}
	});



	$(document).on('click','#btn_tambah_PerubahanStatus',function (e) {
		e.preventDefault();
		if (frmRegister_perubahan_employee.valid()) {
			var newForm =  new FormData($('#register_perubahan_employee')[0]);

			swal({
		        title: "Apakah anda yakin?",
		        text: "Pastikan tidak ada data yang keliru!",
		        icon: "warning",
		        buttons: true,
		        dangerMode: true,
	      	})
	      	.then((willSend) => {
	        	if (willSend) {
		           	$.ajax({
		              method :'post',
		              url  : base_url + 'kepeg/perubahan-status-pegawai/store',
		              data : new FormData($('#register_perubahan_employee')[0]),
		              cache:false,
		              processData:false,
		              contentType:false,   
                                //dataType: 'JSON',
		              success : function (data) {
		                // body...
		                var obj = jQuery.parseJSON(data);
                             //   console.log(data);
		                if (obj.results == "success") {
		                  swal("Form berhasil di kirim!", {
		                    icon: "success",
		                  })
		                }else{
		                  swal("Form gagal kirim!", {
		                    icon: "error",
		                  })
		                }
		              },
		              error : function(){
		                 swal("Ooops tidak bisa tersambung dengan server");
		              }

		            });
	        	} else {
		          swal("Form tidak di kirim!");
		        }
	      	})	
		}
	});

	/*Cek History Jabatan For New Data*/
	var nipEmp = $('#pst-nipEmp');
	
	$(document).on('click','#cek-history',function (e) {
		console.log()
		e.preventDefault();
		if ($('#pst-nipEmp').val() == null || $('#pst-nipEmp').val() == '') {
			swal('Pastikan Sudah Memilih Pegawai');
		}else{
			$('#pst-history-table').DataTable({
				 columnDefs: [
		        	{ "width": "300px", "targets": [1] },    
		        // 	{ "width": "200px", "targets": [3] },    
		        ],
				"destroy" : true,
        		"ajax" : {
        			"url"  : base_url + "kepeg/pegawai/history",
        			"type" : 'POST',
        			"data" : {
        				"n"  : $('#pst-nipEmp').val(),
        				"dt" : 1
        			},

        		},
        		"columns": [
		            { "data": "nama_pegawai" },
		            { "data": "nip_pegawai" },
		            { "data": "cabang_baru" },
		            { "data": "mr_baru" },
		            { "data": "dept_baru" },
		            { "data": "subdept_baru" },
		            { "data": "jabatan_baru" },
		            { "data": "tgl_masuk" },
		            { "data": "status_baru" },
		            { "data": "kontrak_awal_baru" },
		            { "data": "kontrak_akhir_baru" },
		            { "data": "catatan" },
                            { "data": "mutasi" },
                            { "data": "demosi" },
                            { "data": "rotasi" },
                            { "data": "promosi" },
		        ],
				'info'       : true,
				// 'lengthChange': true,
				// 'autoWidth'   : true,
				scrollX:        true,
		        scrollCollapse: true,
		        "pageLength": 5,
		        fixedHeader: true,
		       
        	});

			$("#pst-modal-history").modal('show');

		}
	})
	/*Cek History Jabatan For New Data*/
/*Edit Data Perubahan Status Pegawai*/
	  	// Cabang Lama
    $(document).on('change','#e-pst-branchEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#e-pst-mrEmp');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/branch/findmr',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#e-pst-mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});// 
  	// Cabang Baru
    $(document).on('change','#e-pst-baru-branchEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#e-pst-baru-mrEmp');
	    var op        = " ";
	   	$("#e-pst-baru-atasan").val('');
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/branch/findmr',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#e-pst-baru-mrEmp').select2({placeholder: 'Silahkan Pilih MR'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
	// Departments Baru
    $(document).on('change','#e-pst-baru-deptEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#e-pst-baru-subdeptEmp');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findsubdept',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#e-pst-baru-subdeptEmp').select2({placeholder: 'Silahkan Pilih Sub Departments'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  	// Sub Departments
    $(document).on('change','#e-pst-baru-subdeptEmp',function () {
	    var deptid = $(this).val();
	    var div       = $('#e-pst-baru-jabatanEmp');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/departments/findjabatan',
	        data:{'id':deptid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].jabatan+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
			    $('#e-pst-baru-jabatanEmp').select2({placeholder: 'Silahkan Pilih Jabatan'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
  	// Jabatan
    $(document).on('change','#e-pst-baru-jabatanEmp',function () {
	    var deptid = $(this).val();
	    var level       = $('#e-pst-baru-level');
	    var grade       = $('#e-pst-baru-grade');
	    var op = "";
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/jabatan/findjabatanchild',
	        data:{'id':deptid},
	        success:function (data) {
		          data =  $.parseJSON(data);
		          data = data[0];
	        	if (data !== undefined) {
		          level.val(data.level);
		          grade.val(data.grade);
	        	}
	            
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});
	// Status Karyawan
	 $("#pst-baru-statskarEmp").change(function () {
    	$('#pst-baru-statskarEmp option:selected').each(function () {
    		var id = $(this).val(),
    			text = $(this).text();

			if (id == 5) {
				$('#pst-konawal,#pst-konakhir').each(function(index){
		    		$(this).find('small').css('display','none');
		    	})
				$('#pst-baru-kontrak-awal,#pst-baru-kontrak-akhir').removeAttr('required');
				$('#pst-baru-kontrak-awal,#pst-baru-kontrak-akhir').prop('readonly', true);		
			}else{
				$('#pst-konawal,#pst-konakhir').each(function(index){
		    		$(this).find('small').css('display','');
		    	})
				$('#pst-baru-kontrak-awal,#pst-baru-kontrak-akhir').attr('required');
				$('#pst-baru-kontrak-awal,#pst-baru-kontrak-akhir').prop('readonly', false);		
			}
    		console.log(`id : ${id}, text = ${text}`);
    	})	
    	console.log(this)
    });

	// $("#pst-baru-statskarEmp").change(function() {
		/*$('#pst-baru-statskarEmp option:selected').each(function () {
			var id = $(this).val(),
    			text = $(this).text();
    		
			
		})*/
	// });

    var tablePegawai,
    	mrTitle;
    
    var today  = new Date().toISOString().slice(0,10),
	tgl2 = $("#e-pst-tanggal-masuk").val();

	if (tgl2 !== '' || tgl2 !== null || tgl2 !== undefined) {
		var lama_kerja = getAge(new Date(tgl2), new Date(today));

		$("#e-pst-lamakerja").val(lama_kerja)
	}
	// Search Pegawai
	$(document).on('click','#e-pst-searchNama', function (e) {
		e.preventDefault();

		var cabang  = $("#e-pst-branchEmp"),
			mr  	= $("#e-pst-mrEmp");
		
		if (cabang.val() == null) {
			alert('silahkan isi cabang terlebih dahulu');
			// swal('silahkan isi cabang terlebih dahulu');
		}else{
			if (mr.val() == null) {
				alert('silahkan isi mr terlebih dahulu');
			}else{
				if ($('#e-pst-mrEmp option:selected').text() !== mrTitle) {

					$('#e-pst-pegawai-table').DataTable({
						"destroy" : true,
                		"ajax" : {
                			"url"  : base_url + "kepeg/pegawai/empfilter",
                			"type" : 'POST',
                			"data" : {
                				"id" : cabang.val(),
                				"subid" : mr.val(),
                				"dt" : 1
                			},

                		},
                		"columns": [
				            { "data": "nama" },
				            { "data": "nip" },
				            { "data": "cabang" },
				            { "data": "mr" },
				            { "data": "deptname" },
				            { "data": "jabatan" }
				        ],
						'ordering'    : true,
						'info'       : true,
						'lengthChange': true,
						'autoWidth'   : true,
						scrollX:        true,
				        scrollCollapse: true,
				        "pageLength": 5,
				        fixedHeader: true,
                	});
		            mrTitle = $("#e-pst-mrEmp option:selected").text();
				}

				$('#e-pst-titlePegawai').text('Daftar Karyawan '+$("#e-pst-branchEmp option:selected").text()+ ' di MR ' + $("#e-pst-mrEmp option:selected").text());
				$("#e-pst-modal-pegawai").modal('show');

			var htmlDept = '',
		   		htmlSubDept = '',
		   		htmlJabatan = '',
		   		htmlLevel = '',
		   		htmlGrade = '',
		   		htmlTglMasuk = '',
		   		htmlKontrakAwal = '',
		   		htmlKontrakAkhir = '',
		   		htmlStatsKar = '';
		   		


		   		$('#e-pst-pegawai-table tbody').on('click', 'tr', function () {
		   			
		   			var pstPegawaiTable = $('#e-pst-pegawai-table').DataTable();
			        var data = pstPegawaiTable.row( this ).data();
			        var nama = data.nama,
			        	nip  = data.nip;
			        
				   $('#e-pst-namaEmp').val(nama);
				   $('#e-pst-nipEmp').val(nip);

				   // Ajax menampilkan status yang lama
				   $.ajax({
				   	url: base_url + 'kepeg/pegawai/search',
				   	type: 'POST',
				   	dataType: 'json',
				   	data: {nip: nip},
				   })
				   .done(function(data) {
				   	
				   	htmlDept 	= '<option value='+data['pegawai']['deptid'] +' selected>' + data['pegawai']['deptname']+ '</option>';
				   	htmlSubDept = '<option value='+data['pegawai']['subdeptid'] +' selected>' + data['pegawai']['subdept']+ '</option>';
				   	htmlJabatan = '<option value='+data['pegawai']['jabatan_id'] +' selected>' + data['pegawai']['jabatan']+ '</option>';
				   	htmlLevel   = data['pegawai']['level'];
				   	htmlGrade   = data['pegawai']['grade'];
				   	htmlTglMasuk = data['pegawai']['tgl_masuk'];
				   	htmlKontrakAwal = data['pegawai']['kontrak_awal'];
				   	htmlKontrakAkhir = data['pegawai']['kontrak_akhir'];
				   	htmlStatsKar = '<option value='+data['pegawai']['status_karyawan_id'] +' selected>' + data['pegawai']['status_karyawan']+ '</option>';
				   	$("#e-pst-deptEmp").append(htmlDept);
				   	$("#e-pst-subdeptEmp").append(htmlSubDept);
				   	$("#e-pst-jabatanEmp").append(htmlJabatan);
				   	$("#e-pst-level").val(htmlLevel);
				   	$("#e-pst-grade").val(htmlGrade);
				   	$("#e-pst-tanggal-masuk").val(htmlTglMasuk);
				   	$("#e-pst-kontrak-awal").val(htmlKontrakAwal);
				   	$("#e-pst-kontrak-akhir").val(htmlKontrakAkhir);
				   	$("#e-pst-statskarEmp").append(htmlStatsKar);

				   	var today  = new Date().toISOString().slice(0,10),
				   		tgl2 = $("#e-pst-tanggal-masuk").val();
				   	var lama_kerja = getAge(new Date(tgl2), new Date(today));

				   	$("#e-pst-lamakerja").val(lama_kerja)

				   	
				   	return false;
				   });
				   
				   $('#e-pst-modal-pegawai').modal('hide');
			    });
			}
		}
	});
	
	var cabangTitle;
	// Search Atasan Perubahan Status Baru
	$(document).on('click','#e-pst-baru-searchAtasan', function (e) {
		e.preventDefault();
		var cabang  = $("#e-pst-baru-branchEmp"),
			mr  	= $("#e-pst-baru-mrEmp");
		
		if (cabang.val() == null) {
			alert('silahkan isi cabang terlebih dahulu');
			// swal('silahkan isi cabang terlebih dahulu');
		}else{
			if ($('#e-pst-baru-branchEmp option:selected').text() !== cabangTitle) {

				$('#e-pst-atasan-table').DataTable({
					"destroy" : true,
					"processing" : true,
					"serverside" : true,
            		"ajax" : {
            			"url"  : base_url + "kepeg/pegawai/empfilter",
            			"type" : 'POST',
            			"data" : {
            				"id" : cabang.val(),
            				"dt" : 1
            			},

            		},
            		"columns": [
			            { "data": "nama" },
			            { "data": "nip" },
			            { "data": "cabang" },
			            { "data": "mr" },
			            { "data": "deptname" },
			            { "data": "jabatan" }
			        ],
					'info'       : true,
					'lengthChange': true,
					'autoWidth'   : true,
					scrollX:        true,
			        scrollCollapse: true,
			        "pageLength": 5,
			        fixedHeader: true,
            	});
	            cabangTitle = $("#e-pst-baru-branchEmp option:selected").text();
			}

			$('#e-pst-titleAtasan').text('Daftar Karyawan '+$("#e-pst-baru-branchEmp option:selected").text());
			$("#e-pst-atasan-pegawai").modal('show');


	   		$('#e-pst-atasan-table tbody').on('click', 'tr', function () {
	   			
	   			var pstAtasanTable = $('#e-pst-atasan-table').DataTable();
		        var data = pstAtasanTable.row( this ).data();
		        var nama = data.nama,
		        	nip  = data.nip,
		        	nama_nip = nama + " - " + nip;
		       	
		       $("#e-pst-baru-atasan").val(nama_nip); 
			   
			   $('#e-pst-atasan-pegawai').modal('hide');
		    });
		}
	});

	/*Send Data*/

	// Validate Form
	var EditfrmRegister_perubahan_employee = $("#e-register_perubahan_employee");
	var validator = EditfrmRegister_perubahan_employee.validate({
		rules: {
			'e-pst-branchEmp'     : {required: true},
			'e-pst-mrEmp'         : {required: true},
			'e-pst-namaEmp'           : {required: true},
			'e-pst-namaEmp'       : {required: true},
			'e-pst-nipEmp'        : {required: true},
			'e-pst-deptEmp'       : {required: true},
			'e-pst-subdeptEmp'    : {required: true},
			'e-pst-jabatanEmp'    : {required: true},
			'e-pst-level'         : {required: true},
			'e-pst-tanggal-masuk' : {required: true},
			'e-pst-kontrak-awal'  : {required: true},
			'e-pst-kontrak-akhir' : {required: true},
		    'e-pst-statskarEmp'   : {required: true},
		}
	});



	$(document).on('click','#btn_ubah_PerubahanStatus',function (e) {
		e.preventDefault();
		if (EditfrmRegister_perubahan_employee.valid()) {
			var newForm =  new FormData($('#e-register_perubahan_employee')[0]);

			swal({
		        title: "Apakah anda yakin?",
		        text: "Pastikan tidak ada data yang keliru!",
		        icon: "warning",
		        buttons: true,
		        dangerMode: true,
	      	})
	      	.then((willSend) => {
	        	if (willSend) {
		           	$.ajax({
		              method :'post',
		              url  : base_url + 'kepeg/perubahan-status-pegawai/update',
		              data : new FormData($('#e-register_perubahan_employee')[0]),
		              cache:false,
		              processData:false,
		              contentType:false,              
		              success : function (data) {
		                // body...
		                var obj = jQuery.parseJSON(data);
		                if (obj.results == "success") {
		                  swal("Form berhasil di kirim!", {
		                    icon: "success",
		                  })
		                }else{
		                  swal("Form gagal kirim!", {
		                    icon: "error",
		                  })
		                }
		              },
		              error : function(){
		                 swal("Ooops tidak bisa tersambung dengan server");
		              }

		            });
	        	} else {
		          swal("Form tidak di kirim!");
		        }
	      	})	
		}
	});

	/*Cek History Jabatan For New Data*/
	var nipEmp = $('#e-pst-nipEmp');
	
	$(document).on('click','#e-cek-history',function (e) {
		e.preventDefault();
		if (nipEmp.val() == null || nipEmp.val() == '') {
			swal('Pastikan sudah memilih pegawai');
		}else{
			$('#e-pst-history-table').DataTable({
				 columnDefs: [
		        	{ "width": "300px", "targets": [1] },    
		        // 	{ "width": "200px", "targets": [3] },    
		        ],
				"destroy" : true,
        		"ajax" : {
        			"url"  : base_url + "kepeg/pegawai/history",
        			"type" : 'POST',
        			"data" : {
        				"n"  : nipEmp.val(),
        				"dt" : 1
        			},

        		},
        		"columns": [
		            { "data": "nama_pegawai" },
		            { "data": "nip_pegawai" },
		            { "data": "cabang_baru" },
		            { "data": "mr_baru" },
		            { "data": "dept_baru" },
		            { "data": "subdept_baru" },
		            { "data": "jabatan_baru" },
		            { "data": "tgl_masuk" },
		            { "data": "status_baru" },
		            { "data": "kontrak_awal_baru" },
		            { "data": "kontrak_akhir_baru" },
		            { "data": "catatan" },
                             { "data": "mutasi" },
                            { "data": "demosi" },
                            { "data": "rotasi" },
                            { "data": "promosi" },
		        ],
				'info'       : true,
				// 'lengthChange': true,
				// 'autoWidth'   : true,
				scrollX:        true,
		        scrollCollapse: true,
		        "pageLength": 5,
		        fixedHeader: true,
		       
        	});

			$("#e-pst-modal-history").modal('show');

		}
	})
	/*Cek History Jabatan For New Data*/

})



/*Custom Function*/

function getAge(date_1, date_2){
  
//convert to UTC
	var date2_UTC = new Date(Date.UTC(date_2.getUTCFullYear(), date_2.getUTCMonth(), date_2.getUTCDate()));
	var date1_UTC = new Date(Date.UTC(date_1.getUTCFullYear(), date_1.getUTCMonth(), date_1.getUTCDate()));


	var yAppendix, mAppendix, dAppendix;


	//--------------------------------------------------------------
	var days = date2_UTC.getDate() - date1_UTC.getDate();
	if (days < 0)
	{

	    date2_UTC.setMonth(date2_UTC.getMonth() - 1);
	    days += DaysInMonth(date2_UTC);
	}
	//--------------------------------------------------------------
	var months = date2_UTC.getMonth() - date1_UTC.getMonth();
	if (months < 0)
	{
	    date2_UTC.setFullYear(date2_UTC.getFullYear() - 1);
	    months += 12;
	}
	//--------------------------------------------------------------
	var years = date2_UTC.getFullYear() - date1_UTC.getFullYear();




	if (years > 1) yAppendix = " years";
	else yAppendix = " year";
	if (months > 1) mAppendix = " months";
	else mAppendix = " month";
	if (days > 1) dAppendix = " days";
	else dAppendix = " day";


	return years + yAppendix + ", " + months + mAppendix + ", and " + days + dAppendix + ".";
}


function DaysInMonth(date2_UTC)
{
	var monthStart = new Date(date2_UTC.getFullYear(), date2_UTC.getMonth(), 1);
	var monthEnd = new Date(date2_UTC.getFullYear(), date2_UTC.getMonth() + 1, 1);
	var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
	return monthLength;
}
$(function () {
	// body...
	'use strict';

	/*var n = document.createElement('script');
	n.setAttribute('language', 'JavaScript');
	n.setAttribute('src', 'https://debug.datatables.net/debug.js');
	document.body.appendChild(n);*/

	var arr,rosterTable;
	var pages = "roster";
	var d1,d2,idCombo ;
	var usernik;
	var unip = 'nip';

     $("#roster-table").DataTable({
		'lengthChange': false,
		'autoWidth'   : false,
        // "pageLength": ,
        'ordering' : false,
        // 'filter' : false,
        scrollCollapse: true,
		scrollX:        true,
        // 'paging' : false,
	});

	$.ajax({
		async:false,
		url: base_url + 'dropros',
	})
	.done(function(data) {
		arr = data;
		arr = JSON.parse(data);
	})
	.fail(function() {
		// console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});

	var comboTree1, comboTree2;

	comboTree2 = $('#inputCmbRos').comboTree({
				source : arr,
				isMultiple: false
	});

	/*Generate Roster*/
	$(document).on('click','#btnGenRos',function (e) {
		idCombo = comboTree2.getSelectedItemsId();
		
		d1 = $('#fromdate').val();
		d2 = $('#todate').val();

		if (d1 > d2) {
			swal('Ooops rentang tanggal salah');
		}else{
			if (d1 && d2 !== '' && idCombo !== false) {
				$("#loadRos").modal('show');
				create_roster(d1,d2,idCombo);
			}else{
				if (idCombo == false) {
					swal('Silahkan Pilih Area');
				}else if (d1 == '' && d2 == '') {
					swal('Silahkan Pilih Tanggal Awal & Akhir');
				}else if (d1 == '') {
					swal('Silahkan Pilih Tanggal Awal');
				}else if (d2 == '') {
					swal('Silahkan Pilih Tanggal Akhir');
				}else{
					swal('Ooops Sepertinya ada yang salah, silahkan hubungi administrator');
				}
			}
		}
	})
	/*End Generate Roster*/




	// Show Work Modal
	$("#workday").on('click',function (e) {
		var user = $("#unip").val();
		if (user == '') {
			swal('Please select user');
		}else{
			$("#workdayModal").modal('show');
		}
	})

	// Show Att Modal
	$("#attmod").on('click',function (e) {
		var user = $("#unip").val();
		if (user == '') {
			swal('Please select user');
		}else{
			$('#unipAtt').val(usernik);
			$("#attModal").modal('show');
		}
	})

	var el = false;
	let tglakhir = {};
	// rosatt on change
	$("#rosatt").on("change",function () {
		var id = $(this).attr("id");
		var selected = $('#'+id + ' option:selected');

		var html = '';

		html += '<div class="form-group">';
		html += '<label for="" class="control-label">Jam Masuk</label>';
		html += '<input type="time" name="stime" id="stime" class="form-control">';
		html += '</div>';
		html += '<div class="form-group">';
		html += '<label for="" class="control-label">Jam Keluar</label>';
		html += '<input type="time" name="etime" id="etime" class="form-control">';
		html += '</div>';

	    var formAttModal = $("#frmAttModal");

		if (selected.val().search('IA') > 0) {
			if (!el) {
				$("#ia").html(html);
				el = true;
				tglakhir = formAttModal.children().eq(3).clone();
				formAttModal.children().eq(3).empty();
			}
		}else{
			$('#ia').empty();
			el = false;
			formAttModal.children().eq(2).after(tglakhir);
		}
	})


	// Show Abs Modal
	$("#absmod").on('click',function (e) {
		var user = usernik;
		if (user == '') {
			swal('Please select user');
		}else{
			$('#unipAbs').val(usernik);
			$("#absModal").modal('show');
		}
		// swal('a');
	})


	/*Add Shift*/
	var frmWorkDay = $("#frmWorkDay");
	frmWorkDay.validate();

	var user = $("#unip");
	var fromdate = $("#sdate");
	var todate = $("#edate");
	
	$(document).on('click','#AddAssignShift',function () {
		
		if (frmWorkDay.valid()) {

			$.ajax({
				url: base_url + 'assignshift',
				type: 'POST',
				data: new FormData($("#frmWorkDay")[0]),
				cache:false,
                processData:false,
                contentType:false, 
			})
			.done(function(data) {
			 	var obj = jQuery.parseJSON(data);
                
                if (obj.results == "success") {
					$("#workdayModal").modal('hide');
					
					swal(obj.reason, {
						buttons:false,
						icon: "success",
						timer: 1000,
					})

				    $("#loadRos").modal('hide');

					idCombo = comboTree2.getSelectedItemsId();
					d1 = $('#fromdate').val();
					d2 = $('#todate').val();

					create_roster(d1,d2,idCombo);

                }else{
                  swal(obj.reason, {
                    icon: "error",
                  })
                }
			})
			.fail(function() {
            	swal("Ooops tidak bisa tersambung dengan server");
			});
		}		
	})



	/*Add Attendance Status*/
	var frmAttModal = $("#frmAttModal");
	frmAttModal.validate();

	var useratt = $("#unipAtt");
	var fromdate = $("#sdateatt");
	var todate = $("#edateatt");
	
	$(document).on('click','#uploadAttros',function () {
		if (frmAttModal.valid()) {

			$.ajax({
				url: base_url + 'assignatt',
				method: 'post',
				data: new FormData($("#frmAttModal")[0]),
				cache:false,
                processData:false,
                contentType:false, 
			})
			.done(function(data) {
			 	var obj = jQuery.parseJSON(data);
                
                if (obj.results == "success") {
					$("#attModal").modal('hide');
					
					swal(obj.reason, {
						buttons:false,
						icon: "success",
						timer: 1000,
					})

					$("#unipAtt").val('');

					$("#frmAttModal").trigger("reset");

					idCombo = comboTree2.getSelectedItemsId();
					d1 = $('#fromdate').val();
					d2 = $('#todate').val();

					create_roster(d1,d2,idCombo);
				    $("#loadRos").modal('hide');

                }else{
                  swal(obj.reason, {
                    icon: "error",
                  })
                }
			})
			.fail(function() {
            	swal("Ooops tidak bisa tersambung dengan server");
			});
		}		
	})

	/*Add Absence Status*/
	var frmAbsModal = $("#frmAbsModal");
	frmAbsModal.validate();

	var userabs  = $('#unipAbs');
	var fromdate = $('#sdateabs');
	var todate   = $('#edateabs');
	var notes    = $("#noteros_abs");

	$(document).on("click","#uploadAbsros",function  () {
		if (frmAbsModal.valid()) {
			$.ajax({
				url: base_url + 'assignabs',
				type: 'POST',
				data: {
					u: userabs.val(),
					sdate : fromdate.val(),
					edate : todate.val(),
					shiftcode: $("#rosabs option:selected").val(),
					notes :notes.val()
				},
			})
			.done(function(data) {
			 	var obj = jQuery.parseJSON(data);
                
                if (obj.results == "success") {
					$("#absModal").modal('hide');
					
					swal(obj.reason, {
						buttons:false,
						icon: "success",
						timer: 1000,
					})

					$("#unipAbs").val('');

					idCombo = comboTree2.getSelectedItemsId();
					d1 = $('#fromdate').val();
					d2 = $('#todate').val();

					create_roster(d1,d2,idCombo);
				    $("#loadRos").modal('hide');

                }else{
                  swal(obj.reason, {
                    icon: "error",
                  })
                }
			})
			.fail(function() {
            	swal("Ooops tidak bisa tersambung dengan server");
			});
		}
	})

	/*Export*/
	$(document).on('click','#genExcel',function () {
		idCombo = comboTree2.getSelectedItemsId();
		
		d1 = $('#fromdate').val();
		d2 = $('#todate').val();

		var date1 = new Date(d1);
		var date2 = new Date(d2);


		if (idCombo !== false) {
			var url = base_url + 'roster/excel?area='+idCombo;
			window.open(url);
		}else{
			swal('Silahkan Pilih Area');
		}

	})

	/*Import*/
	$(document).on('click','#genImport',function () {
		idCombo = comboTree2.getSelectedItemsId();
		
		if (idCombo !== false) {
			$("#import_roster").modal('show');
		}else{
			swal('Silahkan Pilih Area');
		}
	});

	/*Import Roster*/
	  $(document).on('click','#uploadRos',function (e) {
	    e.preventDefault();
	    var text = $("#uploadRos").text();
	    var form = $("#frmImportRoster");
	    // console.log(text);

	    form.validate();

	    var s;

	    if (form.valid()) {
	      swal({
	        title: "Apakah anda yakin?",
	        text: "Pastikan tidak ada data yang keliru!",
	        icon: "warning",
	        buttons: true,
	        dangerMode: true,
	      })
	      .then((willSend) => {
	        if (willSend) {
	          $("#uploadRos").html("");
	          $("#uploadRos").html("<i class=\"fa fa-spinner fa-spin\"></i>&nbsp;Loading");
	          $("#uploadRos").attr('disabled','disabled');
	           $.ajax({
	              method :'post',
	              url  : base_url + 'roster/import',
	              data : new FormData($('#frmImportRoster')[0]),
	              cache:false,
	              processData:false,
	              contentType:false,              
	              success : function (data) {
	                // body...
	                var obj = jQuery.parseJSON(data);
	                if (obj.results == "success") {
	                  $("#uploadRos").html("").html("Upload");
	                  $("#uploadRos").removeAttr("disabled");
	                  
	                  s = 1;    
					  d1 = obj.sdate;
					  d2 = obj.edate;

	                  swal(obj.reason, {
	                    icon: "success",
	                  }).then((value) => {
				        $("#import_roster").modal("hide");
	                  });
	                }else{
	                  $("#uploadRos").html("").html("Upload");
	                  $("#uploadRos").removeAttr("disabled");
	                  swal(obj.reason, {
	                    icon: "error",
	                  })
	                }
	              },
	              error : function(){
	                $("#uploadRos").html("").html("Upload");
	                $("#uploadRos").removeAttr("disabled");
	                swal("Ooops file tidak bisa di upload");
	              }
	            });
	        } else {
	          swal("Form tidak di kirim!");
	          $("#import_roster").modal("hide");
	          $("#uploadRos").html("").html("Upload");
	          $("#uploadRos").removeAttr("disabled");
	        }
	        
	      })

		
	    }
	  })
	/*Import Roster*/

	function create_roster(d1,d2,idCombo) {
		let thead = [];
		$.ajax({
				url: base_url + 'createroster',
				dataType : "json",
				data : {d1 : d1,d2: d2, id : idCombo}
				// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
				// data: {param1: 'value1'},
			})
			.done(function(data) {
				
				if (data.response == 'success') {
					// var dataSet = data.field;
					var tableHeaders = '';

	                $.each(data.columns, function(i, val){
	                	thead.push(val);
	                	var ndate = new Date(val * 1000);

	                	var dd = ndate.getDate();
						var mm = ndate.getMonth()+1; //January is 0!
						var yyyy = ndate.getFullYear();
	                	

	                	if(dd<10) {
						    dd = '0'+dd
						} 

						if(mm<10) {
						    mm = '0'+mm
						} 

						var today = yyyy + '-' + mm + '-' + dd;
	                	if (!isNaN(ndate)) {
	                		var tgl = pad(ndate.getDate());
	                		
	                		if (jQuery.inArray(today,data.holiday) > -1) {
			                    tableHeaders += "<th style=\"background-color:red;color:white;text-align:center\">" + pad(ndate.getDate()) + "</th>";
	                		}else{
		                		if (tgl == "01") {
				                    tableHeaders += "<th style=\"background-color:#00ff37;text-align:center\">" + pad(ndate.getDate()) + "</th>";
		                		}else{
				                    tableHeaders += "<th style=\"text-align:center\">" + tgl + "</th>";
		                		}
	                		}
	                	}else{
		                	var string = val + '';
		                	string = string.toUpperCase();
		                    tableHeaders += "<th style=\"text-align:center\">" + string + "</th>";
	                	}
	                });
	                
	                var html = '';
	                var i;
	                var shiftcode;
	                for(i=0; i<data.field.length; i++){

	                    html += '<tr>';
	                    for (var j = 0; j < data.field[i].length; j++) {
		                    shiftcode = data.field[i][j];

	                    	if (data.rgb[shiftcode] == undefined) {
	                    		// console.log(shiftcode)
	                    		if (shiftcode !== null) {

		                    		if (shiftcode.indexOf("AT_") >= 0) {
			                    		html += '<td align="center" style="background:red;color:#fff;" class="hris-cell"><i class="fa fa-clock-o"></i></td>';
			                    		// html += '<td>'+ data.field[i][j]+'</td>'
		                    		}else if (shiftcode.indexOf("AB_") > -1) {
			                    		html += '<td align="center" style="background:red;color:#fff;" class="hris-cell"><i class="fa fa-envelope"></i></td>';
			                    		// html += '<td>'+ data.field[i][j]+'</td>'
		                    		}else{
			                    		html += '<td>'+ shiftcode+'</td>';
		                    		}
	                    		}
	                    	}else{
			                    html += '<td style=\"background-color:'+ data.rgb[shiftcode] +';text-align:center\" class="hris-cell">'+data.field[i][j]+'</td>';
	                    	}
	                    }
	                    html += '</tr>';
	                }

	                $("#ros-table").empty();
	                $("#ros-table").append('<table id="'+ pages +'-table" class="table table-bordered table-stripped" cellspacing="0" width ="100%"><thead><tr>' + tableHeaders + '</tr></thead><tbody id="rosdat"></tbody></table>');
	                $('#rosdat').html(html);


	                rosterTable = $("#"+pages+"-table").DataTable({
						'destroy'      : true,
						'lengthChange' : false,
						'autoWidth'    : false,
						'ordering'     : false,
						scrollCollapse : true,
						scrollX        : true,
					});

				    $("#"+pages+"-table").dataTable().fnDraw();
				    $("#loadRos").modal('hide');

				    $('#unip').val('');
					$('#sdate').val('');
					$('#edate').val('');
				   	var data_arr = [];
				
				    $('#' + pages +'-table tbody').on('click', 'tr', function () {
				    	$("#unip").val('');
				    	$("#" + pages + "-table tbody tr").hasClass("selected");
				    	$("#" + pages + "-table tbody tr").removeClass("selected");

				        $(this).toggleClass('selected');
				        data_arr = rosterTable.row( this ).data();
				    	$("#unip").val(data_arr[0] + ' - ' + data_arr[1]);
				    	usernik = data_arr[0] + ' - ' + data_arr[1];
				    	unip = data_arr[0];
				    } );

				    $('#' + pages +'-table tbody').on('click', 'td', function () {
				    	var fromdate = $('#fromdate').val();
				    	var todate = $('#todate').val();
				    	//var rowIdx = rosterTable.cell( this ).index().columnVisible;
				    	var data = rosterTable.row( $(this).parents('tr') ).data();
				    	unip = data[0];

				        $("#body_translogroster").html('');

			            // $("#translogroster").DataTable().state.clear();
			            
				    	$.ajax({
			    			url: base_url + 'getlog',
			    			type: 'post',
			    			data: {
			    				usernip: unip,
			    				fromdate : fromdate,
			    				todate : todate
			    			},
			    			success: function (data) {
			    				// console.log(data);
			    				// console.log(typeof data);
                                                    
			    				var obj = JSON.parse(data);

			                	var html = '';
			                	
			                	if (obj.data.length > 0) {
					                $.each(obj.data, function(i, val){
					                	html += "<tr>";
					                	html += '<td>'+val.fid +'</td>';
					                	html += '<td>'+val.checktime +'</td>';
					                	html += '<td></td>';
					                	html += "</tr>";
					                });
					                $("#body_translogroster").html(html);

					                // $("#translogroster").dataTable().fnDraw();

			                	}else{
							        $("#body_translogroster").html('');
					                $("#body_translogroster").empty();
					                // $("#translogroster").DataTable().state.clear();
			                	}
				                
				                /*$("#translogroster").DataTable({
				                	'destroy'      : true,
									'lengthChange' : false,
									'autoWidth'    : false,
									'ordering'     : false,
									scrollCollapse : true,
									scrollX        : true,
				                })*/
			    			}
			    		});
				    	
				    })

				}else{
					swal('Ooops terjadi kesalahan, silahkan hubungi administrator');
				    $("#loadRos").modal('hide');
				}

			})
			.fail(function() {
			    $("#loadRos").modal('hide');
			})
	}

	
	function pad(n) {
		return n<10 ? '0'+n : n
	}
})
$(function () {
    var id = [];
    var selected = [];
	
    var tblDevice = $('#device-table').DataTable({
		'lengthChange': true,
        'autoWidth'   : false,
        scrollX:        true,
        scrollCollapse: true,
        "pageLength": 25,
        // fixedHeader: true,
        // columnDefs: [

        // 	{ "width": "180px", "targets": [0,1] },       
        // ],
        // 	{ "width": "180px", "targets": [2] },       
        // 	{ "width": "120px", "targets": [3,4,5] },       
        // 	{ "width": "100px", "targets": [6,7,8] },       
        // 	{ "width": "180px", "targets": [9] },       
        	
        // ],
        "drawCallback": function( settings ) {

        	$("#load-devinfo").css("display","none");
        	$("#load-devinfo").remove();
	        $('#devinfo-table').removeAttr("style");
	    },
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
	});

    $("#device-table").dataTable().fnDraw();


    var data = [];
    $('#device-table tbody').on('click', 'tr', function () {
        // var id = this.id;
        // var index = $.inArray(id, selected);
 
        // if ( index === -1 ) {
        //     selected.push( id );
        // } else {
        //     selected.splice( index, 1 );
        // }
        $("#device-table tbody tr").hasClass("selected");
        $("#device-table tbody tr").removeClass("selected");
        $(this).toggleClass('selected');

        data = tblDevice.row( this ).data();

        

    } );

    $("#device-table").dataTable().fnDraw();

    id = tblDevice.column(4).data();
    
    if (id && id.length) {
            var len = id.length,
                    host;
            for (var i = 0; i < id.length; i++) {
                    host = id[i];
                    states(host,i);
                    (function loop(host,key) {
                            setTimeout(function() {
                                    states(host,i);
                                    loop(host,i);
                                    // 1 detik = 1000
                            },60000);
                    })(host);
            }
    }

    $(document).on('click','#btn_downDevice',function (e) {
    e.preventDefault();
       if (data.length > 0) {
            swal({
                title: 'Apakah anda yakin?',
                text: "Ingin mendownload data dari mesin " + data[4] ,
                icon: 'warning',
                buttons: true,
                closeOnClickOutside : false,
                closeOnEsc : false,
                // allowEnterKey: false,
                }).then((willSend) => {

                if (willSend)
                {
                    $("#showModal").modal('show');
                    $.ajax({
                        url: base_url + 'kehadiran/device/download',
                        type: 'POST',
                        data: {host: data[4]},
                    })
                    .done(function(data) {
                        var obj = jQuery.parseJSON(data);
                        swal({
                            text : obj.reason,
                            icon : 'info'
                        });
                        $('#showModal').modal('hide');
                        console.log(obj.reason);
                    })
                    .fail(function() {
                        console.log("error");
                    })
                    .always(function() {
                        console.log("complete");
                    });
                    
                }
            });
        }else{
            swal("Silahkan Pilih Mesin Terlebih Dahulu");
        }
        // swal(data[4]);
    });

    $(document).on('click','#btn_upDevice',function (e) {
        e.preventDefault();
        // if (data.length > 0) {
            $("#modal-uploadLog").modal("show");
        // }else{
        //     swal("Silahkan Pilih Mesin Terlebih Dahulu");
        // }
    });

    $(document).on('click','#uploadLog',function (e) {
        e.preventDefault();
        $('#closeBtnLog').css('display','none');
        var form = $("#frmUploadLog");
        form.validate();

        if (form.valid()) {
            swal({
                title: "Apakah anda yakin?",
                text: "Ingin Upload data ?!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willSend) => {
                if (willSend) {
                  $("#uploadLog").html("");
                  $("#uploadLog").html("<i class=\"fa fa-spinner fa-spin\"></i>&nbsp;Loading");
                  $("#uploadLog").attr('disabled','disabled');
                  $.ajax({
                      url: base_url + 'kehadiran/device/upload',
                      method: 'POST',
                      data: new FormData($('#frmUploadLog')[0]),
                      cache:false,
                      processData:false,
                      contentType:false,
                  })
                  .done(function(data) {
                        
                        obj = JSON.parse(data);

                        if (obj.results == "succes") {
                          $("#uploadLog").html('Upload');
                          $("#uploadLog").removeAttr('disabled');
                          $('#closeBtnLog').css('display','');

                          swal(obj.reason, {
                            icon: "success",
                          }).then((value) => {
                            location.reload(true);
                          });
                        }else{
                          $("#uploadLog").html('Upload');
                          $("#uploadLog").removeAttr('disabled');
                          $('#closeBtnLog').css('display','');
                          swal(obj.reason, {
                            icon: "error",
                          })
                        }
                  })
                  .fail(function() {
                    $("#uploadLog").html('Upload');
                    $("#uploadLog").removeAttr('disabled');
                    $('#closeBtnLog').css('display','');
                    swal("Ooops file tidak bisa di upload");
                  })
                  .always(function() {
                      console.log("complete");
                  });
                  
                }else {
                  swal("Form tidak di kirim!");
                  $("#uploadModal").modal("hide");
                  $("#upload").html("").html("Upload");
                  $("#upload").removeAttr("disabled");
                }
            })
        }
    })
});


function states(host,key) {
        var ihost = host.replace(".","");
                        ihost = ihost.replace(".","");
                        ihost = ihost.replace(".","");

        $.ajax({
                url: base_url + 'settings/device/state',
                type: 'GET',
                data: {host: host},
                beforeSend : function () {

                        html = '<span class="label label-info">Checking</span><img src='+ base_url + 'assets/img/ajax-loader.gif alt="checking">';
                        $("#kehadiran-" + ihost).html("");
                        $("#kehadiran-" + ihost).html(html);
                }
        })
        .done(function(data) {
                var res = JSON.parse(data);

                var html = '';
                
                if (res.result == 'success') {
                        $("#last-upload-"+ihost).html("");
                        $("#last-upload-"+ihost).html(res.lastupload);
                        $("#last-activity-"+ihost).html(res.activity);
                        html = '<span class="label label-success">Online</span>';
                        $("#kehadiran-" + ihost).html("");
                        $("#kehadiran-" + ihost).html(html);
                }else{
                        html = '<span class="label label-danger">Offline</span>';
                        $("#kehadiran-" + ihost).html("");
                        $("#kehadiran-" + ihost).html(html);
                }
        })
        .fail(function() {
                // console.log("error");
        })
        .always(function() {
                // console.log("complete");
        });
}
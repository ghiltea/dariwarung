$(document).ready(function () {
    var arr, rosterTable;
    var id = [];
    var table = "lembr";
    var a, d1, d2;
    var tableLembur = '';
    /*Initialize DataTable*/
    tableLembur = $("#" + table + "-table").DataTable({
        'searching': false,
        'ordering': false,
        'filtering': false,
        'autoWidth': false,
        'lengthChange': false,
        scrollX: true,
        scrollCollapse: true,
        "columnDefs": [
            {
                "targets": [6],
                "visible": false,
                "searchable": false
            }
        ],
        "drawCallback": function (settings) {

            $("#load-lembur-1").css("display", "none");
            $("#load-lembur-1").remove();
            $('#lmbr-table').removeAttr("style");
        }

    });
    $("#" + table + "-table").dataTable().fnDraw();
    $.ajax({
        async: false,
        url: base_url + 'dropros',
    })
            .done(function (data) {
                arr = data;
                arr = JSON.parse(data);
            })
            .fail(function () {
                // console.log("error");
            })
            .always(function () {
                // console.log("complete");
            });
    var comboTree2;
    comboTree2 = $('#inputCmbLembr').comboTree({
        source: arr,
        isMultiple: false,
        page: table

    });
    /*Fill DataTable*/
    $('.comboTreeItemTitle-' + table).on('click', function () {

    })

    /*btnCLick*/
    $("#vLembur").on('click', function () {

        d1 = $('#fromdate').val();
        d2 = $('#todate').val();
        a = comboTree2.getSelectedItemsId();
        if (d1 > d2) {
            swal('Ooops rentang tanggal salah');
        } else if (d1 == '' && d2 == '') {
            swal('Silahkan Pilih Tanggal Awal & Akhir');
        } else if (d1 == '') {
            swal('Silahkan Pilih Tanggal Awal');
        } else if (d2 == '') {
            swal('Silahkan Pilih Tanggal Akhir');
        } else if (d1 && d2 !== '' && a !== false) {
            $("#loadLembr").modal('show');
            load_data();
        } else {
            swal('Ooops Sepertinya ada yang salah, silahkan hubungi administrator');
        }
    });

    $("#vLApprove").on('click', function () {

        $("input:checked", tableLembur.rows().nodes().to$()).each(function () {
            id.push($(this).val());
        });
        if (id.length == 0) {
            swal('Silahkan pilih pegawai terlebih dahulu');
        } else {
            swal({
                title: "Apakah anda yakin?",
                text: id.length + " lembur akan di Approve !",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willSend) => {
                        if (willSend) {
                            $.ajax({
                                method: 'post',
                                url: base_url + 'kehadiran/lembur/verifikasi/',
                                dataType: "json",
                                data: {'uid': id},
                                success: function (data) {
                                    // body...
                                    if (data['results'] == "success") {
                                        swal("Pegawai berhasil di Approve!", {
                                            icon: "success",
                                        })
                                                .then((value) => {
                                                    location.reload(true);
                                                });
                                    } else {
                                        swal("Pegawai gagal di Approve!", {
                                            icon: "error",
                                        })
                                        id.length = 0;
                                    }
                                },
                                error: function () {
                                    swal("Ooops tidak bisa tersambung dengan server");
                                    id.length = 0;
                                }
                            });
                        } else {
                            // swal("Pegawai Gagak di verifikasi!");
                            id.length = 0;
                        }
                    })
        }

    });



    $("#vLReject").on('click', function () {

        $("input:checked", tableLembur.rows().nodes().to$()).each(function () {
            id.push($(this).val());
        });
        if (id.length == 0) {
            swal('Silahkan pilih pegawai terlebih dahulu');
        } else {
            swal({
                title: "Apakah anda yakin?",
                text: id.length + " lembur akan di Reject !",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willSend) => {
                        if (willSend) {
                            $.ajax({
                                method: 'post',
                                url: base_url + 'kehadiran/lembur/reject/',
                                dataType: "json",
                                data: {'uid': id},
                                success: function (data) {
                                    // body...
                                    if (data['results'] == "success") {
                                        swal("Lembur Pegawai berhasil di reject!", {
                                            icon: "success",
                                        })
                                                .then((value) => {
                                                    location.reload(true);
                                                });
                                    } else {
                                        swal("Lembur Pegawai gagal di reject!", {
                                            icon: "error",
                                        })
                                        id.length = 0;
                                    }
                                },
                                error: function () {
                                    swal("Ooops tidak bisa tersambung dengan server");
                                    id.length = 0;
                                }
                            });
                        } else {
                            // swal("Pegawai Gagak di verifikasi!");
                            id.length = 0;
                        }
                    })
        }

    });
    $("#searchLmbr").keyup(function () {
        var id = comboTree2.getSelectedItemsId();
        if (!id || id == '') {
            swal('Silahkan Pilih Area');
        } else {
            load_data(this.value);
        }

    })


    function load_data(query) {
        $.ajax({
            url: base_url + '/kehadiran/lembur/u',
            type: 'POST',
            data: {
                a: a,
                fromdate: d1,
                todate: d2,
                q: query,
            },
            beforeSend: function () {
                // Handle the beforeSend event
            },
            success: function (data) {

                var tableHeaders = '';
                var dataSet = JSON.parse(data);
                /*Start checking result*/
                if (dataSet.result != 'failed') {
                    $.each(dataSet.columns, function (i, val) {
                        var string = val + '';
                        if (i == 0) {
                            tableHeaders += "<th width=\"21px\">" + string + "</th>";
                        } else {
                            tableHeaders += "<th>" + string + "</th>";
                        }
                    })

                    var html = '';
                    for (var i = 0; i < dataSet.field.length; i++) {

                        html += '<tr>';
                        for (var j = 0; j < dataSet.field[i].length; j++) {
                            // console.log(dataSet.field[i][j]); return false;
                            if (j == 0) {
                                html += '<td > <input type="checkbox" name="idVerifikasiLembur[]" value="' + dataSet.field[i][j] + '"></td>';
                            } else {
                                html += '<td >' + dataSet.field[i][j] + '</td>';
                            }
                        }
                        html += '</tr>';
                    }

                    $("#lmbr-table").empty();
                    $("#lmbr-table").append('<table id="' + table + '-table" class="table table-bordered table-stripped" cellspacing="0" width ="100%"><thead><tr>' + tableHeaders + '</tr></thead><tbody id="lmbrdat"></tbody></table>');
                    $("#lmbrdat").html(html);
                    tableLembur = $("#" + table + "-table").DataTable({
                        'destroy': true,
                        'lengthChange': false,
                        'searching': false,
                        'ordering': false,
                        'filtering': false,
                        'autoWidth': false,
                        'lengthChange': false,
                        scrollX: true,
                        scrollCollapse: true,
                        "columnDefs": [
                            {
                                "targets": [6],
                                "visible": false,
                                "searchable": false
                            }
                        ],
                        "drawCallback": function (settings) {

                            // $("#load-resign").css("display","none");
                            $("#loadLembr").modal('hide');
                        }

                    });
                    $("#" + table + "-table").dataTable().fnDraw();
                    $('#lembur-select-all').on('click', function () {
                        var rows = tableLembur.rows({'search': 'applied'}).nodes();
                        $('input[type="checkbox"]', rows).prop('checked', this.checked);
                    });
                }
                /*End checking result*/

            }
        });
    }
});
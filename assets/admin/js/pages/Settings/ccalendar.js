$(function () {
	'use strict'

	var d1,d2;
	
	var ccalendarTable = $("#ccalendar-table").DataTable({
		ordering : false,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 5,
        lengthChange : false,
        // 'columnDef'
        // responsive:true,
        "autoWidth": false,
		
	})

	$("#ccalendar-table").dataTable().fnDraw();


	// Button Add
	$(document).on('click','#btn_newCcalendar',function (e) {
		location.replace(base_url + "settings/company-calendar/add");
	})


	/*Begin Add Calendar*/
	var frmAddCalendar = $("#frmAddCalendar");

	frmAddCalendar.validate();

	frmAddCalendar.submit(function (e) {
    	e.preventDefault();

		d1 = $('#sdate').val();
		d2 = $('#edate').val();

		if (d1 > d2) {
			swal('Ooops rentang tanggal salah');
			return false;
		}

		if (frmAddCalendar.valid()) {
			swal({
		        title: "Apakah anda yakin?",
		        text: "Pastikan tidak ada data yang keliru!",
		        icon: "warning",
		        buttons: true,
		        dangerMode: true,
	      	})
	      	.then((willSend) => {
	        	if (willSend) {
		           	$.ajax({
		              method :'post',
		              url  : base_url + 'settings/company-calendar/store',
		              data : new FormData($('#frmAddCalendar')[0]),
		              cache:false,
		              processData:false,
		              contentType:false,              
		              success : function (data) {
		                // body...
		                var obj = jQuery.parseJSON(data);
		                if (obj.results == "success") {
		                    swal(obj.reason, {
		                    	icon: "success",
		                  	})
		                    .then((value) => {
				              location.reload(true);
				            });
		                }else{
		                  swal(obj.reason, {
		                    icon: "error",
		                  })
		                }
		              },
		              error : function(){
		                 swal("Ooops tidak bisa tersambung dengan server");
		              }

		            });
	        	} else {
		          swal("Form tidak di kirim!");
		        }
	      	})	
    	}
    })
    /*End Add Calendar*/

    /*Begin Edit Calendar*/
	var frmEditCalendar = $('#frmEditCalendar');

	frmEditCalendar.validate();

	frmEditCalendar.submit(function (e) {
		e.preventDefault();

		d1 = $('#sdate').val();
		d2 = $('#edate').val();

		if (d1 > d2) {
			swal('Ooops rentang tanggal salah');
			return false;
		}

		if (frmEditCalendar.valid()) {
			swal({
		        title: "Apakah anda yakin ingin mengubah data?",
		        text: "Pastikan tidak ada data yang keliru!",
		        icon: "warning",
		        buttons: true,
		        dangerMode: true,
	      	})
	      	.then((willSend) => {
	        	if (willSend) {
		           	$.ajax({
		              method :'post',
		              url  : base_url + 'settings/company-calendar/update',
		              data : new FormData($('#frmEditCalendar')[0]),
		              cache:false,
		              processData:false,
		              contentType:false,              
		              success : function (data) {
		                // body...
		                var obj = jQuery.parseJSON(data);
		                if (obj.results == "success") {
		                    swal(obj.reason, {
		                    	icon: "success",
		                  	})
		                    .then((value) => {
				              location.reload(true);
				            });
		                }else{
		                  swal(obj.reason, {
		                    icon: "error",
		                  })
		                }
		              },
		              error : function(){
		                 swal("Ooops tidak bisa tersambung dengan server");
		              }

		            });
	        	} else {
		          swal("Form tidak di kirim!");
		        }
	      	})	
		}
	})

    /*End Edit Calendar*/

    /*Begin Delete Calendar*/
	  $(document).on('click','.delcal',function (e) {
	  	// alert('s')
	      e.preventDefault();
	      var id = $(this).attr('name')

	      swal({
	        title: "Apakah anda yakin?",
	        text: "Ingin menghapus calendar ini ?!",
	        icon: "warning",
	        buttons: true,
	        dangerMode: true,
	      })
	      .then((willSend) => {
	        if (willSend) {
	           return fetch(base_url + `settings/company-calendar/delete?id=${id}`);
	        } else {
	          throw null;
	          swal("Oops Data gagal di hapus!");
	        }
	      })
	      .then(results => {
	          return results.json();
	        })
	        .then(json => {
	          var  result = json.results;
	         
	          if (result !== 'success') {
	            return swal("Data gagal di hapus!");
	          }

	          swal({
	            title: "information",
	            text: 'Data berhasil di hapus',
	            icon:'success',
	          }).then((value) => {
	            location.reload(true);
	          });

	        })
	        .catch(err => {
	          if (err) {
	            swal("Ooops!", "Koneksi Bermasalah!", "error");
	          } else {
	            swal.stopLoading();
	            swal.close();
	          }
	        });
	    })
	/*End Delete Calendar*/
})
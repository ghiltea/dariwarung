$(function () {
	'use strict'

	var d1,d2;
	var table = "user";

	$('#roles_user').select2({placeholder: 'Please Select Roles User',allowClear:true});


	var userTable = $("#"+table+"-table").DataTable({
		ordering : false,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 10,
        lengthChange : false,
        // 'columnDef'
        // responsive:true,
        "autoWidth": false,
                     'searching': true,
        'ordering': false,
        'filtering': true,
        lengthChange: true,
		
	})

	$("#"+table+"-table").dataTable().fnDraw();


	// Button Add
	$(document).on('click','#btnAddUser',function (e) {
		location.replace(base_url + "settings/user/add");
	})
	//End Button Add


	//  Submit New User
	$(document).on('click','#subAddUser',function (e) {
		e.preventDefault();

		// Declare Form for validation
		var form = $("#frmAddUser");

		//Validation Form
	    form.validate();

	    //Cek form valid or not
	    if (form.valid()) {
	    	// alert('valid');
	    	swal({
		        title: "Apakah anda yakin?",
		        text: "Pastikan tidak ada data yang keliru!",
		        icon: "warning",
		        buttons: true,
		        dangerMode: true,
		      })
	    	.then((willSend) => {
	    		//Cek result dialog
	    		if (willSend) {
					$.ajax({
		              method :'post',
		              url  : base_url + 'settings/user/store',
		              data : new FormData($('#frmAddUser')[0]),
		              cache:false,
		              processData:false,
		              contentType:false,              
		              success : function (data) {
		                // body...
		                var obj = jQuery.parseJSON(data);
		                if (obj.results == "success") {
		                  swal("Form berhasil di kirim!", {
		                    icon: "success",
		                  }).then((value) => {
		                    location.reload(true);
		                  });
		                }else{
		                  swal(obj.reason, {
		                    icon: "error",
		                  })
		                }
		              },
		              error : function(){
		                swal("Ooops file tidak bisa di upload");
		              }
		            });
	    		}else{
			        swal("Form tidak di kirim!");
	    		}
	    	})
	    }
	})
	//End Submit New User


	// Update User
	$(document).on('click','#subEditUser',function (e) {
		e.preventDefault();

		// Declare Form for validation
		var form = $("#frmEditUser");

		//Validation Form
	    form.validate();
		
		if (form.valid()) {
			swal({
		        title: "Apakah anda yakin?",
		        text: "Pastikan tidak ada data yang keliru!",
		        icon: "warning",
		        buttons: true,
		        dangerMode: true,
		      })
	    	.then((willSend) => {
	    		//Cek result dialog
	    		if (willSend) {
					$.ajax({
		              method :'post',
		              url  : base_url + 'settings/user/update',
		              data : new FormData($('#frmEditUser')[0]),
		              cache:false,
		              processData:false,
		              contentType:false,              
		              success : function (data) {
		                // body...
		                var obj = jQuery.parseJSON(data);
		                if (obj.results == "success") {
		                  swal(obj.reason, {
		                    icon: "success",
		                  }).then((value) => {
		                    location.reload(true);
		                  });
		                }else{
		                  swal(obj.reason, {
		                    icon: "error",
		                  })
		                }
		              },
		              error : function(){
		                swal("Ooops file tidak bisa di upload");
		              }
		            });
	    		}else{
			        swal("Form tidak di kirim!");
	    		}
	    	})
		}
	})
	//End Update User
	

	  /*Begin Delete User*/
      $(document).on('click','.del-set-user', function (e) {
        e.preventDefault();

        var id = $(this).attr('name')

        swal({
          title: "Apakah anda yakin?",
          text: "Ingin menghapus data ini ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSend) => {
          if (willSend) {
             return fetch(base_url + `settings/user/delete/${id}`);
          } else {
            throw null;
            swal("Oops Data gagal di hapus!");
          }
        })
        .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di hapus!");
            }

            swal({
              title: "information",
              text: 'Data berhasil di hapus',
              icon:'info',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
      })
    /*End Delete User*/
})
$(function () {
	'use strict'

	

	var d1,d2;

	/**
	 *
	 * Data Table
	 * Inisialisasi table role
	 */
	
	var table = "role";
	var roleTable = $("#"+table+"-table").DataTable({
		ordering : false,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 10,
        lengthChange : false,
        // 'columnDef'
        // responsive:true,
        "autoWidth": false,
              'searching': true,
        'ordering': false,
        'filtering': true,
        lengthChange: true,
		
	})

	$("#"+table+"-table").dataTable().fnDraw();

	// Button Add
	$(document).on('click','#btnAddRole',function (e) {
		location.replace(base_url + "settings/role/add");
	})
	//End Button Add
	
	/**
	 *
	 * Bonsai Tree
	 * Inisialisai Treecheckbox
	 */
	
	// Initialize tree


	$('#akses-menu').bonsai({
	  expandAll: false,
	  expand:null,
	  checkboxes: true,
	  createInputs: 'checkbox',
	  idAttribute: 'id',
	});	

	/*=============Edit================*/

	
	$('#akses-menu-edit').bonsai({
	  expandAll: false,
	  checkboxes: true,
	  createInputs: 'checkbox',
	  idAttribute: 'id',
	});	


	/*Begin Form role Add*/
	var formRole = $("#formRole");
	
	formRole.submit(function (e) {
		e.preventDefault();
		var arrMenu = [];
		/*Get Value from checkbox menu*/
		$("input:checkbox[name=menu]:checked").each(function(){
	        arrMenu.push($(this).val());
	    });



		
		var roleName = $("#rolename").val();

		if (arrMenu.length == 0) {
			swal("Akses Menu tidak boleh kosong");
			return false;
		}

		
		swal({
	        title: "Apakah anda yakin?",
	        text: "Pastikan tidak ada data yang keliru!",
	        icon: "warning",
	        buttons: true,
	        dangerMode: true,
      	})
      	.then((willSend) => {
        	if (willSend) {
        		var data = $(this).serializeArray();
        		for (var i = 0; i < arrMenu.length; i++) {
			        data.push({
			            name: "menu[]", // These blank empty brackets are imp!
			            value: arrMenu[i]
			        });
			    }
        	

	           	$.ajax({
	              method : 'post',
	              url  : base_url + 'settings/role/store',
	              data : data,
	              cache:false,
	              dataType: 'json',
	              // processData:false,
	              // contentType:false,              
	              success : function (data) {
	                // body...
	                var obj = data;
	                
	                if (obj.results == "success") {
	                    swal(obj.reason, {
	                    	icon: "success",
	                  	})
	                    .then((value) => {
			              location.reload(true);
			            });
	                }else{
	                  swal(obj.reason, {
	                    icon: "error",
	                  })
	                }
	              },
	              error : function(){
	                 swal("Ooops tidak bisa tersambung dengan server");
	              }

	            });
        	} else {
	          swal("Form tidak di kirim!");
	        }
      	})	
	})
	/*End Form role Add*/

	/*Begin Form role Edit*/
	var formRolEdit = $("#formRoleEdit");
	formRolEdit.submit(function (e) {
		e.preventDefault();
		var arrMenuE = [];
		

		/*Get Value from checkbox menu*/
		$("input:checkbox[name=menu-edit]:checked").each(function(){
	        arrMenuE.push($(this).val());
	    });

		/*Get Value from checkbox cabang*/
		
		var roleName = $("#e-rolename").val();

		if (arrMenuE.length == 0) {
			swal("Akses Menu tidak boleh kosong");
			return false;
		}

		
		swal({
	        title: "Apakah anda yakin?",
	        text: "Pastikan tidak ada data yang keliru!",
	        icon: "warning",
	        buttons: true,
	        dangerMode: true,
      	})
      	.then((willSend) => {
        	if (willSend) {
        		var data = $(this).serializeArray();
        		for (var i = 0; i < arrMenuE.length; i++) {
			        data.push({
			            name: "menu-edit[]", // These blank empty brackets are imp!
			            value: arrMenuE[i]
			        });
			    }
        	

	           	$.ajax({
	              method : 'post',
	              url  : base_url + 'settings/role/update',
	              data : data,
	              cache:false,
	              dataType: 'json',
	              // processData:false,
	              // contentType:false,              
	              success : function (data) {
	                // body...
	                var obj = data;
	                
	                if (obj.results == "success") {
	                    swal(obj.reason, {
	                    	icon: "success",
	                  	})
	                    .then((value) => {
			              location.reload(true);
			            });
	                }else{
	                  swal(obj.reason, {
	                    icon: "error",
	                  })
	                }
	              },
	              error : function(){
	                 swal("Ooops tidak bisa tersambung dengan server");
	              }

	            });
        	} else {
	          swal("Form tidak di kirim!");
	        }
      	})	
	})

	/*End Form role Edit*/

	/*Delete Role*/
	$(document).on('click','.del-set-role',function (e) {
		e.preventDefault();

		var id = $(this).attr('name')

        swal({
          title: "Apakah anda yakin?",
          text: "Ingin menghapus data ini ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSend) => {
          if (willSend) {
             return fetch(base_url + `settings/role/delete?id=${id}`);
          } else {
            throw null;
            
            swal("Oops Data gagal di hapus!");
          }
        })
        .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di hapus!");
            }

            swal({
              title: "Success",
              text: 'Data berhasil di hapus',
              icon:'success',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
   
	})
	/*Delete Role*/
})
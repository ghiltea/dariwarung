$(function () {
	'use strict'

	/*Initialize DataTable*/

	var attTable = $("#att-table").DataTable({
		'lengthChange': false,
		'autoWidth'   : false,
		scrollX :        true,
        scrollCollapse: true,
        ordering : false,
        "pageLength": 5,	

	});


/*Begin Add Status*/
	var frmAddAtt = $("#frmAddAtt");

	frmAddAtt.validate();

	$(document).on("click","#subAddAtt",function (e) {
		e.preventDefault();
		// Check Valid Form
		if (frmAddAtt.valid()) {
			swal({
		          title: "Apakah anda yakin?",
		          text: "Ingin Menambah Data ?!",
		          icon: "warning",
		          buttons: true,
		          dangerMode: true,
		    })
		    .then((willSend) => {
		          if (willSend) {
		            $.ajax({
		                url: base_url + 'settings/attendance/store',
		                method: 'POST',
		                data: new FormData($('#frmAddAtt')[0]),
		                cache:false,
		                processData:false,
		                contentType:false,
		            })
		            .done(function(data) {
		                  
		                  var obj = JSON.parse(data);
		                  
		                  if (obj.results == "success") {

		                    swal(obj.reason, {
		                      icon: "success",
		                    }).then((value) => {
		                      location.reload(true);
		                    });
		                  }else{
		                    swal(obj.reason, {
		                      icon: "error",
		                    })
		                  }

		            })
		            .fail(function() {
		              swal("Ooops terjadi masalah, coba hubungi administrator");
		            })
		            .always(function() {
		                console.log("complete");
		            });
		            
		          }else {
		            swal("Form tidak di kirim!");
		          }
		    })
		}
		
	})
/*End Add Status*/

/*Begin Edit Status*/
	var frmEditAtt = $("#frmEditAtt");

	frmEditAtt.validate();

	$(document).on("click","#subEditAtt",function (e) {
		e.preventDefault();
		// Check Valid Form
		if (frmEditAtt.valid()) {
			swal({
		          title: "Apakah anda yakin?",
		          text: "Ingin Mengubah Data ?!",
		          icon: "warning",
		          buttons: true,
		          dangerMode: true,
		    })
		    .then((willSend) => {
		          if (willSend) {
		            $.ajax({
		                url: base_url + 'settings/attendance/update',
		                method: 'POST',
		                data: new FormData($('#frmEditAtt')[0]),
		                cache:false,
		                processData:false,
		                contentType:false,
		            })
		            .done(function(data) {
		                  
		                  var obj = JSON.parse(data);
		                  
		                  if (obj.results == "success") {

		                    swal(obj.reason, {
		                      icon: "success",
		                    }).then((value) => {
		                      location.reload(true);
		                    });
		                  }else{
		                    swal(obj.reason, {
		                      icon: "error",
		                    })
		                  }

		            })
		            .fail(function() {
		              swal("Ooops terjadi masalah, coba hubungi administrator");
		            })
		            .always(function() {
		                console.log("complete");
		            });
		            
		          }else {
		            swal("Form tidak di kirim!");
		          }
		    })
		}
		
	})
/*End Edit Status*/

/*Begin Delete Device*/
    $(document).on('click','.attdel',function (e) {
      // alert('s')
        e.preventDefault();
        var id = $(this).attr('name')

        swal({
          title: "Apakah anda yakin?",
          text: "Ingin menghapus status ini ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSend) => {
          if (willSend) {
             return fetch(base_url + `settings/attendance/delete?id=${id}`);
          } else {
            throw null;
            swal("Oops Status gagal di hapus!");
          }
        })
        .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Status gagal di hapus!");
            }

            swal({
              title: "information",
              text: 'Shift berhasil di hapus',
              icon:'success',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
      })
  /*End Delete Device*/

})
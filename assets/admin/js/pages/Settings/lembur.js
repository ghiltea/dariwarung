$(function () {
	'use strict'

	

	var d1,d2;

	/**
	 *
	 * Data Table
	 * Inisialisasi table role
	 */
	
	var table = "lembur";
	var lemburTable = $("#"+table+"-table").DataTable({
		ordering : false,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 5,
        lengthChange : false,
        // 'columnDef'
        // responsive:true,
        "autoWidth": false,
		
	})

	$("#"+table+"-table").dataTable().fnDraw();

	// Button Add
	$(document).on('click','#btnAddLembur',function (e) {
		location.replace(base_url + "settings/lembur/add");
	})
	//End Button Add
	
	/*Begin Form Lembur Add*/
	var formLembur = $("#formLembur");
	
	formLembur.submit(function (e) {
		e.preventDefault();
		var arrLembur = [];
		
		// Get Value from checkbox menu
		$("input:checkbox[name=levelot]:checked").each(function(){
	        arrLembur.push($(this).val());
	    });

	    if (arrLembur.length == 0) {
			swal("Silahkan Pilih level yang berhak dapat lembur");
			return false;	
		}

		swal({
	        title: "Apakah anda yakin?",
	        text: "Pastikan tidak ada data yang keliru!",
	        icon: "warning",
	        buttons: true,
	        dangerMode: true,
      	})
      	.then((willSend) => {
        	if (willSend) {
        		var data = [];
        		for (var i = 0; i < arrLembur.length; i++) {
			        data.push({
			            name: "levelot[]", // These blank empty brackets are imp!
			            value: arrLembur[i]
			        });
			    }
        		

	           	$.ajax({
	              method : 'post',
	              url  : base_url + 'settings/lembur/store',
	              data : data,
	              cache:false,
	              dataType: 'json',
	              // processData:false,
	              // contentType:false,              
	              success : function (data) {
	                // body...
	                var obj = data;
	                
	                if (obj.results == "success") {
	                    swal(obj.reason, {
	                    	icon: "success",
	                  	})
	                    .then((value) => {
			              location.replace(base_url + "settings/lembur");
			            });
	                }else{
	                  swal(obj.reason, {
	                    icon: "error",
	                  })
	                }
	              },
	              error : function(){
	                 swal("Ooops tidak bisa tersambung dengan server");
	              }

	            });
        	} else {
	          swal("Form tidak di kirim!");
	        }
      	})
	})
	/*End Form Lembur Add*/

	/*Begin Form Lembur Edit*/
	var formLemburEdit = $("#formLemburEdit");
	
	formLemburEdit.submit(function (e) {
		e.preventDefault();
		var arrLemburE = [];
		
		// Get Value from checkbox menu
		$("input:checkbox[name=e-levelot]:checked").each(function(){
	        arrLemburE.push($(this).val());
	    });

		// console.log(arrLemburE); return false;
	    if (arrLemburE.length == 0) {
			swal("Silahkan Pilih level yang berhak dapat lembur");
			return false;	
		}

		swal({
	        title: "Apakah anda yakin?",
	        text: "Pastikan tidak ada data yang keliru!",
	        icon: "warning",
	        buttons: true,
	        dangerMode: true,
      	})
      	.then((willSend) => {
        	if (willSend) {
        		var data = [];
        		for (var i = 0; i < arrLemburE.length; i++) {
			        data.push({
			            name: "elevelot[]", // These blank empty brackets are imp!
			            value: arrLemburE[i]
			        });
			    }
        		

	           	$.ajax({
	              method : 'post',
	              url  : base_url + 'settings/lembur/update',
	              data : data,
	              cache:false,
	              dataType: 'json',
	              // processData:false,
	              // contentType:false,              
	              success : function (data) {
	                // body...
	                var obj = data;
	                
	                if (obj.results == "success") {
	                    swal(obj.reason, {
	                    	icon: "success",
	                  	})
	                    .then((value) => {
			              location.replace(base_url + "settings/lembur");
			            });
	                }else{
	                  swal(obj.reason, {
	                    icon: "error",
	                  })
	                }
	              },
	              error : function(){
	                 swal("Ooops tidak bisa tersambung dengan server");
	              }

	            });
        	} else {
	          swal("Form tidak di kirim!");
	        }
      	})
	})
	/*End Form Lembur Edit*/


	/*Delete Role*/
	$(document).on('click','.del-set-lembur',function (e) {
		e.preventDefault();

		var id = $(this).attr('name')

        swal({
          title: "Apakah anda yakin?",
          text: "Ingin menghapus data ini ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSend) => {
          if (willSend) {
             return fetch(base_url + `settings/lembur/delete?id=${id}`);
          } else {
            throw null;
            
            swal("Oops Data gagal di hapus!");
          }
        })
        .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di hapus!");
            }

            swal({
              title: "Success",
              text: 'Data berhasil di hapus',
              icon:'success',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
   
	})
	/*Delete Role*/
})
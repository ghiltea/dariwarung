$(function () {
  'use strict'

  var shiftTable = $('#shift-table').DataTable({
  	'ordering' : false,
  	'filtering' : false,
  	'autoWidth' : false,
  	'lengthChange' : false,
    scrollX:        true,
        scrollCollapse: true,
  });

  /*Begin Add Shift*/
  var frmAddShift = $("#frmAddShift");
  frmAddShift.validate({
    rules: {
      late_tolerance : {
        number : true
      },
      early_tolerance : {
        number : true
      }
    }
  });

  $("#subAddShift").on('click',function(e) {
    e.preventDefault();
    
    if (frmAddShift.valid()) {
      swal({
          title: "Apakah anda yakin?",
          text: "Ingin Menambah Data ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((willSend) => {
          if (willSend) {
            $.ajax({
                url: base_url + 'settings/shift/store',
                method: 'POST',
                data: new FormData($('#frmAddShift')[0]),
                cache:false,
                processData:false,
                contentType:false,
            })
            .done(function(data) {
                  
                  var obj = JSON.parse(data);
                  
                  if (obj.results == "succes") {

                    swal(obj.reason, {
                      icon: "success",
                    }).then((value) => {
                      location.reload(true);
                    });
                  }else{
                    swal(obj.reason, {
                      icon: "error",
                    })
                  }

            })
            .fail(function() {
              swal("Ooops terjadi masalah, coba hubungi administrator");
            })
            .always(function() {
                console.log("complete");
            });
            
          }else {
            swal("Form tidak di kirim!");
          }
      })
    }

  })
  /*End Add Shit*/

  /*Begin Edit Shift*/
   var frmEditShift = $("#frmEditShift");
    frmEditShift.validate({
    rules: {
      late_tolerance : {
        number : true
      },
      early_tolerance : {
        number : true
      }
    }
  });

  $("#subEditShift").on('click',function(e) {
    e.preventDefault();
    if (frmEditShift.valid()) {
      swal({
          title: "Apakah anda yakin?",
          text: "Ingin Mengubah Data ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((willSend) => {
          if (willSend) {
            $.ajax({
                url: base_url + 'settings/shift/update',
                method: 'POST',
                data: new FormData($('#frmEditShift')[0]),
                cache:false,
                processData:false,
                contentType:false,
            })
            .done(function(data) {
                  
                  var obj = JSON.parse(data);
                  
                  if (obj.results == "succes") {

                    swal(obj.reason, {
                      icon: "success",
                    }).then((value) => {
                      location.reload(true);
                    });
                  }else{
                    swal(obj.reason, {
                      icon: "error",
                    })
                  }

            })
            .fail(function() {
              swal("Ooops terjadi masalah, coba hubungi administrator");
            })
            .always(function() {
                console.log("complete");
            });
            
          }else {
            swal("Form tidak di kirim!");
          }
      })
    }

  })
  /*End Edit Shift*/

  /*Begin Delete Device*/
    $(document).on('click','.shiftdel',function (e) {
      // alert('s')
        e.preventDefault();
        var id = $(this).attr('name')

        swal({
          title: "Apakah anda yakin?",
          text: "Ingin menghapus shift ini ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSend) => {
          if (willSend) {
             return fetch(base_url + `settings/shift/delete?id=${id}`);
          } else {
            throw null;
            swal("Oops Shift gagal di hapus!");
          }
        })
        .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Shift gagal di hapus!");
            }

            swal({
              title: "information",
              text: 'Shift berhasil di hapus',
              icon:'success',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
      })
  /*End Delete Device*/
})
$(function () {
	'use strict'

	

	var d1,d2;

	/**
	 *
	 * Data Table
	 * Inisialisasi table item
	 */
	
	var table = "item";
	var itemTable = $("#"+table+"-table").DataTable({
		ordering : false,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 5,
        lengthChange : false,
        // 'columnDef'
        // responsive:true,
        "autoWidth": false,
		
	})

	$("#"+table+"-table").dataTable().fnDraw();

	// Button Add
	$(document).on('click','#btnAddItem',function (e) {
		location.replace(base_url + "settings/item/add");
	})
	//End Button Add
	

	/*Add Item*/
	var frmAddItem = $("#frmAddItem");


	var validator = frmAddItem.validate({
	  	rules: {
		    itemname: {
		      required: true,
		      remote: base_url + "exitem"
		    },
		},
		messages: {
		    itemname   : {remote: "Item "+ $(this).val() +"sudah di gunakan!"},
		}
	});

	$(document).on('click','#subAdItem', function (e) {
		e.preventDefault();
        
		if (frmAddItem.valid()) {
			swal({
		          title: "Apakah anda yakin?",
		          text: "Ingin Menambah Data ?!",
		          icon: "warning",
		          buttons: true,
		          dangerMode: true,
		    })
		    .then((willSend) => {
		          if (willSend) {
		            $.ajax({
		                url: base_url + 'settings/item/store',
		                method: 'POST',
		                data: new FormData($('#frmAddItem')[0]),
		                cache:false,
		                processData:false,
		                contentType:false,
		            })
		            .done(function(data) {
		                  
		                  var obj = JSON.parse(data);
		                  
		                  if (obj.results == "success") {

		                    swal(obj.reason, {
		                      icon: "success",
		                    }).then((value) => {
		                      location.reload(true);
		                    });
		                  }else{
		                    swal(obj.reason, {
		                      icon: "error",
		                    })
		                  }

		            })
		            .fail(function() {
		              swal("Ooops terjadi masalah, coba hubungi administrator");
		            })
		            /*.always(function() {
		            });*/
		            
		          }else {
		            swal("Form tidak di kirim!");
		          }
		    })
		}
	});
	/*Add item*/

	/*Edit item*/
	var frmEdititem = $("#frmEdititem");
	var validator = frmEdititem.validate({
	  	rules: {
		    itemname: {
		      required: true,
		      remote: base_url + "exitem"
		    },
		},
		messages: {
		    itemname   : {remote: "Item "+ $(this).val() +"sudah di gunakan!"},
		}
	});
	$(document).on('click','#subEdititem', function (e) {
		e.preventDefault();

			if (frmEdititem.valid()) {
			swal({
		          title: "Apakah anda yakin?",
		          text: "Ingin Mengubah Data ?!",
		          icon: "warning",
		          buttons: true,
		          dangerMode: true,
		    })
		    .then((willSend) => {
		          if (willSend) {
		            $.ajax({
		                url: base_url + 'settings/item/update',
		                method: 'POST',
		                data: new FormData($('#frmEdititem')[0]),
		                cache:false,
		                processData:false,
		                contentType:false,
		            })
		            .done(function(data) {
		                  
		                  var obj = JSON.parse(data);
		                  
		                  if (obj.results == "success") {

		                    swal(obj.reason, {
		                      icon: "success",
		                    }).then((value) => {
		                      location.reload(true);
		                    });
		                  }else{
		                    swal(obj.reason, {
		                      icon: "error",
		                    })
		                  }

		            })
		            .fail(function() {
		              swal("Ooops terjadi masalah, coba hubungi administrator");
		            })
		            /*.always(function() {
		            });*/
		            
		          }else {
		            swal("Form tidak di kirim!");
		          }
		    })
		}
	});
	/*Edit item*/

	/*Delete item*/
	$(document).on('click','.del-set-item',function (e) {
		e.preventDefault();

		var id = $(this).attr('name')

        swal({
          title: "Apakah anda yakin?",
          text: "Ingin menghapus data ini ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSend) => {
          if (willSend) {
             return fetch(base_url + `settings/item/delete?id=${id}`);
          } else {
            throw null;
            
            swal("Oops Data gagal di hapus!");
          }
        })
        .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di hapus!");
            }

            swal({
              title: "Success",
              text: 'Data berhasil di hapus',
              icon:'success',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
   
	})
	/*Delete Role*/
})
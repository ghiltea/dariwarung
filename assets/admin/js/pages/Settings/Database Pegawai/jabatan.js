$(function () {
    'use strict'



    var parentid,
            jabatan = $("#jabatan"),
            level = $("#level"),
            grade = $("#grade");


    $('#parentid').change(function () {
        parentid = $(this).val();
    })
    /*Begin Form Add Departments*/
    var frmAddJabatan = $('#frmAddJabatan');


    frmAddJabatan.validate();
    frmAddJabatan.submit(function (e) {
        e.preventDefault();


        if (frmAddJabatan.valid()) {
            var deptid = $("#departments option:selected").val();
            var sid = $("#subdepartments option:selected").val();

            swal({
                text: "Apakah data yang di isi sudah sesuai ?",
                icon: "warning",
                buttons: ["Tidak", "Ya"],
                dangerMode: true,
            })
                    .then((willStore) => {
                        if (willStore) {
                            /*
                             did   = deptid
                             sid   = subdeptid
                             j     = jabatan
                             l     = level
                             g     = grade
                             */
                            return fetch(base_url + `settings/jabatan/store?did=${deptid}&sid=${sid}&j=${jabatan.val()}&l=${level.val()}&g=${grade.val()}`);
                        } else {
                            throw null;
                            swal("Data batal ditambah");
                        }
                    })
                    .then(results => {
                        return results.json();
                    })
                    .then(json => {
                        var result = json.results;

                        if (result !== 'success') {
                            return swal("Data gagal di tambah! atau data sudah tersedia");
                        }

                        swal({
                            title: "information",
                            text: 'Data berhasil di tambah',
                            icon: 'info',
                        }).then((value) => {
                            location.reload(true);
                        });

                    })
                    .catch(err => {
                        if (err) {
                            // console.log(err);
                            swal("Ooops!", err, "error");
                            // swal("Ooops!", "Koneksi Bermasalah!", "error");
                        } else {
                            swal.stopLoading();
                            swal.close();
                        }
                    });
        }

        // return false;

    });

    /*End Form Add Departments*/

    /*Begin Form Edit Departments*/
    var frmEditJabatan = $('#frmEditJabatan'),
            tempURL = window.location.href.split('/'),
            id = tempURL[7],
            jabatan = $("#jabatan"),
            level = $("#level"),
            grade = $("#grade"),
            deptid = $("#deptid").val(),
            subdeptid = $("#subdeptid").val();

    $(document).on('change', '#deptid', function () {
        deptid = $(this).val();
    });

    $(document).on('change', '#subdeptid', function () {
        subdeptid = $(this).val();
    })

    frmEditJabatan.validate();
    frmEditJabatan.submit(function (e) {

        if (frmEditJabatan.valid()) {
            swal({
                text: "Apakah data yang di isi sudah sesuai ?",
                icon: "warning",
                buttons: ["Tidak", "Ya"],
                dangerMode: true,
            })
                    .then((willStore) => {
                        if (willStore) {

                            /* i = id 
                             d = deptid
                             s = subdeptid
                             j = jabatan
                             l = level
                             g = grade*/
                            console.log(id);
                            console.log(deptid);
                            console.log(subdeptid);
                            console.log(jabatan.val());
                            console.log(level.val());
                            console.log(grade.val());
                            return fetch(base_url + `settings/jabatan/update?i=${id}&d=${deptid}&s=${subdeptid}&j=${jabatan.val()}&l=${level.val()}&g=${grade.val()}`);
                        } else {
                            throw null;
                            swal("Data batal di ubah");
                        }
                    })
                    .then(results => {
                        return results.json();
                    })
                    .then(json => {
                        var result = json.results;

                        if (result !== 'success') {
                            return swal("Data gagal di ubah!");
                        }

                        swal({
                            title: "information",
                            text: 'Data berhasil di ubah',
                            icon: 'info',
                        }).then((value) => {
                            location.reload(true);
                        });

                    })
                    .catch(err => {
                        if (err) {
                            console.log(err);
                            swal("Ooops!", "Koneksi Bermasalah!", "error");
                        } else {
                            swal.stopLoading();
                            swal.close();
                        }
                    });


        }

        return false;
    })

    /*End Form Edit Departments*/

    /*Begin Delete Departments*/
    $(document).on('click', '.del-jabatan', function (e) {
//        alert('test')
//        return;
        e.preventDefault();
        var id = $(this).attr('name')

        swal({
            title: "Apakah anda yakin?",
            text: "Ingin menghapus data ini ?!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willSend) => {
                    if (willSend) {
                        return fetch(base_url + `settings/jabatan/delete?id=${id}`);
                    } else {
                        throw null;
                        swal("Oops Data gagal di hapus!");
                    }
                })
                .then(results => {
                    return results.json();
                })
                .then(json => {
                    var result = json.results;

                    if (result !== 'success') {
                        return swal("Data gagal di hapus!");
                    }

                    swal({
                        title: "information",
                        text: 'Data berhasil di hapus',
                        icon: 'info',
                    }).then((value) => {
                        location.reload(true);
                    });

                })
                .catch(err => {
                    if (err) {
                        swal("Ooops!", "Koneksi Bermasalah!", "error");
                    } else {
                        swal.stopLoading();
                        swal.close();
                    }
                });
    })
    /*End Delete Departments*/

});
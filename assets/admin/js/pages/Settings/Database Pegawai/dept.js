$(function () {
    'use strict'

    var parentid,
            deptname = $("#departments");


    $('#parentid').change(function () {
        parentid = $(this).val();
    })
    /*Begin Form Add Departments*/
    var frmAddDepartments = $('#frmAddDepartments');


    frmAddDepartments.validate();
    frmAddDepartments.submit(function (e) {

        if (frmAddDepartments.valid()) {
            swal({
                text: "Apakah data yang di isi sudah sesuai ?",
                icon: "warning",
                buttons: ["Tidak", "Ya"],
                dangerMode: true,
            })
                    .then((willStore) => {
                        if (willStore) {
                            /*
                             pid = parentid
                             n   = name
                             */
                            return fetch(base_url + `settings/departments/store?pid=${parentid}&n=${deptname.val()}`);
                        } else {
                            throw null;
                            swal("Data batal ditambah");
                        }
                    })
                    .then(results => {
                        return results.json();
                    })
                    .then(json => {
                        var result = json.results;

                        if (result !== 'success') {
                            return swal("Data gagal di tambah! atau data sudah tersedia");
                        }

                        swal({
                            title: "information",
                            text: 'Data berhasil di tambah',
                            icon: 'info',
                        }).then((value) => {
                            location.reload(true);
                        });

                    })
                    .catch(err => {
                        if (err) {
                            swal("Ooops!", "Koneksi Bermasalah!", "error");
                        } else {
                            swal.stopLoading();
                            swal.close();
                        }
                    });
        }

        return false;

    });
    /*End Form Add Departments*/

    /*Begin Form Edit Departments*/
    var frmEditDept = $('#frmEditDept'),
            tempURL = window.location.href.split('/'),
            subdeptid = tempURL[7],
            subdeptname = $('#subdeptname'),
            pid = $("#parentid").val();

    $(document).on('change', '#parentid', function () {
        pid = $(this).val();
    })

    frmEditDept.validate();
    frmEditDept.submit(function (e) {
        // console.log(deptname);

        if (frmEditDept.valid()) {
            swal({
                text: "Apakah data yang di isi sudah sesuai ?",
                icon: "warning",
                buttons: ["Tidak", "Ya"],
                dangerMode: true,
            })
                    .then((willStore) => {
                        if (willStore) {
                            /*
                             pid = parentid
                             n   = name
                             */
                            return fetch(base_url + `settings/departments/update?pid=${pid}&n=${subdeptname.val()}&subdeptid=${subdeptid}`);
                        } else {
                            throw null;
                            swal("Data batal di ubah");
                        }
                    })
                    .then(results => {
                        return results.json();
                    })
                    .then(json => {
                        var result = json.results;

                        if (result !== 'success') {
                            return swal("Data gagal di ubah!");
                        }

                        swal({
                            title: "information",
                            text: 'Data berhasil di ubah',
                            icon: 'info',
                        }).then((value) => {
                            location.reload(true);
                        });

                    })
                    .catch(err => {
                        if (err) {
                            swal("Ooops!", "Koneksi Bermasalah!", "error");
                        } else {
                            swal.stopLoading();
                            swal.close();
                        }
                    });


        }

        return false;
    })

    /*End Form Edit Departments*/

    /*Begin Delete Departments*/
    //  $('.deldept').on('click',function (e) {
    $(document).on('click', '.deldept', function (e) {
//        alert('cek');
//        return ;
        e.preventDefault();
        var id = $(this).attr('name')

        swal({
            title: "Apakah anda yakin?",
            text: "Ingin menghapus data ini ?!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willSend) => {
                    if (willSend) {
                        return fetch(base_url + `settings/Departments/delete?id=${id}`);
                    } else {
                        throw null;
                        swal("Oops Data gagal di hapus!");
                    }
                })
                .then(results => {
                    return results.json();
                })
                .then(json => {
                    var result = json.results;

                    if (result !== 'success') {
                        return swal("Data gagal di hapus!");
                    }

                    swal({
                        title: "information",
                        text: 'Data berhasil di hapus',
                        icon: 'info',
                    }).then((value) => {
                        location.reload(true);
                    });

                })
                .catch(err => {
                    if (err) {
                        swal("Ooops!", "Koneksi Bermasalah!", "error");
                    } else {
                        swal.stopLoading();
                        swal.close();
                    }
                });
    })
    /*End Delete Departments*/

});
$(function () {
  'use strict'
    
    /*Begin Form Add Status Nikah*/

    var frmAddStatsnikah = $('#frmAddStatsnikah'),
        name = $("#name"),
        kode = $("#kode");
        

    frmAddStatsnikah.validate()   ;
    frmAddStatsnikah.submit(function (e) {
          
      if (frmAddStatsnikah.valid()) {
         swal({
            text: "Apakah data yang di isi sudah sesuai ?",
            icon: "warning",
            buttons: ["Tidak","Ya"],
            dangerMode: true,
          })
          .then((willStore) => {
            if (willStore) {
              /*
                k = kode
                n   = name
               */
              return fetch(base_url + `settings/statsnikah/store?k=${kode.val()}&n=${name.val()}`);
            } else {
              throw null;
              swal("Data batal ditambah");
            }
          })
          .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di tambah! atau data sudah tersedia");
            }

            swal({
              title: "information",
              text: 'Data berhasil di tambah',
              icon:'info',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
      }

      return false;

    });
    /*End Form Add Status Nikah*/

    /*Begin Form Edit Status Nikah*/
    var frmEditStatsNikah = $('#frmEditStatsNikah'),
        tempURL = window.location.href.split('/'),
        id = tempURL[7],
        kode = $("#kode"),
        name = $("#name");
    frmEditStatsNikah.validate();
    frmEditStatsNikah.submit(function (e) {
      
      if (frmEditStatsNikah.valid()) {
        swal({
            text: "Apakah data yang di isi sudah sesuai ?",
            icon: "warning",
            buttons: ["Tidak","Ya"],
            dangerMode: true,
          })
          .then((willStore) => {
            if (willStore) {
              /*
                i = id
                k = kode
                n   = name
               */
              return fetch(base_url + `settings/statsnikah/update?i=${id}&k=${kode.val()}&n=${name.val()}`);
            } else {
              throw null;
              swal("Data batal di ubah");
            }
          })
          .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di ubah!");
            }

            swal({
              title: "information",
              text: 'Data berhasil di ubah',
              icon:'info',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
        
        
      }

      return false;
    })
    
    /*End Form Edit Status Nikah*/

    /*Begin Delete Status Nikah*/
      $('.del-statsnikah').on('click',function (e) {
        e.preventDefault();
        var id = $(this).attr('name')

        swal({
          title: "Apakah anda yakin?",
          text: "Ingin menghapus data ini ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSend) => {
          if (willSend) {
             return fetch(base_url + `settings/statsnikah/delete?id=${id}`);
          } else {
            swal("Oops Data gagal di hapus!");
          }
        })
        .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di hapus!");
            }

            swal({
              title: "information",
              text: 'Data berhasil di hapus',
              icon:'info',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
      })
    /*End Delete Status Nikah*/

});
$(function () {
  'use strict'

    /*Begin Form Add Pendidikan*/
    var frmAddAgama = $('#frmAddAgama'),
        agama = $("#name");
        

    frmAddAgama.validate()   ;
    frmAddAgama.submit(function (e) {
      if (frmAddAgama.valid()) {
         swal({
            text: "Apakah data yang di isi sudah sesuai ?",
            icon: "warning",
            buttons: ["Tidak","Ya"],
            dangerMode: true,
          })
          .then((willStore) => {
            if (willStore) {
              /*
                n   = name
               */
              return fetch(base_url + `settings/agama/store?n=${agama.val()}`);
            } else {
              throw null;
              swal("Data batal ditambah");
            }
          })
          .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di tambah! atau data sudah tersedia");
            }

            swal({
              title: "information",
              text: 'Data berhasil di tambah',
              icon:'info',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
      }

      return false;

    });
    /*End Form Add Pendidikan*/

    /*Begin Form Edit Pendidikan*/
    var frmEditAgama = $('#frmEditAgama'),
        tempURL = window.location.href.split('/'),
        id = tempURL[7];
        

    frmEditAgama.validate();
    frmEditAgama.submit(function (e) {
      
      if (frmEditAgama.valid()) {
        swal({
            text: "Apakah data yang di isi sudah sesuai ?",
            icon: "warning",
            buttons: ["Tidak","Ya"],
            dangerMode: true,
          })
          .then((willStore) => {
            if (willStore) {
              /*
                i = id
                n   = name
               */
              return fetch(base_url + `settings/agama/update?i=${id}&n=${agama.val()}`);
            } else {
              throw null;
              swal("Data batal di ubah");
            }
          })
          .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di ubah!");
            }

            swal({
              title: "information",
              text: 'Data berhasil di ubah',
              icon:'info',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
        
        
      }

      return false;
    })
    
    /*End Form Edit Pendidikan*/

    /*Begin Delete Pendidikan*/
      $('.del-agama').on('click',function (e) {
        e.preventDefault();
        var id = $(this).attr('name')

        swal({
          title: "Apakah anda yakin?",
          text: "Ingin menghapus data ini ?!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willSend) => {
          if (willSend) {
             return fetch(base_url + `settings/agama/delete?id=${id}`);
          } else {
            swal("Oops Data gagal di hapus!");
          }
        })
        .then(results => {
            return results.json();
          })
          .then(json => {
            var  result = json.results;
           
            if (result !== 'success') {
              return swal("Data gagal di hapus!");
            }

            swal({
              title: "information",
              text: 'Data berhasil di hapus',
              icon:'info',
            }).then((value) => {
              location.reload(true);
            });

          })
          .catch(err => {
            if (err) {
              swal("Ooops!", "Koneksi Bermasalah!", "error");
            } else {
              swal.stopLoading();
              swal.close();
            }
          });
      })
    /*End Delete Pendidikan*/

});
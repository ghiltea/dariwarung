$(function () {
    'use strict'

    var parentid,
            deptname = $("#deptname");


    $('#parentid').change(function () {
        parentid = $(this).val();
    })
    /*Begin Form Add Branch*/
    var formaddBranch = $('#frmAddBranch');


    formaddBranch.validate();
    formaddBranch.submit(function (e) {

        if (formaddBranch.valid()) {
            swal({
                text: "Apakah data yang di isi sudah sesuai ?",
                icon: "warning",
                buttons: ["Tidak", "Ya"],
                dangerMode: true,
            })
                    .then((willStore) => {
                        if (willStore) {
                            /*
                             pid = parentid
                             n   = name
                             */
                            return fetch(base_url + `settings/branch/store?pid=${parentid}&n=${deptname.val()}`);
                        } else {
                            throw null;
                            swal("Data batal ditambah");
                        }
                    })
                    .then(results => {
                        return results.json();
                    })
                    .then(json => {
                        var result = json.results;

                        if (result !== 'success') {
                            return swal("Data gagal di tambah! atau data sudah tersedia");
                        }

                        swal({
                            title: "information",
                            text: 'Data berhasil di tambah',
                            icon: 'info',
                        }).then((value) => {
                            location.reload(true);
                        });

                    })
                    .catch(err => {
                        if (err) {
                            swal("Ooops!", "Koneksi Bermasalah!", "error");
                        } else {
                            swal.stopLoading();
                            swal.close();
                        }
                    });
        }

        return false;

    });
    /*End Form Add Branch*/

    /*Begin Form Edit Branch*/
    var frmEditBranch = $('#frmEditBranch'),
            tempURL = window.location.href.split('/'),
            mrid = tempURL[7],
            pid = $("#parentid").val();

    $(document).on('change', '#parentid', function () {
        pid = $(this).val();
    })

    frmEditBranch.validate();
    frmEditBranch.submit(function (e) {

        if (frmEditBranch.valid()) {
            swal({
                text: "Apakah data yang di isi sudah sesuai ?",
                icon: "warning",
                buttons: ["Tidak", "Ya"],
                dangerMode: true,
            })
                    .then((willStore) => {
                        if (willStore) {
                            /*
                             pid = parentid
                             n   = name
                             */
                            return fetch(base_url + `settings/branch/update?pid=${pid}&n=${deptname.val()}&mrid=${mrid}`);
                        } else {
                            throw null;
                            swal("Data batal di ubah");
                        }
                    })
                    .then(results => {
                        return results.json();
                    })
                    .then(json => {
                        var result = json.results;

                        if (result !== 'success') {
                            return swal("Data gagal di ubah!");
                        }

                        swal({
                            title: "information",
                            text: 'Data berhasil di ubah',
                            icon: 'info',
                        }).then((value) => {
                            location.reload(true);
                        });

                    })
                    .catch(err => {
                        if (err) {
                            swal("Ooops!", "Koneksi Bermasalah!", "error");
                        } else {
                            swal.stopLoading();
                            swal.close();
                        }
                    });


        }

        return false;
    })

    /*End Form Edit Branch*/

    /*Begin Delete Branch*/
    // $('.delbranch').on('click',function (e) {
    $(document).on('click', '.delbranch', function (e) {
        e.preventDefault();
        var id = $(this).attr('name');

        swal({
            title: "Apakah anda yakin?",
            text: "Ingin menghapus data ini ?!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willSend) => {
                    if (willSend) {
                        return fetch(base_url + `settings/branch/delete?id=${id}`);
                    } else {
                        throw null;

                        swal("Oops Data gagal di hapus!");
                    }
                })
                .then(results => {
                    return results.json();
                })
                .then(json => {
                    var result = json.results;

                    if (result !== 'success') {
                        return swal("Data gagal di hapus!");
                    }

                    swal({
                        title: "information",
                        text: 'Data berhasil di hapus',
                        icon: 'info',
                    }).then((value) => {
                        location.reload(true);
                    });

                })
                .catch(err => {
                    if (err) {
                        swal("Ooops!", "Koneksi Bermasalah!", "error");
                    } else {
                        swal.stopLoading();
                        swal.close();
                    }
                });
    })
    /*End Delete Branch*/

});
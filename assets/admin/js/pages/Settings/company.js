$(function () {
    'use strict'



    var d1, d2;

    /**
     *
     * Data Table
     * Inisialisasi table company
     */

    var table = "company";
    var companyTable = $("#" + table + "-table").DataTable({
        scrollX: true,
        scrollCollapse: true,
        //"pageLength": 1,
        'searching': false,
        'ordering': false,
        'filtering': false,
        lengthChange: false,
        // 'columnDef'
        // responsive:true,
        "autoWidth": false,

    })

    $("#" + table + "-table").dataTable().fnDraw();

    // Button Add
    $(document).on('click', '#btnAddCompany', function (e) {
        location.replace(base_url + "settings/company/add");
    })
    //End Button Add


    /*Add Company*/
    var frmAddCompany = $("#frmAddCompany");

    frmAddCompany.validate();
    $(document).on('click', '#subAdCompany', function (e) {
        e.preventDefault();

        if (frmAddCompany.valid()) {
            swal({
                title: "Apakah anda yakin?",
                text: "Ingin Menambah Data ?!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willSend) => {
                        if (willSend) {
                            $.ajax({
                                url: base_url + 'settings/company/store',
                                method: 'POST',
                                data: new FormData($('#frmAddCompany')[0]),
                                cache: false,
                                processData: false,
                                contentType: false,
                            })
                                    .done(function (data) {

                                        var obj = JSON.parse(data);

                                        if (obj.results == "success") {

                                            swal(obj.reason, {
                                                icon: "success",
                                            }).then((value) => {
                                                location.reload(true);
                                            });
                                        } else {
                                            swal(obj.reason, {
                                                icon: "error",
                                            })
                                        }

                                    })
                                    .fail(function () {
                                        swal("Ooops terjadi masalah, coba hubungi administrator");
                                    })
                            /*.always(function() {
                             });*/

                        } else {
                            swal("Form tidak di kirim!");
                        }
                    })
        }
    });
    /*Add Company*/

    /*Edit Company*/
    var frmEditCompany = $("#frmEditCompany");

    frmEditCompany.validate();
    $(document).on('click', '#subEditCompany', function (e) {
        e.preventDefault();

        if (frmEditCompany.valid()) {
            swal({
                title: "Apakah anda yakin?",
                text: "Ingin Mengubah Data ?!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willSend) => {
                        if (willSend) {
                            $.ajax({
                                url: base_url + 'settings/company/update',
                                method: 'POST',
                                data: new FormData($('#frmEditCompany')[0]),
                                cache: false,
                                processData: false,
                                contentType: false,
                            })
                                    .done(function (data) {

                                        var obj = JSON.parse(data);

                                        if (obj.results == "success") {

                                            swal(obj.reason, {
                                                icon: "success",
                                            }).then((value) => {
                                                location.reload(true);
                                            });
                                        } else {
                                            swal(obj.reason, {
                                                icon: "error",
                                            })
                                        }

                                    })
                                    .fail(function () {
                                        swal("Ooops terjadi masalah, coba hubungi administrator");
                                    })
                            /*.always(function() {
                             });*/

                        } else {
                            swal("Form tidak di kirim!");
                        }
                    })
        }
    });
    /*Edit Company*/

    /*Delete Company*/
    $(document).on('click', '.del-set-company', function (e) {
        e.preventDefault();

        var id = $(this).attr('name')

        swal({
            title: "Apakah anda yakin?",
            text: "Ingin menghapus data ini ?!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willSend) => {
                    if (willSend) {
                        return fetch(base_url + `settings/company/delete?id=${id}`);
                    } else {
                        throw null;

                        swal("Oops Data gagal di hapus!");
                    }
                })
                .then(results => {
                    return results.json();
                })
                .then(json => {
                    var result = json.results;

                    if (result !== 'success') {
                        return swal("Data gagal di hapus!");
                    }

                    swal({
                        title: "Success",
                        text: 'Data berhasil di hapus',
                        icon: 'success',
                    }).then((value) => {
                        location.reload(true);
                    });

                })
                .catch(err => {
                    if (err) {
                        swal("Ooops!", "Koneksi Bermasalah!", "error");
                    } else {
                        swal.stopLoading();
                        swal.close();
                    }
                });

    })
    /*Delete Role*/
})
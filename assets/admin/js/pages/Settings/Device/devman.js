$(function () {
	'use strict';
	var id = [];

	$("#device_cabid").select2({placeholder: 'Silahkan Pilih Cabang',allowClear:true});
	$("#device_subcabid").select2({placeholder: 'Silahkan Pilih MR'});

	$("#e-device_cabid").select2({placeholder: 'Silahkan Pilih Cabang',allowClear:true});
	$("#e-device_subcabid").select2({placeholder: 'Silahkan Pilih MR'});

	var tableDevman = $('#devman-table').DataTable({
		// 'lengthChange': true,
		// 'autoWidth'   : true,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 15,
        // 'columnDef'
        // responsive:true,
        "autoWidth": false,
        "drawCallback": function( settings ) {

        	$("#load-devm").css("display","none");
        	$("#load-devm").remove();
	        $('#devm-table').removeAttr("style");
	    }
	});

	$("#devman-table").dataTable().fnDraw();
	id = tableDevman.column(3).data();
	
	if (id && id.length) {
		var len = id.length,
			host;
		for (var i = 0; i < id.length; i++) {
			host = id[i];
			state(host,i);
			(function loop(host,key) {
				setTimeout(function() {
					state(host,i);
					loop(host,i);
					// 1 detik = 1000
				},60000);
			})(host);
		}
	}
	// else{
	// 	// console.log('not')
	// }



	// Button Add
	$(document).on('click','#btn_addMesin',function (e) {
		location.replace(base_url + "settings/device/add");
	});

	// On Change Cabang
	$(document).on('change','#device_cabid',function (e) {
		var cabid = $(this).val();
	    var div       = $('#device_subcabid');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/branch/findmr',
	        data:{'id':cabid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#device_subcabid').select2({placeholder: 'Silahkan Pilih MR'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});

	/*Begin Add Device*/
	var frmAddDevice = $('#frmAddDevice');
        

    frmAddDevice.validate()   ;
    frmAddDevice.submit(function (e) {
    	e.preventDefault();
      
		if (frmAddDevice.valid()) {
			var newForm =  new FormData($('#frmAddDevice')[0]);

			swal({
		        title: "Apakah anda yakin?",
		        text: "Pastikan tidak ada data yang keliru!",
		        icon: "warning",
		        buttons: true,
		        dangerMode: true,
	      	})
	      	.then((willSend) => {
	        	if (willSend) {
		           	$.ajax({
		              method :'post',
		              url  : base_url + 'settings/device/store',
		              data : new FormData($('#frmAddDevice')[0]),
		              cache:false,
		              processData:false,
		              contentType:false,              
		              success : function (data) {
		                // body...
		                var obj = jQuery.parseJSON(data);
		                if (obj.results == "success") {
		                    swal("Form berhasil di kirim!", {
		                    	icon: "success",
		                  	})
		                    .then((value) => {
				              location.reload(true);
				            });
		                }else{
		                  swal("Form gagal kirim!", {
		                    icon: "error",
		                  })
		                }
		              },
		              error : function(){
		                 swal("Ooops tidak bisa tersambung dengan server");
		              }

		            });
	        	} else {
		          swal("Form tidak di kirim!");
		        }
	      	})	
		}
	})
	/*End Add Device*/

	/*Begin Edit Mesin*/
	// On Change Cabang
	$(document).on('change','#e-device_cabid',function (e) {
		var cabid = $(this).val();
	    var div       = $('#e-device_subcabid');
	    var op        = " ";
	   
	    $.ajax({
	        type:'post',
	        url: base_url+'settings/branch/findmr',
	        data:{'id':cabid},
	        success:function (data) {
	          
	          data =  $.parseJSON(data);
	            op+='<option value="" selected disabled></option>';
	            for (var i = 0; i < data.length; i++) {
	            	
	                op+='<option value="'+data[i].id+'">'+data[i].name+'</option>'
	            }

	            div.html(" ");
	            div.append(op);
				$('#e-device_subcabid').select2({placeholder: 'Silahkan Pilih MR'});
	        },
	        error:function () {
	            console.log('Something wrong !!!');
	        }
	    });
	});

	/*Begin Edit Device*/
	var frmEditDevice = $('#frmEditDevice');
        

    frmEditDevice.validate()   ;
    frmEditDevice.submit(function (e) {
    	e.preventDefault();
      
		if (frmEditDevice.valid()) {
			var newForm =  new FormData($('#frmEditDevice')[0]);

			swal({
		        title: "Apakah anda yakin?",
		        text: "Pastikan tidak ada data yang keliru!",
		        icon: "warning",
		        buttons: true,
		        dangerMode: true,
	      	})
	      	.then((willSend) => {
	        	if (willSend) {
		           	$.ajax({
		              method :'post',
		              url  : base_url + 'settings/device/update',
		              data : new FormData($('#frmEditDevice')[0]),
		              cache:false,
		              processData:false,
		              contentType:false,              
		              success : function (data) {
		                // body...
		                var obj = jQuery.parseJSON(data);
		                if (obj.results == "success") {
		                    swal("Form berhasil di kirim!", {
		                    	icon: "success",
		                  	})
		                    .then((value) => {
				              location.reload(true);
				            });
		                }else{
		                  swal("Form gagal kirim!", {
		                    icon: "error",
		                  })
		                }
		              },
		              error : function(){
		                 swal("Ooops tidak bisa tersambung dengan server");
		              }

		            });
	        	} else {
		          swal("Form tidak di kirim!");
		        }
	      	})	
		}
	})
	/*End Edit Mesin*/

	/*Begin Delete Device*/
	  $(document).on('click','.devman',function (e) {
	  	// alert('s')
	      e.preventDefault();
	      var id = $(this).attr('name')

	      swal({
	        title: "Apakah anda yakin?",
	        text: "Ingin menghapus mesin ini ?!",
	        icon: "warning",
	        buttons: true,
	        dangerMode: true,
	      })
	      .then((willSend) => {
	        if (willSend) {
	           return fetch(base_url + `settings/device/delete?id=${id}`);
	        } else {
	          throw null;
	          swal("Oops Data gagal di hapus!");
	        }
	      })
	      .then(results => {
	          return results.json();
	        })
	        .then(json => {
	          var  result = json.results;
	         
	          if (result !== 'success') {
	            return swal("Data gagal di hapus!");
	          }

	          swal({
	            title: "information",
	            text: 'Data berhasil di hapus',
	            icon:'success',
	          }).then((value) => {
	            location.reload(true);
	          });

	        })
	        .catch(err => {
	          if (err) {
	            swal("Ooops!", "Koneksi Bermasalah!", "error");
	          } else {
	            swal.stopLoading();
	            swal.close();
	          }
	        });
	    })
	/*End Delete Device*/
	
	
})


function state(host,key) {
	var ihost = host.replace(".","");
			ihost = ihost.replace(".","");
			ihost = ihost.replace(".","");

	$.ajax({
		url: base_url + 'settings/device/state',
		type: 'GET',
		data: {host: host},
		beforeSend : function () {

			html = '<span class="label label-info">Checking</span><img src='+ base_url + 'assets/img/ajax-loader.gif alt="checking">';
			$("#" + ihost).html("");
			$("#" + ihost).html(html);
		}
	})
	.done(function(data) {
		var res = JSON.parse(data);

		var html = '';
		
		if (res.result == 'success') {
			$("#last-upload-"+ihost).html("");
			$("#last-upload-"+ihost).html(res.activity);
			html = '<span class="label label-success">Online</span>';
			$("#" + ihost).html("");
			$("#" + ihost).html(html);
		}else{
			html = '<span class="label label-danger">Offline</span>';
			$("#" + ihost).html("");
			$("#" + ihost).html(html);
		}
	})
	.fail(function() {
		// console.log("error");
	})
	.always(function() {
		// console.log("complete");
	});
}
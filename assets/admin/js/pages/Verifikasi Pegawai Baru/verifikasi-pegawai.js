$(document).ready(function() {
	var id =[];
	var tableVerifikasi = $("#verifikasi-table").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Approve',
                action: function ( e, dt, node, config ) {
                    $("input:checked", tableVerifikasi.rows().nodes().to$()).each(function(){
						id.push($(this).val());
					});
					if (id.length == 0) {
						swal('Silahkan pilih pegawai terlebih dahulu');
					}else{
						swal({
					        title: "Apakah anda yakin?",
					        text: id.length + " Pegawai akan di verifkasi !",
					        icon: "warning",
					        buttons: true,
					        dangerMode: true,
					      })
					      .then((willSend) => {
					        if (willSend) {
					           $.ajax({
						            method :'post',
						            url  : base_url + 'kepeg/verifikasi/aktif',
						            dataType: "json",
									data: {'uid' : id},
						            success : function (data) {
						                // body...
									    if (data['results'] == "success") {
									        swal("Pegawai berhasil di verifikasi!", {
									            icon: "success",
									        })
									        .then((value) => {
								              location.reload(true);
								            });
									    }else{
									    	swal("Pegawai gagal di verifikasi!", {
									        	icon: "error",
									        })
										    id.length = 0;
									    }
								    },
								    error : function(){
								    	swal("Ooops tidak bisa tersambung dengan server");
									    id.length = 0;
								    }
							    });
							} else {
							    // swal("Pegawai Gagak di verifikasi!");
							    id.length = 0;
						    }
					    })
					}
                },
                "className": 'btn-default'
            }
        ],
		'lengthChange': true,
		'autoWidth'   : false,
		scrollX:        true,
        scrollCollapse: true,
        "pageLength": 5,
        // fixedHeader: true,
        "drawCallback": function( settings ) {
        	$("#load-ver").css("display","none");
        	$("#load-ver").remove();
	        $('#ver-table').removeAttr("style");
	    }

    } );

    $("#verifikasi-table").dataTable().fnDraw();

	$('#verifikasi-select-all').on('click', function(){
	  // Get all rows with search applied
	  var rows = tableVerifikasi.rows({ 'search': 'applied' }).nodes();
	  console.log(rows);
	  // Check/uncheck checkboxes for all rows in the table
	  $('input[type="checkbox"]', rows).prop('checked', this.checked);
	});

	// Handle click on checkbox to set state of "Select all" control
    $('#verifikasi-table tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#verifikasi-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
    });
});
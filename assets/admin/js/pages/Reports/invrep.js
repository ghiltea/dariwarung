$(function () {
	'use strict';
	var arr;
	var pages = 'invreport';
	var tableInvReport = $("#"+pages+"-table");

	$.ajax({
		async:false,
		url: base_url + 'dropros',
	})
	.done(function(data) {
		arr = data;
		arr = JSON.parse(data);
	});

	var comboTree;

	comboTree = $('#inputCmbInvrep').comboTree({
				source : arr,
				isMultiple: false,
				page:pages
	});

	/*Initialize DataTable*/
	$("#"+pages+"-table").DataTable({
		'destroy' : true,
		'searching' : true,
		'ordering' : false,
		'filtering' : false,
		'autoWidth' : false,
		'lengthChange' : false,
		scrollX:        true,
		scrollCollapse: true,
		"drawCallback": function( settings ) {

        	$("#load-invrep").css("display","none");
        	$("#load-invrep").remove();
	        $('#invrep-table').removeAttr("style");
	    }

	});
	
	$("#" + pages + "-table").dataTable().fnDraw();

	/*$('.comboTreeItemTitle-'+pages).on('click',function () {
		$("#loadInvrep").modal('show');
		var aktif = [];
		$.each($("input[name='aktif']:checked"),function () {
			aktif.push($(this).val());
		})

		aktif = aktif.join(",");
		load_data(null,aktif);
		$("#loadInvrep").modal('hide');
			
	})*/

	var data = [];
    $('#'+ pages + '-table tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        data = tableInvReport.DataTable().row( this ).data();
    } );

    /*View Report*/
    $("#vReportInvrep").click(function () {
    	$("#loadInvrep").modal('show');
    	var aktif = [];
		$.each($("input[name='aktif']:checked"),function () {
			aktif.push($(this).val());
		})

		aktif = aktif.join(",");
		console.log(aktif)
		load_data(null,aktif);
		$("#loadInvrep").modal('hide');
    })

    /*View Report Excel*/
    $("#vReportInvrepExcel").click(function () {
    	vReportInvrepExcel
    	var id = comboTree.getSelectedItemsId();
		var arr_user = getValues();

    	if (id == '') {
    		swal('Please Select Area');
    	}else{
    		var tableInvReport = $("#"+pages+"-table").DataTable();
    		if ( ! tableInvReport.data().any() ) {
			    swal( 'Generate Report First' );
			}else{
				// exportTableToExcel(pages+"-table");
				$("#"+pages+"-table").table2excel({
				    name: "Data Inventaris",
				    filename: "Inventaris Report" + new Date().toISOString().replace(/[\-\:\.]/g, "") //do not include extension
				});
			}	
    	}
    })

 	function load_data(query,aktif = null) {
		var a = comboTree.getSelectedItemsId();
		$.ajax({
			url: base_url+ 'reports/inventaris/u',
			type: 'POST',
			data: {
				a : a,
				q : query,
				aktif:aktif,
			},
			beforeSend: function(){
		     // Handle the beforeSend event
		   },
			success: function (data) {
				var dataSet = JSON.parse(data);
				// console.log(dataSet.data);
				 $("#"+pages+"-table").DataTable({
					'destroy' : true,
		    		'data' : dataSet.data,
					'searching' : true,
					'ordering' : false,
					'filtering' : false,
					'autoWidth' : false,
					'lengthChange' : false,
					scrollX:        true,
					scrollCollapse: true,
					"drawCallback": function( settings ) {

			            // $("#load-resign").css("display","none");
			            $("#loadInvrep").modal('hide');
			        }

				});
			}
		})
 	}

	function getValues(){
	  var ids = $.map(tableInvReport.DataTable().rows('.selected').data(), function (item) {
	     return item[1]
	  });
	  return ids
	}

	/*function exportTableToExcel(tableID, filename = ''){
	    var downloadLink;
	    var dataType = 'application/vnd.ms-excel';
	    var tableSelect = document.getElementById(tableID);
	    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
	    
	    // Specify file name
	    filename = filename?filename+'.xls':'excel_data.xls';
	    
	    // Create download link element
	    downloadLink = document.createElement("a");
	    
	    document.body.appendChild(downloadLink);
	    
	    if(navigator.msSaveOrOpenBlob){
	        var blob = new Blob(['\ufeff', tableHTML], {
	            type: dataType
	        });
	        navigator.msSaveOrOpenBlob( blob, filename);
	    }else{
	        // Create a link to the file
	        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
	    
	        // Setting the file name
	        downloadLink.download = filename;
	        
	        //triggering the function
	        downloadLink.click();
	    }
	}*/
})

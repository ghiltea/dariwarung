$(function () {
	'use strict';
	var arr;
	var pages = 'itemreport';
	var tableItemReport = $("#"+pages+"-table");

	/*Initialize DataTable*/
	$("#"+pages+"-table").DataTable({
		'destroy' : true,
		'searching' : false,
		'ordering' : false,
		'filtering' : false,
		'autoWidth' : false,
		'lengthChange' : false,
		scrollX:        true,
		scrollCollapse: true,

	});
	
	$("#" + pages + "-table").dataTable().fnDraw();

	$("#searchItemrep").keyup(function () {
		load_data(this.value);
	})

	var data = [];
    $('#'+ pages + '-table tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        data = tableItemReport.DataTable().row( this ).data();
    } );

    /*View Report Excel*/
    $("#vReportItemrepExcel").click(function () {
    	
    	$("#"+pages+"-table").table2excel({
		    name: "Data Item Inventaris",
		    filename: "Item Report" + new Date().toISOString().replace(/[\-\:\.]/g, "") //do not include extension
		});    	
    })

 	function load_data(query) {
 		$.ajax({
			url: base_url+ 'reports/item/u',
			type: 'POST',
			data: {
				q : query
			},
			beforeSend: function(){
		     // Handle the beforeSend event
		   },
			success: function (data) {
				var dataSet = JSON.parse(data);
				// console.log(dataSet.data);
				 $("#"+pages+"-table").DataTable({
					'destroy' : true,
		    		'data' : dataSet.data,
					'searching' : false,
					'ordering' : false,
					'filtering' : false,
					'autoWidth' : false,
					'lengthChange' : false,
					scrollX:        true,
					scrollCollapse: true
				});
			}
		})
 	}

})

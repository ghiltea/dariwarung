$(function () {
	'use strict';
	var arr;
	var pages = 'otreport';
	var tableInOut = $("#"+pages+"-table");

	$.ajax({
		async:false,
		url: base_url + 'dropros',
	})
	.done(function(data) {
		arr = data;
		arr = JSON.parse(data);
	});

	var comboTree;

	comboTree = $('#inputCmbOtrep').comboTree({
				source : arr,
				isMultiple: false,
				page:pages
	});

	/*Initialize DataTable*/
	$("#"+pages+"-table").DataTable({
		'destroy' : true,
		'searching' : false,
		'ordering' : false,
		'filtering' : false,
		'autoWidth' : false,
		'lengthChange' : false,
		scrollX:        true,
		scrollCollapse: true,
		"drawCallback": function( settings ) {

        	$("#load-otrep").css("display","none");
        	$("#load-otrep").remove();
	        $('#otrep-table').removeAttr("style");
	    }

	});
	
	$("#" + pages + "-table").dataTable().fnDraw();
	/*Fill DataTable*/
	$('.comboTreeItemTitle-'+pages).on('click',function () {
		$("#loadOtrep").modal('show');
		load_data();
		$("#loadOtrep").modal('hide');
			
	})

	$("#searchOtrep").keyup(function () {
		var id = comboTree.getSelectedItemsId();
		if (!id || id == '') {
			swal('Silahkan Pilih Area');
		}else{
			load_data(this.value);
		}

	})

	var data = [];
    $('#'+ pages + '-table tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        data = tableInOut.DataTable().row( this ).data();
    } );

    /*View Report*/
    $("#vReportOtrep").click(function () {
    	var fromdate,todate;
		var id = comboTree.getSelectedItemsId();
		console.log(id)
		var arr_user = getValues();

    	fromdate = $("#fromdate").val();
    	todate = $("#todate").val();

    	if (fromdate == '' || todate == '') {
    		swal('Please Select Date ');
    	}else if (id == undefined || id == '') {
    		swal('Please Select Area');
    	}else{
    		if (arr_user.length) {
    			arr_user = arr_user.join();
    		}else{
    			arr_user = 0;
    		}
    		var url = base_url + 'reports/ot/report?fromdate=' + fromdate + '&todate=' + todate + '&area=' + id +'&user=' + arr_user + '&excel=0';
			window.open(url);	
    	}
    	// swal();
    })

    /*View Report Excel*/
    $("#vReportOtrepExcel").click(function () {
    	var countUser  = tableInOut.DataTable().rows('.selected').data().length;
    	var fromdate,todate;
		var id = comboTree.getSelectedItemsId();

		var arr_user = getValues();

    	fromdate = $("#fromdate").val();
    	todate = $("#todate").val();

    	if (fromdate == '' || todate == '') {
    		// swal('Please Select Date ');
    	}else if (id == false || id == '') {
    		swal('Please Select Area');
    	}else{
    		if (arr_user.length) {
    			arr_user = arr_user.join();
    		}else{
    			arr_user = 0;
    		}
    		var url = base_url + 'reports/ot/report?fromdate=' + fromdate + '&todate=' + todate + '&area=' + id +'&user=' + arr_user + '&excel=1';
			window.open(url);	
    	}
    	// swal();
    })

 	function load_data(query) {
		var a = comboTree.getSelectedItemsId();
 		$.ajax({
			url: base_url+ 'reports/ot/u',
			type: 'POST',
			data: {
				a : a,
				q : query
			},
			beforeSend: function(){
		     // Handle the beforeSend event
		   },
			success: function (data) {
				var dataSet = JSON.parse(data);
				// console.log(dataSet.data);
				$("#"+pages+"-table").DataTable({
					'destroy' : true,
		    		'data' : dataSet.data,
					'searching' : false,
					'ordering' : false,
					'filtering' : false,
					'autoWidth' : false,
					'lengthChange' : false,
					scrollX:        true,
					scrollCollapse: true,
					"drawCallback": function( settings ) {

			            // $("#load-resign").css("display","none");
			            $("#loadOtrep").modal('hide');
			        }

				});
			}
		})
 	}

	function getValues(){
	  var ids = $.map(tableInOut.DataTable().rows('.selected').data(), function (item) {
	     return item[0]
	  });
	  return ids
	}
})

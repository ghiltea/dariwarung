$(function () {
	'use strict';
	var arr;
	var pages = 'spreport';
	var tableInOut = $("#"+pages+"-table");

	$.ajax({
		async:false,
		url: base_url + 'dropros',
	})
	.done(function(data) {
		arr = data;
		arr = JSON.parse(data);
	});

	var comboTree;

	comboTree = $('#inputCmbSprep').comboTree({
				source : arr,
				isMultiple: false,
				page:pages
	});

	/*Initialize DataTable*/
	$("#"+pages+"-table").DataTable({
		'destroy' : true,
		'searching' : false,
		'ordering' : false,
		'filtering' : false,
		'autoWidth' : false,
		'lengthChange' : false,
		scrollX:        true,
		scrollCollapse: true,
		"drawCallback": function( settings ) {

        	$("#load-sprep").css("display","none");
        	$("#load-sprep").remove();
	        $('#sprep-table').removeAttr("style");
	    }

	});
	
	$("#" + pages + "-table").dataTable().fnDraw();
	/*Fill DataTable*/
	$('.comboTreeItemTitle-'+pages).on('click',function () {
		$("#loadSprep").modal('show');
		load_data();
		$("#loadSprep").modal('hide');
			
	})

	$("#searchSprep").keyup(function () {
		var id = comboTree.getSelectedItemsId();
		if (!id || id == '') {
			swal('Silahkan Pilih Area');
		}else{
			load_data(this.value);
		}

	})

	var data = [];
    $('#'+ pages + '-table tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        data = tableInOut.DataTable().row( this ).data();
    } );

    /*View Report*/
    $("#vReportSprep").click(function () {
		var id = comboTree.getSelectedItemsId();

		var arr_user = getValues();
		

		if (arr_user.length == 0) {
			swal("Please Select User");
		}else{
			var url = base_url + 'reports/sp/report?user=' + arr_user + '&excel=0&pdf=0';
			window.open(url);	
		}

    })

    /*View Report Excel*/
    $("#vReportSprepExcel").click(function () {
    	var countUser  = tableInOut.DataTable().rows('.selected').data().length;
		var id = comboTree.getSelectedItemsId();

		var arr_user = getValues();

		if (arr_user.length == 0) {
			swal("Please Select User");
		}else{
			var url = base_url + 'reports/sp/report?user=' + arr_user + '&excel=1&pdf=0';
			window.open(url);	
		}
    	// swal();
    })

    /*View Report Pdf*/
    $("#vReportSprepPdf").click(function () {
    	var countUser  = tableInOut.DataTable().rows('.selected').data().length;
		var id = comboTree.getSelectedItemsId();

		var arr_user = getValues();

		if (arr_user.length == 0) {
			swal("Please Select User");
		}else{
			var url = base_url + 'reports/sp/report?user=' + arr_user + '&excel=0&pdf=1';
			window.open(url);	
		}
    	// swal();
    })

 	function load_data(query) {
		var a = comboTree.getSelectedItemsId();
 		$.ajax({
			url: base_url+ '/reports/sp/u',
			type: 'POST',
			data: {
				a : a,
				q : query
			},
			beforeSend: function(){
		     // Handle the beforeSend event
		   },
			success: function (data) {
				var dataSet = JSON.parse(data);
				// console.log(dataSet.data);
				$("#"+pages+"-table").DataTable({
					'destroy' : true,
		    		'data' : dataSet.data,
					'searching' : false,
					'ordering' : false,
					'filtering' : false,
					'autoWidth' : false,
					'lengthChange' : false,
					scrollX:        true,
					scrollCollapse: true,
					"drawCallback": function( settings ) {

			            // $("#load-resign").css("display","none");
			            $("#loadSprep").modal('hide');
			        }

				});

				$('#' + pages +'-table tbody').on('click', 'tr', function () {
			    	$("#" + pages + "-table tbody tr").hasClass("selected");
			    	$("#" + pages + "-table tbody tr").removeClass("selected");

			        $(this).toggleClass('selected');
			    } );
			}
		})
 	}

	function getValues(){
	  var ids = $.map(tableInOut.DataTable().rows('.selected').data(), function (item) {
	     return item[0]
	  });
	  return ids
	}
})

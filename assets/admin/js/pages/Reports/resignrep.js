$(function () {
	'use strict';
	var arr;
	var pages = 'resignreport';
	var tableResignReport = $("#"+pages+"-table");

	$.ajax({
		async:false,
		url: base_url + 'dropros',
	})
	.done(function(data) {
		arr = data;
		arr = JSON.parse(data);
	});

	var comboTree;

	comboTree = $('#inputCmbResignrep').comboTree({
				source : arr,
				isMultiple: false,
				page:pages
	});

	/*Initialize DataTable*/
	$("#"+pages+"-table").DataTable({
		'destroy' : true,
		'searching' : false,
		'ordering' : false,
		'filtering' : false,
		'autoWidth' : false,
		'lengthChange' : false,
		scrollX:        true,
		scrollCollapse: true,
		"drawCallback": function( settings ) {

        	$("#load-resignrep").css("display","none");
        	$("#load-resignrep").remove();
	        $('#resignrep-table').removeAttr("style");
	    }

	});
	
	$("#" + pages + "-table").dataTable().fnDraw();

	$("#searchResignrep").keyup(function () {
		var id = comboTree.getSelectedItemsId();
		if (!id || id == '') {
			swal('Silahkan Pilih Area');
		}else{
			load_data(this.value);
		}

	})

	var data = [];
    $('#'+ pages + '-table tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        data = tableResignReport.DataTable().row( this ).data();
    } );

    /*View Report*/
    $("#vReportResignrep").click(function () {

    	var fromdate,todate;
    	fromdate = $("#fromdateResign").val();
    	todate = $("#todateResign").val();

    	var id = comboTree.getSelectedItemsId();

    	if (fromdate > todate) {
			swal('Ooops rentang tanggal salah');
		}else if (id == '') {
    		swal('Please Select Area');
    	}else{
	    	$("#loadResignrep").modal('show');
			load_data();
			$("#loadResignrep").modal('hide');
    	}
    })

    /*View Report Excel*/
    $("#vReportResignrepExcel").click(function () {
    	
    	var fromdate,todate;
    	fromdate = $("#fromdateResign").val();
    	todate = $("#todateResign").val();

    	var id = comboTree.getSelectedItemsId();

    	if (fromdate > todate) {
			swal('Ooops rentang tanggal salah');
		}else if (id == '') {
    		swal('Please Select Area');
    	}else{
    		var tableResignReport = $("#"+pages+"-table").DataTable();
    		if ( ! tableResignReport.data().any() ) {
			    swal( 'Generate Report First' );
			}else{
				// exportTableToExcel(pages+"-table");
				$("#"+pages+"-table").table2excel({
				    name: "Data Resign",
				    filename: "Resign Report" + new Date().toISOString().replace(/[\-\:\.]/g, "") //do not include extension
				});
			}
	    	/*$("#loadResignrep").modal('show');
			load_data();
			$("#loadResignrep").modal('hide');*/
    	}    	
    })

 	function load_data(query) {
		var a = comboTree.getSelectedItemsId();
 		$.ajax({
			url: base_url+ 'reports/resign/u',
			type: 'POST',
			data: {
				a : a,
				q : query
			},
			beforeSend: function(){
		     // Handle the beforeSend event
		   },
			success: function (data) {
				var dataSet = JSON.parse(data);
				// console.log(dataSet.data);
				 $("#"+pages+"-table").DataTable({
					'destroy' : true,
		    		'data' : dataSet.data,
					'searching' : false,
					'ordering' : false,
					'filtering' : false,
					'autoWidth' : false,
					'lengthChange' : false,
					scrollX:        true,
					scrollCollapse: true,
					"drawCallback": function( settings ) {

			            // $("#load-resign").css("display","none");
			            $("#loadResignrep").modal('hide');
			        }

				});
			}
		})
 	}

	function getValues(){
	  var ids = $.map(tableResignReport.DataTable().rows('.selected').data(), function (item) {
	     return item[0]
	  });
	  return ids
	}

	/*function exportTableToExcel(tableID, filename = ''){
	    var downloadLink;
	    var dataType = 'application/vnd.ms-excel';
	    var tableSelect = document.getElementById(tableID);
	    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
	    
	    // Specify file name
	    filename = filename?filename+'.xls':'excel_data.xls';
	    
	    // Create download link element
	    downloadLink = document.createElement("a");
	    
	    document.body.appendChild(downloadLink);
	    
	    if(navigator.msSaveOrOpenBlob){
	        var blob = new Blob(['\ufeff', tableHTML], {
	            type: dataType
	        });
	        navigator.msSaveOrOpenBlob( blob, filename);
	    }else{
	        // Create a link to the file
	        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
	    
	        // Setting the file name
	        downloadLink.download = filename;
	        
	        //triggering the function
	        downloadLink.click();
	    }
	}*/
})

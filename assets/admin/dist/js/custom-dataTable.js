$(document).ready(function () {

    /*Modul Kepegawaian*/
    var tablePegawai = $("#pegawai-table").DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Add Data',
                action: function (e, dt, node, config) {
                    location.replace(base_url + 'kepeg/pegawai/add')
                },
                "className": 'btn btn-default'
            }
//            , {
//                text: 'Generate Template ',
//                action: function (e, dt, node, config) {
//                    location.replace(base_url + 'kepeg/pegawai/export')
//                },
//                "className": 'btn btn-default'
//            }, {
//                text: 'Import Pegawai Via Excel',
//                action: function (e, dt, node, config) {
//                    // swal('Maaf Fitur Belum Tersedia Saat Ini')
//                    $("#uploadModal").modal("show");
//
//                },
//                "className": 'btn btn-default'
//            }
        ],
        'lengthChange': true,
        'autoWidth': true,
        scrollX: true,
        scrollCollapse: true,
        //       "pageLength": 5,
        // fixedHeader: true,
        // responsive: true,
        // order:false,
        "drawCallback": function (settings) {

            $("#load-p").css("display", "none");
            $("#load-p").remove();
            $('#peg-table').removeAttr("style");
        }

    });

    $("#pegawai-table").dataTable().fnDraw();

    // new $.fn.dataTable.FixedHeader( tablePegawai );


    /*Modul Setting*/
    // Data Table
    $('#unit-table').DataTable({
        'lengthChange': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    });

    // Data Table
    $('#mdept-table').DataTable({
        'lengthChange': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    });

    // Data Table
    $('#mjab-table').DataTable({
        'lengthChange': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    });

    // Data Table
    $('#mstatskar-table').DataTable({
        'lengthChange': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    });

    // Data Table
    $('#mstatsnikah-table').DataTable({
        'lengthChange': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    });

    // Data Table
    $('#mpendidikan-table').DataTable({
        'lengthChange': false,
        // 'ordering'    : true,
        "order": [[0, "asc"]],
        'info': true,
        'autoWidth': false
    });
    
       // Data Table
    $('#pangkat-table').DataTable({
        'lengthChange': false,
        // 'ordering'    : true,
        "order": [[0, "asc"]],
        'info': true,
        'autoWidth': false
    });

    // Data Table
    $('#magama-table').DataTable({
        'lengthChange': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    });

    // Data Table
    $('#mbank-table').DataTable({
        'lengthChange': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
    });

    // Data Table Periode Aktif
    $('#periode-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Tambah Periode',
                action: function (e, dt, node, config) {
                    location.replace(base_url + 'settings/periode/add')
                },
                "className": 'btn btn-default'
            }
        ],
        'lengthChange': false,
        'ordering': true,
        'info': true,
        'autoWidth': false,
        "pageLength": 5

    });

    // Data Table Pegawai Aktif
    $('#pegaktif-table').DataTable({
        'lengthChange': true,
        'autoWidth': false,
        scrollX: true,
        scrollCollapse: true,
        "pageLength": 50,
        fixedHeader: true,
        responsive: true,

        "drawCallback": function (settings) {

            $("#load-paktif").css("display", "none");
            $("#load-paktif").remove();
            $('#paktif-table').removeAttr("style");
        }
    });

    $("#pegaktif-table").dataTable().fnDraw();


}); 
var formRegisterEmployee = $('#register_user_employee'),
        formEditEmployee = $('#edit_user_employee'),
        v_data = {};

// Tab Register
var tab_1 = $('#tab_1'),
        tab_2 = $('#tab_2'),
        tab_3 = $('#tab_3'),
        tab_4 = $('#tab_4'),
        tab_5 = $('#tab_5'),
        tab_6 = $('#tab_6'),
        tab_7 = $('#tab_7');


/*Register Employee*/
var validator = formRegisterEmployee.validate({
    rules: {
        nipEmp: {
            required: true,
            minlength: 11,
            maxlength: 11,
            number: true,
            remote: base_url + "exnip"
        },
        nik: {
            required: true,
            number: true,
            minlength: 16,
            maxlength: 16,
            remote: base_url + "exnik"
        },
        nokk: {
            required: true,
            number: true,
            minlength: 16,
            maxlength: 16
        },
        rt: {required: true, number: true},
        tinggi: {number: true},
        berat: {number: true},
        rw: {required: true, number: true},
        kodepos: {required: true, number: true},
        phoneEmp: {number: true},
        phoneFamily: {number: true},
        phoneInventaris: {number: true},
        emailEmp: {email: true},
        rt: {required: true, number: true},
        rw: {required: true, number: true},
        rtdomisili: {number: true},
        rwdomisili: {number: true},
        rtOrtu: {number: true},
        rwOrtu: {number: true},
        rtPasutri: {number: true},
        rwPasutri: {number: true},
        phonePasutri: {number: true},
        jmlAnak: {number: true},
        norek: {number: true},
        kodesales: {
            required: true,
            remote: base_url + "exkode"

        },
        ibu: {required: true}
    },
    messages: {
        nik: {remote: "KTP sudah di gunakan!"},
        nokk: {remote: "No Kartu Keluarga sudah di gunakan!"},
        nipEmp: {remote: "NIP sudah di gunakan!"},
        kodesales: {remote: "Kode Sales sudah di gunakan!"},
    }
});

$('#btn_PenempatanBaru').on('click', function (e) {
    e.preventDefault();
    var
            validBranch = true;
    validSubBranch = true;
    validDept = true;
    validSubDept = true;
    validJabatan = true;
    validStatsKar = true;
    validTglMulai = true;
    validKontrakAwal = true;
    validKontrakAkhir = true;
    validAtasan = true;

    /*validBranch = validator.element('#branchEmp'),
     validSubBranch = validator.element('#mrEmp'),
     validDept = validator.element('#deptEmp'),
     validSubDept = validator.element('#subdeptEmp'),
     validJabatan = validator.element('#jabatanEmp'),
     validStatsKar = validator.element('#statskarEmp'),
     validTglMulai = validator.element('#tanggal-mulai'),
     validKontrakAwal = validator.element('#kontrak-awal'),
     validKontrakAkhir = validator.element('#kontrak-akhir'),
     validAtasan = validator.element('#atasan');*/

    if ($("#subdeptEmp option:selected").text() == "SALES") {
        var validKodeSales = validator.element('#kodesales');
        if (validBranch && validSubBranch && validDept && validSubDept && validJabatan && validStatsKar && validTglMulai && validKontrakAwal && validKontrakAkhir && validAtasan && validKodeSales == true) {

            tab_1.removeClass('active_tab1');
            tab_1.addClass('inactive_tab1');
            tab_1.parent().removeClass('active');
            $('#penempatanBaru').removeClass('active');
            $('#penempatanBaru').addClass('fade');
            tab_2.parent().addClass('active');
            $('#dataPribadi').removeClass('fade');
            $('#dataPribadi').addClass('active');
        }
    } else {

        if (validBranch && validSubBranch && validDept && validSubDept && validJabatan && validStatsKar && validTglMulai && validKontrakAwal && validKontrakAkhir && validAtasan == true) {

            tab_1.removeClass('active_tab1');
            tab_1.addClass('inactive_tab1');
            tab_1.parent().removeClass('active');
            $('#penempatanBaru').removeClass('active');
            $('#penempatanBaru').addClass('fade');
            tab_2.parent().addClass('active');
            $('#dataPribadi').removeClass('fade');
            $('#dataPribadi').addClass('active');
        }
    }

});

$('#btn_PrevDataPribadi').on('click', function (e) {
    e.preventDefault();
    tab_2.addClass('inactive_tab1');
    tab_2.parent().removeClass('active');
    $('#dataPribadi').addClass('fade');
    $('#dataPribadi').removeClass('active');
    tab_1.removeClass('inactive_tab1');
    tab_1.parent().addClass('active');
    $('#penempatanBaru').addClass('active');
    $('#penempatanBaru').removeClass('fade');
})

$("#btn_Pribadi").on('click', function (e) {
    e.preventDefault();
    var
            /*validKtpName     = validator.element('#ktpName'),
             validNip         = validator.element('#nipEmp'),
             validFid         = validator.element('#fid'),
             validTmptLahir   = validator.element('#tmptLahir'),
             validTglLahir    = validator.element('#tglLahir'),
             validJk          = validator.element('#jk'),
             validNoKtp       = validator.element('#nik'),
             validNoKK        = validator.element('#nokk'),
             validAgama       = validator.element('#agamaEmp'),
             validProvKtp     = validator.element('#provktp'),
             validKabKota     = validator.element('#kabkota'),
             validKec         = validator.element('#kec'),
             validKel         = validator.element('#kel'),
             validRt          = validator.element('#rt'),
             validRw          = validator.element('#rw'),
             validKodepos     = validator.element('#kodepos'),
             validAlamat      = validator.element('#alamatktp'),
             validProvDom     = validator.element('#provdomisili'),
             validKabDom      = validator.element('#kabkotadomisili'),
             validKecDom      = validator.element('#kecdomisili'),
             validKelDom      = validator.element('#keldomisili'),
             validRtDom       = validator.element('#rtdomisili'),
             validRwDom       = validator.element('#rwdomisili'),
             validAlamatDom   = validator.element('#alamatdomisili'),
             validPhoneEmp    = validator.element('#phoneEmp'),
             validPhoneFamily = validator.element('#phoneFamily');
             */
            validKtpName = true,
            validNip = true,
            validFid = true,
            validTmptLahir = true,
            validTglLahir = true,
            validJk = true,
            validNoKtp = true,
            validNoKK = true,
            validAgama = true,
            validProvKtp = true,
            validKabKota = true,
            validKec = true,
            validKel = true,
            validRt = true,
            validRw = true,
            validKodepos = true,
            validAlamat = true,
            validProvDom = true,
            validKabDom = true,
            validKecDom = true,
            validKelDom = true,
            validRtDom = true,
            validRwDom = true,
            validAlamatDom = true,
            validPhoneEmp = true,
            validPhoneFamily = true;

    if (validKtpName && validNip && validFid && validTmptLahir && validTglLahir && validJk && validNoKtp && validNoKK && validAgama && validProvKtp && validKabKota && validKec && validKel && validRt && validRw && validKodepos && validAlamat && validProvDom && validKabDom && validKecDom && validKelDom && validRtDom && validRwDom && validPhoneEmp && validPhoneFamily && validAlamatDom == true) {
        tab_2.addClass('inactive_tab1');
        tab_2.parent().removeClass('active');
        $('#dataPribadi').removeClass('active');
        $('#dataPribadi').addClass('fade');
        tab_3.parent().addClass('active');
        $('#dataOrangTua').removeClass('fade');
        $('#dataOrangTua').addClass('active');
    }
});

$("#btn_PrevDataOrtu").on('click', function (e) {
    e.preventDefault();
    tab_3.addClass('inactive_tab1');
    tab_3.parent().removeClass('active');
    $('#dataOrangTua').addClass('fade');
    $('#dataOrangTua').removeClass('active');
    tab_2.removeClass('inactive_tab1');
    tab_2.parent().addClass('active');
    $('#dataPribadi').addClass('active');
    $('#dataPribadi').removeClass('fade');
});

$("#btn_Ortu").on('click', function (e) {
    e.preventDefault();
    var validIbu = true;
    // var validIbu = validator.element('#ibu');
    if (validIbu == true) {
        tab_3.addClass('inactive_tab1');
        tab_3.parent().removeClass('active');
        $('#dataOrangTua').removeClass('active');
        $('#dataOrangTua').addClass('fade');
        tab_4.removeClass('inactive_tab1');
        tab_4.parent().addClass('active');
        $('#dataKeluarga').removeClass('fade');
        $('#dataKeluarga').addClass('active');
    }
});

$("#btn_PrevDataKeluarga").on('click', function (e) {
    e.preventDefault();
    tab_4.addClass('inactive_tab1');
    tab_4.parent().removeClass('active');
    $('#dataKeluarga').addClass('fade');
    $('#dataKeluarga').removeClass('active');
    tab_3.removeClass('inactive_tab1');
    tab_3.parent().addClass('active');
    $('#dataOrangTua').addClass('active');
    $('#dataOrangTua').removeClass('fade');
});

$("#btn_Keluarga").on('click', function (e) {
    e.preventDefault();
    tab_4.addClass('inactive_tab1');
    tab_4.parent().removeClass('active');
    $('#dataKeluarga').removeClass('active');
    $('#dataKeluarga').addClass('fade');
    tab_5.removeClass('incactive_tab1');
    tab_5.parent().addClass('active');
    $('#dataBpjs').removeClass('fade');
    $('#dataBpjs').addClass('active');
});

$("#btn_PrevDataBpjs").on('click', function (e) {
    e.preventDefault();
    tab_5.addClass('inactive_tab1');
    tab_5.parent().removeClass('active');
    $('#dataBpjs').addClass('fade');
    $('#dataBpjs').removeClass('active');
    tab_4.removeClass('inactive_tab1');
    tab_4.parent().addClass('active');
    $('#dataKeluarga').addClass('active');
    $('#dataKeluarga').removeClass('fade');
});

$("#btn_Bpjs").on('click', function (e) {
    e.preventDefault();
    tab_5.addClass('inactive_tab1');
    tab_5.parent().removeClass('active');
    $('#dataBpjs').removeClass('active');
    $('#dataBpjs').addClass('fade');
    tab_6.removeClass('incactive_tab1');
    tab_6.parent().addClass('active');
    $('#dataBank').removeClass('fade');
    $('#dataBank').addClass('active');
});

$("#btn_PrevDataBank").on('click', function (e) {
    e.preventDefault();
    tab_6.addClass('inactive_tab1');
    tab_6.parent().removeClass('active');
    $('#dataBank').removeClass('active');
    $('#dataBank').addClass('fade');
    tab_5.removeClass('inactive_tab1');
    tab_5.parent().addClass('active');
    $('#dataBpjs').addClass('active');
    $('#dataBpjs').removeClass('fade');
});

$("#btn_Bank").on('click', function (e) {
    e.preventDefault();
    tab_6.addClass('inactive_tab1');
    tab_6.parent().removeClass('active');
    $('#dataBank').removeClass('active');
    $('#dataBank').addClass('fade');
    tab_7.removeClass('incactive_tab1');
    tab_7.parent().addClass('active');
    $('#dataPendidikan').removeClass('fade');
    $('#dataPendidikan').addClass('active');
});

$("#btn_PrevDataPendidikan").on('click', function (e) {
    e.preventDefault();
    tab_7.addClass('inactive_tab1');
    tab_7.parent().removeClass('active');
    $('#dataPendidikan').addClass('fade');
    $('#dataPendidikan').removeClass('active');
    tab_6.removeClass('inactive_tab1');
    tab_6.parent().addClass('active');
    $('#dataBank').addClass('active');
    $('#dataBank').removeClass('fade');
});

$("#btn_Pendidikan").on('click', function (e) {
    e.preventDefault();
//    alert('cek action');
//    return ;
    var form = formRegisterEmployee.serializeArray();
   // newForm = new FormData(formRegisterEmployee);

    swal({
        title: "Apakah anda yakin?",
        text: "Pastikan tidak ada data yang keliru!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willSend) => {
                if (willSend) {
                    $.ajax({
                        method: 'post',
                        url: base_url + 'kepeg/pegawai/store',
                        data: new FormData($('#register_user_employee')[0]),
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            // body...
                            var obj = jQuery.parseJSON(data);
                            if (obj.results == "success") {
                                swal("Form berhasil di kirim!", {
                                    icon: "success",
                                }).then((value) => {
                                    location.reload(true);
                                });
                            } else {
                                swal("Form gagal kirim!", {
                                    icon: "error",
                                })
                            }
                        },
                        error: function () {
                            swal("Ooops tidak bisa tersambung dengan server");
                        }

                    });
                } else {
                    swal("Form tidak di kirim!");
                }
            })
});
/*End Register Employee
 
 /*Edit Employee*/

var evalidator = formEditEmployee.validate({
    rules: {
        'e-nipEmp': {
            required: true,
            minlength: 11,
            maxlength: 11,
            number: true,
            remote: base_url + "exnip"
        },
        'e-nik': {
            required: true,
            number: true,
            remote: base_url + "exnik"
        },
        'e-nokk': {required: true, number: true},
        'e-rt': {required: true, number: true},
        'e-tinggi': {number: true},
        'e-berat': {number: true},
        'e-rw': {required: true, number: true},
        'e-kodepos': {required: true, number: true},
        'e-phoneEmp': {number: true},
        'e-phoneFamily': {number: true},
        'e-phoneInventaris': {number: true},
        'e-emailEmp': {email: true},
        'e-rt': {required: true, number: true},
        'e-rw': {required: true, number: true},
        'e-rtdomisili': {number: true},
        'e-rwdomisili': {number: true},
        'e-rtOrtu': {number: true},
        'e-rwOrtu': {number: true},
        'e-rtPasutri': {number: true},
        'e-rwPasutri': {number: true},
        'e-phonePasutri': {number: true},
        'e-jmlAnak': {number: true},
        'e-norek': {number: true},
        'e-atasan': {
            required: true,
            normalizer: function (value) {
                // Note: the value of `this` inside the `normalizer` is the corresponding
                // DOMElement. In this example, `this` reference the `username` element.
                // Trim the value of the input
                return $.trim(value);
            }
        },
        'e-ibu': {required: true}
    },
    messages: {
        'e-nik': {remote: "KTP sudah di gunakan!"},
        'e-nokk': {remote: "No Kartu Keluarga sudah di gunakan!"},
        'e-nipEmp': {remote: "NIP sudah di gunakan!"},
    }
});


// Tab Edit
var etab_1 = $('#etab_1'),
        etab_2 = $('#etab_2'),
        etab_3 = $('#etab_3'),
        etab_4 = $('#etab_4'),
        etab_5 = $('#etab_5'),
        etab_6 = $('#etab_6'),
        etab_7 = $('#etab_7');

// Next & Previous Form Button
$('#btn_edit_PenempatanBaru').on('click', function (e) {
    e.preventDefault();
    var
            /*evalidBranch       = true,
             evalidSubBranch    = true,
             evalidDept         = true,
             evalidSubDept      = true,
             evalidJabatan      = true,
             evalidStatsKar     = true,
             evalidTglMulai     = true,
             evalidKontrakAwal  = true,
             evalidKontrakAkhir = true,
             evalidAtasan       = true;*/

            evalidBranch = evalidator.element('#e-branchEmp'),
            evalidSubBranch = evalidator.element('#e-mrEmp'),
            evalidDept = evalidator.element('#e-deptEmp'),
            evalidSubDept = evalidator.element('#e-subdeptEmp'),
            evalidJabatan = evalidator.element('#e-jabatanEmp'),
            evalidStatsKar = evalidator.element('#e-statskarEmp'),
            evalidTglMulai = evalidator.element('#e-tanggal-mulai'),
            evalidKontrakAwal = evalidator.element('#e-kontrak-awal'),
            evalidKontrakAkhir = evalidator.element('#e-kontrak-akhir'),
            evalidAtasan = evalidator.element('#e-atasan');

    if (evalidBranch && evalidSubBranch && evalidDept && evalidSubDept && evalidJabatan && evalidStatsKar && evalidTglMulai && evalidKontrakAwal && evalidKontrakAkhir && evalidAtasan == true) {

        etab_1.removeClass('active_tab1');
        etab_1.addClass('inactive_tab1');
        etab_1.parent().removeClass('active');
        $('#e_penempatanBaru').removeClass('active');
        $('#e_penempatanBaru').addClass('fade');
        etab_2.parent().addClass('active');
        $('#e_dataPribadi').removeClass('fade');
        $('#e_dataPribadi').addClass('active');
    }
});

$('#btn_edit_PrevDataPribadi').on('click', function (e) {
    e.preventDefault();
    etab_2.addClass('inactive_tab1');
    etab_2.parent().removeClass('active');
    $('#e_dataPribadi').addClass('fade');
    $('#e_dataPribadi').removeClass('active');
    etab_1.removeClass('inactive_tab1');
    etab_1.parent().addClass('active');
    $('#e_penempatanBaru').addClass('active');
    $('#e_penempatanBaru').removeClass('fade');
})

$("#btn_edit_Pribadi").on('click', function (e) {
    e.preventDefault();
    var
            /*evalidKtpName = true,
             evalidNip         = true,
             evalidFid         = true,
             evalidTmptLahir   = true,
             evalidTglLahir    = true,
             evalidJk          = true,
             evalidNoKtp       = true,
             evalidNoKK        = true,
             evalidAgama       = true,
             evalidProvKtp     = true,
             evalidKabKota     = true,
             evalidKec         = true,
             evalidKel         = true,
             evalidRt          = true,
             evalidRw          = true,
             evalidKodepos     = true,
             evalidAlamat      = true,
             evalidProvDom     = true,
             evalidKabKotaDom  = true,
             evalidKecDom      = true,
             evalidKelDom      = true,
             evalidRtDom       = true,
             evalidRwDom       = true,
             evalidAlamatDom   = true,
             evalidPhoneEmp   = true,
             evalidPhoneFamily   = true;*/
            evalidKtpName = evalidator.element('#e-ktpName'),
            evalidNip = evalidator.element('#e-nipEmp'),
            evalidFid = evalidator.element('#e-fid'),
            evalidTmptLahir = evalidator.element('#e-tmptLahir'),
            evalidTglLahir = evalidator.element('#e-tglLahir'),
            evalidJk = evalidator.element('#e-jk'),
            evalidNoKtp = evalidator.element('#e-nik'),
            evalidNoKK = evalidator.element('#e-nokk'),
            evalidAgama = evalidator.element('#e-agamaEmp'),
            evalidProvKtp = evalidator.element('#e-provktp'),
            evalidKabKota = evalidator.element('#e-kabkota'),
            evalidKec = evalidator.element('#e-kec'),
            evalidKel = evalidator.element('#e-kel'),
            evalidRt = evalidator.element('#e-rt'),
            evalidRw = evalidator.element('#e-rw'),
            evalidKodepos = evalidator.element('#e-kodepos'),
            evalidAlamat = evalidator.element('#e-alamatktp'),
            evalidProvDom = evalidator.element('#e-provdomisili'),
            evalidKabKotaDom = evalidator.element('#e-kabkotadomisili'),
            evalidKecDom = evalidator.element('#e-kecdomisili'),
            evalidKelDom = evalidator.element('#e-keldomisili'),
            evalidRtDom = evalidator.element('#e-rtdomisili'),
            evalidRwDom = evalidator.element('#e-rwdomisili'),
            evalidAlamatDom = evalidator.element('#e-alamatdomisili'),
            evalidPhoneEmp = evalidator.element('#e-phoneEmp'),
            evalidPhoneFamily = evalidator.element('#e-phoneFamily');

    if (evalidKtpName && evalidNip && evalidFid && evalidTmptLahir && evalidTglLahir && evalidJk && evalidNoKtp && evalidNoKK && evalidAgama && evalidProvKtp && evalidKabKota && evalidKec && evalidKel && evalidRt && evalidRw && evalidKodepos && evalidAlamat && evalidProvDom & evalidKabKotaDom & evalidKecDom & evalidKelDom & evalidRtDom & evalidRwDom & evalidAlamatDom && evalidPhoneEmp && evalidPhoneFamily == true) {
        etab_2.addClass('inactive_tab1');
        etab_2.parent().removeClass('active');
        $('#e_dataPribadi').removeClass('active');
        $('#e_dataPribadi').addClass('fade');
        etab_3.parent().addClass('active');
        $('#e_dataOrangTua').removeClass('fade');
        $('#e_dataOrangTua').addClass('active');
    }
});

$("#btn_edit_PrevDataOrtu").on('click', function (e) {
    e.preventDefault();
    etab_3.addClass('inactive_tab1');
    etab_3.parent().removeClass('active');
    $('#e_dataOrangTua').addClass('fade');
    $('#e_dataOrangTua').removeClass('active');
    etab_2.removeClass('inactive_tab1');
    etab_2.parent().addClass('active');
    $('#e_dataPribadi').addClass('active');
    $('#e_dataPribadi').removeClass('fade');
});

$("#btn_edit_Ortu").on('click', function (e) {
    e.preventDefault();
    var evalidibu = evalidator.element('#e-ibu');
    if (evalidibu == true) {
        etab_3.addClass('inactive_tab1');
        etab_3.parent().removeClass('active');
        $('#e_dataOrangTua').removeClass('active');
        $('#e_dataOrangTua').addClass('fade');
        etab_4.removeClass('inactive_tab1');
        etab_4.parent().addClass('active');
        $('#e_dataKeluarga').removeClass('fade');
        $('#e_dataKeluarga').addClass('active');
    }
});

$("#btn_edit_PrevDataKeluarga").on('click', function (e) {
    e.preventDefault();
    etab_4.addClass('inactive_tab1');
    etab_4.parent().removeClass('active');
    $('#e_dataKeluarga').addClass('fade');
    $('#e_dataKeluarga').removeClass('active');
    etab_3.removeClass('inactive_tab1');
    etab_3.parent().addClass('active');
    $('#e_dataOrangTua').addClass('active');
    $('#e_dataOrangTua').removeClass('fade');
});

$("#btn_edit_Keluarga").on('click', function (e) {
    e.preventDefault();
    etab_4.addClass('inactive_tab1');
    etab_4.parent().removeClass('active');
    $('#e_dataKeluarga').removeClass('active');
    $('#e_dataKeluarga').addClass('fade');
    etab_5.removeClass('incactive_tab1');
    etab_5.parent().addClass('active');
    $('#e_dataBpjs').removeClass('fade');
    $('#e_dataBpjs').addClass('active');
});

$("#btn_edit_PrevDataBpjs").on('click', function (e) {
    e.preventDefault();
    etab_5.addClass('inactive_tab1');
    etab_5.parent().removeClass('active');
    $('#e_dataBpjs').addClass('fade');
    $('#e_dataBpjs').removeClass('active');
    etab_4.removeClass('inactive_tab1');
    etab_4.parent().addClass('active');
    $('#e_dataKeluarga').addClass('active');
    $('#e_dataKeluarga').removeClass('fade');
});

$("#btn_edit_Bpjs").on('click', function (e) {
    e.preventDefault();
    etab_5.addClass('inactive_tab1');
    etab_5.parent().removeClass('active');
    $('#e_dataBpjs').removeClass('active');
    $('#e_dataBpjs').addClass('fade');
    etab_6.removeClass('incactive_tab1');
    etab_6.parent().addClass('active');
    $('#e_dataBank').removeClass('fade');
    $('#e_dataBank').addClass('active');
});

$("#btn_edit_PrevDataBank").on('click', function (e) {
    e.preventDefault();
    etab_6.addClass('inactive_tab1');
    etab_6.parent().removeClass('active');
    $('#e_dataBank').removeClass('active');
    $('#e_dataBank').addClass('fade');
    etab_5.removeClass('inactive_tab1');
    etab_5.parent().addClass('active');
    $('#e_dataBpjs').addClass('active');
    $('#e_dataBpjs').removeClass('fade');
});

$("#btn_edit_Bank").on('click', function (e) {
    e.preventDefault();
    etab_6.addClass('inactive_tab1');
    etab_6.parent().removeClass('active');
    $('#e_dataBank').removeClass('active');
    $('#e_dataBank').addClass('fade');
    etab_7.removeClass('incactive_tab1');
    etab_7.parent().addClass('active');
    $('#e_dataPendidikan').removeClass('fade');
    $('#e_dataPendidikan').addClass('active');
});

$("#btn_edit_PrevDataPendidikan").on('click', function (e) {
    e.preventDefault();
    etab_7.addClass('inactive_tab1');
    etab_7.parent().removeClass('active');
    $('#e_dataPendidikan').addClass('fade');
    $('#e_dataPendidikan').removeClass('active');
    etab_6.removeClass('inactive_tab1');
    etab_6.parent().addClass('active');
    $('#e_dataBank').addClass('active');
    $('#e_dataBank').removeClass('fade');
});

$("#btn_edit_Pendidikan").on('click', function (e) {
    e.preventDefault();
//    var form = formEditEmployee.serializeArray(),
//            newForm = new FormData(formEditEmployee);
    swal({
        title: "Apakah anda yakin?",
        text: "Pastikan tidak ada data yang keliru!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willSend) => {
                if (willSend) {
                    $.ajax({
                        method: 'post',
                        url: base_url + 'kepeg/pegawai/update',
                        data: new FormData($('#edit_user_employee')[0]),
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            // body...
                            var obj = jQuery.parseJSON(data);
                            if (obj.results == "success") {
                                swal("Form berhasil di kirim!", {
                                    icon: "success",
                                })
                            } else {
                                swal("Form gagal kirim!", {
                                    icon: "error",
                                })
                            }
                        },
                        error: function () {
                            swal("Ooops tidak bisa tersambung dengan server");
                        }

                    });
                } else {
                    swal("Form tidak di kirim!");
                }
            })
});


/*End Edit Employee*/


$(document).on('click', '#upload', function (e) {
    e.preventDefault();
    var text = $("#upload").text();
    var form = $("#frmImportPegawai");

    form.validate();

    if (form.valid()) {
        swal({
            title: "Apakah anda yakin?",
            text: "Pastikan tidak ada data yang keliru!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willSend) => {
                    if (willSend) {
                        $("#upload").html("");

                        $("#upload").html("<i class=\"fa fa-spinner fa-spin\"></i>&nbsp;Loading");
                        $("#upload").attr('disabled', 'disabled');
                        $.ajax({
                            method: 'post',
                            url: base_url + 'kepeg/pegawai/import',
                            data: new FormData($('#frmImportPegawai')[0]),
                            cache: false,
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                // body...
                                var obj = (data);
                                if (obj.results == "success") {
                                    $("#upload").html("").html("Upload");
                                    $("#upload").removeAttr("disabled");
                                    swal("Form berhasil di kirim!", {
                                        icon: "success",
                                    }).then((value) => {
                                        location.reload(true);
                                    });
                                } else {
                                    $("#upload").html("").html("Upload");
                                    $("#upload").removeAttr("disabled");
                                    swal(obj.reason, {
                                        icon: "error",
                                    })
                                }
                            },
                            error: function () {
                                $("#upload").html("").html("Upload");
                                $("#upload").removeAttr("disabled");
                                swal("Ooops file tidak bisa di upload");
                            }

                        });
                    } else {
                        swal("Form tidak di kirim!");
                        $("#uploadModal").modal("hide");
                        $("#upload").html("").html("Upload");
                        $("#upload").removeAttr("disabled");
                    }
                })
    }
})
/**
 * AdminLTE Demo Menu
 * ------------------
 * You should not use this file in production.
 * This file is for demo purposes only.
 */
$(function () {
  'use strict'
    // Get Base Url
   var url = window.location.href.split('/');
   var uri = url[0]+"//"+url[2]+"/"+url[3];
  /**
   * Get access to plugins
   */

  $('[data-toggle="control-sidebar"]').controlSidebar()
  $('[data-toggle="push-menu"]').pushMenu()

  var $pushMenu       = $('[data-toggle="push-menu"]').data('lte.pushmenu')
  var $controlSidebar = $('[data-toggle="control-sidebar"]').data('lte.controlsidebar')
  var $layout         = $('body').data('lte.layout')

  /**
   * List of all the available skins
   *
   * @type Array
   */
  var mySkins = [
    'skin-blue',
    'skin-black',
    'skin-red',
    'skin-yellow',
    'skin-purple',
    'skin-green',
    'skin-blue-light',
    'skin-black-light',
    'skin-red-light',
    'skin-yellow-light',
    'skin-purple-light',
    'skin-green-light'
  ]

  /**
   * Get a prestored setting
   *
   * @param String name Name of of the setting
   * @returns String The value of the setting | null
   */
  function get(name) {
    if (typeof (Storage) !== 'undefined') {
      return localStorage.getItem(name)
    } else {
      window.alert('Please use a modern browser to properly view this template!')
    }
  }

  /**
   * Store a new settings in the browser
   *
   * @param String name Name of the setting
   * @param String val Value of the setting
   * @returns void
   */
  function store(name, val) {
    if (typeof (Storage) !== 'undefined') {
      localStorage.setItem(name, val)
    } else {
      window.alert('Please use a modern browser to properly view this template!')
    }
  }

  /**
   * Toggles layout classes
   *
   * @param String cls the layout class to toggle
   * @returns void
   */
  function changeLayout(cls) {
    $('body').toggleClass(cls)
    $layout.fixSidebar()
    if ($('body').hasClass('fixed') && cls == 'fixed') {
      $pushMenu.expandOnHover()
      $layout.activate()
    }
    $controlSidebar.fix()
  }

  /**
   * Replaces the old skin with the new skin
   * @param String cls the new skin class
   * @returns Boolean false to prevent link's default action
   */
  function changeSkin(cls) {
    $.each(mySkins, function (i) {
      $('body').removeClass(mySkins[i])
    })

    $('body').addClass(cls)
    store('skin', cls)
    return false
  }

  /**
   * Retrieve default settings and apply them to the template
   *
   * @returns void
   */
  function setup() {
    var tmp = get('skin')
    if (tmp && $.inArray(tmp, mySkins))
      changeSkin(tmp)

    // Add the change skin listener
    $('[data-skin]').on('click', function (e) {
      if ($(this).hasClass('knob'))
        return
      e.preventDefault()
      changeSkin($(this).data('skin'))
    })

    // Add the layout manager
    $('[data-layout]').on('click', function () {
      changeLayout($(this).data('layout'))
    })

    $('[data-controlsidebar]').on('click', function () {
      changeLayout($(this).data('controlsidebar'))
      var slide = !$controlSidebar.options.slide

      $controlSidebar.options.slide = slide
      if (!slide)
        $('.control-sidebar').removeClass('control-sidebar-open')
    })

    $('[data-sidebarskin="toggle"]').on('click', function () {
      var $sidebar = $('.control-sidebar')
      if ($sidebar.hasClass('control-sidebar-dark')) {
        $sidebar.removeClass('control-sidebar-dark')
        $sidebar.addClass('control-sidebar-light')
      } else {
        $sidebar.removeClass('control-sidebar-light')
        $sidebar.addClass('control-sidebar-dark')
      }
    })

    $('[data-enable="expandOnHover"]').on('click', function () {
      $(this).attr('disabled', true)
      $pushMenu.expandOnHover()
      if (!$('body').hasClass('sidebar-collapse'))
        $('[data-layout="sidebar-collapse"]').click()
    })

    //  Reset options
    if ($('body').hasClass('fixed')) {
      $('[data-layout="fixed"]').attr('checked', 'checked')
    }
    if ($('body').hasClass('layout-boxed')) {
      $('[data-layout="layout-boxed"]').attr('checked', 'checked')
    }
    if ($('body').hasClass('sidebar-collapse')) {
      $('[data-layout="sidebar-collapse"]').attr('checked', 'checked')
    }

  }

  // Create the new tab
  var $tabPane = $('<div />', {
    'id'   : 'control-sidebar-theme-demo-options-tab',
    'class': 'tab-pane active'
  })

  // Create the tab button
  var $tabButton = $('<li />', { 'class': 'active' })
    .html('<a href=\'#control-sidebar-theme-demo-options-tab\' data-toggle=\'tab\'>'
      + '<i class="fa fa-wrench"></i>'
      + '</a>')

  // Add the tab button to the right sidebar tabs
  $('[href="#control-sidebar-home-tab"]')
    .parent()
    .before($tabButton)

  // Create the menu
  var $demoSettings = $('<div />')

  // Layout options
  $demoSettings.append(
    '<h4 class="control-sidebar-heading">'
    + 'Layout Options'
    + '</h4>'
    // Fixed layout
    + '<div class="form-group">'
    + '<label class="control-sidebar-subheading">'
    + '<input type="checkbox"data-layout="fixed"class="pull-right"/> '
    + 'Fixed layout'
    + '</label>'
    + '<p>Activate the fixed layout. You can\'t use fixed and boxed layouts together</p>'
    + '</div>'
    // Boxed layout
    + '<div class="form-group">'
    + '<label class="control-sidebar-subheading">'
    + '<input type="checkbox"data-layout="layout-boxed" class="pull-right"/> '
    + 'Boxed Layout'
    + '</label>'
    + '<p>Activate the boxed layout</p>'
    + '</div>'
    // Sidebar Toggle
    + '<div class="form-group">'
    + '<label class="control-sidebar-subheading">'
    + '<input type="checkbox"data-layout="sidebar-collapse"class="pull-right"/> '
    + 'Toggle Sidebar'
    + '</label>'
    + '<p>Toggle the left sidebar\'s state (open or collapse)</p>'
    + '</div>'
    // Sidebar mini expand on hover toggle
    + '<div class="form-group">'
    + '<label class="control-sidebar-subheading">'
    + '<input type="checkbox"data-enable="expandOnHover"class="pull-right"/> '
    + 'Sidebar Expand on Hover'
    + '</label>'
    + '<p>Let the sidebar mini expand on hover</p>'
    + '</div>'
    // Control Sidebar Toggle
    + '<div class="form-group">'
    + '<label class="control-sidebar-subheading">'
    + '<input type="checkbox"data-controlsidebar="control-sidebar-open"class="pull-right"/> '
    + 'Toggle Right Sidebar Slide'
    + '</label>'
    + '<p>Toggle between slide over content and push content effects</p>'
    + '</div>'
    // Control Sidebar Skin Toggle
    + '<div class="form-group">'
    + '<label class="control-sidebar-subheading">'
    + '<input type="checkbox"data-sidebarskin="toggle"class="pull-right"/> '
    + 'Toggle Right Sidebar Skin'
    + '</label>'
    + '<p>Toggle between dark and light skins for the right sidebar</p>'
    + '</div>'
  )
  var $skinsList = $('<ul />', { 'class': 'list-unstyled clearfix' })

  // Dark sidebar skins
  var $skinBlue =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-blue" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Blue</p>')
  $skinsList.append($skinBlue)
  var $skinBlack =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-black" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Black</p>')
  $skinsList.append($skinBlack)
  var $skinPurple =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Purple</p>')
  $skinsList.append($skinPurple)
  var $skinGreen =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Green</p>')
  $skinsList.append($skinGreen)
  var $skinRed =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Red</p>')
  $skinsList.append($skinRed)
  var $skinYellow =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin">Yellow</p>')
  $skinsList.append($skinYellow)

  // Light sidebar skins
  var $skinBlueLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Blue Light</p>')
  $skinsList.append($skinBlueLight)
  var $skinBlackLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-black-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Black Light</p>')
  $skinsList.append($skinBlackLight)
  var $skinPurpleLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-purple-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Purple Light</p>')
  $skinsList.append($skinPurpleLight)
  var $skinGreenLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-green-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Green Light</p>')
  $skinsList.append($skinGreenLight)
  var $skinRedLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-red-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Red Light</p>')
  $skinsList.append($skinRedLight)
  var $skinYellowLight =
        $('<li />', { style: 'float:left; width: 33.33333%; padding: 5px;' })
          .append('<a href="javascript:void(0)" data-skin="skin-yellow-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">'
            + '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div>'
            + '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div>'
            + '</a>'
            + '<p class="text-center no-margin" style="font-size: 12px">Yellow Light</p>')
  $skinsList.append($skinYellowLight)

  $demoSettings.append('<h4 class="control-sidebar-heading">Skins</h4>')
  $demoSettings.append($skinsList)

  $tabPane.append($demoSettings)
  $('#control-sidebar-home-tab').after($tabPane)

  setup()

  $('[data-toggle="tooltip"]').tooltip()


  // loadPeg
  if ($('#load-p')[0]) {

    var loadPeg = $("#load-p");
    if (loadPeg.css('display') === 'none') {
      loadPeg.removeAttr('style');
    }
  }else{
    var el = $("#peg-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-p"><img src="'+ base_url+"assets/img/ajax-loader.gif" + '" alt=""> Loading . . .</div>';
      el.append(div);
  }

  // loadVer
  if ($('#load-ver')[0]) {
    var loadVer = $("#load-ver");
    if (loadVer.css('display') === 'none') {
      loadVer.removeAttr('style');
    }
  }else{
    var el = $("#ver-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-ver"><img src="'+ base_url + "assets/img/ajax-loader.gif" +'" alt=""> Loading . . .</div>'
      el.append(div);
  }

  // loadpaktif-table
  if ($('#load-paktif')[0]) {
    var loadPaktif = $("#load-paktif");
    if (loadPaktif.css('display') === 'none') {
      loadPaktif.removeAttr('style');
    }
  }else{
    var elaktif = $("#paktif-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-paktif"><img src="'+ base_url + "assets/img/ajax-loader.gif" +'" alt=""> Loading . . .</div>'
      elaktif.append(div);
  }

  // loadpstatus
  if ($('#load-pstatus')[0]) {
    var loadpstatus = $("#load-pstatus");
    if (loadpstatus.css('display') === 'none') {
      loadpstatus.removeAttr('style');
    }
  }else{
    var elaktif = $("#pst-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-pstatus"><img src="'+ base_url + "assets/img/ajax-loader.gif" +'" alt=""> Loading . . .</div>'
      elaktif.append(div);
  }

  // loadresign
  if ($('#load-resign')[0]) {
    var loadresign = $("#load-resign");
    if (loadresign.css('display') === 'none') {
      loadresign.removeAttr('style');
    }
  }else{
    var elaktif = $("#r-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-resign"><img src="'+ base_url + "assets/img/ajax-loader.gif" +'" alt=""> Loading . . .</div>'
      elaktif.append(div);
  }

  // loadsp
  if ($('#load-sp')[0]) {
    var loadsp = $("#load-sp");
    if (loadsp.css('display') === 'none') {
      loadsp.removeAttr('style');
    }
  }else{
    var elaktif = $("#spt-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-sp"><img src="'+ base_url + "assets/img/ajax-loader.gif" +'" alt=""> Loading . . .</div>'
      elaktif.append(div);
  }

  /*Kehadiran*/

  // loaddevinfo
  if ($('#load-devinfo')[0]) {
    var loaddevinfo = $("#load-devinfo");
    if (loaddevinfo.css('display') === 'none') {
      loaddevinfo.removeAttr('style');
    }
  }else{
    var elaktif = $("#devinfo-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-devinfo"><img src="'+ base_url + "assets/img/ajax-loader.gif" +'" alt=""> Loading . . .</div>'
      elaktif.append(div);
  }
  // loadlembur
  if ($('#load-lembur-1')[0]) {
    var loadlembur1 = $("#load-lembur-1");
    if (loadlembur1.css('display') === 'none') {
      loadlembur1.removeAttr('style');
    }
  }else{
    var elaktif = $("#lembr-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-lembur-1"><img src="'+ base_url + "assets/img/ajax-loader.gif" +'" alt=""> Loading . . .</div>'
      elaktif.append(div);
  }

  // loaddevman
  if ($('#load-devm')[0]) {
    var loaddevm = $("#load-devm");
    if (loaddevm.css('display') === 'none') {
      loaddevm.removeAttr('style');
    }
  }else{
    var elaktif = $("#devm-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-devm"><img src="'+ base_url + "assets/img/ajax-loader.gif" +'" alt=""> Loading . . .</div>'
      elaktif.append(div);
  }

  // -----------------------------------
  // Reports
  // loadtranslog
  if ($('#load-translog')[0]) {
    var loadtanslog = $("#load-translog");
    if (loadtanslog.css('display') === 'none') {
      loadtanslog.removeAttr('style');
    }
  }else{
    var elaktif = $("#trans-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-translog"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading . . .</div>'
      elaktif.append(div);
  }
  // In Out Report
  if ($('#load-inout')[0]) {
    var loadInOut = $("#load-inout");
    if (loadInOut.css('display') === 'none') {
      loadInOut.removeAttr('style');
    }
  }else{
    var elaktif = $("#inoutreport-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-inout"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading . . .</div>'
      elaktif.append(div);
  }
  // Attendance Report
  if ($('#load-attrep')[0]) {
    var loadattrep = $("#load-attrep");
    if (loadattrep.css('display') === 'none') {
      loadattrep.removeAttr('style');
    }
  }else{
    var elaktif = $("#attreport-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-attrep"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading . . .</div>'
      elaktif.append(div);
  }

  // Overtime Report
  if ($('#load-otrep')[0]) {
    var loadotrep = $("#load-otrep");
    if (loadotrep.css('display') === 'none') {
      loadotrep.removeAttr('style');
    }
  }else{
    var elaktif = $("#otreport-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-otrep"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading . . .</div>'
      elaktif.append(div);
  }

  // Detail Pegawai Report
  if ($('#load-detpeg')[0]) {
    var loaddetpeg = $("#load-detpeg");
    if (loaddetpeg.css('display') === 'none') {
      loaddetpeg.removeAttr('style');
    }
  }else{
    var elaktif = $("#detpeg-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-detpeg"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading . . .</div>'
      elaktif.append(div);
  } 
  // SP Report
  if ($('#load-sprep')[0]) {
    var loadsprep = $("#load-sprep");
    if (loadsprep.css('display') === 'none') {
      loadsprep.removeAttr('style');
    }
  }else{
    var elaktif = $("#sprep-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-sprep"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading . . .</div>'
      elaktif.append(div);
  }
  // Resign Report
  if ($('#load-resignrep')[0]) {
    var loadresignrep = $("#load-resignrep");
    if (loadresignrep.css('display') === 'none') {
      loadresignrep.removeAttr('style');
    }
  }else{
    var elaktif = $("#resignrep-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-resignrep"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading . . .</div>'
      elaktif.append(div);
  }
  // Inventaris Report
  if ($('#load-invrep')[0]) {
    var loadinvrep = $("#load-invrep");
    if (loadinvrep.css('display') === 'none') {
      loadinvrep.removeAttr('style');
    }
  }else{
    var elaktif = $("#invrep-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-invrep"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading . . .</div>'
      elaktif.append(div);
  }

  // -----------------------------------
  // Process
  // loadprocess
  if ($('#load-process')[0]) {
    var loadprocess = $("#load-process");
    if (loadprocess.css('display') === 'none') {
      loadprocess.removeAttr('style');
    }
  }
  else{
    var elaktif = $("#pros-table").parents().eq(2).find('h3');
    var div = '';
      div = '<div class="col-xs-12" id="load-process"><i class="fa fa-refresh fa-spin"></i>&nbsp;Loading . . .</div>'
      elaktif.append(div);
  }

})



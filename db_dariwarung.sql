-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_dariwarung.tbl_banner
CREATE TABLE IF NOT EXISTS `tbl_banner` (
  `id_banner` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(50) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  PRIMARY KEY (`id_banner`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_banner: ~5 rows (approximately)
/*!40000 ALTER TABLE `tbl_banner` DISABLE KEYS */;
INSERT INTO `tbl_banner` (`id_banner`, `page`, `position`, `title`, `image`, `link`) VALUES
	(1, 'home', 'top', 'jual barang jadi lebih mudah di dariwarung', 'img1.jpg', 'http://www.dariwarung.com/'),
	(2, 'home', 'left', 'beli kebutuhan poko dariwarung aja', 'img1.jpg', 'http://www.dariwarung.com/'),
	(3, 'home', 'center_left', 'beli kebutuhan poko dariwarung aja', 'img1.jpg', 'http://www.dariwarung.com/'),
	(4, 'home', 'left_list', 'beli kebutuhan poko dariwarung aja', 'img1.jpg', 'http://www.dariwarung.com/'),
	(18, 'home', 'center_right', 'beli kebutuhan poko dariwarung aja', 'img1.jpg', 'http://www.dariwarung.com/');
/*!40000 ALTER TABLE `tbl_banner` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_blog
CREATE TABLE IF NOT EXISTS `tbl_blog` (
  `id_blog` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL DEFAULT 0,
  `title` varchar(50) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `status` enum('Active','No Active') DEFAULT NULL,
  PRIMARY KEY (`id_blog`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_blog: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_blog` DISABLE KEYS */;
INSERT INTO `tbl_blog` (`id_blog`, `id_category`, `title`, `description`, `image`, `create_by`, `create_date`, `status`) VALUES
	(6, 2, 'sds ddcek', '<p>sdsa tetetetetetssgtststst</p>\r\n', '1588754447product-7.jpg', NULL, '2020-05-06 15:40:47', 'Active');
/*!40000 ALTER TABLE `tbl_blog` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_blog_category
CREATE TABLE IF NOT EXISTS `tbl_blog_category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_category`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_blog_category: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_blog_category` DISABLE KEYS */;
INSERT INTO `tbl_blog_category` (`id_category`, `title`, `image`) VALUES
	(2, 'Buah', '1588753119');
/*!40000 ALTER TABLE `tbl_blog_category` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_cart
CREATE TABLE IF NOT EXISTS `tbl_cart` (
  `id_cart` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_cart`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_cart: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_cart` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_company
CREATE TABLE IF NOT EXISTS `tbl_company` (
  `id_company` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address` tinytext DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `gplus` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `playstore` varchar(100) DEFAULT NULL,
  `appstore` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_company: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_company` DISABLE KEYS */;
INSERT INTO `tbl_company` (`id_company`, `company`, `name`, `address`, `phone`, `email`, `description`, `image`, `website`, `facebook`, `gplus`, `twitter`, `instagram`, `playstore`, `appstore`) VALUES
	(1, 'Mang Tatang Corp', 'DariWarung', 'Jl Majalaya', '08787728728', 'dariwarung@gmail.com', '-', 'logo.png', 'www.dariwarung.com', 'facebook.com', 'gplus.com', 'twitter.com', 'instagram.com', 'https://play.google.com/store/search?q=dariwarung', 'appstore.com');
/*!40000 ALTER TABLE `tbl_company` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_content
CREATE TABLE IF NOT EXISTS `tbl_content` (
  `id_content` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `googleplus` mediumtext DEFAULT NULL,
  `address` longtext DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `website` varchar(50) NOT NULL,
  `Column 16` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `tagline` mediumtext DEFAULT NULL,
  `twitter` mediumtext DEFAULT NULL,
  `facebook` mediumtext DEFAULT NULL,
  `instagram` mediumtext DEFAULT NULL,
  `youtube` mediumtext DEFAULT NULL,
  `promo_img1` varchar(50) DEFAULT NULL,
  `promo_img2` varchar(50) DEFAULT NULL,
  `promo_img3` varchar(50) DEFAULT NULL,
  `promo_img4` varchar(50) DEFAULT NULL,
  `promo_img5` varchar(50) DEFAULT NULL,
  `promo_link1` varchar(50) DEFAULT NULL,
  `promo_link2` varchar(50) DEFAULT NULL,
  `promo_link3` varchar(50) DEFAULT NULL,
  `promo_link4` varchar(50) DEFAULT NULL,
  `promo_link5` varchar(50) DEFAULT NULL,
  `promo_title1` varchar(50) DEFAULT NULL,
  `promo_title2` varchar(50) DEFAULT NULL,
  `promo_title3` varchar(50) DEFAULT NULL,
  `promo_title4` varchar(50) DEFAULT NULL,
  `promo_title5` varchar(50) DEFAULT NULL,
  `image_banner` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_content`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_content: ~9 rows (approximately)
/*!40000 ALTER TABLE `tbl_content` DISABLE KEYS */;
INSERT INTO `tbl_content` (`id_content`, `title`, `description`, `googleplus`, `address`, `email`, `website`, `Column 16`, `phone`, `tagline`, `twitter`, `facebook`, `instagram`, `youtube`, `promo_img1`, `promo_img2`, `promo_img3`, `promo_img4`, `promo_img5`, `promo_link1`, `promo_link2`, `promo_link3`, `promo_link4`, `promo_link5`, `promo_title1`, `promo_title2`, `promo_title3`, `promo_title4`, `promo_title5`, `image_banner`) VALUES
	(1, 'About', '<div class="mb-12 text-center">\r\n                    <h1>Tentang Dariwarung</h1>\r\n                    <p class="text-gray-44">Sejarah Singkat, DariWarung Mang Tatang Corp</p>\r\n                </div>\r\n                \r\n                </div>', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Contact', '<p>asfasfasdas12312</p>\r\n', NULL, 'JL. Rancajigang, No. 180, Padamulya, Majalaya, 40392', 'info@yourdomain.com', 'https://www.idredline.com', NULL, '+62 811-240-4327', 'Sebagai platform penjualan baru yang menyediakan produk-produk dengan harga SPECIAL, fokus pada kebutuhan rumah tangga sehari-hari, terutama makanan. ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Privacy Policy', '<p>aasdas</p>\r\n', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'Term Conditions', '<p>12311111</p>\r\n', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'Sosial Media', NULL, '', NULL, NULL, '', NULL, NULL, NULL, 'https://www.twitter.com/idredlinebakery', 'https://www.facebook.com/idredlinebakery', 'https://www.instagram.com/idredlinebakery', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'Faqs', '<p>faqs</p>\r\n', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'Journal', '<p>journalsdfs</p>\r\n', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 'Promotion', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1588843313category.jpg', '1588843313category-1.jpg', '1588843313category-2.jpg', '1588843313category-3.jpg', '1588843313category-4.jpg', '', '', '', '', '', '', 'Bagelen', 'Roti Tawar', 'Cake', 'Sandwich', NULL),
	(11, 'Help', '<div class="mb-12 text-center">\r\n                    <h1>Bantuan</h1>\r\n                    <p class="text-gray-44">Informasi & Bantuan</p>\r\n                </div>\r\n                \r\n                </div>', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tbl_content` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_member
CREATE TABLE IF NOT EXISTS `tbl_member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `update_lat` varchar(50) DEFAULT NULL,
  `update_lon` varchar(50) DEFAULT NULL,
  `image` mediumtext DEFAULT NULL,
  `type_seller` enum('Premium','Basic') DEFAULT NULL,
  `status` enum('Active','No Active') DEFAULT 'Active',
  PRIMARY KEY (`id_member`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_member: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_member` DISABLE KEYS */;
INSERT INTO `tbl_member` (`id_member`, `fullname`, `email`, `username`, `password`, `address`, `phone`, `update_lat`, `update_lon`, `image`, `type_seller`, `status`) VALUES
	(5, 'member', 'member@gmail.com', 'member', '81dc9bdb52d04dc20036dbd8313ed055', 'member list ', '0101010101', '010101010', '0101010101', NULL, 'Premium', 'Active'),
	(23, 'Yogi gumbira', 'ghiltea@gmail.com', NULL, '81dc9bdb52d04dc20036dbd8313ed055', NULL, '087822572787', NULL, NULL, NULL, 'Basic', 'Active'),
	(24, 'te', 'ghil@g.com', NULL, '81dc9bdb52d04dc20036dbd8313ed055', NULL, '0808080808', NULL, NULL, NULL, NULL, 'Active');
/*!40000 ALTER TABLE `tbl_member` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_message
CREATE TABLE IF NOT EXISTS `tbl_message` (
  `id_message` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `message` longtext DEFAULT NULL,
  PRIMARY KEY (`id_message`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_message: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_message` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_order
CREATE TABLE IF NOT EXISTS `tbl_order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `code_order` varchar(50) DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `total_qty` int(11) DEFAULT NULL,
  `total_discount` int(11) DEFAULT NULL,
  `total_delivery` int(11) DEFAULT NULL,
  `total_tax` int(11) DEFAULT NULL,
  `total_amount` int(11) DEFAULT NULL,
  `status` enum('Paid','Unpaid','Pending','Proses','Delivery','Accept','Finish') DEFAULT 'Unpaid',
  PRIMARY KEY (`id_order`),
  UNIQUE KEY `code_order` (`code_order`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_order: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_order` DISABLE KEYS */;
INSERT INTO `tbl_order` (`id_order`, `code_order`, `id_member`, `date`, `total_qty`, `total_discount`, `total_delivery`, `total_tax`, `total_amount`, `status`) VALUES
	(11, 'PO1589444625', 5, '2020-05-14 15:23:45', 3, 0, 0, 0, 44000, 'Pending'),
	(12, 'PO1589444661', 5, '2020-05-14 15:24:21', 3, 0, 0, 0, 44000, 'Unpaid'),
	(13, 'PO1589444701', 5, '2020-05-14 15:25:01', 1, 0, 0, 0, 9000, 'Unpaid'),
	(14, 'PO1589786099', 5, '2020-05-18 14:14:59', 4, 0, 0, 0, 36000, 'Unpaid'),
	(15, 'PO1590572095', 5, '2020-05-27 16:34:55', 1, 0, 0, 0, 9000, 'Unpaid'),
	(16, 'PO1591934519', 5, '2020-06-12 11:01:59', 7, 0, 0, 0, 92000, 'Unpaid');
/*!40000 ALTER TABLE `tbl_order` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_order_detail
CREATE TABLE IF NOT EXISTS `tbl_order_detail` (
  `id_order_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) DEFAULT NULL,
  `code_order` varchar(50) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `id_vendor` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `price_net` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_order_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_order_detail: ~11 rows (approximately)
/*!40000 ALTER TABLE `tbl_order_detail` DISABLE KEYS */;
INSERT INTO `tbl_order_detail` (`id_order_detail`, `id_order`, `code_order`, `id_product`, `id_vendor`, `name`, `price`, `discount`, `price_net`, `qty`, `amount`) VALUES
	(12, 11, 'PO1589444625', 16, NULL, 'Productadmin', 10000, 10, 9000, 1, 9000),
	(13, 11, 'PO1589444625', 18, 6, 'Productvendor2', 15000, 0, 15000, 1, 15000),
	(14, 11, 'PO1589444625', 17, 2, 'Productvendor1', 20000, 0, 20000, 1, 20000),
	(15, 12, 'PO1589444661', 25, NULL, 'Productadmin', 10000, 10, 9000, 1, 9000),
	(16, 12, 'PO1589444661', 17, 2, 'Productvendor1', 20000, 0, 20000, 1, 20000),
	(17, 12, 'PO1589444661', 18, 2, 'Productvendor2', 15000, 0, 15000, 1, 15000),
	(18, 13, 'PO1589444701', 20, NULL, 'Productadmin', 10000, 10, 9000, 1, 9000),
	(19, 14, 'PO1589786099', 23, NULL, 'Productadmin', 10000, 10, 9000, 4, 36000),
	(20, 15, 'PO1590572095', 16, NULL, 'Productadmin', 10000, 10, 9000, 1, 9000),
	(21, 16, 'PO1591934519', 22, NULL, 'Paroti Cream Messes 200g', 15000, 10, 13500, 2, 27000),
	(22, 16, 'PO1591934519', 18, 6, 'Paroti Tawar Kupas 200g', 13000, 0, 13000, 5, 65000);
/*!40000 ALTER TABLE `tbl_order_detail` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_product
CREATE TABLE IF NOT EXISTS `tbl_product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `code_product` varchar(50) DEFAULT NULL,
  `code_sku` varchar(50) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `price_buy` int(11) DEFAULT NULL,
  `price_sell` int(11) DEFAULT NULL,
  `margin` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `featured` enum('Terbaru','Terlaris','Promo') DEFAULT 'Terbaru',
  `status` enum('Aktif','Tidak Aktif') DEFAULT NULL,
  `seo_title` tinytext NOT NULL,
  `seo_keyword` tinytext NOT NULL,
  `seo_description` tinytext NOT NULL,
  `seo_canonical` tinytext NOT NULL,
  `seo_img_alt` tinytext NOT NULL,
  PRIMARY KEY (`id_product`),
  UNIQUE KEY `sku_product` (`code_product`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_product: ~7 rows (approximately)
/*!40000 ALTER TABLE `tbl_product` DISABLE KEYS */;
INSERT INTO `tbl_product` (`id_product`, `id_category`, `id_member`, `code_product`, `code_sku`, `name`, `description`, `price_buy`, `price_sell`, `margin`, `discount`, `image`, `weight`, `featured`, `status`, `seo_title`, `seo_keyword`, `seo_description`, `seo_canonical`, `seo_img_alt`) VALUES
	(16, 4, NULL, 'PKT-001', NULL, 'Paket 1 - Oval Kecil Butter (OKBT)', '<p>Dari paket ini kamu akan mendapatkan item sebagai berikut:<br />\r\n<br />\r\n- Oval Kecil Butter (OKBT) 3 pcs<br />\r\n<br />\r\nDengan paket ini kamu sudah hemat 5% lho!</p>\r\n', NULL, 54000, NULL, 5, '1592628273Paket-1C.jpg', 1000, 'Terbaru', '', '', '                                                    ', '                                                    ', '                                                    ', '                                                    '),
	(18, 8, NULL, 'PKT-004', NULL, 'Paket 4 - Toast Kotak (TSKT) + Toast Kupas (TSKP) ', '<p>Dari paket ini kamu akan mendapatkan item sebagai berikut:<br />\r\n<br />\r\n- Toast Kotak (TSKT) 1 pcs<br />\r\n- Toast Kupas (TSKP) 1 pcs<br />\r\n- Oval Kecil Keju (OKKJ) 1 pcs<br />\r\n<br />\r\nDengan paket ini kamu sudah hemat 5% lho!</p>\r\n', NULL, 45500, NULL, 5, '1592628689Paket-4C.jpg', 1000, 'Terlaris', '', '', '', '', '', ''),
	(19, 12, NULL, 'PKT-006', NULL, 'Paket 6 - Bagelen Kering Isi 5 (BGK5) + Toast Panjang (TSPJ) + Sisir Mentega (SSRM) + Coklat Premium (CKPM)', '<p>Dari paket ini kamu akan mendapatkan item sebagai berikut:<br />\r\n<br />\r\n- Bagelen Kering Isi 5 (BGK5) 1 pcs<br />\r\n- Toast Panjang (TSPJ) 1 pcs<br />\r\n- Sisir Mentega (SSRM) 1 pcs<br />\r\n- Coklat Premium (CKPM) 1 pcs<br />\r\n<br />\r\nDengan paket ini kamu sudah hemat 5% lho!</p>\r\n', NULL, 51000, NULL, 5, '1592628855Paket-6C.jpg', 1000, 'Promo', '', '                                                    ', '                                                    ', '                                                    ', '                                                    ', '                                                    '),
	(20, 4, NULL, 'PKT-002', NULL, 'Paket 2 - Cream Messes (CRMS)', '<p>Dari paket ini kamu akan mendapatkan item sebagai berikut:<br />\r\n<br />\r\n- Cream Messes (CRMS) 3 pcs<br />\r\n<br />\r\nDengan paket ini kamu sudah hemat 5% lho!</p>\r\n', NULL, 60000, NULL, 5, '1592628398Paket-2C.jpg', 1000, 'Terbaru', '', '', '', '', '', ''),
	(21, 8, NULL, 'PKT-003', NULL, 'Paket 3 - Toast Panjang (TSPJ) + Coklat Premium (CKPM)', '<p>Dari paket ini kamu akan mendapatkan item sebagai berikut:<br />\r\n<br />\r\n- Toast Panjang (TSPJ) 2 pcs<br />\r\n- Coklat Premium (CKPM) 2 pcs<br />\r\n<br />\r\nDengan paket ini kamu sudah hemat 5% lho!</p>\r\n', NULL, 46000, NULL, 5, '1592628526Paket-3C.jpg', 1000, 'Terlaris', '', '                                                    ', '                                                    ', '                                                    ', '                                                    ', '                                                    '),
	(23, 12, NULL, 'PKT-005', NULL, 'Paket 5 - Mini Disc Coklat (MDCK) + Oval Kecil Keju (OKKJ)', '<p>Dari paket ini kamu akan mendapatkan item sebagai berikut:<br />\r\n<br />\r\n- Mini Disc Coklat (MDCK) 2 pcs<br />\r\n- Oval Kecil Keju (OKKJ) 1 pcs<br />\r\n<br />\r\nDengan paket ini kamu sudah hemat 5% lho!</p>\r\n', NULL, 70000, NULL, 5, '1592628768Paket-5C.jpg', 1000, 'Promo', '', '                                                    ', '                                                    ', '                                                    ', '                                                    ', '                                                    '),
	(25, 12, NULL, 'PKT-007', NULL, 'Paket 7 - Mini Disc Coklat (MDCK) + Oval Kevil Kej', '<p>Dari paket ini kamu akan mendapatkan item sebagai berikut:<br />\r\n<br />\r\n- Mini Disc Coklat (MDCK) 1 pcs<br />\r\n- Oval Kecil Keju (OKKJ) 1 pcs<br />\r\n- Sisir Mentega (SSRM) 2 pcs<br />\r\n<br />\r\nDengan paket ini kamu sudah hemat 5% lho!</p>\r\n', NULL, 65000, NULL, 5, '1592628964Paket-7C.jpg', 1000, 'Promo', '', '', '', '', '', '');
/*!40000 ALTER TABLE `tbl_product` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_product_category
CREATE TABLE IF NOT EXISTS `tbl_product_category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `image` text DEFAULT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_product_category: ~18 rows (approximately)
/*!40000 ALTER TABLE `tbl_product_category` DISABLE KEYS */;
INSERT INTO `tbl_product_category` (`id_category`, `name`, `image`) VALUES
	(3, 'Fashion Pria', '1588900138'),
	(4, 'Makanan Kaleng', '1588900209'),
	(5, 'Minuman Kaleng', '1588900219'),
	(6, 'Fashion Wanita', NULL),
	(7, 'Fashion Anak - Anak', NULL),
	(8, 'Bajigur Haneut', NULL),
	(9, 'Bajigur Haneut', NULL),
	(10, 'Bajigur Haneut', NULL),
	(11, 'Bajigur Haneut', NULL),
	(12, 'Bajigur Haneut', NULL),
	(13, 'Bajigur Haneut', NULL),
	(14, 'Bajigur Haneut', NULL),
	(15, 'Bajigur Haneut', NULL),
	(16, 'Bajigur Haneut', NULL),
	(17, 'Bajigur Haneut', NULL),
	(18, 'Bajigur Haneut', NULL),
	(19, 'Bajigur Haneut', NULL),
	(20, 'Bajigur Haneut', NULL);
/*!40000 ALTER TABLE `tbl_product_category` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_role
CREATE TABLE IF NOT EXISTS `tbl_role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT NULL,
  `role_access` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_role: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_role` DISABLE KEYS */;
INSERT INTO `tbl_role` (`id_role`, `role_name`, `role_access`) VALUES
	(1, 'Administrator', NULL),
	(2, 'User', NULL);
/*!40000 ALTER TABLE `tbl_role` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_setting
CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(50) NOT NULL DEFAULT '',
  `script_include` longtext DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_setting`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_setting: ~5 rows (approximately)
/*!40000 ALTER TABLE `tbl_setting` DISABLE KEYS */;
INSERT INTO `tbl_setting` (`id_setting`, `page`, `script_include`, `status`) VALUES
	(6, 'home', '                            <!-- Search Engine -->\r\n<meta name="description" content="Menjual berbagai jenis Paket Roti Sharon Bakery. Roti bagelen, roti kering, roti manis dan kue. Dikirim langsung kerumah kamu. Ada diskon menariknya juga lho!">\r\n<meta name="image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<meta name="keywords" content="redline, idredline, redline bakery, redline indonesia, sharon bakery, roti sharon, pt multi star rukun abadi, beli roti online, roti murah, roti pasar, roti bagelen, roti bagelen di bandung, roti bagelen sharon, roti sharon, roti online murah">\r\n<link rel="canonical" href="https://www.idredline.com" />\r\n<!-- Schema.org for Google -->\r\n<meta itemprop="name" content="Redline - Jual Paket Roti Online Sharon Bakery">\r\n<meta itemprop="description" content="Menjual berbagai jenis Paket Roti Sharon Bakery. Roti bagelen, roti kering, roti manis dan kue. Dikirim langsung kerumah kamu. Ada diskon menariknya juga lho!">\r\n<meta itemprop="image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Twitter -->\r\n<meta name="twitter:card" content="summary">\r\n<meta name="twitter:title" content="Redline - Jual Paket Roti Online Sharon Bakery">\r\n<meta name="twitter:description" content="Menjual berbagai jenis Paket Roti Sharon Bakery. Roti bagelen, roti kering, roti manis dan kue. Dikirim langsung kerumah kamu. Ada diskon menariknya juga lho!">\r\n<meta name="twitter:image:src" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Open Graph general (Facebook, Pinterest & Google+) -->\r\n<meta property="og:title" content="Redline - Jual Paket Roti Online Sharon Bakery">\r\n<meta property="og:description" content="Menjual berbagai jenis Paket Roti Sharon Bakery. Roti bagelen, roti kering, roti manis dan kue. Dikirim langsung kerumah kamu. Ada diskon menariknya juga lho!">\r\n<meta property="og:image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<meta property="og:url" content="https://www.idredline.com">\r\n<meta property="og:site_name" content="Redline">\r\n<meta property="og:locale" content="id">\r\n<meta property="fb:admins" content="102227581541396">\r\n<meta property="og:type" content="website">\r\n                                                ', 'Active'),
	(7, 'shop', '                           <!-- Search Engine -->\r\n\r\n\r\n<!-- Schema.org for Google -->\r\n<meta itemprop="name" content="Jual Produk Roti Online Sharon Bakery | Redline">\r\n<meta itemprop="description" content="Kami menyediakan berbagai macam jenis roti, roti bagelen, roti tawar, roti manis, roti kering dan kue. Berbagai macam roti bisa kamu pilih disini.">\r\n<link rel="canonical" href="https://www.idredline.com/shop/" />\r\n<meta name="keywords" content="redline, sharon bakery, roti sharon, pt multi star rukun abadi, beli roti online, roti murah, roti pasar, roti bagelen, roti bagelen di bandung, roti bagelen sharon, roti sharon, roti online murah">\r\n<meta itemprop="image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Twitter -->\r\n<meta name="twitter:card" content="summary">\r\n<meta name="twitter:title" content="Jual Produk Roti Online Sharon Bakery | Redline">\r\n<meta name="twitter:description" content="Kami menyediakan berbagai macam jenis roti, roti bagelen, roti tawar, roti manis, roti kering dan kue. Berbagai macam roti bisa kamu pilih disini.">\r\n<meta name="twitter:image:src" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Open Graph general (Facebook, Pinterest & Google+) -->\r\n<meta property="og:title" content="Jual Produk Roti Online Sharon Bakery | Redline">\r\n<meta property="og:description" content="Kami menyediakan berbagai macam jenis roti, roti bagelen, roti tawar, roti manis, roti kering dan kue. Berbagai macam roti bisa kamu pilih disini.">\r\n<meta property="og:image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<meta property="og:url" content="https://www.idredline.com/beli-roti-online/">\r\n<meta property="og:site_name" content="Redline">\r\n<meta property="og:locale" content="id">\r\n<meta property="fb:admins" content="102227581541396">\r\n<meta property="og:type" content="website">                        ', 'Active'),
	(8, 'about', '                           <!-- Search Engine -->\r\n<meta name="description" content="Pengen lebih jauh kenal dengan Redline? Yuk kunjungi halaman tentang kami. Disana kami akan bercerita banyak tentang Redline.">\r\n<meta name="image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<meta name="keywords" content="redline, idredline, redline bakery, redline indonesia,  sharon bakery, roti sharon, pt multi star rukun abadi, beli roti online, roti murah, roti pasar, roti bagelen, roti bagelen di bandung, roti bagelen sharon, roti sharon, tentang redline, tentang perusahaan redline, roti online murah">\r\n<link rel="canonical" href="https://www.idredline.com/about/" />\r\n<!-- Schema.org for Google -->\r\n<meta itemprop="name" content="Kenalan Lebih Jauh Tentang Kami | Redline">\r\n<meta itemprop="description" content="Pengen lebih jauh kenal dengan Redline? Yuk kunjungi halaman tentang kami. Disana kami akan bercerita banyak tentang Redline.">\r\n<meta itemprop="image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Twitter -->\r\n<meta name="twitter:card" content="summary">\r\n<meta name="twitter:title" content="Kenalan Lebih Jauh Tentang Kami | Redline">\r\n<meta name="twitter:description" content="Pengen lebih jauh kenal dengan Redline? Yuk kunjungi halaman tentang kami. Disana kami akan bercerita banyak tentang Redline.">\r\n<meta name="twitter:image:src" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Open Graph general (Facebook, Pinterest & Google+) -->\r\n<meta property="og:title" content="Kenalan Lebih Jauh Tentang Kami | Redline">\r\n<meta property="og:description" content="Pengen lebih jauh kenal dengan Redline? Yuk kunjungi halaman tentang kami. Disana kami akan bercerita banyak tentang Redline.">\r\n<meta property="og:image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<meta property="og:url" content="https://www.idredline.com/tentang-kami/">\r\n<meta property="og:site_name" content="Redline">\r\n<meta property="og:locale" content="id">\r\n<meta property="fb:admins" content="102227581541396">\r\n<meta property="og:type" content="website">                        ', 'Active'),
	(9, 'blog', '                           <!-- Search Engine -->\r\n<meta name="description" content="Cari berita, artikel,dan tips menarik lainnya. Dirangkum khusus oleh tim Redline hanya untuk kamu.">\r\n<meta name="image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<meta name="keywords" content="redline, idredline, redline bakery, redline indonesia,  sharon bakery, roti sharon, pt multi star rukun abadi, beli roti online, roti murah, roti pasar, roti bagelen, roti bagelen di bandung, roti bagelen sharon, roti sharon, cerita terbaru redline, blog redline, redline blog, cerita mengenai redline, roti online murah">\r\n<link rel="canonical" href="https://www.idredline.com/blog/" />\r\n<!-- Schema.org for Google -->\r\n<meta itemprop="name" content="Cerita Blog Terbaru Dari Redline | Redline">\r\n<meta itemprop="description" content="Cari berita, artikel,dan tips menarik lainnya. Dirangkum khusus oleh tim Redline hanya untuk kamu.">\r\n<meta itemprop="image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Twitter -->\r\n<meta name="twitter:card" content="summary">\r\n<meta name="twitter:title" content="Cerita Blog Terbaru Dari Redline | Redline">\r\n<meta name="twitter:description" content="Cari berita, artikel,dan tips menarik lainnya. Dirangkum khusus oleh tim Redline hanya untuk kamu.">\r\n<meta name="twitter:image:src" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Open Graph general (Facebook, Pinterest & Google+) -->\r\n<meta property="og:title" content="Cerita Blog Terbaru Dari Redline | Redline">\r\n<meta property="og:description" content="Cari berita, artikel,dan tips menarik lainnya. Dirangkum khusus oleh tim Redline hanya untuk kamu.">\r\n<meta property="og:image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<meta property="og:url" content="https://www.idredline.com/blog/">\r\n<meta property="og:site_name" content="Redline">\r\n<meta property="og:locale" content="id">\r\n<meta property="fb:admins" content="102227581541396">\r\n<meta property="og:type" content="website">                        ', 'Active'),
	(10, 'contact', '                                                      <!-- Search Engine -->\r\n<meta name="description" content="Hubungi tim Redline kami, jika kamu punya saran dan kritik. Tim kami siap untuk menampung masukkan dari kamu.!">\r\n<meta name="image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<meta name="keywords" content="redline, idredline, redline bakery, redline indonesia, sharon bakery, roti sharon, pt multi star rukun abadi, beli roti online, roti murah, roti pasar, roti bagelen, roti bagelen di bandung, roti bagelen sharon, roti sharon, contact center redline, hubungi redline, hubungi kami di redline, tim contact center redline, roti online murah">\r\n<link rel="canonical" href="https://www.idredline.com/contact/" />\r\n<!-- Schema.org for Google -->\r\n<meta itemprop="name" content="Hubungi Kami | Redline">\r\n<meta itemprop="description" content="Hubungi tim Redline kami, jika kamu punya saran dan kritik. Tim kami siap untuk menampung masukkan dari kamu.!">\r\n<meta itemprop="image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Twitter -->\r\n<meta name="twitter:card" content="summary">\r\n<meta name="twitter:title" content="Hubungi Kami | Redline">\r\n<meta name="twitter:description" content="Hubungi tim Redline kami, jika kamu punya saran dan kritik. Tim kami siap untuk menampung masukkan dari kamu.!">\r\n<meta name="twitter:image:src" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<!-- Open Graph general (Facebook, Pinterest & Google+) -->\r\n<meta property="og:title" content="Hubungi Kami | Redline">\r\n<meta property="og:description" content="Hubungi tim Redline kami, jika kamu punya saran dan kritik. Tim kami siap untuk menampung masukkan dari kamu.!">\r\n<meta property="og:image" content="https://www.idredline.com/uploads/banner/ogp-redline.jpeg">\r\n<meta property="og:url" content="https://www.idredline.com/hubungi-kami/">\r\n<meta property="og:site_name" content="Redline">\r\n<meta property="og:locale" content="id">\r\n<meta property="fb:admins" content="102227581541396">\r\n<meta property="og:type" content="website">\r\n                                                ', 'Active');
/*!40000 ALTER TABLE `tbl_setting` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_slide
CREATE TABLE IF NOT EXISTS `tbl_slide` (
  `id_slide` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  PRIMARY KEY (`id_slide`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_slide: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_slide` DISABLE KEYS */;
INSERT INTO `tbl_slide` (`id_slide`, `title`, `image`, `description`, `link`) VALUES
	(13, 'Diproduksi dengan cara TERBAIK', 'bg_11-min.jpg', NULL, NULL),
	(14, 'Dibuat oleh orang TERBAIK', 'bg_22-min.jpg', NULL, NULL);
/*!40000 ALTER TABLE `tbl_slide` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_store
CREATE TABLE IF NOT EXISTS `tbl_store` (
  `id_store` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `store_name` varchar(100) DEFAULT NULL,
  `category_store` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `create_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_store`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_store: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_store` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_subcribe
CREATE TABLE IF NOT EXISTS `tbl_subcribe` (
  `id_subscribe` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id_subscribe`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_subcribe: ~12 rows (approximately)
/*!40000 ALTER TABLE `tbl_subcribe` DISABLE KEYS */;
INSERT INTO `tbl_subcribe` (`id_subscribe`, `email`) VALUES
	(29, ''),
	(14, 'bebek@gmail.com'),
	(23, 'c'),
	(20, 'd'),
	(18, 'ds'),
	(24, 'f'),
	(10, 'fdsd'),
	(11, 'gh'),
	(15, 'ghil@fg.com'),
	(32, 'ghil@gmail.com'),
	(9, 'ghiltea@gmail.com'),
	(13, 'john@gmail.com');
/*!40000 ALTER TABLE `tbl_subcribe` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_subcribe_brodcast
CREATE TABLE IF NOT EXISTS `tbl_subcribe_brodcast` (
  `id_brodcast` int(11) NOT NULL,
  `title` varchar(50) NOT NULL DEFAULT '',
  `description` longtext NOT NULL DEFAULT '',
  `create_by` longtext NOT NULL DEFAULT '',
  `update_by` longtext NOT NULL DEFAULT '',
  PRIMARY KEY (`id_brodcast`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_subcribe_brodcast: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_subcribe_brodcast` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_subcribe_brodcast` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_testimoni
CREATE TABLE IF NOT EXISTS `tbl_testimoni` (
  `id_testimoni` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` enum('Active','No Active') DEFAULT NULL,
  `create_by` varchar(50) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_testimoni`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_testimoni: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_testimoni` DISABLE KEYS */;
INSERT INTO `tbl_testimoni` (`id_testimoni`, `name`, `position`, `message`, `status`, `create_by`, `create_date`) VALUES
	(2, 'wef22', 'aefwe22', '<p>wefwewef222</p>\r\n', 'No Active', 'Administrator', '2020-05-07 13:32:12');
/*!40000 ALTER TABLE `tbl_testimoni` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `status` enum('Active','No Active') DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_user: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`id_user`, `id_role`, `username`, `password`, `status`) VALUES
	(1, 1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'Active'),
	(4, 2, 'user', '81dc9bdb52d04dc20036dbd8313ed055', 'Active');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;

-- Dumping structure for table db_dariwarung.tbl_whislist
CREATE TABLE IF NOT EXISTS `tbl_whislist` (
  `id_whislist` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_whislist`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_dariwarung.tbl_whislist: ~5 rows (approximately)
/*!40000 ALTER TABLE `tbl_whislist` DISABLE KEYS */;
INSERT INTO `tbl_whislist` (`id_whislist`, `id_member`, `id_product`, `name`, `price`, `qty`, `discount`, `amount`, `weight`, `create_date`) VALUES
	(14, 5, 2, 'cek', 200000, 1, 10, 200000, 1000, '2020-05-12 11:55:40'),
	(15, 5, 2, 'cek', 200000, 1, 10, 200000, 1000, '2020-05-12 11:56:40'),
	(19, NULL, 11, 't1', 10000, 1, 0, 10000, 1000, '2020-05-13 23:35:15'),
	(20, 10, 12, 't2', 20000, 1, 0, 20000, 1000, '2020-05-14 00:55:25'),
	(21, 5, 16, 'Productadmin', 10000, 1, 10, 10000, 1000, '2020-05-27 16:34:27');
/*!40000 ALTER TABLE `tbl_whislist` ENABLE KEYS */;

-- Dumping structure for view db_dariwarung.v_order
/* SQL Error (1356): View 'db_dariwarung.v_order' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them */-- Dumping structure for view db_dariwarung.v_order_vendor
/* SQL Error (1356): View 'db_dariwarung.v_order_vendor' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them */-- Dumping structure for view db_dariwarung.v_order_vendor_sum
/* SQL Error (1356): View 'db_dariwarung.v_order_vendor_sum' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them */-- Dumping structure for view db_dariwarung.v_order
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_order`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_order` AS select `tbl_order_detail`.`id_order` AS `id_order`,`tbl_order_detail`.`code_order` AS `code_order`,`tbl_order_detail`.`id_product` AS `id_product`,`tbl_order_detail`.`id_vendor` AS `id_vendor`,`tbl_order_detail`.`name` AS `name`,`tbl_order_detail`.`price` AS `price`,`tbl_order_detail`.`discount` AS `discount`,`tbl_order_detail`.`price_net` AS `price_net`,`tbl_order_detail`.`qty` AS `qty`,`tbl_order_detail`.`amount` AS `amount`,`tbl_order`.`id_member` AS `id_member`,`tbl_order`.`total_qty` AS `total_qty`,`tbl_order`.`total_discount` AS `total_discount`,`tbl_order`.`total_tax` AS `total_tax`,`tbl_order`.`total_delivery` AS `total_delivery`,`tbl_order`.`total_amount` AS `total_amount`,`tbl_order`.`status` AS `status`,`tbl_order`.`date` AS `date`,`tbl_order_detail`.`id_order_detail` AS `id_order_detail`,`tbl_member`.`fullname` AS `fullname`,`tbl_member`.`email` AS `email`,`tbl_member`.`address` AS `address`,`tbl_member`.`phone` AS `phone` from ((`tbl_order` left join `tbl_order_detail` on(`tbl_order`.`id_order` = `tbl_order_detail`.`id_order`)) left join `tbl_member` on(`tbl_order`.`id_member` = `tbl_member`.`id_member`)) group by `tbl_order`.`id_order`,`tbl_order_detail`.`id_product`,`tbl_order_detail`.`id_vendor` ;

-- Dumping structure for view db_dariwarung.v_order_vendor
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_order_vendor`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_order_vendor` AS select distinct `tbl_order_detail`.`id_vendor` AS `id_vendor`,`tbl_order_detail`.`name` AS `name`,`tbl_order_detail`.`amount` AS `amount`,sum(`tbl_order_detail`.`qty`) AS `tot_qty` from (`tbl_product` join `tbl_order_detail` on(`tbl_product`.`id_product` = `tbl_order_detail`.`id_product`)) group by `tbl_order_detail`.`id_product` ;

-- Dumping structure for view db_dariwarung.v_order_vendor_sum
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_order_vendor_sum`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_order_vendor_sum` AS select `tbl_order_detail`.`id_order` AS `id_order`,`tbl_order_detail`.`code_order` AS `code_order`,`tbl_order_detail`.`id_product` AS `id_product`,`tbl_order_detail`.`id_vendor` AS `id_vendor`,`tbl_order_detail`.`name` AS `name`,`tbl_order_detail`.`price` AS `price`,`tbl_order_detail`.`discount` AS `discount`,`tbl_order_detail`.`price_net` AS `price_net`,sum(`tbl_order_detail`.`qty`) AS `tot_qty`,sum(`tbl_order_detail`.`amount`) AS `tot_amount`,`tbl_order`.`id_member` AS `id_member`,`tbl_order`.`total_qty` AS `total_qty`,`tbl_order`.`total_discount` AS `total_discount`,`tbl_order`.`total_tax` AS `total_tax`,`tbl_order`.`total_delivery` AS `total_delivery`,`tbl_order`.`total_amount` AS `total_amount`,`tbl_order`.`status` AS `status`,`tbl_order`.`date` AS `date`,`tbl_order_detail`.`id_order_detail` AS `id_order_detail`,`tbl_member`.`fullname` AS `fullname`,`tbl_member`.`email` AS `email`,`tbl_member`.`address` AS `address`,`tbl_member`.`phone` AS `phone` from ((`tbl_order` left join `tbl_order_detail` on(`tbl_order`.`id_order` = `tbl_order_detail`.`id_order`)) left join `tbl_member` on(`tbl_order`.`id_member` = `tbl_member`.`id_member`)) group by `tbl_order`.`id_order`,`tbl_order_detail`.`id_vendor` ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

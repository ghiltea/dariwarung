<?php

class ListProduct_m extends CI_Model {

    function get_list($id) {
        $this->db->where('id_direct', $id);
        $data = $this->db->get("master_direct");
        return $data;
    }
    function get_barang() {
        $this->db->where('kat', 10);
        $data = $this->db->get("barang");
        return $data;
    }

    function get_sales() {
        $data = $this->db->get("sales");
        return $data;
    }

    function select() {
        $data = $this->db->get('master_direct');
        return $data;
    }
    function select_detail($id) {
        $this->db->where('id_master_direct',$id);
        $data = $this->db->get('master_direct_detail');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('master_direct', $data);
        return $data;
    }

    function update($data, $id_direct){
        $this->db->where('id_direct',$id_direct);
        $data = $this->db->update('master_direct', $data);
        return $data;
    }

    function update_detail($data) {
        $data = $this->db->insert('master_direct_detail', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_direct', $id);
        $data = $this->db->delete('master_direct');
        return $data;
    }

    function delete_detail($id) {
        $this->db->where('id_master_direct_detail', $id);
        $data = $this->db->delete('master_direct_detail');
        return $data;
    }

}

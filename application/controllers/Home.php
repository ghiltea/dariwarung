<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('index');
	}
	public function about()
	{
		$this->load->view('about');
	}
	public function blog()
	{
		$this->load->view('blog');
	}
	public function blog_single()
	{
		$this->load->view('blog_single');
	}
	public function contact()
	{
		$this->load->view('contact');
	}
	public function login()
	{
		$this->load->view('login');
	}
	public function register()
	{
		$this->load->view('register');
	}

	public function view_shop()
	{
		$this->load->view('shop/shop');
	}
	public function wishlist()
	{
		$this->load->view('shop/wishlist');
	}
	public function cart()
	{
		$this->load->view('shop/cart');
	}
	public function checkout()
	{
		$this->load->view('shop/checkout');
	}
	public function single()
	{
		$this->load->view('shop/singel_product');
	}
}

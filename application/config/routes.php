<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// route default 
$route['default_controller'] = 'member/home';

// route admin
$route['admin'] = 'admin/dashboard';

// route vendor
$route['vendor'] = 'vendor/dashboard';

// route member
$route['home'] = 'member/home';
$route['about'] = 'member/about';
$route['store'] = 'member/store';
$route['store/register'] = 'member/store/register';
$route['content/help'] = 'member/content/help';
$route['store/location'] = 'member/store/location';
$route['store/search'] = 'member/store/search';
$route['tracking'] = 'member/tracking';
$route['shop/whislist'] = 'member/shop/whislist';
$route['shop'] = 'member/shop';
$route['shop/category/(:any)'] = 'member/shop/category/$1';
$route['shop/detail/(:any)'] = 'member/shop/detail/$1';
$route['shop/cart/(:any)'] = 'member/shop/cart/$1';
$route['shop/search'] = 'member/shop/search';
$route['blog'] = 'member/blog';
$route['blog/detail/(:any)'] = 'member/blog/detail/$1';
$route['contact'] = 'member/contact';
$route['faq'] = 'member/faq';
$route['termcondition'] = 'member/termcondition';
$route['privacypolicy'] = 'member/privacypolicy';
$route['login/loreg'] = 'member/login/loreg';
//$route['register'] = 'member/register';
$route['profil'] = 'member/profil';
$route['profil/whislist'] = 'member/profil/whislist';
$route['profil/cart'] = 'member/profil/cart';
$route['profil/order'] = 'member/profil/order';

$route['404_override'] = 'error/my404';
$route['translate_uri_dashes'] = FALSE;
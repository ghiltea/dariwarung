<?php

class Template extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    function admin_template($data = NULL) {
        $this->load->view('template/admin_template_v', $data);
    }

    function admin_login_template($data = NULL) {
        $this->load->view('template/admin_login_template_v', $data);
    }

    function member_template($data = NULL) {

        //url segment
        $url = $this->uri->segment(1);
        if ($url == "") {
            $url = "home";
        }
        //category all
        $category_all = $this->db->query('SELECT * FROM  tbl_product_category');
        $data['category_all'] = $category_all;
        
        //company
        $company = $this->db->get('tbl_company');
        $data['company'] = $company->row();
        
        $this->db->where('page', $url);
        $home = $this->db->get('tbl_setting');
        $h = $home->result();
        $data['home'] = $h;

        $this->load->view('template/member_template_v', $data);
    }

}

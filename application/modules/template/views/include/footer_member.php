<footer>


    <!-- Footer-newsletter -->
    <div class="bg-primary py-3">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 mb-md-3 mb-lg-0">
                    <div class="row align-items-center">
                        <div class="col-auto flex-horizontal-center">
                            <i class="ec ec-newsletter font-size-40"></i>
                            <h2 class="font-size-20 mb-0 ml-3">Berlangganan Informasi Terbaru</h2>
                        </div>
                        <div class="col my-4 my-md-0">
                            <h5 class="font-size-15 ml-4 mb-0">...dan dapatkan <strong>Kupon Promo.</strong></h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <!-- Subscribe Form -->
                    <form class="js-validate js-form-message">
                        <label class="sr-only" for="subscribeSrEmail">Alamat Email</label>
                        <div class="input-group input-group-pill">
                            <input type="email" class="form-control border-0 height-40" name="email_subcribe" id="subscribeSrEmail" placeholder="Masukan Alamat Email " aria-label="Alamat Email" aria-describedby="subscribeButton" required
                                   data-msg="Silahkan masukan alamat email dengan benar !.">
                            <div class="input-group-append">
                                <button type="button" id="btn_subcribe" class="btn btn-dark btn-sm-wide height-40 py-2" >Kirim</button>
                            </div>
                        </div>
                    </form>
                    <!-- End Subscribe Form -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer-newsletter -->

    <!-- Footer-bottom-widgets -->
    <div class="pt-8 pb-4 bg-gray-13">
        <div class="container mt-1">
            <div class="row">
                <div class="col-lg-5">
                    <div class="mb-6">
                        <a href="#" class="d-inline-block">
                            <img class="img-responsive" style="width: 250px;" src="<?php echo base_url('uploads/logo/') . $company->image; ?>">
                        </a>
                    </div>
                    <div class="mb-4">
                        <div class="row no-gutters">
                            <div class="col-auto">
                                <i class="ec ec-support text-primary font-size-56"></i>
                            </div>
                            <div class="col pl-3">
                                <div class="font-size-13 font-weight-light">Bantuan Informasi 24/7</div>
                                <a href="info@dariwarung.com" class="font-size-20 text-gray-90">info@dariwarung.com</a>
                            </div>
                        </div>
                    </div>
                    <div class="my-4 my-md-4">
                        <ul class="list-inline mb-0 opacity-7">
                            <li class="list-inline-item mr-0">
                                <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="<?php echo $company->facebook; ?>">
                                    <span class="fab fa-facebook-f btn-icon__inner"></span>
                                </a>
                            </li>
                            <li class="list-inline-item mr-0">
                                <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="<?php echo $company->gplus; ?>">
                                    <span class="fab fa-google btn-icon__inner"></span>
                                </a>
                            </li>
                            <li class="list-inline-item mr-0">
                                <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="<?php echo $company->twitter; ?>">
                                    <span class="fab fa-twitter btn-icon__inner"></span>
                                </a>
                            </li>
                            <li class="list-inline-item mr-0">
                                <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="<?php echo $company->instagram; ?>">
                                    <span class="fab fa-instagram btn-icon__inner"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-12 col-md mb-4 mb-md-0">
                            <h6 class="mb-3 font-weight-bold">Tentang DariWarung</h6>
                            <!-- List Group -->
                            <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('about'); ?>">Tentang Kami</a></li>
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('store/register'); ?>">Mulai Jualan</a></li>
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('store'); ?>">Etalase Produk</a></li>
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('blog'); ?>">Artikel | Blog</a></li>

                            </ul>
                            <!-- End List Group -->
                        </div>

                        <div class="col-12 col-md mb-4 mb-md-0">
                            <h6 class="mb-3 font-weight-bold">Informasi & Bantuan</h6>
                            <!-- List Group -->
                            <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">

                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('content/help'); ?>"> Bantuan</a></li>
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('content/term'); ?>"> Syarat dan Ketentuan </a></li>
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('content/privacy'); ?>"> Kerahasiaan Data Pribadi</a></li>
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('content/faq'); ?>">FAQs</a></li>
                            </ul>
                            <!-- End List Group -->
                        </div>

                        <div class="col-12 col-md mb-4 mb-md-0">
                            <h6 class="mb-3 font-weight-bold">Akun Saya</h6>
                            <!-- List Group -->
                            <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('login/logreg'); ?>">Daftar / Login</a></li>
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('shop/whislist'); ?>">Daftar Keinginan</a></li>
                                <li><a class="list-group-item list-group-item-action" href="<?php echo base_url('shop/cart'); ?>">Order Tracking</a></li>
                            </ul>
                            <!-- End List Group -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer-bottom-widgets -->

    <!-- Footer-copy-right -->
    <div class="bg-gray-14 py-2">
        <div class="container">
            <div class="flex-center-between d-block d-md-flex">
                <div class="mb-3 mb-md-0">© <a href="#" class="font-weight-bold text-gray-90">Dariwarung</a> - All rights Reserved</div>
                <div class="text-md-right">
                    <span class="d-inline-block bg-white border rounded p-1">
                        <img class="max-width-5" src="<?php echo base_url('uploads/bank/'); ?>1.png" alt="Image Description">
                    </span>
                    <span class="d-inline-block bg-white border rounded p-1">
                        <img class="max-width-5" src="<?php echo base_url('uploads/bank/'); ?>2.png" alt="Image Description">
                    </span>
                    <span class="d-inline-block bg-white border rounded p-1">
                        <img class="max-width-5" src="<?php echo base_url('uploads/bank/'); ?>3.png" alt="Image Description">
                    </span>
                    <span class="d-inline-block bg-white border rounded p-1">
                        <img class="max-width-5" src="<?php echo base_url('uploads/bank/'); ?>4.png" alt="Image Description">
                    </span>
                    <span class="d-inline-block bg-white border rounded p-1">
                        <img class="max-width-5" src="<?php echo base_url('uploads/bank/'); ?>5.png" alt="Image Description">
                    </span>
                    <span class="d-inline-block bg-white border rounded p-1">
                        <img class="max-width-5" src="<?php echo base_url('uploads/bank/'); ?>6.png" alt="Image Description">
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer-copy-right -->

</footer>

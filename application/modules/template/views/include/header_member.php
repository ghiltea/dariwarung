<header id="header" class="u-header u-header-left-aligned-nav">
    <div class="u-header__section">

        <!-- Topbar -->
        <div class="u-header-topbar py-2 d-none d-xl-block">
            <div class="container">
                <div class="d-flex align-items-center">
                    <div class="topbar-left">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <a href="<?php echo $company->playstore; ?>" class="text-gray-110 font-size-13 u-header-topbar__nav-link">Download App</a>
                            </li>
                            <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <a href="<?php echo base_url('content/help');?>" class="text-gray-110 font-size-13 u-header-topbar__nav-link">Bantuan</a>
                            </li>
                        </ul>
                    </div>
                    <div class="topbar-right ml-auto">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <a href="<?php echo base_url('store/location'); ?> " class="u-header-topbar__nav-link"><i class="ec ec-map-pointer mr-1"></i> Lokasi Warung</a>
                            </li>
                            <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <a href="<?php echo base_url('tracking'); ?>" class="u-header-topbar__nav-link"><i class="ec ec-transport mr-1"></i> Tracking Order Kamu</a>
                            </li>

                            <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                <!-- Account Sidebar Toggle Button -->
                                <a id="sidebarNavToggler" href="javascript:;" role="button" class="u-header-topbar__nav-link"
                                   aria-controls="sidebarContent"
                                   aria-haspopup="true"
                                   aria-expanded="false"
                                   data-unfold-event="click"
                                   data-unfold-hide-on-scroll="false"
                                   data-unfold-target="#sidebarContent"
                                   data-unfold-type="css-animation"
                                   data-unfold-animation-in="fadeInRight"
                                   data-unfold-animation-out="fadeOutRight"
                                   data-unfold-duration="500">

                                    <?php if (!$this->session->member_islogin) { ?>
                                        <i class="ec ec-user mr-1"></i> Daftar <span class="text-gray-50">atau</span> Login
                                    <?php } ?>
                                </a>
                                <a id="sidebarNavToggler" href="javascript:;" class="u-header-topbar__nav-link">
                                    <?php if ($this->session->member_islogin) { ?>
                                        <i class="ec ec-user mr-1"></i> <?php echo $this->session->userdata('fullname') . ","; ?><span class="text-gray-50"> | </span> <span id="logout" >Logout</span>
                                    <?php } ?>
                                </a>
                                <!-- End Account Sidebar Toggle Button -->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Topbar -->

        <!-- Logo and Menu -->
        <div class="py-2 py-xl-4 bg-primary-down-lg">
            <div class="container my-0dot5 my-xl-0">
                <div class="row align-items-center">
                    <!-- Logo-offcanvas-menu -->
                    <div class="col-auto">
                        <!-- Nav -->
                        <nav class="navbar navbar-expand u-header__navbar py-0 justify-content-xl-between max-width-270 min-width-270">
                            <!-- Logo -->
                            <a class="order-1 order-xl-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-center" href="<?php echo base_url(); ?>" aria-label="DARI WARUNG">
                                <!--<h4 style="color: #393e48;"><b>DARI WARUNG</b></h4>-->
                             <img  style="width: 250px;" src="<?php echo base_url('uploads/logo/'); ?>logo.png" class="img-fluid img-responsive">
                            </a>
                            <!-- End Logo -->

                            <!-- Fullscreen Toggle Button -->
                            <button id="sidebarHeaderInvokerMenu" type="button" class="navbar-toggler d-block btn u-hamburger mr-3 mr-xl-0"
                                    aria-controls="sidebarHeader"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    data-unfold-event="click"
                                    data-unfold-hide-on-scroll="false"
                                    data-unfold-target="#sidebarHeader1"
                                    data-unfold-type="css-animation"
                                    data-unfold-animation-in="fadeInLeft"
                                    data-unfold-animation-out="fadeOutLeft"
                                    data-unfold-duration="500">
                                <span id="hamburgerTriggerMenu" class="u-hamburger__box">
                                    <span class="u-hamburger__inner"></span>
                                </span>
                            </button>
                            <!-- End Fullscreen Toggle Button -->
                        </nav>
                        <!-- End Nav -->

                        <!-- ========== HEADER SIDEBAR ========== -->
                        <aside id="sidebarHeader1" class="u-sidebar u-sidebar--left" aria-labelledby="sidebarHeaderInvokerMenu">
                            <div class="u-sidebar__scroller">
                                <div class="u-sidebar__container">
                                    <div class="u-header-sidebar__footer-offset pb-0">
                                        <!-- Toggle Button -->
                                        <div class="position-absolute top-0 right-0 z-index-2 pt-4 pr-7">
                                            <button type="button" class="close ml-auto"
                                                    aria-controls="sidebarHeader"
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                    data-unfold-event="click"
                                                    data-unfold-hide-on-scroll="false"
                                                    data-unfold-target="#sidebarHeader1"
                                                    data-unfold-type="css-animation"
                                                    data-unfold-animation-in="fadeInLeft"
                                                    data-unfold-animation-out="fadeOutLeft"
                                                    data-unfold-duration="500">
                                                <span aria-hidden="true"><i class="ec ec-close-remove text-gray-90 font-size-20"></i></span>
                                            </button>
                                        </div>
                                        <!-- End Toggle Button -->

                                        <!-- Content -->
                                        <div class="js-scrollbar u-sidebar__body">
                                            <div id="headerSidebarContent" class="u-sidebar__content u-header-sidebar__content">
                                                <!-- Logo -->
                                                <a class="d-flex ml-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-vertical" href="../home/index.html" aria-label="Electro">

                                                </a>
                                                <!-- End Logo -->

                                                <!-- List -->
                                                <ul id="headerSidebarList" class="u-header-collapse__nav">
                                                    <!-- Home Section -->
                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                        <a class="u-header-collapse__nav-link " href="<?php echo base_url('about'); ?>">
                                                            Tentang Dariwarung
                                                        </a>
                                                    </li>
                                                    <!-- End Home Section -->

                                                    <!-- Shop Pages -->
                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                        <a class="u-header-collapse__nav-link " href="<?php echo base_url('store/register'); ?>">
                                                            Mulai Berjualan
                                                        </a>
                                                    </li>
                                                    <!-- End Shop Pages -->

                                                    <!-- Shop Pages -->
                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                        <a class="u-header-collapse__nav-link "  href="<?php echo base_url('store'); ?>">
                                                            Etalase Warung
                                                        </a>
                                                    </li>
                                                    <!-- End Shop Pages -->

                                                    <!-- Shop Pages -->
                                                    <li class="u-has-submenu u-header-collapse__submenu">
                                                        <a class="u-header-collapse__nav-link "  href="<?php echo base_url('blog'); ?>">
                                                            Artikel Terbaru
                                                        </a>
                                                    </li>
                                                    <!-- End Shop Pages -->

                                                </ul>
                                                <!-- End List -->
                                            </div>
                                        </div>
                                        <!-- End Content -->
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <!-- ========== END HEADER SIDEBAR ========== -->
                    </div>
                    <!-- End Logo-offcanvas-menu -->
                    <!-- Primary Menu -->
                    <div class="col d-none d-xl-block">
                        <!-- Nav -->
                        <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
                            <!-- Navigation -->
                            <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                                <ul class="navbar-nav u-header__navbar-nav">

                                    <!-- About us -->  <li class="nav-item u-header__nav-item">
                                        <a class="nav-link u-header__nav-link" href="<?php echo base_url('about'); ?>">Tentang Dariwarung</a>
                                    </li>
                                    <li class="nav-item u-header__nav-item">
                                        <a class="nav-link u-header__nav-link" href="<?php echo base_url('store/register'); ?>">Mulai Berjualan</a>
                                    </li>
                                    <!-- End About us -->

                                    <!-- Contact Us -->
                                    <li class="nav-item u-header__nav-item">
                                        <a class="nav-link u-header__nav-link" href="<?php echo base_url('store'); ?>">Etalase Warung</a>
                                    </li>
                                    <!-- End Contact Us -->
                                    <!-- Contact Us -->
                                    <li class="nav-item u-header__nav-item">
                                        <a class="nav-link u-header__nav-link" href="<?php echo base_url('blog'); ?>">Artikel </a>
                                    </li>
                                    <!-- End Contact Us -->


                                </ul>
                            </div>
                            <!-- End Navigation -->
                        </nav>
                        <!-- End Nav -->
                    </div>
                    <!-- End Primary Menu -->
                    <!-- Customer Care -->
                    <div class="d-none d-xl-block col-md-auto">
                        <div class="d-flex">
                            <i class="ec ec-support font-size-50 text-primary"></i>
                            <div class="ml-2">
                                <div class="phone">
                                    <strong>Bantuan Informasi</strong> 
                                </div>
                                <div class="email">
                                    E-mail: <a href="mailto:info@dariwarung.com?subject=Help Need" class="text-gray-90">info@dariwarung.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Customer Care -->

                    <!-- Header Icons -->
                    <div class="d-xl-none col col-xl-auto text-right text-xl-left pl-0 pl-xl-3 position-static">
                        <div class="d-inline-flex">
                            <ul class="d-flex list-unstyled mb-0 align-items-center">
                                <!-- Search -->
                                <li class="col d-xl-none px-2 px-sm-3 position-static">
                                    <a id="searchClassicInvoker" class="font-size-22 text-gray-90 text-lh-1 btn-text-secondary" href="javascript:;" role="button"
                                       data-toggle="tooltip"
                                       data-placement="top"
                                       title="Search"
                                       aria-controls="searchClassic"
                                       aria-haspopup="true"
                                       aria-expanded="false"
                                       data-unfold-target="#searchClassic"
                                       data-unfold-type="css-animation"
                                       data-unfold-duration="300"
                                       data-unfold-delay="300"
                                       data-unfold-hide-on-scroll="true"
                                       data-unfold-animation-in="slideInUp"
                                       data-unfold-animation-out="fadeOut">
                                        <span class="ec ec-search"></span>
                                    </a>

                                    <!-- Input -->
                                    <div id="searchClassic" class="dropdown-menu dropdown-unfold dropdown-menu-right left-0 mx-2" aria-labelledby="searchClassicInvoker">
                                        <form class="js-focus-state input-group px-3" method="post" action="<?php echo base_url('store/search');?>">
                                            <input class="form-control" type="search" placeholder="Search Product">
                                            <div class="input-group-append">
                                                <a class="btn btn-primary px-3" type="submit"><i class="font-size-18 ec ec-search"></i></a>
                                            </div>
                                        </form>
                                    </div>
                                    
                                    <!-- End Input -->
                                </li>
                                <!-- End Search -->
                                <?php if ($this->session->member_islogin) { ?>
                                    <li class="col d-xl-none px-2 px-sm-3"><a href="../shop/my-account.html" class="text-gray-90" data-toggle="tooltip" data-placement="top" title="<?php echo $this->session->userdata('fullname'); ?>"><i class="font-size-22 ec ec-user"></i></a></li>
                                <?php } else { ?>
                                    <li class="col d-xl-none px-2 px-sm-3"><a href="../shop/my-account.html" class="text-gray-90" data-toggle="tooltip" data-placement="top" title="Login / Daftar"><i class="font-size-22 ec ec-user"></i></a></li>
                                <?php } ?>
                                    <li class="col d-xl-none px-2 px-sm-3"><a href="<?php echo base_url('shop/whislist'); ?>" class="text-gray-90" data-toggle="tooltip" data-placement="top" title="Daftar Keinginan"><i class="font-size-22 ec ec-category-icon"></i></a></li>
                                <li class="col pr-xl-0 px-2 px-sm-3">
                                    <a href="<?php echo base_url('shop/cart'); ?>" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Keranjang Belanja">
                                        <i class="font-size-22 ec ec-shopping-bag"></i>
                                        <span class="width-22 height-22 bg-dark position-absolute d-flex align-items-center justify-content-center rounded-circle left-12 top-8 font-weight-bold font-size-12 text-white">2</span>
                                        <span class="d-none d-xl-block font-weight-bold font-size-16 text-gray-90 ml-3">$1785.00</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Header Icons -->
                </div>
            </div>
        </div>
        <!-- End Logo and Menu -->

        <!-- Vertical-and-Search-Bar -->
        <div class="d-none d-xl-block bg-primary">
            <div class="container">
                <div class="row align-items-stretch min-height-50">
                    <!-- Vertical Menu -->
                    <div class="col-md-auto d-none d-xl-flex align-items-end">
                        <div class="max-width-270 min-width-270">
                            <!-- Basics Accordion -->
                            <div id="basicsAccordion">
                                <!-- Card -->
                                <div class="card border-0 rounded-0">
                                    <div class="card-header bg-primary rounded-0 card-collapse border-0" id="basicsHeadingOne">
                                        <button type="button" class="btn-link btn-remove-focus btn-block d-flex card-btn py-3 text-lh-1 px-4 shadow-none btn-primary rounded-top-lg border-0 font-weight-bold text-gray-90 collapsed"
                                                data-toggle="collapse"
                                                data-target="#basicsCollapseOne"
                                                aria-expanded="true"
                                                aria-controls="basicsCollapseOne">
                                            <span class="pl-1 text-gray-90">Kategori Produk </span>
                                            <span class="text-gray-90 ml-3">
                                                <span class="ec ec-arrow-down-search"></span>
                                            </span>
                                        </button>
                                    </div>
                                    <div id="basicsCollapseOne" class="vertical-menu v1  collapse <?php if (!$this->uri->segment(1) == "about") { echo "show"; }  ?>"
                                         aria-labelledby="basicsHeadingOne"
                                         data-parent="#basicsAccordion">
                                        <div class="card-body p-0">
                                            <nav class="js-mega-menu navbar navbar-expand-xl u-header__navbar u-header__navbar--no-space hs-menu-initialized">
                                                <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                                                    <ul class="navbar-nav u-header__navbar-nav border-primary border-top-0">
                                                        <?php foreach ($category_all->result() as $val) { ?>


                                                            <li class="nav-item hs-has-mega-menu u-header__nav-item"
                                                                data-event="hover"
                                                                data-position="left">
                                                                <a id="basicMegaMenu1" class="nav-link u-header__nav-link " href="javascript:;" aria-haspopup="true" aria-expanded="false"><?php echo $val->name; ?></a>

                                                                <!-- Nav Item - Mega Menu -->
                                                                <!--                                                                <div class="hs-mega-menu vmm-tfw u-header__sub-menu" aria-labelledby="basicMegaMenu1">
                                                                                                                                    <div class="vmm-bg">
                                                                                                                                        <img class="img-fluid" src="../../assets/img/500X400/img4.png" alt="Image Description">
                                                                                                                                    </div>
                                                                                                                                    <div class="row u-header__mega-menu-wrapper">
                                                                                                                                        <div class="col mb-3 mb-sm-0">
                                                                                                                                            <span class="u-header__sub-menu-title">Cameras & Photography</span>
                                                                                                                                            <ul class="u-header__sub-menu-nav-group mb-3">
                                                                                                                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="#">Lenses</a></li>
                                                                                                                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="#">Camera Accessories</a></li>
                                                                                                                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="#">Security & Surveillance</a></li>
                                                                                                                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="#">Binoculars & Telescopes</a></li>
                                                                                                                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="#">Camcorders</a></li>
                                                                                                                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="#">Software</a></li>
                                                                                                                                                <li>
                                                                                                                                                    <a class="nav-link u-header__sub-menu-nav-link u-nav-divider border-top pt-2 flex-column align-items-start" href="#">
                                                                                                                                                        <div class="">All Electronics</div>
                                                                                                                                                        <div class="u-nav-subtext font-size-11 text-gray-30">Discover more products</div>
                                                                                                                                                    </a>
                                                                                                                                                </li>
                                                                                                                                            </ul>
                                                                                                                                        </div>
                                                                
                                                                                                                                        <div class="col mb-3 mb-sm-0">
                                                                                                                                            <span class="u-header__sub-menu-title">Audio & Video</span>
                                                                                                                                            <ul class="u-header__sub-menu-nav-group">
                                                                                                                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="#">All Audio & Video</a></li>
                                                                                                                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="#">Headphones & Speakers</a></li>
                                                                                                                                            </ul>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>-->
                                                                <!-- End Nav Item - Mega Menu -->
                                                            <?php } ?>
                                                    </ul>
                                                </div>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Card -->
                            </div>
                            <!-- End Basics Accordion -->
                        </div>
                    </div>
                    <!-- End Vertical Menu -->
                    <!-- Search bar -->
                    <div class="col align-self-center">
                        <!-- Search-Form -->
                        <form class="js-focus-state" action="<?php echo base_url('store/search');?>" method="post">
                            <label class="sr-only" for="searchProduct">Search</label>
                            <div class="input-group">
                                <input type="text" class="form-control py-2 pl-5 font-size-15 border-0 height-40 rounded-left-pill" name="email" id="searchProduct" placeholder="Cari Produk" aria-label="Search for Products" aria-describedby="searchProduct1" required>
                                <div class="input-group-append">
                                    <!-- Select -->
                                    <select class="js-select selectpicker dropdown-select custom-search-categories-select"
                                            data-style="btn height-40 text-gray-60 font-weight-normal border-0 rounded-0 bg-white px-5 py-2">
                                        <option value="one" selected>Semua</option>
                                        <option value="two">Warung</option>
                                        <option value="three">Produk</option>
                                    </select>
                                    <!-- End Select -->
                                    <button class="btn btn-dark height-40 py-2 px-3 rounded-right-pill" type="submit" id="searchProduct1">
                                        <span class="ec ec-search font-size-24"></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!-- End Search-Form -->
                    </div>
                    <!-- End Search bar -->
                    <!-- Header Icons -->
                    <div class="col-md-auto align-self-center">
                        <div class="d-flex">
                            <ul class="d-flex list-unstyled mb-0">
                                <li class="col"><a href="<?php echo base_url('shop/whislist'); ?>" class="text-gray-90" data-toggle="tooltip" data-placement="top" title="Daftar keinginan"><i class="font-size-22 ec ec-favorites"></i></a></li>
                                <li class="col pr-0">
                                    <a href="<?php echo base_url('shop/cart/1'); ?>" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Keranjang Belanja">
                                        <i class="font-size-22 ec ec-shopping-bag"></i>
                                        <span class="width-22 height-22 bg-dark position-absolute flex-content-center text-white rounded-circle left-12 top-8 font-weight-bold font-size-12">2</span>
                                        <span class="font-weight-bold font-size-14 text-gray-90 ml-3">Rp&nbsp;1.785.000.000</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Header Icons -->
                </div>
            </div>
        </div>
        <!-- End Vertical-and-secondary-menu -->

    </div>
</header>

<script>
    $('#logout').on('click', function () {
        logout();
    })
    function logout() {
        $.ajax({
            url: "<?php echo base_url('member/login/logout'); ?>",
            type: 'POST',
            data: {},
            success: function (data) {
                var rs = JSON.parse(data)
                if (rs.response == 'success') {
                    window.location.reload();
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: "Logout Gagal!",
                        text: "Clean Browser History,dan Coba Kembali !",
                        confirmButtonColor: 'LightSeaGreen',
                        confirmButtonText: 'Tutup!'
                    });
                }
            }
        })
    }

</script>
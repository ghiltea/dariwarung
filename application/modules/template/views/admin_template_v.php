<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Administrator | Dariwarung</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/main.css">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/tree-bonsai/css/jquery.bonsai.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/dist/css/custom.css">  
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/combo-tree-master/style.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/skins/_all-skins.css">
       <link rel="shortcut icon" href="<?php echo base_url('assets/member/'); ?>favicon.png">  
        <!-- DataTables -->
        <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net/css/select.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net/css/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net/css/buttons.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">
        <!-- daterange picker -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/flat/blue.css">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/timepicker/bootstrap-timepicker.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.css">
        <!-- Dual List Box -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-duallistbox-master/bootstrap-duallistbox.css"> -->

        <!-- Gijgo -->
        <!-- <link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css"> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery 3 -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net/js/dataTables.select.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/datatables.net/js/buttons.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/js-url-2.5.3/url.min.js"></script>
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script>var base_url = '<?php echo base_url() ?>';</script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169597821-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-169597821-1');
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo base_url(); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>DARI</b>WARUNG</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>DARI</b>WARUNG</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown messages-menu" id="notify_pelaporan_st_pegawai">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-info-circle"></i>
                                    <span class="label label-warning" id="total_notify_bad_st_pegawai"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">Terdapat <span id="total_notify_bar_st_pegawai"></span> Informasi</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu" id="list_notif_st_pegawai">

                                            <!-- end message -->
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="<?php echo base_url('admin/konfirmasi'); ?>">Lihat Di Konfirmasi Bayar</a></li>
                                </ul>
                            </li>

                            <li class="dropdown messages-menu" id="notify_pelaporan" onclick="notif_list();">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-danger" id="notif_count"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">Terdapat <span id="notif_count"></span> Notifikasi</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu" id="notif_list">

                                            <!-- end message -->
                                        </ul>
                                    </li>

                                    <li class="footer"><a href="<?php echo base_url('admin/order'); ?>">Lihat Di Order</a></li>
                                </ul>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="<?php echo base_url() ?>assets/admin/dist/img/user2-160x160.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $this->session->userdata('username') ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url() ?>assets/admin/dist/img/user2-160x160.png" class="img-circle" alt="User Image">

                                        <p>
                                            <?php echo $this->session->userdata('username') ?>  <?php //echo $this->session->userdata('rolearea') ?>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo base_url('admin/profil') ?>" class="btn btn-default btn-flat">Setting</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url('admin/login/r_logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->

                        </ul>
                    </div>
                </nav>
            </header>

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">


                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">
                            <div class="user-panel">
                                <div class="pull-left image">
                                    <img src="<?php echo base_url(); ?>assets/admin/dist/img/user2-160x160.png" class="img-circle" alt="User Image">
                                </div>
                                <div class="pull-left info">
                                    <p> <?php echo $this->session->userdata('r_name'); ?></p>
                                    <a><i class="fa fa-circle text-success"></i> Online</a>
                                </div>
                            </div>
                        </li>
                        <!--menu-->
                        <li class="header">MAIN MENU</li>
                        <li class="treeview">
                        <li><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user-circle"></i> <span>User</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('admin/user'); ?>"><i class="fa fa-circle-o"></i>Manage User</a></li>
                                <li><a href="<?php echo base_url('admin/role'); ?>"><i class="fa fa-circle-o"></i>Manage Role</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                        <li><a href="<?php echo base_url('admin/member'); ?>"><i class="fa fa-user-plus"></i> <span>Member</span></a></li>
                        </li>
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-barcode"></i> <span>Product</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('admin/productcategory'); ?>"><i class="fa fa-circle-o"></i>Manage Category</a></li>
                                <li><a href="<?php echo base_url('admin/product'); ?>"><i class="fa fa-circle-o"></i>Manage Product</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url('admin/order'); ?>"><i class="fa fa-cart-arrow-down"></i> <span>Order</span></a></li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-picture-o"></i> <span>Content Management</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('admin/content/about'); ?>"><i class="fa fa-circle-o"></i>Manage About</a></li>
                                <li><a href="<?php echo base_url('admin/content/contact'); ?>"><i class="fa fa-circle-o"></i>Manage Contact</a></li>
                                <li><a href="<?php echo base_url('admin/content/privacypolicy'); ?>"><i class="fa fa-circle-o"></i>Manage Privacy Policy</a></li>
                                <li><a href="<?php echo base_url('admin/content/termsconditions'); ?>"><i class="fa fa-circle-o"></i>Manage Terms & Conditions</a></li>
                                <li><a href="<?php echo base_url('admin/content/returnexchange'); ?>"><i class="fa fa-circle-o"></i>Manage Returns & Exchange</a></li>
                                <li><a href="<?php echo base_url('admin/content/shippinginformation'); ?>"><i class="fa fa-circle-o"></i>Manage Shiping Information</a></li>
                                <li><a href="<?php echo base_url('admin/content/faqs'); ?>"><i class="fa fa-circle-o"></i>Manage FAQs</a></li>
                                <li><a href="<?php echo base_url('admin/content/journal'); ?>"><i class="fa fa-circle-o"></i>Manage Journal</a></li>
                                <li><a href="<?php echo base_url('admin/content/sosmed'); ?>"><i class="fa fa-circle-o"></i>Manage Sosial Media</a></li>
                                <li><a href="<?php echo base_url('admin/content/banner'); ?>"><i class="fa fa-circle-o"></i>Manage Banner</a></li>
                                <li><a href="<?php echo base_url('admin/content/featuredcategory'); ?>"><i class="fa fa-circle-o"></i>Manage Featured Category</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-newspaper-o"></i> <span>Blogs</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url('admin/blogcategory'); ?>"><i class="fa fa-circle-o"></i>Manage Category</a></li>
                                <li><a href="<?php echo base_url('admin/blog'); ?>"><i class="fa fa-circle-o"></i>Manage Blogs</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url('admin/slide'); ?>"><i class="fa fa-sliders"></i>Slider News</a></li>
                        <li><a href="<?php echo base_url('admin/testimoni'); ?>"><i class="fa fa-comment"></i> <span>Post Testimoni</span></a></li>
                        <li><a href="<?php echo base_url('admin/message'); ?>"><i class="fa fa-envelope"></i> <span>Inbox Message</span></a></li>
                        <li><a href="<?php echo base_url('admin/subcribe'); ?>"><i class="fa fa-mail-reply-all"></i> <span>Inbox Subcribe News</span></a></li>
                        <li><a href="<?php echo base_url('admin/setting'); ?>"><i class="fa fa-gear"></i> <span>Setting Digintal Marketing</span></a></li>


                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->

            <!-- 
             Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <?php if (!empty($title_header)) echo "<h1>" . $title_header;"</h1>" ?>
                </section>

                <!-- Main content -->
                <section class="content">

                    <?php echo $this->load->view($content_view); ?>

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 0.1
                </div>
                <strong>Copyright &copy; 2019 <a href="#"> Dariwarung.com</a>.</strong> All rights
                reserved.
            </footer>

            <span id="alertId" style="display:none"></span>
            <audio id='alert' src='<?php echo base_url(); ?>assets/admin/media/Alarm-tone01.mp3'/>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- Jquery Table2Excel -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/jquery-table2excel/jquery.table2excel.js"></script>
        <!-- Bonsai Plugin Tree -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/tree-bonsai/js/jquery.qubit.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/tree-bonsai/js/jquery.json-list.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/tree-bonsai/js/jquery.bonsai.js"></script>
        <!-- Dual List Box -->
        <!-- jQuery Validation -->
        <script src="<?php echo base_url() ?>assets/admin/bower_components/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-duallistbox-master/jquery.bootstrap-duallistbox.js"></script> -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/combo-tree-master/icontains.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/combo-tree-master/comboTreePlugin.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/fastclick/lib/fastclick.js"></script>

        <!-- Select2 -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
        <!-- InputMask -->
        <script src="<?php echo base_url(); ?>assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- date-range-picker -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/moment/min/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url(); ?>assets/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/admin/dist/js/adminlte.min.js"></script>
        <!-- Sweet Alert -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/sweet-alert2.1.0/sweet-alert-2.1.0.min.js"></script>
        <!-- Gijgo -->
        <!-- <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js"></script> -->
        <!-- CK Editor -->
        <script src="<?php echo base_url(); ?>assets/admin/bower_components/ckeditor/ckeditor.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/dist/js/custom.js"></script>


        <script src="<?php echo base_url(); ?>assets/admin/js/fn.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.js"></script>
        <script>
                                $(document).ready(function () {
                                    $('.select2').select2();
                                });

        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                setInterval(function () {
                    $.ajax({
                        url: "<?php echo base_url('admin/order/notif_count') ?>",
                        type: 'POST',
                        success: function (response) {
                            $("#notif_count").html(response);
                        },
                    })

                }, 3000);


            });
            function notif_list() {
                $.ajax({
                    url: "<?php echo base_url('admin/order/notif_list') ?>",
                    type: 'POST',
                    success: function (response) {
                        $("#notif_list").html(response);
                    },
                })
            }
        </script>
    </body>
</html>


<!--  -->
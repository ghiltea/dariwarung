<div class="row">
    <div class="col-xs-12">

        <!-- return message add-->
        <?php if ($this->session->flashdata('msg_error')) { ?>
            <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('msg_success')) { ?>
            <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <!-- end message -->

        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>
            <div class="box-body">

                <form id="add-row-form" method="post" action="<?php echo base_url() . 'admin/setting/r_insert' ?>" enctype="multipart/form-data">
                           
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Page :</label>
                        <input autofocus  type="text" name="page" class="form-control" id="focus">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Script Include :</label>
                        <textarea id="editor1" name="script_include" rows="10" cols="80">
                           
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Status :</label>
                        <select class="select2 form-control " name="status" style="width: 100%; height:36px;">
                            <option selected=""> - Select - </option>
                            <option value="Active" selected="">Active</option>
                            <option value="No Active">No Active</option>
                        </select>
                    </div>

                    <div class="modal-footer">
                        <a href="<?php echo base_url('admin/setting'); ?>" type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</a>
                        <button type="submit" class="btn btn-success">Save Data <span class="fa fa-save"></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>




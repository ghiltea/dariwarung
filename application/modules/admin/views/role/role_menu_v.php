
<div class="row">
    <div class="col-xs-12">
         <!-- return message add-->
    <?php if ($this->session->flashdata('msg_error')) { ?>
        <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('msg_success')) { ?>
        <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <!-- end message -->
        <div class="box box-info">
            <div class="box box-header">
                <div class="row button-group">

                        <div class="col-lg-2 col-md-4">
                            <a  id="tambah" href="<?php echo base_url('admin/role'); ?>" type="button" class="btn btn-block btn-info "><i class="fa fa-arrow-circle-left"></i> Back</a>

                        </div>
                    </div>
            </div>
            <div class="box-body">
                
                <div class="modal-body">
                 
                      <form id="add-row-form" method="post" action="<?php echo base_url() . 'admin/role/r_update_role_menu' ?>">

                            <input hidden="" type="text" name="id_role_edit" class="form-control hidden" id="recipient-name" readonly value="<?php echo $role[0]->id_role; ?>">


                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Role Name :</label>
                                <input readonly="" type="text" name="role_name_edit" class="form-control" id="recipient-name" value="<?php echo $role[0]->role_name; ?>">
                            </div>
                            <hr>

                     
                            <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h5 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                                                <span class="fa fa-angle-right"></span> User's
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse1" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="card-body">
                                            <div class="form-check form-check-inline">
                                                <div class="custom-control custom-checkbox">
                                                    <input  <?php
                                                    for ($i = 0; $i < count($role_menu); $i++) {
                                                        if ($role_menu[$i] == "admin") {
                                                            echo "checked=''";
                                                        }
                                                    }
                                                    ?>    value="admin" type="checkbox" name="ckb[ ]" class="custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">User's</label>
                                                </div>
                                            </div>  
                                            <div class="form-check form-check-inline">
                                                <div class="custom-control custom-checkbox">
                                                    <input  <?php
                                                    for ($i = 0; $i < count($role_menu); $i++) {
                                                        if ($role_menu[$i] == "role") {
                                                            echo "checked=''";
                                                        }
                                                    }
                                                    ?>   value="role" type="checkbox" name="ckb[ ]"  class="custom-control-input" id="customCheck2">
                                                    <label class="custom-control-label" for="customCheck2">Role's (Access Menu)</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse3" aria-expanded="false" aria-controls="collapseThree">
                                                <span class="fa fa-angle-right"></span>  Member's
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="card-body">
                                            <div class="form-check form-check-inline">
                                                <div class="custom-control custom-checkbox">
                                                    <input  <?php
                                                    for ($i = 0; $i < count($role_menu); $i++) {
                                                        if ($role_menu[$i] == "member") {
                                                            echo "checked=''";
                                                        }
                                                    }
                                                    ?>    value="marketplace" type="checkbox" name="ckb[ ]"  class="custom-control-input" id="customCheck4">
                                                    <label class="custom-control-label" for="customCheck4">Member's</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="modal-footer">
                                <a href="<?php echo base_url('admin/role'); ?>" type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</a>
                                <button type="submit" class="btn btn-success">Update, Data  <span class="fa fa-save"></span></button>
                            </div>
                        </form>
                    
                    
                    </div>
                    </div>
            </div>
        </div>
    </div>


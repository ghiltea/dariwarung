<div class="row">
    <div class="col-xs-12">
        <!-- return message add-->
        <?php if ($this->session->flashdata('msg_error')) { ?>
            <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('msg_success')) { ?>
            <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <!-- end message -->
        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>
            <div class="box-body">
                <form id="add-row-form" method="post" action="<?php echo base_url() . 'admin/user/r_update' ?>">
                    <input hidden="" type="text" name="id_role_edit" class="form-control hidden" id="recipient-name" readonly value="<?php echo $role[0]->id_role; ?>">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Role Name :</label>
                        <input type="text" name="role_name_edit" class="form-control" id="recipient-name" value="<?php echo $role[0]->role_name; ?>">
                    </div>

                    <div class="modal-footer">
                        <a href="<?php echo base_url('admin/role'); ?>" type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</a>
                        <button type="submit" class="btn btn-success">Update, Data  <span class="fa fa-save"></span></button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">

    <!--form edit-->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row button-group">

                        <div class="col-lg-2 col-md-4">
                            <a   href="<?php echo base_url('admin/role'); ?>" id="tambah" type="button" class="btn btn-block btn-info "><i class="fa fa-arrow-circle-left"></i> Back</a>

                        </div>
                    </div>
                    <hr>
                    <div class="modal-body">
                        <form id="add-row-form" method="post" action="<?php echo base_url() . 'admin/role/r_update' ?>">

                            <input hidden="" type="text" name="id_role_edit" class="form-control hidden" id="recipient-name" readonly value="<?php echo $role[0]->id_role; ?>">


                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Role Name :</label>
                                <input type="text" name="role_name_edit" class="form-control" id="recipient-name" value="<?php echo $role[0]->role_name; ?>">
                            </div>

                            <div class="modal-footer">
                                <a href="<?php echo base_url('admin/role'); ?>" type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</a>
                                <button type="submit" class="btn btn-success">Update, Data  <span class="fa fa-save"></span></button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end form -->

</div>


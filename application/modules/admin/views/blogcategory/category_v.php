<div class="row">


    <div class="col-xs-12">
         <!-- include view modal delete -->
    <?php echo $this->load->view('admin/blogcategory/category_delete_v'); ?>
    <!-- end include -->



    <!-- return message delete and update-->
    <?php if ($this->session->flashdata('msg_error')) { ?>
        <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('msg_success')) { ?>
        <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <!-- end message -->
        <!-- end message -->
        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>


            <div class="box-body">
                <form action="" id="frmAddPendidikan" class="form-horizontal" role="form" method="post">
                    <div class="row button-group">

                        <div class="col-lg-2 col-md-4">
                            <a  id="tambah" href="<?php echo base_url('admin/blogcategory/add'); ?>" type="button" class="btn btn-block btn-info "><i class="fa fa-plus-circle"></i> Add Data</a>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table id="table_slide" class="table table-striped table-bordered display" style="width:100%">
                            <thead>
                                <tr>

                                    <th>Action</th>
                                    <th>Category Name</th>
                                    <th>Image </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        
        //view datatable
        $('#table_slide').DataTable({
            "pageLength": 50,
             "scrollX": true,
            "scrollY": "500px",
            "scrollCollapse": true,
            "paging": true,
            'searching': true,
            'ordering': true,
            'filtering': true,
            'autoWidth': true,
            'lengthChange': true,
            "bDestroy": true,
            "ajax": {
                url: "<?php echo base_url('admin/blogcategory/r_select'); ?>",
                type: 'GET'
            },

        });
        //end view datatable


        //delete data
        $('#table_slide').on('click', '#delete_btn', function () {
            var id_category = $.trim($(this).attr("id_category"));
            var category_name = $.trim($(this).attr("category_name"));
            $("input[name=id_category_delete]").val(id_category);
            $("#slide_delete").text(category_name);
        });
        //end delete


    });
  
</script>

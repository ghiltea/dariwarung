<!-- Info boxes -->
<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><b>Total User </b></span>
                <h2 style="margin-top: 8px;"> <?php echo $tot_user; ?></h2>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-clock-o"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><b>Total Member </b></span>
                <h2 style="margin-top: 8px;"> <?php echo $tot_customer; ?></h2>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- /.col -->
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-paper-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><b>Total Order</b></span>
                <h2 style="margin-top: 8px;"> <?php echo $tot_order; ?></h2>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>

<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <div class="col-md-8">
        <!-- MAP & BOX PANE -->

        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info" style="height: 400px; overflow-y: scroll;">
            <div class="box-header with-border">
                <h3 class="box-title">Transaction monitoring</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                            <tr>
                                <th>No order</th>
                                <th>Customer | Reseller</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($order->result() as $row) { ?>
                                <tr>
                                    <td><a href="<?php echo base_url('/admin/order/edit/') . $row->id_order; ?>"><?php echo $row->code_order; ?></a></td>
                                    <td><?php echo $row->fullname; ?></td>

                                    <?php if ($row->st == "Paid") { ?>
                                        <td><span class="label label-success"><?php echo $row->st; ?></span></td>
                                    <?php } else if ($row->st == "Unpaid") { ?>
                                        <td><span class="label label-danger"><?php echo $row->st; ?></span></td>
                                    <?php } else { ?>
                                        <td><span class="label label-info"><?php echo $row->st; ?></span>
                                    <?php } ?>
                                    <td>
                                        <div class="sparkbar" data-color="#00a65a" data-height="20"><?php echo $row->date; ?></div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="<?php echo base_url('admin/order'); ?>" class="btn btn-sm btn-info btn-flat pull-left">View Order</a>
                <label style="width: 85%; font-size: 22px; padding-right: 10px; background-color: silver; margin-right: 2px;"  class="pull-right text-right"><b> <i>TOTAL NOMINAL :  <?php echo "Rp " .number_format($tot_nominalorder->total_amount,0,',','.');?></i></b></label> 
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

    <div class="col-md-4">
        <div class="box box-primary" style="height: 400px; overflow-y: scroll;">
            <div class="box-header with-border">
                <h3 class="box-title">Analysis Product</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    <?php foreach ($top_prod->result() as $row) { ?>
                    <li class="item">
                        <div class="product-img">
                            <div class="fa fa-3x fa-gift" style="margin-left: 5px;"></div>
                        </div>
                        <div class="product-info">
                            <a href="<?php echo base_url('admin/product/edit/').$row->id_product; ?>" class="product-title"><?php echo $row->name;?>
                                <span class="label label-success pull-right"> Order :  <?php echo $row->tot_qty;?>  Item</span></a>
                            <span class="product-description">
                               <?php echo "Rp " .number_format($row->price,0,',','.');?>
                            </span>
                        </div>
                    </li>
                    <?php } ?>
                    <!-- /.item -->
                </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                   <a href="<?php echo base_url('admin/product'); ?>" class="btn btn-sm btn-info btn-flat pull-left">View Product</a>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
    <!-- /.col -->
</div>

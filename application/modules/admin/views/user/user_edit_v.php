<div class="row">
    <div class="col-xs-12">
         <!-- return message add-->
    <?php if ($this->session->flashdata('msg_error')) { ?>
        <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('msg_success')) { ?>
        <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <!-- end message -->
        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>
            <div class="box-body">
                <form id="add-row-form" method="post" action="<?php echo base_url() . 'admin/user/r_update' ?>">

                            <input hidden="" type="text" name="id_user_edit" class="form-control hidden" id="recipient-name" readonly value="<?php echo $user[0]->id_user;  ?>">


                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Username :</label>
                                <input type="text" name="username_edit" class="form-control" id="recipient-name" value="<?php echo $user[0]->username; ?>">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Password :</label>
                                <input type="password" placeholder="*****" name="password_edit" class="form-control" id="recipient-name" >
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Role :</label>
                                <select class="select2 form-control " name="id_role_edit" style="width: 100%; height:36px;">
                                    <option value=""> - Select - </option>
                                    <?php foreach ($user_role->result() as $row) { ?>
                                        <option value="<?php echo $row->id_role; ?>"   <?php if ($user[0]->id_role == $row->id_role): ?> selected <?php endif ?>    ><?php echo $row->role_name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Status :</label>
                                <select class="select2 form-control " name="status_edit" style="width: 100%; height:36px;">
                                    <option value=""> - Select - </option>
                                    <option value="Active" <?php if ($user[0]->status == "Active"): ?> selected <?php endif ?>>Active</option>
                                    <option value="No Active" <?php if ($user[0]->status == "No Active"): ?> selected <?php endif ?>>No Active</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <a href="<?php echo base_url('admin/user'); ?>"  type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</a>
                                <button type="submit" class="btn btn-success">Update, Data <span class="fa fa-save"></span></button>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>


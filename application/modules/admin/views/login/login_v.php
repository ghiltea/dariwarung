
<div class="box-body" style="margin-top: 10px;">
<form id="loginform" method="POST" action="<?php echo base_url('admin/login/r_login'); ?>">
    <?php if ($this->session->flashdata('msg_error_login')) { ?>
        <div class="alert alert-warning alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error_login'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <div style="color: orange; "><?php echo form_error('username') ?></div>
    <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" autocomplete="off">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    <div style="color: orange;"><?php echo form_error('password') ?></div>
    <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>

    <div class="form-group row">
        <div class="col-md-12">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customCheck1" <?php
                if (isset($_COOKIE['remember_me'])) {
                    echo 'checked="checked"';
                } else {
                    echo '';
                }
                ?>>
                <label class="custom-control-label" for="customCheck1" >Remember me</label>

            </div>
        </div>
    </div>
    <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
            <button type="submit" class="btn btn-default btn-block btn-flat form">Sign In</button>
        </div>
        <!-- /.col -->

    </div>
</form>
    </div>
<style>
    @media print{
        .non-print{
            display: none;
        }
    }
    .table { 
        margin-bottom: 2px; 
        border-width: 0px; 
    }
    .select2-container .select2-selection--single{
        height: 35px;
    }

</style> 


<div class="row">
    <div class="col-xl-12">
        <section class="content">
            <div class="box " style="border: 0px; border-top: none;" >
                <div class="box box-header non-print" style="border: 0px; border-top: none;">
                    <h4 ><?php echo $title; ?></h4> 
                </div>
                <div class="box-body">
                    <div class="non-print">
                        <form id="form_update_status" action="" method="POST">
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <select class="select2 form-control " name="status" style="width: 100%; height:45px;">
                                        <option value=""> - Select - </option>
                                        <option value="Paid" <?php if ($order->st == "Paid"): ?> selected <?php endif ?>>Paid</option>
                                        <option value="Unpaid" <?php if ($order->st == "Unpaid"): ?> selected <?php endif ?>>Unpaid</option>
                                        <option value="Pending" <?php if ($order->st == "Pending"): ?> selected <?php endif ?>>Pending</option>
                                        <option value="Proses" <?php if ($order->st == "Proses"): ?> selected <?php endif ?>>Proses</option>
                                        <option value="Delivery" <?php if ($order->st == "Delivery"): ?> selected <?php endif ?>>Delivery</option>
                                        <option value="Accept" <?php if ($order->st == "Accept"): ?> selected <?php endif ?>>Accept</option>
                                        <option value="Finish" <?php if ($order->st == "Finish"): ?> selected <?php endif ?>>Finish</option>
                                    </select>
                                    <input type="text" name="id_order" hidden="" class="hidden" value="<?php echo $order->id_order; ?>" >
                                </div>
                            </div>
                            <div class="col-md-3"> 
                                <button  type="button" id="btn_update_status" class="btn btn-danger con_txt2 non-print" >
                                    <span class="fa fa-edit"> Update Status</span>
                                </button>
                            </div>
                        </form>
                        <div class="col-md-3">
                            <button type="button"  onclick="window.print()" class="btn btn-success con_txt2 non-print">
                                <span class="fa fa-print"> Print</span>
                            </button>
                            <button type="button"  onclick="window.print()" class="btn btn-warning con_txt2 non-print" >
                                <span class="fa fa-file-pdf-o"> Export Pdf</span>
                            </button>
                        </div> 

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <!-- general form elements -->
                            <table class="table table-bordered data_table">
                                <tbody>

                                    <tr>
                                        <td colspan="4">
                                            <table class="table">
                                                <tr>
                                                    <td align="top" style="padding-top: -100px;">
                                                        <strong> Order Id : <?php echo $order->code_order; ?></strong>
                                                        <br />
                                                        <strong> Order Date : <?php echo date("d-m-Y h:i A", strtotime($order->date)); ?></strong>
                                                        <br />
                                                        <h1 style="
                                                        <?php
                                                        if ($order->st == "Unpaid") {
                                                            echo 'color: red;';
                                                        } else if ($order->st == "Paid") {
                                                            echo 'color: greenyellow;';
                                                        }
                                                        ?> ">
                                                            <?php echo $order->st; ?></h1>
                                                    </td>
                                                    <td>
                                                        <strong> Delivery Details :</strong><br />
                                                        <strong> Contact  : <?php echo $order->fullname; ?> <br/> Phone : <?php echo $order->phone; ?></strong><br />
                                                        <strong> Address : </strong>
                                                        <address>
                                                            <?php echo $order->address; ?><br />
                                                        </address>


                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Product Name :</th>
                                        <th> Price :</th>
                                        <th> Qty :</th>
                                        <th> Amount: </th>
                                    </tr>
                                    <?php
                                    $total_price = 0;
                                    foreach ($order_detail->result() as $items) {
                                        ?>
                                        <tr>
                                            <td><?php echo $items->name; ?><br />
                                            </td>
                                            <td><?php echo $items->price_net; ?><br />
                                            </td>
                                            <td>
                                                <?php echo $items->qty; ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo number_format($items->qty * $items->price_net, 0, ',', '.');
                                                $total_price = $total_price + ($items->qty * $items->price);
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right"> Total :</strong></td>
                                        <td >
                                            <strong class=""><?php echo number_format($total_price, 0, ',', '.'); ?>  </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right">Tax :</strong></td>
                                        <td >
                                            <strong class=""><?php echo $order->total_tax; ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right">Discount:</strong></td>
                                        <td >
                                            <strong class=""><?php echo $order->total_discount; ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right">Delivery Charges :</strong></td>
                                        <td >
                                            <strong class=""><?php echo $order->total_delivery; ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right">Net Total Amount :</strong></td>
                                        <td >
                                            <strong class=""><?php echo number_format($total_price, 0, ',', '.'); ?></strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>



            <!-- Main row -->
        </section><!-- /.content -->

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {


        $("#btn_update_status").on('click', function () {
            var data = $("#form_update_status").serialize();
            $.ajax({
                url: "<?php echo base_url('admin/order/r_update_status'); ?>",
                type: 'POST',
                data: data,
                success: function (response) {
                    if (response == "success") {
                        alert("Success, Update status Order");
                       location.reload(); 
                    }
                }
            })

        })



    });
</script>
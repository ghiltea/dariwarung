<div class="row">
    <div class="col-xs-12">

        <!-- return message add-->
        <?php if ($this->session->flashdata('msg_error')) { ?>
            <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('msg_success')) { ?>
            <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <!-- end message -->

        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>
            <div class="box-body">


                <form id="add-row-form" method="post" action="<?php echo base_url() . 'admin/blog/r_update' ?>"  enctype="multipart/form-data">

                    <input hidden="" type="text" name="id_blog_edit" class="form-control hidden" id="recipient-name" readonly value="<?php echo @$blog[0]->id_blog; ?>">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Category :</label>
                        <select class="select2 form-control " name="id_category" style="width: 100%; height:36px;">
                            <option value=""> - Select - </option>
                            <?php foreach ($category->result() as $row) { ?>
                                <option value="<?php echo $row->id_category; ?>" <?php if ($blog[0]->id_category == $row->id_category): ?> selected <?php endif ?> ><?php echo $row->title; ?></option>
                            <?php } ?>
                        </select>
                    </div>        
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Title :</label>
                        <input autofocus  type="text" name="title" class="form-control" id="focus" value="<?php echo @$blog[0]->title; ?>">
                    </div>
                    <div class="form-group">
                        <textarea id="editor1" name="description" rows="10" cols="80">
                     <?php echo @$blog[0]->description; ?>
                        </textarea>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Image (png/jpg and size max 1Mb):</label>
                        <input type="file" name="logo_edit" class="form-control" id="recipient-name">
                    </div>


                     <div class="form-group">
                        <label for="recipient-name" class="control-label">Status :</label>
                        <select class="select2 form-control " name="status" style="width: 100%; height:36px;">
                            <option selected=""> - Select - </option>
                            <option value="Active" <?php if ($blog[0]->status == "Active"): ?> selected <?php endif ?>>Active</option>
                            <option value="No Active" <?php if ($blog[0]->status == "No Active"): ?> selected <?php endif ?>>No Active</option>
                        </select>
                    </div>


                    <div class="modal-footer">
                        <a href="<?php echo base_url('admin/blog'); ?>" type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</a>
                        <button type="submit" class="btn btn-success">Update Data <span class="fa fa-save"></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>






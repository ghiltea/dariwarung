<div class="row">
    <div class="col-xs-12">
        <!-- return message add-->
        <?php if ($this->session->flashdata('msg_error')) { ?>
            <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('msg_success')) { ?>
            <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <!-- end message -->
        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>
            <div class="box-body">
                <form id="add-row-form" method="post" action="<?php echo base_url() . 'admin/member/r_update' ?>">

                    <input hidden="" type="text" name="id_member_edit" class="form-control hidden" id="recipient-name" readonly value="<?php echo $member[0]->id_member; ?>">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">FullName :</label>
                        <input autofocus  type="text" name="fullname" class="form-control" id="focus" value="<?php echo $member[0]->fullname; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Email :</label>
                        <input autofocus  type="text" name="email" class="form-control" id="focus" value="<?php echo $member[0]->email; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Username :</label>
                        <input autofocus  type="text" name="membername" class="form-control" id="focus" value="<?php echo $member[0]->username; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Password:</label>
                        <input autofocus  type="password" name="password" class="form-control" id="focus" value="<?php echo $member[0]->password; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Address:</label>
                        <input autofocus  type="text" name="address" class="form-control" id="focus" value="<?php echo $member[0]->address; ?>">
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Phone :</label>
                        <input type="password" name="phone" class="form-control" id="recipient-name" value="<?php echo $member[0]->phone; ?>">
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Update Lat :</label>
                        <input type="text" name="update_lat" class="form-control" id="recipient-name" value="<?php echo $member[0]->update_lat; ?>">
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Update lon :</label>
                        <input type="text" name="update_lon" class="form-control" id="recipient-name" value="<?php echo $member[0]->update_lon; ?>">
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Type Reseller :</label>
                        <select class="select2 form-control " name="type_reseller" style="width: 100%; height:36px;" >
                            <option selected=""> - Select - </option>
                            <option value="Yes" <?php if ($member[0]->type_reseller == "Yes"): ?> selected <?php endif ?>>Yes</option>
                            <option value="No" <?php if ($member[0]->type_reseller == "No"): ?> selected <?php endif ?>>No</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Type Customer :</label>
                        <select class="select2 form-control " name="type_customer" style="width: 100%; height:36px;">
                            <option selected=""> - Select - </option>
                            <option value="Yes" <?php if ($member[0]->type_customer == "Yes"): ?> selected <?php endif ?>>Yes</option>
                            <option value="No" <?php if ($member[0]->type_customer == "No"): ?> selected <?php endif ?>>No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Status :</label>
                        <select class="select2 form-control " name="status_edit" style="width: 100%; height:36px;">
                            <option value=""> - Select - </option>
                            <option value="Active" <?php if ($member[0]->status == "Active"): ?> selected <?php endif ?>>Active</option>
                            <option value="No Active" <?php if ($member[0]->status == "No Active"): ?> selected <?php endif ?>>No Active</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo base_url('admin/member'); ?>"  type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</a>
                        <button type="submit" class="btn btn-success">Update, Data <span class="fa fa-save"></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


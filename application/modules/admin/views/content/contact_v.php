<div class="row">
  

    <div class="col-xs-12">
          <!-- return message add-->
    <?php if ($this->session->flashdata('msg_error')) { ?>
        <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('msg_success')) { ?>
        <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <!-- end message -->
        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>
            <div class="box-body">
                <form action="<?php echo base_url() . 'admin/content/update_contact' ?>"  role="form" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Address :</label>
                        <input autofocus   type="text" name="address" class="form-control" id="focus"  value="<?php echo @$content->address; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Phone :</label>
                        <input autofocus  type="text" name="phone" class="form-control" id="focus"  value="<?php echo @$content->phone; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Email :</label>
                        <input autofocus   type="text" name="email" class="form-control" id="focus"  value="<?php echo @$content->email; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Website :</label>
                        <input autofocus  type="text" name="website" class="form-control" id="focus"  value="<?php echo @$content->website; ?>">
                    </div>
                    
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Tagline:</label>
                        <input autofocus  type="text" name="tagline" class="form-control" id="focus"  value="<?php echo @$content->tagline; ?>">
                    </div>
                    <br>
                    <button type="submit" name="update_content" class="btn btn-sm btn-primary" ><span class="fa fa-pencil"></span>Update Data</button>

                </form>
            </div>

        </div>
    </div>
</div>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>
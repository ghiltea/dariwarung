<div class="row">


    <div class="col-xs-12">
        <!-- return message add-->
        <?php if ($this->session->flashdata('msg_error')) { ?>
            <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('msg_success')) { ?>
            <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <!-- end message -->
        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>
            <div class="box-body">
                <form id="add-row-form" method="post" action="<?php echo base_url() . 'admin/content/update_featuredcategory' ?>" enctype="multipart/form-data">
                    <hr>        
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Name:</label>
                        <input autofocus   type="text" name="promo_title1" class="form-control" id="focus"  value="<?php echo @$content->promo_title1; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Redirect Link:</label>
                        <input autofocus   type="text" name="promo_link1" class="form-control" id="focus"  value="<?php echo @$content->promo_link1; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Image Center (png/jpg and size max 1Mb):</label>
                        <img style="width: 80px; height: 80px;"src="<?php echo base_url('uploads/category/') . $content->promo_img1; ?>" class="img-responsive">
                        <input  type="file" name="promo_img1" class="form-control" id="recipient-name">
                        <input  hidden="" type="text" name="img1" class="form-control hidden" id="recipient-name" value="<?php echo $content->promo_img1;?>">
                        <input  hidden="" type="text" name="img2" class="form-control hidden" id="recipient-name" value="<?php echo $content->promo_img2;?>">
                        <input  hidden="" type="text" name="img3" class="form-control hidden" id="recipient-name" value="<?php echo $content->promo_img3;?>">
                        <input  hidden="" type="text" name="img4" class="form-control hidden" id="recipient-name" value="<?php echo $content->promo_img4;?>">
                        <input  hidden="" type="text" name="img5" class="form-control hidden" id="recipient-name" value="<?php echo $content->promo_img5;?>">
                    </div>
                    <hr>        
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Name:</label>
                        <input autofocus   type="text" name="promo_title2" class="form-control" id="focus"  value="<?php echo @$content->promo_title2; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Redirect Link:</label>
                        <input autofocus   type="text" name="promo_link2" class="form-control" id="focus"  value="<?php echo @$content->promo_link2; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Image (png/jpg and size max 1Mb):</label>
                        <img style="width: 80px; height: 80px;"src="<?php echo base_url('uploads/category/') . $content->promo_img2; ?>" class="img-responsive">
                        <input  type="file" name="promo_img2" class="form-control" id="recipient-name">
                    </div>
                    <hr>        
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Name:</label>
                        <input autofocus   type="text" name="promo_title3" class="form-control" id="focus"  value="<?php echo @$content->promo_title3; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Redirect Link:</label>
                        <input autofocus   type="text" name="promo_link3" class="form-control" id="focus"  value="<?php echo @$content->promo_link3; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Image (png/jpg and size max 1Mb):</label>
                        <img style="width: 80px; height: 80px;"src="<?php echo base_url('uploads/category/') . $content->promo_img3; ?>" class="img-responsive">
                        <input  type="file" name="promo_img3" class="form-control" id="recipient-name">
                    </div>
                    <hr>        
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Name:</label>
                        <input autofocus   type="text" name="promo_title4" class="form-control" id="focus"  value="<?php echo @$content->promo_title4; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Redirect Link:</label>
                        <input autofocus   type="text" name="promo_link4" class="form-control" id="focus"  value="<?php echo @$content->promo_link4; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Image (png/jpg and size max 1Mb):</label>
                        <img style="width: 80px; height: 80px;"src="<?php echo base_url('uploads/category/') . $content->promo_img4; ?>" class="img-responsive">
                        <input type="file" name="promo_img4" class="form-control" id="recipient-name">
                    </div>
                    <hr>        
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Name:</label>
                        <input autofocus   type="text" name="promo_title5" class="form-control" id="focus"  value="<?php echo @$content->promo_title5; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Redirect Link:</label>
                        <input autofocus   type="text" name="promo_link5" class="form-control" id="focus"  value="<?php echo @$content->promo_link15; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Image (png/jpg and size max 1Mb):</label>
                        <img style="width: 80px; height: 80px;"src="<?php echo base_url('uploads/category/') . $content->promo_img5; ?>" class="img-responsive">
                        <input  type="file" name="promo_img5" class="form-control" id="recipient-name">
                    </div>
                    <hr>        
                    



                    <div class="modal-footer">
                        <a href="<?php echo base_url('admin/banner'); ?>" type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</a>
                        <button type="submit" class="btn btn-success">Save Data <span class="fa fa-save"></span></button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>
<div class="row">
  

    <div class="col-xs-12">
          <!-- return message add-->
    <?php if ($this->session->flashdata('msg_error')) { ?>
        <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('msg_success')) { ?>
        <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
    <?php } ?>
    <!-- end message -->
        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>
            <div class="box-body">
                <form action="<?php echo base_url() . 'admin/content/update_sosmed' ?>"  role="form" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Twitter :</label>
                        <input autofocus   type="text" name="twitter" class="form-control" id="focus"  value="<?php echo @$content->twitter; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Instagram :</label>
                        <input autofocus  type="text" name="instagram" class="form-control" id="focus"  value="<?php echo @$content->instagram; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Facebook :</label>
                        <input autofocus   type="text" name="facebook" class="form-control" id="focus"  value="<?php echo @$content->facebook; ?>">
                    </div>
                    
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Google+ :</label>
                        <input autofocus   type="text" name="googleplus" class="form-control" id="focus"  value="<?php echo @$content->googleplus; ?>">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Youtube Link:</label>
                        <input autofocus   type="text" name="youtube" class="form-control" id="focus"  value="<?php echo @$content->youtube; ?>">
                    </div>
                    <br>
                    <button type="submit" name="update_content" class="btn btn-sm btn-primary" ><span class="fa fa-pencil"></span>Update Data</button>

                </form>
            </div>

        </div>
    </div>
</div>
<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })
</script>
<form id="add-row-form" action="<?php echo base_url() . 'admin/testimoni/r_delete' ?>" method="post">
    <div id="modal_hapus" class="modal fade bs-example-modal-sm" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" id="modalcustome">
                    <h4 class="modal-title" id="mySmallModalLabel">Delete data </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body"> Delete testimoni : <b><span id="testimoni_delete"></span></b> ?</div>
                <input type="text" hidden="" name="id_testimoni_delete" class="form-control hidden">	
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default waves-effect" data-dismiss="modal">No, Thanks</button>
                    <button type="submit" id="hapus" class="btn btn-sm btn-danger waves-effect waves-light">Yes, Delete <span class="fa fa-trash"></span></button>
                </div>
            </div>
        </div>
    </div>
</form>
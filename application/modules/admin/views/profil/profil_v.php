<div class="row">
    <div class="col-xs-12">

        <!-- return message add-->
        <?php if ($this->session->flashdata('msg_error')) { ?>
            <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('msg_success')) { ?>
            <div class="alert alert-success alert-rounded"> <i class="ti-info-alt"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <!-- end message -->

        <div class="box box-info">
            <div class="box box-header">
                <h4><?php echo $title; ?></h4> 
            </div>
            <div class="box-body">


                <form action="<?php echo base_url('admin/profil/r_update'); ?>" method="POST">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Username :</label>
                            <input autofocus  type="text" name="username" class="form-control" id="focus" value="<?php echo $profil->username; ?>">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">password :</label>
                            <input autofocus  type="text" name="password" class="form-control" id="focus">
                        </div>
                       
                        <button type="submit" class="btn btn-success">Update Data <span class="fa fa-save"></span></button>
                    </form>
            </div>
        </div>
    </div>
</div>

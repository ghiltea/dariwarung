<?php

class Message extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Message_m');
    }

    function index() {
        $data['title'] = "Inbox Message";
        $data['description'] = " Inbox Message Page";
        $data['content_view'] = 'admin/message/message_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Message";
        $data['description'] = "Add Message Page";
        $data['content_view'] = 'admin/message/message_add_v';
        $this->template->admin_template($data);
    }

    function edit($id = '') {
        $data['title'] = "Edit Message";
        $data['description'] = "Edit Message Page";
        $data['content_view'] = 'admin/message/message_edit_v';
        $data['message'] = $this->Message_m->get_message($id)->result();
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Message_m->select();
        $data = [];

        foreach ($res->result() as $r) {
            $data[] = array(
//                "<center><a  href='" . base_url('admin/message/edit/') . $r->id_message . "' type='button' id='edit_btn'
//                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
//                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_message ='" . $r->id_message . "'  message_name='" . $r->name . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
//                
                $r->name,
                $r->email,
                $r->subject,
               $r->message
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        date_default_timezone_set('Asia/Jakarta');
        $create_date = date('Y-m-d H:i:s');
        $tanggal_input = $create_date;
        $id_user = $this->session->userdata('role_name');
        $image_name = $nama;

        $data = array(
            'name' => $this->input->post('name'),
            'position' => $this->input->post('position'),
            'message' => $this->input->post('message'),
            'create_by' => $id_user,
            'create_date' => $tanggal_input,
            'status' => $this->input->post('status'),
        );
        $check_result = $this->Message_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add message data Success.");
            redirect('admin/message/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add message data Failed...!");
            redirect('admin/message/add');
        }
    }

    function r_update() {

        date_default_timezone_set('Asia/Jakarta');
        $create_date = date('Y-m-d H:i:s');
        $tanggal_input = $create_date;
        $id_user = $this->session->userdata('role_name');

            $data = array(
                'id_message' => $this->input->post('id_message_edit'),
                'name' => $this->input->post('name'),
                'position' => $this->input->post('position'),
                'message' => $this->input->post('message'),
                'create_by' => $id_user,
                'create_date' => $tanggal_input,
                'status' => $this->input->post('status'),
            );
            $check_result = $this->Message_m->update($data);
       
       
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Message data Success.");
            redirect('admin/message');
        } else {
            $this->session->set_flashdata('msg_error', "Update Message data Failed...!");
            redirect('admin/message');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_message_delete');
        $check_result = $this->Message_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Message data Success.");
            redirect('admin/message');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Message data Failed...!");
            redirect('admin/message');
        }
    }

}

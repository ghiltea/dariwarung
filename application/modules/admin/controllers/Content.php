<?php

class Content extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Content_m');
    }

    function about() {
        $data['title'] = "About's";
        $data['description'] = "About Page";
        $data['content_view'] = 'admin/content/about_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "About"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_about() {
        $category = "About";
        $data = array(
            'title' => $category,
            'description' => $this->input->post('description'),
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/about');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/about');
        }
    }

    function contact() {
        $data['title'] = "Contact's";
        $data['description'] = "Contact Page";
        $data['content_view'] = 'admin/content/contact_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Contact"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_contact() {
        $category = "Contact";
        $data = array(
            'title' => $category,
            'address' => $this->input->post('address'),
            'phone' => $this->input->post('phone'),
            'email' => $this->input->post('email'),
            'website' => $this->input->post('website'),
            'tagline' => $this->input->post('tagline'),
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/contact');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/contact');
        }
    }

    function privacypolicy() {
        $data['title'] = "Privacy Policy's";
        $data['description'] = "Privacy Policy Page";
        $data['content_view'] = 'admin/content/privacypolicy_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Privacy Policy"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_privacypolicy() {
        $category = "Privacy Policy";
        $data = array(
            'title' => $category,
            'description' => $this->input->post('description'),
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/privacypolicy');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/privacypolicy');
        }
    }

    function termsconditions() {
        $data['title'] = "Term Conditions's";
        $data['description'] = "Term Conditions Page";
        $data['content_view'] = 'admin/content/termsconditions_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Term Conditions"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_termsconditions() {
        $category = "Term Conditions";
        $data = array(
            'title' => $category,
            'description' => $this->input->post('description'),
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/termsconditions');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/termsconditions');
        }
    }

    function sosmed() {
        $data['title'] = "Sosial Media's";
        $data['description'] = "Sosial Media Page";
        $data['content_view'] = 'admin/content/sosmed_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Sosial Media"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_sosmed() {
        $category = "Sosial Media";
        $data = array(
            'title' => $category,
            'instagram' => $this->input->post('instagram'),
            'facebook' => $this->input->post('facebook'),
            'googleplus' => $this->input->post('googleplus'),
            'twitter' => $this->input->post('twitter'),
            'youtube' => $this->input->post('youtube'),
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/sosmed');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/sosmed');
        }
    }

    function returnexchange() {
        $data['title'] = "Return Exchange's";
        $data['description'] = "Return Exchange Page";
        $data['content_view'] = 'admin/content/returnexchange_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Return Exchange"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_returnexchange() {
        $category = "Return Exchange";
        $data = array(
            'title' => $category,
            'description' => $this->input->post('description'),
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/returnexchange');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/returnexchange');
        }
    }

    function shippinginformation() {
        $data['title'] = "Shipping Formation's";
        $data['description'] = "Shipping Formation Page";
        $data['content_view'] = 'admin/content/shipinginformation_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Shipping Formation"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_shippinginformation() {
        $category = "Shipping Formation";
        $data = array(
            'title' => $category,
            'description' => $this->input->post('description'),
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/shippinginformation');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/shippinginformation');
        }
    }

    function journal() {
        $data['title'] = "Journal's";
        $data['description'] = "Journal Page";
        $data['content_view'] = 'admin/content/journal_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Journal"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_journal() {
        $category = "Journal";
        $data = array(
            'title' => $category,
            'description' => $this->input->post('description'),
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/journal');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/journal');
        }
    }

    function faqs() {
        $data['title'] = "Faqs 's";
        $data['description'] = "Faqs Page";
        $data['content_view'] = 'admin/content/faqs_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Faqs"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_faqs() {
        $category = "Faqs";
        $data = array(
            'title' => $category,
            'description' => $this->input->post('description'),
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/faqs');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/faqs');
        }
    }

    function banner() {
        $data['title'] = "Banner 's";
        $data['description'] = "Banner Page";
        $data['content_view'] = 'admin/content/banner_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Banner"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_banner() {
        $category = "Banner";
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg', 'WebM');
        $nama = $time . $_FILES['logo']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo']['size'];
        $file_tmp = $_FILES['logo']['tmp_name'];
        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/banner/' . $nama);
            }
        }

        $image_name = $nama;
        $data = array(
            'title' => $category,
            'image_banner' => $nama,
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/banner');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/banner');
        }
    }

    function featuredcategory() {
        $data['title'] = "Featured Category 's";
        $data['description'] = "Featured Category Page";
        $data['content_view'] = 'admin/content/featuredcategory_v';
        $q = $this->db->query('SELECT * FROM tbl_content WHERE title = "Promotion"');
        $row = $q->row();
        $data['content'] = $row;
        $this->template->admin_template($data);
    }

    function update_featuredcategory() {
        $category = "Promotion";
        
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg', 'WebM');
        $nama1 = $time . $_FILES['promo_img1']['name'];
        $nama2 = $time . $_FILES['promo_img2']['name'];
        $nama3 = $time . $_FILES['promo_img3']['name'];
        $nama4 = $time . $_FILES['promo_img4']['name'];
        $nama5 = $time . $_FILES['promo_img5']['name'];
        $x1 = explode('.', $nama1);
        $x2 = explode('.', $nama2);
        $x3 = explode('.', $nama3);
        $x4 = explode('.', $nama4);
        $x5 = explode('.', $nama5);
        $ekstensi1 = strtolower(end($x1));
        $ekstensi2 = strtolower(end($x2));
        $ekstensi3 = strtolower(end($x3));
        $ekstensi4 = strtolower(end($x4));
        $ekstensi5 = strtolower(end($x5));
        $ukuran1 = $_FILES['promo_img1']['size'];
        $ukuran2 = $_FILES['promo_img2']['size'];
        $ukuran3 = $_FILES['promo_img3']['size'];
        $ukuran4 = $_FILES['promo_img4']['size'];
        $ukuran5 = $_FILES['promo_img5']['size'];
        $file_tmp1 = $_FILES['promo_img1']['tmp_name'];
        $file_tmp2 = $_FILES['promo_img2']['tmp_name'];
        $file_tmp3 = $_FILES['promo_img3']['tmp_name'];
        $file_tmp4 = $_FILES['promo_img4']['tmp_name'];
        $file_tmp5 = $_FILES['promo_img5']['tmp_name'];
        if (in_array($ekstensi1, $ekstensi_diperbolehkan) === true 
                || in_array($ekstensi2, $ekstensi_diperbolehkan) === true
                || in_array($ekstensi5, $ekstensi_diperbolehkan) === true
                || in_array($ekstensi3, $ekstensi_diperbolehkan) === true
                || in_array($ekstensi4, $ekstensi_diperbolehkan) === true
                
                ) {
            if ($ukuran1 < 1044070 
                    || $ukuran2 < 1044070
                    || $ukuran3 < 1044070
                    || $ukuran4 < 1044070
                    || $ukuran5 < 1044070
                    ) {
                move_uploaded_file($file_tmp1, 'uploads/category/' . $nama1);
                move_uploaded_file($file_tmp2, 'uploads/category/' . $nama2);
                move_uploaded_file($file_tmp3, 'uploads/category/' . $nama3);
                move_uploaded_file($file_tmp4, 'uploads/category/' . $nama4);
                move_uploaded_file($file_tmp5, 'uploads/category/' . $nama5);
            }
        }
        //---------------//

        if ($ukuran1 <= 0 ) {
            $nama1 = $this->input->post('img1');
        }
        if ($ukuran2 <= 0 ) {
            $nama2 = $this->input->post('img2');
        }
        if ($ukuran3 <= 0 ) {
            $nama3 = $this->input->post('img3');
        }
        if ($ukuran4 <= 0 ) {
            $nama4 = $this->input->post('img4');
        }
        if ($ukuran5 <= 0 ) {
            $nama5 = $this->input->post('img5');
        }
        
        
        $data = array(
            'promo_img1' => $nama1,
            'promo_link1' => $this->input->post('promo_link1'),
            'promo_title1' => $this->input->post('promo_title1'),
            //------//
            'promo_img2' => $nama2,
            'promo_link2' => $this->input->post('promo_link2'),
            'promo_title2' => $this->input->post('promo_title2'),
            //------//
            'promo_img3' => $nama3,
            'promo_link3' => $this->input->post('promo_link3'),
            'promo_title3' => $this->input->post('promo_title3'),
            //------//
            'promo_img4' => $nama4,
            'promo_link4' => $this->input->post('promo_link4'),
            'promo_title4' => $this->input->post('promo_title4'),
            //------//
            'promo_img5' => $nama5,
            'promo_link5' => $this->input->post('promo_link5'),
            'promo_title5' => $this->input->post('promo_title5'),
                //------//
        );
        $this->db->where('title', $category);
        $check_result = $this->db->update('tbl_content', $data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Content data Success.");
            redirect('admin/content/featuredcategory');
        } else {
            $this->session->set_flashdata('msg_error', "Update Content data Failed...!");
            redirect('admin/content/featuredcategory');
        }
    }

}

<?php

class Testimoni extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Testimoni_m');
    }

    function index() {
        $data['title'] = "Testimoni";
        $data['description'] = "Testimoni Page";
        $data['content_view'] = 'admin/testimoni/testimoni_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Testimoni";
        $data['description'] = "Add Testimoni Page";
        $data['content_view'] = 'admin/testimoni/testimoni_add_v';
        $this->template->admin_template($data);
    }

    function edit($id = '') {
        $data['title'] = "Edit Testimoni";
        $data['description'] = "Edit Testimoni Page";
        $data['content_view'] = 'admin/testimoni/testimoni_edit_v';
        $data['testimoni'] = $this->Testimoni_m->get_testimoni($id)->result();
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Testimoni_m->select();
        $data = [];

        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a  href='" . base_url('admin/testimoni/edit/') . $r->id_testimoni . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_testimoni ='" . $r->id_testimoni . "'  testimoni_name='" . $r->name . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
                $r->name,
                substr($r->message, 0, 50),
                $r->position,
                $r->create_by,
                $r->create_date,
                $r->status,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        date_default_timezone_set('Asia/Jakarta');
        $create_date = date('Y-m-d H:i:s');
        $tanggal_input = $create_date;
        $id_user = $this->session->userdata('role_name');
        $image_name = $nama;

        $data = array(
            'name' => $this->input->post('name'),
            'position' => $this->input->post('position'),
            'message' => $this->input->post('message'),
            'create_by' => $id_user,
            'create_date' => $tanggal_input,
            'status' => $this->input->post('status'),
        );
        $check_result = $this->Testimoni_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add testimoni data Success.");
            redirect('admin/testimoni/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add testimoni data Failed...!");
            redirect('admin/testimoni/add');
        }
    }

    function r_update() {

        date_default_timezone_set('Asia/Jakarta');
        $create_date = date('Y-m-d H:i:s');
        $tanggal_input = $create_date;
        $id_user = $this->session->userdata('role_name');

            $data = array(
                'id_testimoni' => $this->input->post('id_testimoni_edit'),
                'name' => $this->input->post('name'),
                'position' => $this->input->post('position'),
                'message' => $this->input->post('message'),
                'create_by' => $id_user,
                'create_date' => $tanggal_input,
                'status' => $this->input->post('status'),
            );
            $check_result = $this->Testimoni_m->update($data);
       
       
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Testimoni data Success.");
            redirect('admin/testimoni');
        } else {
            $this->session->set_flashdata('msg_error', "Update Testimoni data Failed...!");
            redirect('admin/testimoni');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_testimoni_delete');
        $check_result = $this->Testimoni_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Testimoni data Success.");
            redirect('admin/testimoni');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Testimoni data Failed...!");
            redirect('admin/testimoni');
        }
    }

}

<?php

class Subcribe extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Subcribe_m');
    }

    function index() {
        $data['title'] = "Inbox Subcribe And News";
        $data['description'] = " Inbox Subcribe Page";
        $data['content_view'] = 'admin/subcribe/subcribe_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Subcribe";
        $data['description'] = "Add Subcribe Page";
        $data['content_view'] = 'admin/subcribe/subcribe_add_v';
        $this->template->admin_template($data);
    }

    function edit($id = '') {
        $data['title'] = "Edit Subcribe";
        $data['description'] = "Edit Subcribe Page";
        $data['content_view'] = 'admin/subcribe/subcribe_edit_v';
        $data['subcribe'] = $this->Subcribe_m->get_subcribe($id)->result();
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Subcribe_m->select();
        $data = [];

        foreach ($res->result() as $r) {
            $data[] = array(
//                "<center><a  href='" . base_url('admin/subcribe/edit/') . $r->id_subcribe . "' type='button' id='edit_btn'
//                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
//                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_subcribe ='" . $r->id_subcribe . "'  subcribe_name='" . $r->name . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",

                $r->email,
//                $r->subject,
//               $r->subcribe
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $data = array(
            'email' => $this->input->post('email'),
        );
        $check_result = $this->Subcribe_m->insert($data);
        if ($check_result != FALSE) {
            echo "success";
        } else {
            echo "failed";
        }
    }
    

    function r_update() {

        date_default_timezone_set('Asia/Jakarta');
        $create_date = date('Y-m-d H:i:s');
        $tanggal_input = $create_date;
        $id_user = $this->session->userdata('role_name');

            $data = array(
                'id_subcribe' => $this->input->post('id_subcribe_edit'),
                'name' => $this->input->post('name'),
                'position' => $this->input->post('position'),
                'subcribe' => $this->input->post('subcribe'),
                'create_by' => $id_user,
                'create_date' => $tanggal_input,
                'status' => $this->input->post('status'),
            );
            $check_result = $this->Subcribe_m->update($data);
       
       
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Subcribe data Success.");
            redirect('admin/subcribe');
        } else {
            $this->session->set_flashdata('msg_error', "Update Subcribe data Failed...!");
            redirect('admin/subcribe');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_subcribe_delete');
        $check_result = $this->Subcribe_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Subcribe data Success.");
            redirect('admin/subcribe');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Subcribe data Failed...!");
            redirect('admin/subcribe');
        }
    }

}

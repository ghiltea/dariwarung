<?php

class Order extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Order_m');
    }

    function notif_count() {
        $this->db->select('b.*, a.fullname, a.email');
        $this->db->join('tbl_member a', 'a.id_member = b.id_member', 'left');
        $data = $this->db->get('tbl_order b');
        $row = $data->num_rows();
        echo $row;
    }

    function notif_list() {
        $this->db->select('b.*, a.fullname, a.email');
        $this->db->join('tbl_member a', 'a.id_member = b.id_member', 'left');
        $data = $this->db->get('tbl_order b');
        foreach ($data->result() as $row) {
            $html = ' <li><!-- start message -->
                      <a href="' . base_url('admin/order/edit/') . $row->id_order . '">
                        <div class="pull-left">
                          <!-- User Image -->
                         <span class="fa fa-2x fa-file-text"></span>
                        </div>
                        <!-- Message title and timestamp -->
                        <h4>
                        ' . $row->code_order . '
                          <small><i class="fa fa-clock-o"></i> ' . $row->date . '</small>
                        </h4>
                        <!-- The message -->
                        <p>' . $row->fullname . '</p>
                      </a>
                    </li>';
            echo $html;
        }
    }

    function index() {
        $data['title'] = "Order";
        $data['description'] = "Order Page";
        $data['content_view'] = 'admin/order/order_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Order";
        $data['description'] = "Add Order Page";
        $data['content_view'] = 'admin/order/order_add_v';
        $data['category'] = $this->db->get('tbl_order');
        $this->template->admin_template($data);
    }

    function edit($id = '') {
        $data['title'] = "Update Status Order";
        $data['description'] = "Update Status Order Page";
        $data['content_view'] = 'admin/order/order_edit_v';

        $id_vendor = NULL;

        $order = $this->Order_m->get_order($id);
        $data['order'] = $order->row();

        $order_detail = $this->Order_m->get_order_detail($id, $id_vendor);
        $data['order_detail'] = $order_detail;

        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Order_m->select();
        $data = [];

        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a  href='" . base_url('admin/order/edit/') . $r->id_order . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Update Order' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_order ='" . $r->id_order . "'  code_order='" . $r->code_order . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
                $r->code_order,
                $r->date,
                $r->fullname . "|" . $r->email,
                $r->total_qty,
                number_format($r->total_discount, 0, ",", "."),
                number_format($r->total_tax, 0, ",", "."),
                number_format($r->total_delivery, 0, ",", "."),
                number_format($r->total_amount, 0, ",", "."),
                $r->status,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg', 'WebM');
        $nama = $time . $_FILES['logo']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo']['size'];
        $file_tmp = $_FILES['logo']['tmp_name'];
        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/order/' . $nama);
            }
        }

        $image_name = $nama;
        $data = array(
            'name' => $this->input->post('name'),
            'id_category' => $this->input->post('id_category'),
            'sku' => $this->input->post('sku'),
            'description' => $this->input->post('description'),
            'price' => $this->input->post('price'),
            'discount' => $this->input->post('discount'),
            'image' => $this->input->post('image'),
            'weight' => $this->input->post('weight'),
            'featured' => $this->input->post('featured'),
            'promotion' => $this->input->post('promotion'),
            'status' => $this->input->post('status'),
            'image' => $nama
        );
        $check_result = $this->Order_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add order data Success.");
            redirect('admin/order/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add order data Failed...!");
            redirect('admin/order/add');
        }
    }

    function r_update() {
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg', 'WebM');
        $nama = $time . $_FILES['logo_edit']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo_edit']['size'];
        $file_tmp = $_FILES['logo_edit']['tmp_name'];

        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/order/' . $nama);
            }
        }


        if ($ukuran > 0) {
            $data = array(
                'id_order' => $this->input->post('id_order_edit'),
                'name' => $this->input->post('name'),
                'id_category' => $this->input->post('id_category'),
                'sku' => $this->input->post('sku'),
                'description' => $this->input->post('description'),
                'price' => $this->input->post('price'),
                'discount' => $this->input->post('discount'),
                'weight' => $this->input->post('weight'),
                'featured' => $this->input->post('featured'),
                'promotion' => $this->input->post('promotion'),
                'status' => $this->input->post('status'),
                'image' => $nama
            );
            $check_result = $this->Order_m->update($data);
        } else {
            $data_no_file = array(
                'id_order' => $this->input->post('id_order_edit'),
                'name' => $this->input->post('name'),
                'id_category' => $this->input->post('id_category'),
                'sku' => $this->input->post('sku'),
                'description' => $this->input->post('description'),
                'price' => $this->input->post('price'),
                'discount' => $this->input->post('discount'),
                'weight' => $this->input->post('weight'),
                'featured' => $this->input->post('featured'),
                'promotion' => $this->input->post('promotion'),
                'status' => $this->input->post('status')
            );
            $check_result = $this->Order_m->update($data_no_file);
        }
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Order data Success.");
            redirect('admin/order');
        } else {
            $this->session->set_flashdata('msg_error', "Update Order data Failed...!");
            redirect('admin/order');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_order_delete');
        $check_result = $this->Order_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Order data Success.");
            redirect('admin/order');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Order data Failed...!");
            redirect('admin/order');
        }
    }

    function r_update_status() {
        $status = $this->input->post('status');
        $id_order = $this->input->post('id_order');

        $this->db->where('id_order', $id_order);
        $data = $this->db->update('tbl_order', array("status" => $status));
        if ($data != FALSE) {
            echo "success";
        } else {
            echo "failed";
        }
    }

}

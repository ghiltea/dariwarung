<?php

class Profil extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        $data['title'] = "Setting";
        $data['description'] = "Setting Page";
        $data['content_view'] = 'admin/profil/profil_v';
        $this->db->where('id_user', $this->session->userdata('id_user'));
        $profil = $this->db->get('tbl_user');
        $data['profil'] = $profil->row();
        $this->template->admin_template($data);
    }

    function r_update() {
        $pass = $this->input->post('password');
        if ($pass == "") {
            $data = array(
                "username" => $this->input->post('username'),
            );
            $this->db->where('id_user', $this->session->userdata('id_user'));
            $this->db->update('tbl_user', $data);
            $this->session->set_flashdata('msg_success', "Update data Success.");
            redirect('admin/profil');
        } else {
            $data = array(
                "username" => $this->input->post('username'),
                "password" => md5($this->input->post('password')),
            );
             $this->db->where('id_user', $this->session->userdata('id_user'));
            $this->db->update('tbl_user', $data);
            $this->session->set_flashdata('msg_success', "Update data Success.");
            redirect('admin/profil');
        }
    }

}

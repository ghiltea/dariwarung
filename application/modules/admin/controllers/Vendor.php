<?php

class Vendor extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Vendor_m');
    }

    function index() {
        $data['title'] = "Vendor's";
        $data['description'] = "Vendor Page";
        $data['content_view'] = 'admin/vendor/vendor_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Vendor's";
        $data['description'] = "Add Vendor Page";
        $data['content_view'] = 'admin/vendor/vendor_add_v';
        $this->template->admin_template($data);
    }

    function edit($id = '') {
       
        $data['title'] = "Edit Vendor's";
        $data['description'] = "Edit Vendor Page";
        $data['content_view'] = 'admin/vendor/vendor_edit_v';
        $getdata = $this->Vendor_m->get_vendor($id)->result();
        $data['vendor'] = $getdata;
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Vendor_m->select();
        $data = [];
        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a  href='" . base_url('admin/vendor/edit/') . $r->id_vendor . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_vendor ='" . $r->id_vendor . "'  fullname='" . $r->fullname . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",

                $r->fullname,
                $r->email,
                $r->username,
                $r->password,
                $r->address,
                $r->phone,
                $r->update_lat,
                $r->update_lon,
                $r->status,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $data = array(
            'fullname'=>$this->input->post('fullname'),
            'email'=>$this->input->post('email'),
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'address'=>$this->input->post('address'),
            'phone'=>$this->input->post('phone'),
            'update_lat'=>$this->input->post('update_lat'),
            'update_lon'=>$this->input->post('update_lon'),
            'status'=>$this->input->post('status')
        );
        $check_result = $this->Vendor_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add Vendor data Success.");
            redirect('admin/vendor/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add Vendor data Failed...!");
            redirect('admin/vendor/add');
        }
    }

    function r_update() {
        $data = array(
            'id_vendor' => $this->input->post('id_vendor_edit'),
            'fullname'=>$this->input->post('fullname'),
            'email'=>$this->input->post('email'),
            'username'=>$this->input->post('username'),
            'password'=>md5($this->input->post('password')),
            'address'=>$this->input->post('address'),
            'phone'=>$this->input->post('phone'),
            'update_lat'=>$this->input->post('update_lat'),
            'update_lon'=>$this->input->post('update_lon'),
            'status'=>$this->input->post('status')
        );
        $check_result = $this->Vendor_m->update($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Vendor data Success.");
            redirect('admin/vendor');
        } else {
            $this->session->set_flashdata('msg_error', "Update Vendor data Failed...!");
            redirect('admin/vendor');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_vendor_delete');
        $check_result = $this->Vendor_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Vendor data Success.");
            redirect('admin/vendor');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Vendor data Failed...!");
            redirect('admin/vendor');
        }
    }

}

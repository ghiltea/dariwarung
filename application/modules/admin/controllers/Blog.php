<?php

class Blog extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Blog_m');
    }

    function index() {
        $data['title'] = "Blog";
        $data['description'] = "Blog Page";
        $data['content_view'] = 'admin/blog/blog_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Blog";
        $data['description'] = "Add Blog Page";
        $data['content_view'] = 'admin/blog/blog_add_v';
        $data['category'] = $this->db->get('tbl_blog_category');
        $this->template->admin_template($data);
    }

    function edit($id = '') {
        $data['title'] = "Edit Blog";
        $data['description'] = "Edit Blog Page";
        $data['content_view'] = 'admin/blog/blog_edit_v';
        $data['blog'] = $this->Blog_m->get_blog($id)->result();
        $data['category'] = $this->db->get('tbl_blog_category');
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Blog_m->select();
        $data = [];

        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a  href='" . base_url('admin/blog/edit/') . $r->id_blog . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_blog ='" . $r->id_blog . "'  blog_name='" . $r->title . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
                $r->title,
                "<a  href='" . base_url('uploads/blog/') . $r->image . "' target='_blank'><img  id='img_blog'  style='width:60px; height=60px;' src='" . base_url('uploads/blog/') . $r->image . "'  class='img-responsive img-thumbnail' ></a>",
                substr($r->description, 0, 50),
                $r->create_by,
                $r->create_date,
                $r->status,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg', 'WebM');
        $nama = $time . $_FILES['logo']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo']['size'];
        $file_tmp = $_FILES['logo']['tmp_name'];
        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/blog/' . $nama);
            }
        }
        date_default_timezone_set('Asia/Jakarta');
        $create_date = date('Y-m-d H:i:s');
        $tanggal_input = $create_date;
        $id_user = $this->session->userdata('role_name');
        $image_name = $nama;

        $data = array(
            'title' => $this->input->post('title'),
            'id_category' => $this->input->post('id_category'),
            'description' => $this->input->post('description'),
            'create_by' => $id_user,
            'create_date' => $tanggal_input,
            'status' => $this->input->post('status'),
            'image' => $nama
        );
        $check_result = $this->Blog_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add blog data Success.");
            redirect('admin/blog/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add blog data Failed...!");
            redirect('admin/blog/add');
        }
    }

    function r_update() {
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg', 'WebM');
        $nama = $time . $_FILES['logo_edit']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo_edit']['size'];
        $file_tmp = $_FILES['logo_edit']['tmp_name'];

        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/blog/' . $nama);
            }
        }
        date_default_timezone_set('Asia/Jakarta');
        $create_date = date('Y-m-d H:i:s');
        $tanggal_input = $create_date;
        $id_user = $this->session->userdata('role_name');

        if ($ukuran > 0) {
            $data = array(
                  'id_blog' => $this->input->post('id_blog_edit'),
                'title' => $this->input->post('title'),
                'id_category' => $this->input->post('id_category'),
                'description' => $this->input->post('description'),
                'create_by' => $id_user,
                'create_date' => $tanggal_input,
                'status' => $this->input->post('status'),
                'image' => $nama
            );
            $check_result = $this->Blog_m->update($data);
        } else {
            $data_no_file = array(
                'id_blog' => $this->input->post('id_blog_edit'),
                'title' => $this->input->post('title'),
                'id_category' => $this->input->post('id_category'),
                'description' => $this->input->post('description'),
                'create_by' => $id_user,
                'create_date' => $tanggal_input,
                'status' => $this->input->post('status')
            );
            $check_result = $this->Blog_m->update($data_no_file);
        }
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Blog data Success.");
            redirect('admin/blog');
        } else {
            $this->session->set_flashdata('msg_error', "Update Blog data Failed...!");
            redirect('admin/blog');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_blog_delete');
        $check_result = $this->Blog_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Blog data Success.");
            redirect('admin/blog');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Blog data Failed...!");
            redirect('admin/blog');
        }
    }

}

<?php

class Role extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Role_m');
    }

    function index() {
        $data['title'] = "Role's";
        $data['description'] = "Role Page";
        $data['content_view'] = 'admin/role/role_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Role's";
        $data['description'] = "Add Role Page";
        $data['content_view'] = 'admin/role/role_add_v';
        $this->template->admin_template($data);
    }
            
    function menu($id = '') {
        $getdata = $this->Role_m->get_role($id)->result();
        $data['role'] = $getdata;
        $data['role_menu'] = $this->Role_m->get_role_menu($id);;
        $data['title'] = "Menu Role's";
        $data['description'] = "Menu Role Page";
        $data['content_view'] = 'admin/role/role_menu_v';
        $this->template->admin_template($data);
    }

    
    
    function edit($id = '') {
        $getdata = $this->Role_m->get_role($id)->result();
        $data['role'] = $getdata;
        $data['title'] = "Edit Role's";
        $data['description'] = "Edit Role Page";
        $data['content_view'] = 'admin/role/role_edit_v';
        $this->template->admin_template($data);
    }
    
    

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Role_m->select();
        $data = [];
        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a href='" . base_url('admin/role/edit/') . $r->id_role . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                <a href='" . base_url('admin/role/menu/') . $r->id_role . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-warning waves-effect'  title ='Setting Menu' >&nbsp<i class='fa fa-list'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_role ='" . $r->id_role . "'  role_name='" . $r->role_name . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
                $r->role_name,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $data = array(
            'role_name' => $this->input->post('role_name')
        );
        $check_result = $this->Role_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add Role data Success.");
            redirect('admin/role/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add Role data Failed...!");
            redirect('admin/role/add');
        }
    }

    function r_update_role_menu() {
        $ckb = $this->input->post('ckb');
        $idRole = $this->input->post('id_role_edit');
        $dt = array();
        $data = array();
        for ($i = 0; $i < count($ckb); $i++) {
            $data[] = $ckb[$i];
        }
        $d = implode(",", $data);
        $this->db->where("id_role", $idRole);
        $check_result = $this->db->update("tbl_role", array("role_access" => $d));
          if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Menu data Success.");
            redirect('admin/role');
        } else {
            $this->session->set_flashdata('msg_error', "Update Menu data Failed...!");
            redirect('admin/role');
        }
    }

    function r_update() {

        $data = array(
            'id_role' => $this->input->post('id_role_edit'),
            'role_name' => $this->input->post('role_name_edit')
        );
        $check_result = $this->Role_m->update($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Role data Success.");
            redirect('admin/role');
        } else {
            $this->session->set_flashdata('msg_error', "Update Role data Failed...!");
            redirect('admin/role');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_role_delete');
        $check_result = $this->Role_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Role data Success.");
            redirect('admin/role');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Role data Failed...!");
            redirect('admin/role');
        }
    }

}

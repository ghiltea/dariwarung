<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_m');
    }

    function index() {
        $data['title'] = "User's";
        $data['description'] = "User Page";
        $data['content_view'] = 'admin/user/user_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add User's";
        $data['description'] = "Add User Page";
        $data['content_view'] = 'admin/user/user_add_v';
        $data['user_role'] = $this->User_m->get_role();
        $this->template->admin_template($data);
    }

    function edit($id = '') {
       
        $data['title'] = "Edit User's";
        $data['description'] = "Edit User Page";
        $data['content_view'] = 'admin/user/user_edit_v';
        $data['user_role'] = $this->User_m->get_role();
        $getdata = $this->User_m->get_user($id)->result();
        $data['user'] = $getdata;
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->User_m->select();
        $data = [];
        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a  href='" . base_url('admin/user/edit/') . $r->id_user . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_user ='" . $r->id_user . "'  username='" . $r->username . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
                $r->username,
                "***********",
                $r->role_name,
                $r->status,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $data = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'id_role' => $this->input->post('id_role'),
            'status' => $this->input->post('status')
        );
        $check_result = $this->User_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add User data Success.");
            redirect('admin/user/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add User data Failed...!");
            redirect('admin/user/add');
        }
    }

    function r_update() {
        $data = array(
            'id_user' => $this->input->post('id_user_edit'),
            'username' => $this->input->post('username_edit'),
            'password' => md5($this->input->post('password_edit')),
            'id_role' => $this->input->post('id_role_edit'),
            'status' => $this->input->post('status_edit')
        );
        $check_result = $this->User_m->update($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update User data Success.");
            redirect('admin/user');
        } else {
            $this->session->set_flashdata('msg_error', "Update User data Failed...!");
            redirect('admin/user');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_user_delete');
        $check_result = $this->User_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete User data Success.");
            redirect('admin/user');
        } else {
            $this->session->set_flashdata('msg_error', "Delete User data Failed...!");
            redirect('admin/user');
        }
    }

}

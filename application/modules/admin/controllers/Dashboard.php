<?php

class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Dashboard_m');
    }

    function index() {
        if (!$this->session->admin_islogin) {
            redirect('admin/login');
            exit();
        }

        //r_order
        $this->db->select('b.*,b.status as st, a.*');
        $this->db->order_by('id_order', 'DESC');
        $this->db->join('tbl_member a', 'a.id_member = b.id_member', 'left');
        $order = $this->db->get('tbl_order b');
        $data['order'] = $order;

        //r_top10
        $top_prod = $this->db->query('SELECT
	DISTINCT tbl_order_detail.name, tbl_order_detail.id_product, tbl_order_detail.price, SUM(tbl_order_detail.qty) as tot_qty
        FROM
	tbl_product
	INNER JOIN
	tbl_order_detail
	ON 
		tbl_product.id_product = tbl_order_detail.id_product
        GROUP BY 
        tbl_order_detail.id_product');
        $data['top_prod'] = $top_prod;

        //r_tot_user
        $tot_user = $this->db->get('tbl_user');
        $data['tot_user'] = $tot_user->num_rows();

        //r_tot_customer
        $tot_customer = $this->db->get('tbl_member');
        $data['tot_customer'] = $tot_customer->num_rows();

        //r_tot_order
        $tot_order = $this->db->get('tbl_order');
        $data['tot_order'] = $tot_order->num_rows();

        //r_tot_nominalorder
        $tot_nominalorder = $this->db->query('SELECT SUM(total_amount) AS total_amount FROM tbl_order')->row();
        $data['tot_nominalorder'] = $tot_nominalorder;

        $data['title'] = "Dashboard";
        $data['description'] = "Dashboard Page";
        $data['content_view'] = 'admin/dashboard/dashboard_v';
        $this->template->admin_template($data);
    }

}

<?php

class Setting extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Setting_m');
    }

    function index() {
        $data['title'] = "Setting";
        $data['description'] = "Setting Page";
        $data['content_view'] = 'admin/setting/setting_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Setting";
        $data['description'] = "Add Setting Page";
        $data['content_view'] = 'admin/setting/setting_add_v';
        $this->template->admin_template($data);
    }

    function edit($id = '') {
        $data['title'] = "Edit Setting";
        $data['description'] = "Edit Setting Page";
        $data['content_view'] = 'admin/setting/setting_edit_v';
        $data['setting'] = $this->Setting_m->get_setting($id)->result();
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Setting_m->select();
        $data = [];

        foreach ($res->result() as $r) {
            $script = substr($r->script_include, 0, 150);
            $data[] = array(
                "<center><a  href='" . base_url('admin/setting/edit/') . $r->id_setting . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_setting ='" . $r->id_setting . "'  setting_name='" . $r->page . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
                $r->page,
                "$script",
                $r->status,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {

        $data = array(
            'page' => $this->input->post('page'),
            'script_include' => $this->input->post('script_include'),
            'status' => $this->input->post('status'),
        );
        $check_result = $this->Setting_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add setting data Success.");
            redirect('admin/setting/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add setting data Failed...!");
            redirect('admin/setting/add');
        }
    }

    function r_update() {

        $data = array(
            'id_setting' => $this->input->post('id_setting_edit'),
            'page' => $this->input->post('page'),
            'script_include' => $this->input->post('script_include'),
            'status' => $this->input->post('status'),
        );
        $check_result = $this->Setting_m->update($data);


        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Setting data Success.");
            redirect('admin/setting');
        } else {
            $this->session->set_flashdata('msg_error', "Update Setting data Failed...!");
            redirect('admin/setting');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_setting_delete');
        $check_result = $this->Setting_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Setting data Success.");
            redirect('admin/setting');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Setting data Failed...!");
            redirect('admin/setting');
        }
    }

}

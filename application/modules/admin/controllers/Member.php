<?php

class Member extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Member_m');
    }

    function index() {
        $data['title'] = "Member's";
        $data['description'] = "Member Page";
        $data['content_view'] = 'admin/member/member_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Member's";
        $data['description'] = "Add Member Page";
        $data['content_view'] = 'admin/member/member_add_v';
        $this->template->admin_template($data);
    }

    function edit($id = '') {
       
        $data['title'] = "Edit Member's";
        $data['description'] = "Edit Member Page";
        $data['content_view'] = 'admin/member/member_edit_v';
        $getdata = $this->Member_m->get_member($id)->result();
        $data['member'] = $getdata;
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Member_m->select();
        $data = [];
        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a  href='" . base_url('admin/member/edit/') . $r->id_member . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_member ='" . $r->id_member . "'  fullname='" . $r->fullname . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",

                $r->fullname,
                $r->email,
                $r->username,
                $r->password,
                $r->address,
                $r->phone,
                $r->update_lat,
                $r->update_lon,
                $r->type_seller,
                $r->status,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $data = array(
            'fullname'=>$this->input->post('fullname'),
            'email'=>$this->input->post('email'),
            'username'=>$this->input->post('username'),
            'password'=> md5($this->input->post('password')),
            'address'=>$this->input->post('address'),
            'phone'=>$this->input->post('phone'),
            'update_lat'=>$this->input->post('update_lat'),
            'update_lon'=>$this->input->post('update_lon'),
            'type_seller'=>$this->input->post('type_seller'),
            'status'=>$this->input->post('status')
        );
        $check_result = $this->Member_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add Member data Success.");
            redirect('admin/member/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add Member data Failed...!");
            redirect('admin/member/add');
        }
    }

    function r_update() {
        $data = array(
            'id_member' => $this->input->post('id_member_edit'),
            'fullname'=>$this->input->post('fullname'),
            'email'=>$this->input->post('email'),
            'username'=>$this->input->post('username'),
            'password'=>$this->input->post('password'),
            'address'=>$this->input->post('address'),
            'phone'=>$this->input->post('phone'),
            'update_lat'=>$this->input->post('update_lat'),
            'update_lon'=>$this->input->post('update_lon'),
            'type_seller'=>$this->input->post('type_seller'),
            'status'=>$this->input->post('status')
        );
        $check_result = $this->Member_m->update($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Member data Success.");
            redirect('admin/member');
        } else {
            $this->session->set_flashdata('msg_error', "Update Member data Failed...!");
            redirect('admin/member');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_member_delete');
        $check_result = $this->Member_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Member data Success.");
            redirect('admin/member');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Member data Failed...!");
            redirect('admin/member');
        }
    }

}

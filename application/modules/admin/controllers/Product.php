<?php

class Product extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Product_m');
    }

    function index() {
        $data['title'] = "Product App";
        $data['description'] = "Product App Page";
        $data['content_view'] = 'admin/product/product_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Product App";
        $data['description'] = "Add Product App Page";
        $data['content_view'] = 'admin/product/product_add_v';
        $data['category'] = $this->db->get('tbl_product_category');
        $this->template->admin_template($data);
    }

    function edit($id = '') {
        $data['title'] = "Edit Product App";
        $data['description'] = "Edit Product App Page";
        $data['content_view'] = 'admin/product/product_edit_v';
        $data['product'] = $this->Product_m->get_product($id)->result();
        $data['category'] = $this->db->get('tbl_product_category');
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Product_m->select();
        $data = [];

        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a  href='" . base_url('admin/product/edit/') . $r->id_product . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_product ='" . $r->id_product . "'  product_name='" . $r->name . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
                $r->sku,
                "<a  href='" . base_url('uploads/product/') . $r->image . "' target='_blank'><img  id='img_product'  style='width:60px; height=60px;' src='" . base_url('uploads/product/') . $r->image . "'  class='img-responsive img-thumbnail' ></a>",
                $r->name,
                substr($r->description, 0, 50),
                number_format($r->price, 0, ",", "."),
                $r->discount . "%",
                $r->weight,
                $r->featured,
                $r->promotion,
                $r->status,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg', 'WebM');
        $nama = $time . $_FILES['logo']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo']['size'];
        $file_tmp = $_FILES['logo']['tmp_name'];
        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/product/' . $nama);
            }
        }

        $image_name = $nama;
        $data = array(
            'name' => $this->input->post('name'),
            'id_category' => $this->input->post('id_category'),
            'sku' => $this->input->post('sku'),
            'description' => $this->input->post('description'),
            'seo_title' => $this->input->post('seo_title'),
            'seo_keyword' => $this->input->post('seo_keyword'),
            'seo_description' => $this->input->post('seo_description'),
            'seo_canonical' => $this->input->post('seo_canonical'),
            'seo_img_alt' => $this->input->post('seo_image_alt'),
            'price' => $this->input->post('price'),
            'discount' => $this->input->post('discount'),
            'image' => $this->input->post('image'),
            'weight' => $this->input->post('weight'),
            'featured' => $this->input->post('featured'),
            'promotion' => $this->input->post('promotion'),
            'status' => $this->input->post('status'),
            'image' => $nama
        );
        $check_result = $this->Product_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add product data Success.");
            redirect('admin/product/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add product data Failed...!");
            redirect('admin/product/add');
        }
    }

    function r_update() {
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg', 'WebM');
        $nama = $time . $_FILES['logo_edit']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo_edit']['size'];
        $file_tmp = $_FILES['logo_edit']['tmp_name'];

        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/product/' . $nama);
            }
        }


        if ($ukuran > 0) {
            $data = array(
                'id_product' => $this->input->post('id_product_edit'),
                'name' => $this->input->post('name'),
                'id_category' => $this->input->post('id_category'),
                'sku' => $this->input->post('sku'),
                'description' => $this->input->post('description'),
                  'seo_title' => $this->input->post('seo_title'),
                'seo_keyword' => $this->input->post('seo_keyword'),
                'seo_description' => $this->input->post('seo_description'),
                'seo_canonical' => $this->input->post('seo_canonical'),
                'seo_img_alt' => $this->input->post('seo_image_alt'),
                'price' => $this->input->post('price'),
                'discount' => $this->input->post('discount'),
                'weight' => $this->input->post('weight'),
                'featured' => $this->input->post('featured'),
                'promotion' => $this->input->post('promotion'),
                'status' => $this->input->post('status'),
                'image' => $nama
            );
            $check_result = $this->Product_m->update($data);
        } else {
            $data_no_file = array(
                'id_product' => $this->input->post('id_product_edit'),
                'name' => $this->input->post('name'),
                'id_category' => $this->input->post('id_category'),
                'sku' => $this->input->post('sku'),
                'description' => $this->input->post('description'),
                  'seo_title' => $this->input->post('seo_title'),
                'seo_keyword' => $this->input->post('seo_keyword'),
                'seo_description' => $this->input->post('seo_description'),
                'seo_canonical' => $this->input->post('seo_canonical'),
                'seo_img_alt ' => $this->input->post('seo_image_alt'),
                'price' => $this->input->post('price'),
                'discount' => $this->input->post('discount'),
                'weight' => $this->input->post('weight'),
                'featured' => $this->input->post('featured'),
                'promotion' => $this->input->post('promotion'),
                'status' => $this->input->post('status')
            );
            $check_result = $this->Product_m->update($data_no_file);
        }
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Product data Success.");
            redirect('admin/product');
        } else {
            $this->session->set_flashdata('msg_error', "Update Product data Failed...!");
            redirect('admin/product');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_product_delete');
        $check_result = $this->Product_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Product data Success.");
            redirect('admin/product');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Product data Failed...!");
            redirect('admin/product');
        }
    }

}

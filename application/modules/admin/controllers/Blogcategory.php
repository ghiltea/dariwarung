<?php

class BlogCategory extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('BlogCategory_m');
    }

    function index() {
        $data['title'] = "Blog Category";
        $data['description'] = "Blog Category Page";
        $data['content_view'] = 'admin/blogcategory/category_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Blog Category";
        $data['description'] = "Add Blog Category Page";
        $data['content_view'] = 'admin/blogcategory/category_add_v';
        $this->template->admin_template($data);
    }

    function edit($id = '') {
        $data['title'] = "Edit Blog Category";
        $data['description'] = "Edit Blog Category Page";
        $data['content_view'] = 'admin/blogcategory/category_edit_v';
        $data['category'] = $this->BlogCategory_m->get_category($id)->result();
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->BlogCategory_m->select();
        $data = [];
        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a  href='" . base_url('admin/blogcategory/edit/') . $r->id_category . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_category ='" . $r->id_category . "'  category_name='" . $r->title . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
                $r->title,
                "<a  href='" . base_url('uploads/category/') . $r->image . "' target='_blank'><img  id='img_product'  style='width:60px; height=60px;' src='" . base_url('uploads/category/') . $r->image . "'  class='img-responsive img-thumbnail' ></a>"
               
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg','jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg','WebM');
        $nama = $time . $_FILES['logo']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo']['size'];
        $file_tmp = $_FILES['logo']['tmp_name'];
        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/category/' . $nama);
            }
        }

           $image_name = $nama;
           $title = $this->input->post('category_name');
        $data = array(
            'title' => $title,
            'image' => $nama
        );
        $check_result = $this->BlogCategory_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add category data Success.");
            redirect('admin/blogcategory/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add category data Failed...!");
            redirect('admin/blogcategory/add');
        }
    }

    function r_update() {
        $ekstensi_diperbolehkan = array('png', 'jpg','jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg','WebM');
        $nama = $time . $_FILES['logo_edit']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo_edit']['size'];
        $file_tmp = $_FILES['logo_edit']['tmp_name'];

        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/category/' . $nama);
            }
        }

        $data = array(
            'id_category' => $this->input->post('id_category_edit'),
            'title' => $this->input->post('category_name_edit'),
            'image' => $nama
        );
        $data_no_file = array(
            'id_category' => $this->input->post('id_category_edit'),
            'title' => $this->input->post('category_name_edit')
        );
        if ($ukuran > 0) {
            $check_result = $this->BlogCategory_m->update($data);
        } else {
            $check_result = $this->BlogCategory_m->update($data_no_file);
        }
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Category data Success.");
            redirect('admin/blogcategory');
        } else {
            $this->session->set_flashdata('msg_error', "Update Category data Failed...!");
            redirect('admin/blogcategory');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_category_delete');
        $check_result = $this->BlogCategory_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Category data Success.");
            redirect('admin/blogcategory');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Category data Failed...!");
            redirect('admin/blogcategory');
        }
    }

}

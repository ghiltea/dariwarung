<?php

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
    }

    function index() {
        if ($this->session->admin_islogin) {
            redirect('admin/dashboard');
            exit();
        }
        $data['content_view'] = 'admin/login/login_v';
        $data['title'] = 'Login';
        $data['description'] = 'Login Page';
        $this->template->admin_login_template($data);
    }

    function r_login() {
        $validation_rules = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Username belum diisi !')
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|alpha_numeric',
                'errors' => array('required' => 'Password belum diisi !', 'alpha_numeric' => 'Input symbol/karakter tidak diperbolehkan !')
            )
        );
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login';
            $data['description'] = 'Login Page';
            $data['content_view'] = 'admin/login/login_v';
            $this->template->admin_login_template($data);
        } else {

            if (isset($_COOKIE['username'])) {
                $user = $_COOKIE['username'];
                $pass = $_COOKIE['password'];
            } else {
                $user = strtoupper($this->input->post('username'));
                $pass = md5($this->input->post('password'));
            }

            $check_valid = $this->Login_m->get_data($user, $pass);

            if ($check_valid != FALSE) {
          
                    redirect('admin/dashboard');
            } else {
                $warning = 'Username atau password salah.';
                $data['msg_error_login'] = $this->session->set_flashdata('msg_error_login', $warning);
                $data['title'] = 'Login';
                $data['description'] = 'Login Page';
                $data['content_view'] = 'admin/login/login_v';
                $this->template->admin_login_template($data);
            }
        }
    }

    function r_logout() {
        $CI = & get_instance();
        $CI->load->library('session');
        $CI->session->sess_destroy();
        unset($_COOKIE['username']);
        unset($_COOKIE['password']);
        setcookie('username', null, -1, '/');
        setcookie('password', null, -1, '/');
        $Date_Time = date("Y-m-d H:i:s");
     //   session_destroy();
        //   $this->session->sess_destroy();
        // $this->session->sess_destroy('roles_sess');
        redirect('admin/login');
    }

}

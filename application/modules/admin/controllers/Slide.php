<?php

class Slide extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Slide_m');
    }

    function index() {
        $data['title'] = "Slide";
        $data['description'] = "Slide Page";
        $data['content_view'] = 'admin/slide/slide_v';
        $this->template->admin_template($data);
    }

    function add() {
        $data['title'] = "Add Slide";
        $data['description'] = "Add Slide Page";
        $data['content_view'] = 'admin/slide/slide_add_v';
        $this->template->admin_template($data);
    }

    function edit($id = '') {
        $data['title'] = "Edit Slide";
        $data['description'] = "Edit Slide Page";
        $data['content_view'] = 'admin/slide/slide_edit_v';
        $data['slide'] = $this->Slide_m->get_slide($id)->result();
        $this->template->admin_template($data);
    }

    function r_select() {
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Slide_m->select();
        $data = [];
        foreach ($res->result() as $r) {
            $data[] = array(
                "<center><a  href='" . base_url('admin/slide/edit/') . $r->id_slide . "' type='button' id='edit_btn'
                 class='btn btn-xs btn-info waves-effect'  title ='Edit' >&nbsp<i class='fa fa-edit'></i></a> &nbsp 
                 <button type='button' id='delete_btn' class='btn btn-xs btn-danger waves-effect'  data-toggle='modal' data-target='#modal_hapus' id_slide ='" . $r->id_slide . "'  slide_name='" . $r->title . "'title ='Delete' >&nbsp<i class='fa fa-trash'></i>&nbsp</button></center>",
                $r->title,
                "<a  href='" . base_url('uploads/slide/') . $r->image . "' target='_blank'><img  id='img_product'  style='width:50px; height=50px;' src='" . base_url('uploads/slide/') . $r->image . "'  class='img-responsive img-thumbnail' ></a>",
               
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $res->num_rows(),
            "recordsFiltered" => $res->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    function r_insert() {
        $time = time();
        $ekstensi_diperbolehkan = array('png', 'jpg','jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg','WebM');
        $nama = $time . $_FILES['logo']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo']['size'];
        $file_tmp = $_FILES['logo']['tmp_name'];
        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/slide/' . $nama);
            }
        }

           $image_name = $nama;
           $title = $this->input->post('slide_name');
        $data = array(
            'title' => $title,
            'image' => $nama
        );
        $check_result = $this->Slide_m->insert($data);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Add slide data Success.");
            redirect('admin/slide/add');
        } else {
            $this->session->set_flashdata('msg_error', "Add slide data Failed...!");
            redirect('admin/slide/add');
        }
    }

    function r_update() {
        $ekstensi_diperbolehkan = array('png', 'jpg','jpeg', 'mp4', 'pdf', 'doc', 'ppt', 'xls', 'docx', 'pptx', 'xlsx', 'ogg','WebM');
        $nama = $time . $_FILES['logo_edit']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran = $_FILES['logo_edit']['size'];
        $file_tmp = $_FILES['logo_edit']['tmp_name'];

        if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
            if ($ukuran < 1044070) {
                move_uploaded_file($file_tmp, 'uploads/slide/' . $nama);
            }
        }

        $data = array(
            'id_slide' => $this->input->post('id_slide_edit'),
            'title' => $this->input->post('slide_name_edit'),
            'image' => $nama
        );
        $data_no_file = array(
            'id_slide' => $this->input->post('id_slide_edit'),
            'title' => $this->input->post('slide_name_edit')
        );
        if ($ukuran > 0) {
            $check_result = $this->Slide_m->update($data);
        } else {
            $check_result = $this->Slide_m->update($data_no_file);
        }
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Update Slide data Success.");
            redirect('admin/slide');
        } else {
            $this->session->set_flashdata('msg_error', "Update Slide data Failed...!");
            redirect('admin/slide');
        }
    }

    function r_delete() {
        $id = $this->input->post('id_slide_delete');
        $check_result = $this->Slide_m->delete($id);
        if ($check_result != FALSE) {
            $this->session->set_flashdata('msg_success', "Delete Slide data Success.");
            redirect('admin/slide');
        } else {
            $this->session->set_flashdata('msg_error', "Delete Slide data Failed...!");
            redirect('admin/slide');
        }
    }

}

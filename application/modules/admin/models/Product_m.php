<?php

class Product_m extends CI_Model {

    function get_product($id) {
        $this->db->where('id_product', $id);
        $data = $this->db->get("tbl_product");
        return $data;
    }

    function select() {
        $data = $this->db->get('tbl_product');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_product', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_product', $data['id_product']);
        $data = $this->db->update('tbl_product', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_product', $id);
        $data = $this->db->delete('tbl_product');
        return $data;
    }

}

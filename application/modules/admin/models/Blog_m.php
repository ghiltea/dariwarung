<?php

class Blog_m extends CI_Model {

    function get_blog($id) {
        $this->db->where('id_blog', $id);
        $data = $this->db->get("tbl_blog");
        return $data;
    }

    function select() {
        $data = $this->db->get('tbl_blog');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_blog', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_blog', $data['id_blog']);
        $data = $this->db->update('tbl_blog', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_blog', $id);
        $data = $this->db->delete('tbl_blog');
        return $data;
    }

}

<?php

class Testimoni_m extends CI_Model {

    function get_testimoni($id) {
        $this->db->where('id_testimoni', $id);
        $data = $this->db->get("tbl_testimoni");
        return $data;
    }

    function select() {
        $data = $this->db->get('tbl_testimoni');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_testimoni', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_testimoni', $data['id_testimoni']);
        $data = $this->db->update('tbl_testimoni', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_testimoni', $id);
        $data = $this->db->delete('tbl_testimoni');
        return $data;
    }

}

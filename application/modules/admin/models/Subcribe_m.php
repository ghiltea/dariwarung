<?php

class Subcribe_m extends CI_Model {

    function get_subcribe($id) {
        $this->db->where('id_subcribe', $id);
        $data = $this->db->get("tbl_subcribe");
        return $data;
    }

    function select() {
        $data = $this->db->get('tbl_subcribe');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_subcribe', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_subcribe', $data['id_subcribe']);
        $data = $this->db->update('tbl_subcribe', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_subcribe', $id);
        $data = $this->db->delete('tbl_subcribe');
        return $data;
    }

}

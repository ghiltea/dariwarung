<?php

class Setting_m extends CI_Model {

 function get_setting($id) {
        $this->db->where('id_setting', $id);
        $data = $this->db->get("tbl_setting");
        return $data;
    }

    function select() {
        $data = $this->db->get('tbl_setting');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_setting', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_setting', $data['id_setting']);
        $data = $this->db->update('tbl_setting', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_setting', $id);
        $data = $this->db->delete('tbl_setting');
        return $data;
    }


}

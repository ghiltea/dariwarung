<?php

class Slide_m extends CI_Model {

    function get_slide($id) {
        $this->db->where('id_slide', $id);
        $data = $this->db->get("tbl_slide");
        return $data;
    }

    function select() {
        $data = $this->db->get('tbl_slide');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_slide', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_slide', $data['id_slide']);
        $data = $this->db->update('tbl_slide', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_slide', $id);
        $data = $this->db->delete('tbl_slide');
        return $data;
    }

}

<?php

class Login_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_data($user, $pass) {
            $this->db->select('a.*, b.role_name, b.role_access');    
            $this->db->from('tbl_user a');
            $this->db->join('tbl_role b', 'a.id_role = b.id_role', 'left');
                        $this->db->where('username',$user);
            $this->db->where('password',$pass);
            $this->db->where('status','Active');
            $this->db->limit(1);
            $q = $this->db->get();
                                
                                
        $arr = $q->row();
        $num = $q->num_rows();
        if ($num > 0) {
            $data = array(
                "id_user" => $arr->id_user,
                "id_role" => $arr->id_role,
                "username" => $arr->username,
                "password" => $arr->password,
                "status" => $arr->status,
                "role_name" => $arr->role_name,
            );
          //  setcookie("username", $username, time() + (10 * 365 * 24 * 60 * 60), "/");
         //   setcookie("password", $password, time() + (10 * 365 * 24 * 60 * 60), "/");
            $this->session->set_userdata($data);
            $roles = explode(",", $arr->role_access);
            $this->session->set_userdata(array("roles_sess" => $roles));
            $this->session->set_userdata('admin_islogin', TRUE);
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

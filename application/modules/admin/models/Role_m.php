<?php

class Role_m extends CI_Model {

    function get_role($id) {
        $this->db->where('id_role',$id);
        $data = $this->db->get("tbl_role");
        return $data;
    }
    
    function get_role_menu($id) {
        $this->db->where('id_role',$id);
        $data = $this->db->get("tbl_role");
        $rs = $data->row();
        $role_menu = $rs->role_access;
        $data_ar = explode(",", $role_menu); 
        return $data_ar;
    }
    
    function select() {
        $data = $this->db->get("tbl_role");
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_role', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_role', $data['id_role']);
        $data = $this->db->update('tbl_role', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_role', $id);
        $data = $this->db->delete('tbl_role');
        return $data;
    }

}

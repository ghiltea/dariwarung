<?php

class ProductCategory_m extends CI_Model {

    function get_category($id) {
        $this->db->where('id_category', $id);
        $data = $this->db->get("tbl_product_category");
        return $data;
    }

    function select() {
        $data = $this->db->get('tbl_product_category');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_product_category', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_category', $data['id_category']);
        $data = $this->db->update('tbl_product_category', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_category', $id);
        $data = $this->db->delete('tbl_product_category');
        return $data;
    }

}

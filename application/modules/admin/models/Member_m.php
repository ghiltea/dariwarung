<?php

class Member_m extends CI_Model {

    function get_member($id) {
        $this->db->where('id_member',$id);
        $data = $this->db->get("tbl_member");
        return $data;
    }

    function select() {
        $data = $this->db->get("tbl_member");
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_member', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_member', $data['id_member']);
        $data = $this->db->update('tbl_member', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_member', $id);
        $data = $this->db->delete('tbl_member');
        return $data;
    }

}

<?php

class Message_m extends CI_Model {

    function get_message($id) {
        $this->db->where('id_message', $id);
        $data = $this->db->get("tbl_message");
        return $data;
    }

    function select() {
        $data = $this->db->get('tbl_message');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_message', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_message', $data['id_message']);
        $data = $this->db->update('tbl_message', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_message', $id);
        $data = $this->db->delete('tbl_message');
        return $data;
    }

}

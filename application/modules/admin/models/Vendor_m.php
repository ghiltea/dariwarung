<?php

class Vendor_m extends CI_Model {

    function get_vendor($id) {
        $this->db->where('id_vendor',$id);
        $data = $this->db->get("tbl_vendor");
        return $data;
    }

    function select() {
        $data = $this->db->get("tbl_vendor");
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_vendor', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_vendor', $data['id_vendor']);
        $data = $this->db->update('tbl_vendor', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_vendor', $id);
        $data = $this->db->delete('tbl_vendor');
        return $data;
    }

}

<?php

class User_m extends CI_Model {

    function get_role() {
        $data = $this->db->get("tbl_role");
        return $data;
    }
    
    function get_user($id) {
        $this->db->where('id_user',$id);
        $data = $this->db->get("tbl_user");
        return $data;
    }

    function select() {
        $data = $this->db->query("SELECT a.*, b.* FROM tbl_user a INNER JOIN tbl_role  b ON a.id_role = b.id_role");
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_user', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_user', $data['id_user']);
        $data = $this->db->update('tbl_user', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_user', $id);
        $data = $this->db->delete('tbl_user');
        return $data;
    }

}

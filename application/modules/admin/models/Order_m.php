<?php

class Order_m extends CI_Model {

//    function get_order($id) {
//        $this->db->where('id_order', $id);
//        $data = $this->db->get("tbl_order");
//        return $data;
//    }

    function get_order($id) {
        $this->db->select('b.*,b.status as st, a.*');
        $this->db->where('id_order', $id);
        $this->db->join('tbl_member a', 'a.id_member = b.id_member', 'left');
        $data = $this->db->get('tbl_order b');
        return $data;
    }
    

    function get_order_detail($id_order, $id_vendor) {
        $this->db->where('id_order', $id_order);
        $data = $this->db->get("tbl_order_detail");
        return $data;
    }

    function select() {
        $this->db->select('b.*, a.fullname, a.email');
        $this->db->join('tbl_member a', 'a.id_member = b.id_member', 'left');
        $data = $this->db->get('tbl_order b');
        return $data;
    }

    function insert($data) {
        $data = $this->db->insert('tbl_order', $data);
        return $data;
    }

    function update($data) {
        $this->db->where('id_order', $data['id_order']);
        $data = $this->db->update('tbl_order', $data);
        return $data;
    }

    function delete($id) {
        $this->db->where('id_order', $id);
        $data = $this->db->delete('tbl_order');
        if ($data != FALSE) {
            $this->db->where('id_order', $id);
            $data = $this->db->delete('tbl_order_detail');
        }
        return $data;
    }

}

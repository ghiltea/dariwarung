<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <!--head -->
    <head>
        <title> <?php echo $title ?> </title>
        <link rel='icon' href='images/favicon.png' type='image/x-icon'/ >
              <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
         <?php echo @$meta_shop; ?>
        <?php
        foreach ($home as $r) {
            echo $r->script_include;
        }
        ?>
      
        <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/open-iconic-bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/animate.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/owl.theme.default.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/aos.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/jquery.timepicker.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/flaticon.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/icomoon.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/public/'); ?>css/style.css">
        <script src="<?php echo base_url('assets/public/'); ?>js/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/bootstrap.min.js"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169597821-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-169597821-1');
        </script>


    </head>

    <body >


    <div class="container">
        <?php if ($this->session->flashdata('msg_error_register')) { ?>
            <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error_login'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <div class="row "style="background-color: #ffffff;">
            <div class="col-md-6 order-md-last d-flex">
                <form action="" method="POST"class="bg-white p-5 contact-form">
                    <br>
                    <br>
                    <br>
                    <br>
                    <h4><b>OH TIDAK! HALAMAN </b></h4>
                    <h4 style="margin-top: -12px;"><b>YANG KAMU CARI </b></h4>
                    <h4 style="margin-top: -12px;"><b>TIDAK ADA.</b></h4>
                    </b>
                    <h6>Klik tombol dibawah untuk kembali ke beranda</h6>
                    <a class="btn btn-primary py-3 px-5"href="<?php echo base_url('home'); ?>">Kembali</a>
                </form>

            </div>


            <div class="col-md-6  order-md-last d-flex">
                <img class="img-fluid" src="<?php echo base_url('assets/public/images/4042.jpeg'); ?>">
            </div>
        </div>




        <script src="<?php echo base_url('assets/public/'); ?>js/jquery-migrate-3.0.1.min.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/popper.min.js"></script>

        <script src="<?php echo base_url('assets/public/'); ?>js/jquery.easing.1.3.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/jquery.stellar.min.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/aos.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/jquery.animateNumber.min.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('assets/public/'); ?>js/scrollax.min.js"></script>

        <script src="<?php echo base_url('assets/public/'); ?>js/main.js"></script>
        
    </body>
<?php

class Shop extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Shop_m');
    }

    function index() {
        $data['title'] = "Jual Produk Roti Online Sharon Bakery | Redline";
        $data['description'] = "Shop Page";

        $data['meta_shop'] = '<meta name="description" content=" Kami menyediakan berbagai macam jenis roti, roti bagelen, roti tawar, roti manis, roti kering dan kue. Berbagai macam roti bisa kamu pilih disini.">
                              <meta name="keyword" content="redline, idredline, redline bakery, redline indonesia, sharon bakery, roti sharon, pt multi star rukun abadi, beli roti online, roti murah, roti pasar, roti bagelen, roti bagelen di bandung, roti bagelen sharon, roti sharon, roti online murah">';
        $data['content_view'] = 'member/shop/shop_v';
        //Banner
        $this->db->where('title', "Banner");
        $about = $this->db->get('tbl_content');
        $data['banner'] = $about->row();
        //product
        $product = $this->db->get('tbl_product');
        $data['product'] = $product;
        //category
        $category = $this->db->get('tbl_product_category');
        $data['category'] = $category;

        $this->template->member_template($data);
    }

    function search() {
        $data['title'] = "Search Product";
        $data['description'] = "Search Product Page";
        $data['content_view'] = 'member/shop/search_v';
        //product
        $name = $this->input->post('search');
        $this->db->like('name', $name);
        $product = $this->db->get('tbl_product');
        $data['product'] = $product;
        //Banner
        $this->db->where('title', "Banner");
        $about = $this->db->get('tbl_content');
        $data['banner'] = $about->row();
        $this->template->member_template($data);
    }

    function category($id = '') {
        $data['title'] = "Jual Produk Roti Online Sharon Bakery | Redline";
        $data['description'] = "Shop Page";
        $data['content_view'] = 'member/shop/shop_v';
        //product
        $this->db->where('id_category', $id);
        $product = $this->db->get('tbl_product');
        $data['product'] = $product;
        //category
        $category = $this->db->get('tbl_product_category');
        $data['category'] = $category;
        //Banner
        $this->db->where('title', "Banner");
        $about = $this->db->get('tbl_content');
        $data['banner'] = $about->row();
        $this->template->member_template($data);
    }

    function detail($id = '') {
   
        $data['description'] = "Shop Page";
        $data['content_view'] = 'member/shop/shop_detail_v';
        //product detail
        $this->db->where('id_product', $id);
        $detaiproduct = $this->db->get('tbl_product');
        $rw = $detaiproduct->row();
        $data['detail'] = $detaiproduct->row();
        $data['meta_shop'] = '<meta name="description" content=" ' . $rw->seo_description . '">
                              <meta name="keyword" content=" ' . $rw->seo_keyword . '">';
        $data['title'] = "$rw->seo_title | Redline";
        //product
        $product = $this->db->get('tbl_product', 0, 4);
        $data['product'] = $product;
        //Banner
        $this->db->where('title', "Banner");
        $about = $this->db->get('tbl_content');
        $data['banner'] = $about->row();
        $this->template->member_template($data);
    }

    function cart($id = '') {
        $data['title'] = "Jual Produk Roti Online Sharon Bakery | Redline";
        $data['description'] = "Shop Page";
        $data['content_view'] = 'member/shop/cart_v';
        $id_member = $this->session->userdata('id_member');
        $rs = $this->db->query('SELECT a.*, b.image, b.description
                                FROM tbl_cart a
                                LEFT JOIN tbl_product b ON b.id_product = a.id_product
                                WHERE a.id_member = "' . $id_member . '"');
        $data['cart'] = $rs;

        $this->template->member_template($data);
    }

    function count_cart() {
        $id_member = $this->session->userdata('id_member');
        $this->db->where('id_member', $id_member);
        $rs = $this->db->get('tbl_cart');
        $row = $rs->num_rows();
        echo $row;
    }

    function r_delete_cart() {
        $id_cart = $this->input->post('id_cart');
        $this->db->where('id_cart', $id_cart);
        $rs = $this->db->delete('tbl_cart');
        if ($rs != FALSE) {
            echo "success";
        } else {

            echo "failed";
        }
    }

    function r_checkout_header() {
        date_default_timezone_set('Asia/Jakarta');
        $create_date = date('Y-m-d H:i:s');
        $code_order = "PO" . time();
        $id_member = $this->session->userdata('id_member');
        $tot_qty = $this->input->post('tot_qty');
        $tot_discount = $this->input->post('tot_discount');
        $tot_delivery = $this->input->post('tot_delivery');
        $tot_tax = $this->input->post('tot_tax');
        $tot_amount = $this->input->post('tot_amount');


        $data = array(
            "code_order" => $code_order,
            "id_member" => $id_member,
            "date" => $create_date,
            "total_qty" => $tot_qty,
            "total_discount" => $tot_discount,
            "total_delivery" => $tot_delivery,
            "total_tax" => $tot_tax,
            "total_amount" => $tot_amount,
        );
        $this->db->insert('tbl_order', $data);
        $insert_id = $this->db->insert_id();
        echo json_encode(array("id_order" => $insert_id, "code_order" => $code_order));
    }

    function r_checkout_detail() {

        $id_product = $this->input->post('id_product');
        $code_order = $this->input->post('code_order');
        $id_order = $this->input->post('id_order');
        $this->db->where('id_product', $id_product);
        $r = $this->db->get('tbl_product');
        $row = $r->row();
        $name = $row->name;
        $price_ori = $row->price;
        $discount = $row->discount;
        $id_vendor = $row->id_vendor;
        $qty = $this->input->post('qty');
        $price = $this->input->post('price');
        $amount = $qty * $price;
        $data = array(
            "id_order" => $id_order,
            "code_order" => $code_order,
            "id_product" => $id_product,
            "id_vendor" => $id_vendor,
            "name" => $name,
            "qty" => $qty,
            "discount" => $discount,
            "price" => $price_ori,
            "price_net" => $price,
            "amount" => $amount,
        );
        $this->db->insert('tbl_order_detail', $data);
    }

    function r_cart_delete() {
        $id_member = $this->session->userdata('id_member');
        $this->db->where('id_member', $id_member);
        $this->db->delete('tbl_cart');
    }

    function r_cart_single() {
        if ($this->session->member_islogin) {
            date_default_timezone_set('Asia/Jakarta');
            $create_date = date('Y-m-d H:i:s');

            $id_member = $this->session->userdata('id_member');
            $id_product = $this->input->post('id_product');

            $this->db->where('id_product', $id_product);
            $q = $this->db->get('tbl_product');
            $row = $q->row();

            $name = $row->name;
            $price = $row->price;
            $qty = 1;
            $discount = $row->discount;
            $amount = $price * $qty;
            $weight = $row->weight;
            $data = array(
                "id_member" => $id_member,
                "id_product" => $id_product,
                "name" => $name,
                "price" => $price,
                "qty" => $qty,
                "discount" => $discount,
                "amount" => $amount,
                "weight" => $weight,
                "create_date" => $create_date,
            );
            $check_result = $this->db->insert('tbl_cart', $data);
            if ($check_result != FALSE) {
                echo "success";
            } else {

                echo "failed";
            }
        } else {
            echo "failed";
        }
    }

    function r_whislist_single() {
        if ($this->session->member_islogin) {
            date_default_timezone_set('Asia/Jakarta');
            $create_date = date('Y-m-d H:i:s');

            $id_member = $this->session->userdata('id_member');
            $id_product = $this->input->post('id_product');

            $this->db->where('id_product', $id_product);
            $q = $this->db->get('tbl_product');
            $row = $q->row();

            $name = $row->name;
            $price = $row->price;
            $qty = 1;
            $discount = $row->discount;
            $amount = $price * $qty;
            $weight = $row->weight;
            $data = array(
                "id_member" => $id_member,
                "id_product" => $id_product,
                "name" => $name,
                "price" => $price,
                "qty" => $qty,
                "discount" => $discount,
                "amount" => $amount,
                "weight" => $weight,
                "create_date" => $create_date,
            );
            $check_result = $this->db->insert('tbl_whislist', $data);
            if ($check_result != FALSE) {
                echo "success";
            } else {

                echo "failed";
            }
        } else {
            echo "failed";
        }
    }
    
     function whislist() {
        $data['title'] = "Lokasi Warung";
        $data['description'] = "Lokasi Warung";
        $data['content_view'] = 'member/shop/wishlist_v';
        $this->template->member_template($data);
    }

    function r_insert() {
        if ($this->session->member_islogin) {
            $id_member = $this->session->userdata('id_member');
            $id_product = $this->input->post('id_product');
            $this->db->where('id_member', $id_member);
            $this->db->where('id_product', $id_product);
            $dt = $this->db->get('tbl_cart');
            $row = $dt->row();

            if (@$row->id_member == $id_member && @$row->id_product == $id_product) {
                date_default_timezone_set('Asia/Jakarta');
                $create_date = date('Y-m-d H:i:s');
                $tanggal_input = $create_date;

                $price = $this->input->post('price');
                $qty = $this->input->post('quantity') + $row->qty;
                $discount = $this->input->post('discount');
                $hrg_price = $price - ($discount * $price / 100);
                $amount = $qty * $hrg_price;
                $data = array(
                    'id_member' => $this->session->userdata('id_member'),
                    'id_product' => $this->input->post('id_product'),
                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'price' => $price,
                    'qty' => $qty,
                    'discount' => $discount,
                    'amount' => $amount,
                    'create_date' => $tanggal_input
                );
                $this->db->where('id_member', $id_member);
                $this->db->where('id_product', $id_product);
                $check_result = $this->db->update('tbl_cart', $data);
                if ($check_result != FALSE) {
                    echo "success";
                } else {

                    echo "failed";
                }
            } else {
                date_default_timezone_set('Asia/Jakarta');
                $create_date = date('Y-m-d H:i:s');
                $tanggal_input = $create_date;

                $price = $this->input->post('price');
                $qty = $this->input->post('quantity');
                $discount = $this->input->post('discount');
                $hrg_price = $price - ($discount * $price / 100);
                $amount = $qty * $hrg_price;
                $data = array(
                    'id_member' => $this->session->userdata('id_member'),
                    'id_product' => $this->input->post('id_product'),
                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'price' => $price,
                    'qty' => $qty,
                    'discount' => $discount,
                    'amount' => $amount,
                    'create_date' => $tanggal_input
                );
                $check_result = $this->db->insert('tbl_cart', $data);
                if ($check_result != FALSE) {
                    echo "success";
                } else {

                    echo "failed";
                }
            }
        } else {
            redirect('login');
            exit();
        }
    }

}

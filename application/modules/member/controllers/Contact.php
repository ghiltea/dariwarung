<?php

class Contact extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('contact_m');
    }

    function index() {
        $data['title'] = "Hubungi Kami | Redline";
        $data['description'] = "Contact Page";
        $data['content_view'] = 'member/contact/contact_v';
        //Banner
        $this->db->where('title', "Banner");
        $about = $this->db->get('tbl_content');
        $data['banner'] = $about->row();
        //contact
        $this->db->where('title', "Contact");
        $contact = $this->db->get('tbl_content');
        $data['contact'] = $contact->row();
        $this->template->member_template($data);
    }

    function r_insert() {
        $data = array(
            "name" => $this->input->post('name'),
            "email" => $this->input->post('email'),
            "subject" => $this->input->post('subject'),
            "message" => $this->input->post('message')
        );
        $rs = $this->db->insert('tbl_message', $data);
        if ($rs != FALSE) {
            echo "success";
        }else{
            echo 'failed';
        }
    }

}

<?php

class Store extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('About_m');
    }

    function index() {
        $data['title'] = "Lokasi Warung";
        $data['description'] = "Lokasi Warung";
        $data['content_view'] = 'member/store/store_v';
        $data['about'] = $this->About_m->get_about();
        $this->template->member_template($data);
    }

    function Location() {
        $data['title'] = "Lokasi Warung";
        $data['description'] = "Lokasi Warung";
        $data['content_view'] = 'member/store/location_v';
        $data['about'] = $this->About_m->get_about();
        $this->template->member_template($data);
    }

    function register() {
        $data['title'] = "Lokasi Warung";
        $data['description'] = "Lokasi Warung";
        $data['content_view'] = 'member/login/login_v';
        $data['about'] = $this->About_m->get_about();
        $this->template->member_template($data);
    }
    
    function Search() {
        $data['title'] = "Lokasi Warung";
        $data['description'] = "Lokasi Warung";
        $data['content_view'] = 'member/store/location_v';
        $data['about'] = $this->About_m->get_about();
        $this->template->member_template($data);
    }

}

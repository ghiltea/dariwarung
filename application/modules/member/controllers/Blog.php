<?php

class Blog extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Blog_m');
    }

    function index() {
        $data['title'] = "Cerita Blog Terbaru Dari Redline | Redline";
        $data['description'] = "Blog Page";
        $data['content_view'] = 'member/blog/blog_v';
        //Banner
        $this->db->where('title', "Banner");
        $about = $this->db->get('tbl_content');
        $data['banner'] = $about->row();
        //slide news
        $this->db->order_by('id_blog', 'ASC');
        $data['blog'] = $this->db->get('tbl_blog');
        //category
        $this->db->order_by('id_category', 'ASC');
        $data['blog_category'] = $this->db->get('tbl_blog_category');
        //slide latest
        $this->db->order_by('id_blog', 'DESC');
        $data['latest_blog'] = $this->db->get('tbl_blog', 0, 10);
        $this->template->member_template($data);
    }

    function detail($id = '') {
        $this->db->where('title', "Banner");
        $about = $this->db->get('tbl_content');
        $tit = $about->row();
        $data['title'] = "Blog - " . $tit->title;
        $data['description'] = "Blog Page";
        $data['content_view'] = 'member/blog/blog_detail_v';
        //Banner

        $data['banner'] = $about->row();

        //blog detail
        $this->db->where('id_blog', $id);
        $data['blog'] = $this->db->get('tbl_blog')->row();

        //category
        $this->db->order_by('id_category', 'ASC');
        $data['blog_category'] = $this->db->get('tbl_blog_category');

        //slide latest
        $this->db->order_by('id_blog', 'DESC');
        $data['latest_blog'] = $this->db->get('tbl_blog');

        $this->template->member_template($data);
    }

}

<?php

class About extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('About_m');
    }

    function index() {
        $data['title'] = "Tentang Kami | DariWarung";
        $data['description'] = "Tentang DariWarung";
        $data['content_view'] = 'member/about/about_v';
        $data['about'] = $this->About_m->get_about();
        $this->template->member_template($data);
        
        
    }
    


}

<?php

class Register extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Register_m');
    }

    function index() {
          //Banner
        $this->db->where('title', "Banner");
        $about = $this->db->get('tbl_content');
        $data['banner'] = $about->row();
        $data['title'] = "Register";
        $data['description'] = "Register Page";
        $data['content_view'] = 'member/register/register_v';
        $this->template->member_template($data);
    }

    function r_insert() {
        $email = $this->input->post('email');
        $data = array(
            'fullname' => $this->input->post('fullname'),
            'email' => $this->input->post('email'),
            'type_customer' => 'Yes',
            'password' => md5($this->input->post('password')),
        );
        $check_result = $this->db->insert('tbl_member', $data);
        if ($check_result != FALSE) {
          
            $this->db->where('email', $email);
            $mem = $this->db->get('tbl_member');
            $rm = $mem->row();
            
            $data = array(
                "id_member" => $rm->id_member,
                "fullname" => $rm->fullname,
                "email" => $rm->email,
                "address" => $rm->address,
                "phone" => $rm->phone,
                "username" => $rm->username,
                "password" => $rm->password,
                "status" => $rm->status,
                "type_reseller" => $rm->type_reseller,
                "type_customer" => $rm->type_customer,
            );
            $this->session->set_userdata($data);
            $this->session->set_userdata('member_islogin', TRUE);
            $this->session->set_flashdata('msg_success', "Add Member data Success.");
            redirect('member/profil');
        } else {
            $this->session->set_flashdata('msg_error_register', " Failed Register ...!");
            redirect('member/register');
        }
    }

}

<?php

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Home_m');
    }

    function index() {
        $data['title'] = "Dariwarung | Belanja Online dan Jual Beli Lebih Mudah";
        $data['description'] = "Home Page";
        $data['content_view'] = 'member/home/home_v';
        //slide
        $data['slide'] = $this->db->get('tbl_slide');

        //banner top
        $this->db->where('page', "home");
        $this->db->where('position', "top");
        $banner = $this->db->get('tbl_banner');
        $data['banner_top'] = $banner->row();

        //banner left
        $this->db->where('page', "home");
        $this->db->where('position', "left");
        $banner = $this->db->get('tbl_banner');
        $data['banner_left'] = $banner->row();

        //banner left list
        $this->db->where('page', "home");
        $this->db->where('position', "left_list");
        $banner = $this->db->get('tbl_banner');
        $data['banner_left_list'] = $banner->result();
        
        //banner center_left
        $this->db->where('page', "home");
        $this->db->where('position', "center_left");
        $banner = $this->db->get('tbl_banner');
        $data['banner_center_left'] = $banner->row();

        //banner center_right
        $this->db->where('page', "home");
        $this->db->where('position', "center_right");
        $banner = $this->db->get('tbl_banner');
        $data['banner_center_right'] = $banner->row();

        //product featured Terlaris
        $this->db->where('featured', "Terlaris");
        $product = $this->db->get('tbl_product', 0, 3)->result();
        $data['product_terlaris'] = $product;


        //product featured Terbaru
        $this->db->where('featured', "Terbaru");
        $product_tab = $this->db->get('tbl_product', 0, 3)->result();
        $data['product_terbaru'] = $product_tab;

        //product featured Promo
        $this->db->where('featured', "Promo");
        $product_tab = $this->db->get('tbl_product', 0, 3)->result();
        $data['product_promo'] = $product_tab;

        //product featured terakhir dilihat
        $product_tab = $this->db->query('SELECT * FROM tbl_product ORDER BY id_product DESC LIMIT 5')->result();
        $data['product_terakhir'] = $product_tab;

        //product featured paling termurah 
        $product_tab = $this->db->query('SELECT DISTINCT * FROM tbl_product 
                                            WHERE tbl_product.id_category
                                            GROUP BY tbl_product.id_category
                                            ORDER BY tbl_product.price_sell ASC
                                            LIMIT 15')->result();
        $data['product_termurah'] = $product_tab;
        
        //List Member Warung
        $member_list = $this->db->query('SELECT * FROM tbl_member WHERE type_seller = "Premium"')->result();
        $data['member_list'] = $member_list;
        
        //category 
        $category = $this->db->query('SELECT * FROM  tbl_product_category order by  RAND(id_category) LIMIT 3');
        $data['category'] = $category;
        

        

        $this->template->member_template($data);
    }

}

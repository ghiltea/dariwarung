<?php

class Profil extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Profil_m');
    }

    function index() {
          if (!$this->session->member_islogin) {
            redirect('home');
            exit();
        }
        $data['title'] = "Profil";
        $data['description'] = "Profil Page";
        $data['content_view'] = 'member/profil/profil_v';
        $data['content_cild_view'] = 'member/profil/include/myprofil_v';
        $id_member = $this->session->userdata('id_member');
        
        $this->db->where('id_member', $id_member);
        $rs_notif = $this->db->get('tbl_order');
        $data['notif'] = $rs_notif->num_rows();

        $this->template->member_template($data);
    }

    function whislist() {
        $data['title'] = "Whislist";
        $data['description'] = "Whislist Page";
        $data['content_view'] = 'member/profil/profil_v';
        $data['content_cild_view'] = 'member/profil/include/whislist_v';
        $id_member = $this->session->userdata('id_member');
        $rs = $this->db->query('SELECT a.*, b.image, b.description
                                FROM tbl_whislist a
                                LEFT JOIN tbl_product b ON b.id_product = a.id_product
                                WHERE a.id_member = "' . $id_member . '"');
        $data['cart'] = $rs;

        $this->db->where('id_member', $id_member);
        $rs_notif = $this->db->get('tbl_order');
        $data['notif'] = $rs_notif->num_rows();
        $this->template->member_template($data);
    }

    function cart() {
        $data['title'] = "Cart";
        $data['description'] = "Cart Page";
        $data['content_view'] = 'member/profil/profil_v';
        $data['content_cild_view'] = 'member/profil/include/cart_v';
        $id_member = $this->session->userdata('id_member');
        $rs = $this->db->query('SELECT a.*, b.image, b.description
                                FROM tbl_cart a
                                LEFT JOIN tbl_product b ON b.id_product = a.id_product
                                WHERE a.id_member = "' . $id_member . '"');

        $this->db->where('id_member', $id_member);
        $rs_notif = $this->db->get('tbl_order');

        $data['notif'] = $rs_notif->num_rows();
        $data['cart'] = $rs;
        $this->template->member_template($data);
    }

    function order() {
        $data['title'] = "History Order";
        $data['description'] = "History Order Page";
        $data['content_view'] = 'member/profil/profil_v';
        $data['content_cild_view'] = 'member/profil/include/order_v';
        $id_member = $this->session->userdata('id_member');
        $this->db->where('id_member', $id_member);
        $rs = $this->db->get('tbl_order');
        $data['cart'] = $rs;
        $this->db->where('id_member', $id_member);
        $rs_notif = $this->db->get('tbl_order');
        $data['notif'] = $rs_notif->num_rows();
        $this->template->member_template($data);
    }
    
      function order_preview($id='') {
        $data['title'] = "Preview Order";
        $data['description'] = "Preview Order Page";
        $data['content_view'] = 'member/profil/include/order_preview_v';
        $id_member = $this->session->userdata('id_member');
        
        $order = $this->Profil_m->get_order($id);
        $data['order'] = $order->row();

        $order_detail = $this->Profil_m->get_order_detail($id);
        $data['order_detail'] = $order_detail;
        $this->template->member_template($data);
    }

    function r_delete_whist() {
        $id_whislist = $this->input->post('id_whislist');
        $this->db->where('id_whislist', $id_whislist);
        $rs = $this->db->delete('tbl_whislist');
        if ($rs != FALSE) {
            echo "success";
        } else {

            echo "failed";
        }
    }

    function r_update() {
        $pass = $this->input->post('password');
        $id_member = $this->session->userdata('id_member');
        if ($pass == "") {
            $data = array(
                "fullname" => $this->input->post('fullname'),
                "username" => $this->input->post('username'),
                "email" => $this->input->post('email'),
                "address" => $this->input->post('address'),
                "phone" => $this->input->post('phone')
            );
            $this->db->where('id_member', $id_member);
            $data = $this->db->update('tbl_member', $data);
            if ($data != FALSE) {
                echo "success";
            } else {
                echo "failed";
            }
        } else {
            $data = array(
                "fullname" => $this->input->post('fullname'),
                "username" => $this->input->post('username'),
                "email" => $this->input->post('email'),
                "password" => md5($this->input->post('password')),
                "address" => $this->input->post('address'),
                "phone" => $this->input->post('phone')
            );
            $this->db->where('id_member', $id_member);
            $data = $this->db->update('tbl_member', $data);
            if ($data != FALSE) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

}

<?php

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
    }

       function logReg() {
        $data['title'] = "Bantuan";
        $data['description'] = "Halaman Bantuan";
        $data['content_view'] = 'member/login/login_v';

        $this->template->member_template($data);
    }
    
    function register() {
        $fullname = $this->input->post('fullname');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $pass = md5($this->input->post('pass'));

        $check_valid = $this->Login_m->add_member($fullname, $email, $phone, $pass);
        if ($check_valid != FALSE) {
            $msg = 'Daftar Berhasil.';
            echo json_encode(array("response" => "success", "message" => $msg));
            $this->session->set_flashdata('msg_success', $msg);
        } else {
            $msg = 'Daftar Gagal.';
            echo json_encode(array("response" => "failed", "message" => $msg));
            $this->session->set_flashdata('msg_error', $msg);
        }
    }

    function login() {
        $email = $this->input->post('email');
        $pass = md5($this->input->post('pass'));

        $check_valid = $this->Login_m->get_data($email, $pass);
        if ($check_valid != FALSE) {
            $msg = 'Login Berhasil.';
            echo json_encode(array("response" => "success", "message" => $msg));
            $this->session->set_flashdata('msg_success', $msg);
        } else {
            $msg = 'Login Gagal.';
            echo json_encode(array("response" => "failed", "message" => $msg));
            $this->session->set_flashdata('msg_error', $msg);
        }
    }

    function logout() {
        $this->session->sess_destroy();
        $this->session->set_userdata('member_islogin', FALSE);
        if ($this->session->member_islogin) {
            $msg = 'Logout Gagal.';
            echo json_encode(array("response" => "failed", "message" => $msg));
        } else {
            $msg = 'Logout Berhasil.';
            echo json_encode(array("response" => "success", "message" => $msg));
        }
    }

}

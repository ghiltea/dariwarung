<?php

class Content extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Content_m');
    }


    function help() {
        $data['title'] = "Bantuan";
        $data['description'] = "Halaman Bantuan";
        $data['content_view'] = 'member/content/help_v';

        $this->db->where('title', 'Help');
        $data['help'] = $this->db->get('tbl_content')->row();
        $this->template->member_template($data);
    }

    function shippinginformation() {
        $data['title'] = "Blog";
        $data['description'] = "Blog Page";
        $data['content_view'] = 'member/content/shippinginformation_v';

        $this->db->where('title', 'Shipping Formation');
        $data['help'] = $this->db->get('tbl_content')->row();
        $this->template->member_template($data);
    }

    function returnexchange() {
        $data['title'] = "Retur Exchange";
        $data['description'] = "Retur Exchange Page";
        $data['content_view'] = 'member/content/returnexchange_v';

        $this->db->where('title', 'Return Exchange');
        $data['help'] = $this->db->get('tbl_content')->row();
        $this->template->member_template($data);
    }

    function termsconditions() {
        $data['title'] = "Term Conditions";
        $data['description'] = "Term Conditions Page";
        $data['content_view'] = 'member/help/termsconditions_v';

        $this->db->where('title', 'Term Conditions');
        $data['help'] = $this->db->get('tbl_content')->row();
        $this->template->member_template($data);
    }

    function privacypolicy() {
        $data['title'] = "Privacy Policy";
        $data['description'] = "Privacy Policy Page";
        $data['content_view'] = 'member/help/privacypolicy_v';

        $this->db->where('title', 'Privacy Policy');
        $data['help'] = $this->db->get('tbl_content')->row();
        $this->template->member_template($data);
    }

    function faqs() {
        $data['title'] = "Faqs";
        $data['description'] = "Faqs Page";
        $data['content_view'] = 'member/help/faqs_v';

        $this->db->where('title', 'Faqs');
        $data['help'] = $this->db->get('tbl_content')->row();
        $this->template->member_template($data);
    }

}

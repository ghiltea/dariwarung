<?php

class Member_m extends CI_Model {

    function get_data($user, $pass) {
        $this->db->where('email', $user);
        $this->db->or_where('phone', $user);
        $this->db->where('password', $pass);
        $this->db->where('status', 'Active');
        $this->db->limit(1);
        $q = $this->db->get('tbl_member');
        $arr = $q->row();
        $num = $q->num_rows();
        if ($num > 0) {
            $data = array(
                "id_member" => $arr->id_member,
                "fullname" => $arr->fullname,
                "email" => $arr->email,
                "address" => $arr->address,
                "phone" => $arr->phone,
                "username" => $arr->username,
                "password" => $arr->password,
                "status" => $arr->status,
            );
            $this->session->set_userdata($data);
            $this->session->set_userdata('member_islogin', TRUE);
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

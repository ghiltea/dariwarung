<?php

class About_m extends CI_Model {
    
    function get_about(){
        $this->db->where('title', "About");
        $about = $this->db->get('tbl_content');
        return $about->row();
    }
    
}

<?php

class Login_m extends CI_Model {

    private $table = 'tbl_member';

    public function __construct() {
        parent::__construct();
    }

    function add_member($fullname, $email, $phone, $pass) {
        $data = array(
            'fullname' => $fullname,
            'email' => $email,
            'phone' => $phone,
            'password' =>$pass,
        );
        $check = $this->db->insert($this->table, $data);
        if ($check) {
            $this->db->where('email', $email);
            $this->db->or_where('phone', $phone);
            $this->db->where('password', $pass);
            $this->db->where('status', 'Active');
            $this->db->limit(1);
            $q = $this->db->get('tbl_member');
            $arr = $q->row();
            $num = $q->num_rows();
            if ($num > 0) {
                $data = array(
                    "id_member" => $arr->id_member,
                    "fullname" => $arr->fullname,
                    "email" => $arr->email,
                    "address" => $arr->address,
                    "phone" => $arr->phone,
                    "username" => $arr->username,
                    "password" => $arr->password,
                    "status" => $arr->status,
                );
                $this->session->set_userdata($data);
                $this->session->set_userdata('member_islogin', TRUE);
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function get_data($email, $pass) {
        $this->db->where('email', $email);
        $this->db->or_where('phone', $email);
        $this->db->where('password', $pass);
        $this->db->where('status', 'Active');
        $this->db->limit(1);
        $q = $this->db->get('tbl_member');
        $arr = $q->row();
        $num = $q->num_rows();
        if ($num > 0) {
            $data = array(
                "id_member" => $arr->id_member,
                "fullname" => $arr->fullname,
                "email" => $arr->email,
                "address" => $arr->address,
                "phone" => $arr->phone,
                "username" => $arr->username,
                "password" => $arr->password,
                "status" => $arr->status,
            );
            $this->session->set_userdata($data);
            $this->session->set_userdata('member_islogin', TRUE);
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

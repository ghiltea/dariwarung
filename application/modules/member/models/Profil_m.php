<?php

class Profil_m extends CI_Model {
       function get_order($id) {
        $this->db->select('b.*,b.status as st, a.*');
        $this->db->where('id_order', $id);
        $this->db->join('tbl_member a', 'a.id_member = b.id_member', 'left');
        $data = $this->db->get('tbl_order b');
        return $data;
    }
    function get_order_detail($id_order) {
        $this->db->where('id_order', $id_order);
        $data = $this->db->get("tbl_order_detail");
        return $data;
    }
}

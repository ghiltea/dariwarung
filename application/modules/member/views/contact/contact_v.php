<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHH7usNFSzUTfa8sa30R06AkbQn9xP-m8&sensor=false"></script>
<script src="<?php echo base_url('assets/public/'); ?>js/google-map.js"></script>
<!--<div class="hero-wrap hero-bread" style="background-image: url('<?php echo base_url('uploads/banner/') . $banner->image_banner; ?>');">
<div class="container">
<div class="row no-gutters slider-text align-items-center justify-content-center">
    <div class="col-md-9 ftco-animate text-center">
        <p class="breadcrumbs"><span class="mr-2"><a href="<?php echo base_url('home') ?>">Home</a></span> <span>Contact us</span></p>
        <h1 class="mb-0 bread">Contact us</h1>
    </div>
</div>
</div>
</div>-->
<div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-left" style="margin-top: 30px;">
            <h1 class="mb-0 bread" style="color: #00000;">Kontak Kami </h1>
            <p class="breadcrumbs" ><span class="mr-2"><a style="color: #00000;" href="<?php echo base_url(); ?>">Beranda > </a></span> <span style="color: #00000;">Kontak Kami <?php echo $blog->title; ?></span></p>
        </div>
    </div>
</div>
<section class="ftco-section contact-section bg-light" >
    <div class="container">
        <div class="row d-flex mb-5 contact-info">
            <div class="w-100"></div>
            <div class="col-md-3 d-flex">
                <div class="info bg-white p-4">
                    <p><span>Address:</span> <p><a><?php echo $contact->address; ?></a></p>
                </div>
            </div>
            <div class="col-md-3 d-flex">
                <div class="info bg-white p-4">
                    <p><span>Phone:</span> <a href="tel://<?php echo $contact->phone; ?>"><p><?php echo $contact->phone; ?></a></p>
                </div>
            </div>
            <div class="col-md-3 d-flex">
                <div class="info bg-white p-4">
                    <p><span>Email:</span> <a href="mailto:<?php echo $contact->email; ?>"><p><?php echo $contact->email; ?></a></p>
                </div>
            </div>
            <div class="col-md-3 d-flex">
                <div class="info bg-white p-4">
                    <p><span>Website</span> <a href="<?php echo $contact->website; ?>"><p><?php echo $contact->website; ?></a></p>
                </div>
            </div>
        </div>
        <div class="row block-9">
            <div class="col-md-6 order-md-last d-flex">
                <form id="form_contact" action="POST" class="bg-white p-5 contact-form">
                    <div class="form-group">
                        <input name="name" type="text" class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                        <input name="email"  type="text" class="form-control" placeholder="Your Email">
                    </div>
                    <div class="form-group">
                        <input name="subject" type="text" class="form-control" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="button" id="btn_send" value="Send Message" class="btn btn-primary py-3 px-5">
                    </div>
                </form>

            </div>

            <div class="col-md-6 d-flex">
                <div id="map" class="bg-white"></div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $("#btn_send").on('click', function () {
        var data = $("#form_contact").serialize()
        $.ajax({
            url: "<?php echo base_url('member/contact/r_insert'); ?>",
            type: 'POST',
            data: data,
            success: function (response) {
                if (response = "success") {
                    alert('Thanks, Message has been send');
                    location.reload();
                }
            }

        });
    })
</script>
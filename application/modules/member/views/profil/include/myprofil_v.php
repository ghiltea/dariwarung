<div class="row">
    <div class="col-md-12 ftco-animate">
        <div class="cart-list">
            <form method="POST" action="" id="form_profil">

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstname">Full name</label>
                        <input name="fullname" value="<?php echo $this->session->userdata('fullname'); ?>" type="text" class="form-control text-left" style="padding-left: 10px;" placeholder="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstname">Username</label>
                        <input name="username" value="<?php echo $this->session->userdata('username'); ?>" type="text" class="form-control text-left" style="padding-left: 10px;" placeholder="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstname">Email</label>
                        <input name="email" value="<?php echo $this->session->userdata('email'); ?>" type="text" class="form-control text-left" style="padding-left: 10px;" placeholder="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstname">Password</label>
                        <input name="password" type="password" value="" class="form-control text-left" style="padding-left: 10px;" placeholder="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstname">Address</label>
                        <input name="address" value="<?php echo $this->session->userdata('address'); ?>" type="text" class="form-control text-left" style="padding-left: 10px;" placeholder="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstname">Phone</label>
                        <input name="phone" value="<?php echo $this->session->userdata('phone'); ?>" type="text" class="form-control text-left" style="padding-left: 10px;" placeholder="">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="firstname">Type member</label>
                        <input name="type_reseller" readonly="" value="<?php
                        if ($this->session->userdata('type_reseller') == "Yes") {

                            echo "Reseller";
                        } else {
                            echo "Customer";
                        }
                        ?>" type="text" class="form-control text-left" style="padding-left: 10px;" placeholder="">
                    </div>
                </div>

                <p><a href="#" id="btn_update" class="btn btn-primary py-3 px-4">Update profile</a></p>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#btn_update").on('click', function () {
        var data = $("#form_profil").serialize();
        $.ajax({
            url: "<?php echo base_url('member/profil/r_update'); ?>",
            type: 'POST',
            data: data,
            success: function (response) {
                if (response == "success") {
                    alert("Success, Update Your Profil, Please Logout");
                    location.reload();
                }
            }
        })
    })
</script>
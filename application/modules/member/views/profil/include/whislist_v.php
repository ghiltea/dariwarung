<div class="row">
    <div class="col-md-12 ftco-animate">
        <div class="cart-list">
            <table class="table" id="penjualan">
                <thead class="thead-primary">
                    <tr class="text-center">
                        <th>Action</th>
                        <th>Date</th>
                        <th>Image</th>
                        <th>Product name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cart->result() as $row) { ?>    
                        <tr class="text-center">
                            <td class="product-remove"><a onclick="remove();" href="#"><span class="ion-ios-close"></span></a></td>
                            <td class="product-name">
                                <p><?php echo $row->create_date; ?></p>
                            </td>
                            <td class="image-prod"><div class="img" style="background-image:url(<?php echo base_url('uploads/product/') . $row->image; ?>);"></div></td>

                            <td class="product-name">
                                <div hidden="" class="hidden" id="id_whislist"><?php echo $row->id_whislist ?></div>
                                <h3><?php echo $row->name ?></h3>
                                <p><?php echo substr($row->description, 0, 100); ?></p>
                            </td>

                            <td class="price" ><?php
                                $hrg_price = $row->price - ($row->discount * $row->price / 100);
                                echo "Rp " . number_format($hrg_price, 0, ',', '.');
                                ?>
                                <span id="price" class="hidden" hidden=""><?php echo $hrg_price; ?></span></td>

                            <td class="quantity">
                                <div class="input-group mb-3">
                                    <input type="text" name="quantity" onchange="amount();" class="quantity form-control input-number" value="<?php echo $row->qty ?>" min="1" max="100">
                                </div>
                            </td>

                            <td class="total" id='amount'><?php echo "Rp " . number_format($row->amount, 0, ',', '.'); ?>  <span id="price" class="hidden" hidden="" id='amounthide'><?php echo $row->amount; ?></td>
                        </tr><!-- END TR-->
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    function remove() {
        var id_whislist = 0;
        $('#penjualan > tbody  > tr').each(function () {
            id_whislist = $(this).find('#id_whislist').html();
        });
      
        $.ajax({
            url: "<?php echo base_url('member/profil/r_delete_whist') ?>",
            type: 'POST',
            data: {id_whislist: id_whislist},
            success: function (response) {
                //   alert(response);
                if (response == "success") {
                      alert("Success, Delete Item whislist");
                    location.reload();
                } else {
                    alert("failed, delete");
                }
            }
        });
    }
</script>
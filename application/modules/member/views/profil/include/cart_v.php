<section class="ftco-section ftco-cart" style="margin-top: -50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <table class="table" id="penjualan">
                        <thead class="thead-primary">
                            <tr class="text-center">
                                <th>Action</th>
                                <th>Image</th>
                                <th>Product name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($cart->result() as $row) { ?>
                                <tr class="text-center" >
                                    <td class="product-remove">
                                        <a href="" id="remove_product" onclick="remove();"><span class="ion-ios-close"></span></a>
                                    </td>

                                    <td class="image-prod">
                                        <div class="img" style="background-image:url(<?php echo base_url('uploads/product/') . $row->image; ?>);"></div>
                                    </td>

                                    <td class="product-name">
                                        <div hidden="" class="hidden" id="id_cart"><?php echo $row->id_cart ?></div>
                                        <div hidden="" class="hidden" id="id_product"><?php echo $row->id_product ?></div>
                                        <h3><?php echo $row->name ?></h3>
                                        <p><?php echo substr($row->description, 0, 100); ?></p>
                                    </td>

                                    <td class="price" ><?php
                                        $hrg_price = $row->price - ($row->discount * $row->price / 100);
                                        echo "Rp " . number_format($hrg_price, 0, ',', '.');
                                        ?>
                                        <span id="price" class="hidden" hidden=""><?php echo $hrg_price; ?></span>
                                    </td>

                                    <td class="quantity">
                                        <div class="input-group mb-3">
                                            <input type="text" name="quantity" onchange="amount();" class="quantity form-control input-number" value="<?php echo $row->qty ?>" min="1" max="100">
                                        </div>
                                    </td>

                                    <td class="total" id='amount'>
                                        <?php echo "Rp " . number_format($row->amount, 0, ',', '.'); ?>
                                        <span  id='amounthide'></span>
                                    </td>
                                </tr><!-- END TR-->
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row justify-content-end">

            <div class="col-lg-12 mt-5 cart-wrap ftco-animate">
                <small>
                    Note : Pesanan Akan dikirim sesuai dengan alamat yang tertera pada akun anda.
                    <p>1. Lakukan checkout Orderan anda</p>
                    <p>2. Lakunan Pembayaran Transfer Ke Rekening :xxxxxxxxx an xxxxx xxx </p>
                    <p>3. Konfirmasi pembayaran lewat Contact Kami /Whatsapp Cs : 08276536XXXX </p>
                    <p>4. Pesanan Anda Akan segera di kirim</p>
                </small>
            </div>
            <div class="col-lg-12 mt-5 cart-wrap ftco-animate">
                <div class="cart-total mb-3">
                    <h3>Cart Totals</h3>
                    <p class="d-flex">
                        <span>Subtotal</span>
                        <span>Rp <a id="subtotal">0</a></span>
                        <span><a hidden="" class="hidden" id="subtotalhide">0</a></span>
                        <a hidden="" class="hidden" id="qtyhide"></a>
                    </p>
                    <p class="d-flex">
                        <span>Tax</span>
                        <span>Rp <a id="tax">0</a></span>
                        <span><a hidden="" class="hidden" id="taxhide">0</a></span>
                    </p>
                    <p class="d-flex">
                        <span>Delivery</span>
                        <span>Rp <a id="delivery">0</a></span>
                        <span><a hidden="" class="hidden" id="deliveryhide">0</a></span>
                    </p>
                    <p class="d-flex">
                        <span>Discount</span>
                        <span>Rp <a id="discount">0</a></span>
                        <span><a hidden="" class="hidden" id="discounthide">0</a></span>
                    </p>
                    <hr>
                    <p class="d-flex total-price" style="color: #000;">
                        <span>Total</span>
                        <span>Rp <a id="total">0</a></span>
                        <span><a hidden="" class="hidden" id="totalhide">0</a></span>
                    </p>
                </div>
                <p><a href="#" id="checkout" class="btn btn-primary py-3 px-4">Proceed to Checkout</a></p>
            </div>
        </div>
    </div>
</section>



<script type="text/javascript">

    $('#subcribe').on('click', function () {
        var email = $("input[name=email_subcribe]").val();
        $.ajax({
            url: "<?php echo base_url('admin/subcribe/r_insert') ?>",
            type: 'POST',
            data: {email: email},
            success: function (response) {
                //   alert(response);
                if (response == "success") {
                    alert("Success, Your mail Subcribe Newsletter !");
                    $("input[name=email_subcribe]").val("");
                }
            },
            error: function () {
                alert("Failed, Alerdy Exist mail Subcribe Newsletter !");
            }

        })

    });

    $('#checkout').on('click', function () {
        var tot = $("#totalhide").text();
        if (tot == 0) {
            alert("Cart Empty !")
        } else {
            save();
        }


    });

</script>


<script  type="text/javascript">

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    function amount() {
        var sum = 0;
        var qtysum = 0;
        $('#penjualan > tbody  > tr').each(function () {
            var qty = $(this).find('input[name=quantity]').val();
            var price = $(this).find('#price').text();
            var price_int = parseInt(price);
            var amounts = qty * price_int;
            sum += amounts;
            qtysum += parseInt(qty);
            $(this).find('#amounthide').text(amounts);
            $(this).find('#price').text(price);
            $(this).find('#amount').text("Rp " + numberWithCommas(amounts));
        });
//        var taxsum = (sum*10)/100;
//        var totsum = sum + taxsum;
        //  alert(qtysum);
        $('#subtotal').text(numberWithCommas(sum));
        $('#subtotalhide').text(sum);
        $('#qtyhide').text(qtysum);
        $('#dicount').text(numberWithCommas(0));
        $('#dicounthide').text(0);
        $('#delivery').text(numberWithCommas(0));
        $('#deliveryhide').text(0);
        $('#tax').text(numberWithCommas(0));
        $('#taxhide').text(0);
        $('#total').text(numberWithCommas(sum));
        $('#totalhide').text(sum);
    }

    function remove() {
        var id_cart = 0;
        $('#penjualan > tbody  > tr').each(function () {
            id_cart = $(this).find('#id_cart').html();
        });
        alert("Success, Delete Item Cart");
        $.ajax({
            url: "<?php echo base_url('member/shop/r_delete_cart') ?>",
            type: 'POST',
            data: {id_cart: id_cart},
            success: function (response) {
                //   alert(response);
                if (response == "success") {
                    location.reload();
                } else {
                    alert("failed, delete");
                }
            }
        });
    }

    function save() {
        var code_order = "";
        var id_order = "";
        var tot_amount = $("#totalhide").text();
        var tot_qty = $("#qtyhide").text();
        var tot_delivery = $("#deliveryhide").text();
        var tot_discount = $("#discounthide").text();
        var tot_tax = $("#taxhide").text();
        $.ajax({
            url: "<?php echo base_url('member/shop/r_checkout_header'); ?>",
            type: 'POST',
            data: {tot_tax: tot_tax, tot_amount: tot_amount, tot_qty: tot_qty, tot_delivery: tot_delivery, tot_discount: tot_discount},
            success: function (data) {
                console.log(data);
                var jdata = JSON.parse(data);
                code_order = jdata.code_order;
                id_order = jdata.id_order;
                //insert detail order
                $('#penjualan > tbody  > tr').each(function () {
                    var id_product = $(this).find('#id_product').html();
                    var price = $(this).find('#price').html();
                    var qty = $(this).find('input[name=quantity]').val();
                    //alert(id_product);
                    $.ajax({
                        url: "<?php echo base_url('member/shop/r_checkout_detail'); ?>",
                        type: 'POST',
                        data: {id_product: id_product, price: price, qty: qty, id_order: id_order, code_order: code_order},
                        success: function (data) {
                            console.log(data);
                        }
                    });
                });
            }
        });


        //delete cart

        $.ajax({
            url: "<?php echo base_url('member/shop/r_cart_delete'); ?>",
            type: 'POST',
            data: {tot_tax: tot_tax, tot_amount: tot_amount, tot_qty: tot_qty, tot_delivery: tot_delivery, tot_discount: tot_discount},
            success: function (data) {
                alert("Silahkan Lakukan pembayaran No Pesanan : " + code_order);
                window.location.href = "<?php echo base_url('profil/order'); ?>";
            }
        });



    }

    $(document).ready(function () {
        amount();

    });

</script>

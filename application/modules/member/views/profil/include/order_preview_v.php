<style>
    @media print{
        .non-print{
            display: none;
        }
    }
    .table { 
        margin-bottom: 2px; 
        border-width: 0px; 
    }


</style> 
<div class="container">

    <!-- Row -->
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <h5 class="hk-sec-title"><?php echo $title; ?></h5>
                <hr>


                <section class="content">
                    <div style="margin-top: 10px;">
                        <button type="button"  onclick="window.print()" class="btn btn-success con_txt2 non-print">
                            <span class="fa fa-print"> Print</span>
                        </button>
                        <a type="button"  href="<?php echo base_url('profil/order');?>" class="btn btn-info con_txt2 non-print" >
                            <span class="fa fa-file-pdf-o"> Back</span>
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <!-- general form elements -->
                            <table class="table table-bordered data_table">
                                <tbody>

                                    <tr>
                                        <td colspan="4">
                                            <table class="table">
                                                <tr>
                                                    <td align="top" style="padding-top: -100px;">
                                                        <strong> Order Id : <?php echo $order->code_order; ?></strong>
                                                        <br />
                                                        <strong> Order Date : <?php echo date("d-m-Y h:i A", strtotime($order->date)); ?></strong>
                                                        <br />
                                                        <h1 style="
                                                        <?php
                                                        if ($order->st == "Unpaid") {
                                                            echo 'color: red;';
                                                        }else if ($order->st == "Paid") {
                                                            echo 'color: greenyellow;';
                                                        }
                                                        ?> ">
                                                            <?php echo $order->st; ?></h1>
                                                    </td>
                                                    <td>
                                                        <strong> Delivery Details :</strong><br />
                                                        <strong> Contact  : <?php echo $order->fullname; ?> <br/> Phone : <?php echo $order->phone; ?></strong><br />
                                                        <strong> Address : </strong>
                                                        <address>
                                                            <?php echo $order->address; ?><br />
                                                        </address>


                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Product Name :</th>
                                        <th> Price :</th>
                                        <th> Qty :</th>
                                        <th> Amount: </th>
                                    </tr>
                                    <?php
                                    $total_price = 0;
                                    foreach ($order_detail->result() as $items) {
                                        ?>
                                        <tr>
                                            <td><?php echo $items->name; ?><br />
                                            </td>
                                            <td><?php echo $items->price_net; ?><br />
                                            </td>
                                            <td>
                                                <?php echo $items->qty; ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo number_format($items->qty * $items->price_net,0,',','.');
                                                $total_price = $total_price + ($items->qty * $items->price);
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right"> Total :</strong></td>
                                        <td >
                                            <strong class=""><?php echo number_format($total_price,0,',','.'); ?>  </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right">Tax :</strong></td>
                                        <td >
                                            <strong class=""><?php echo $order->total_tax; ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right">Discount:</strong></td>
                                        <td >
                                            <strong class=""><?php echo $order->total_discount; ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right">Delivery Charges :</strong></td>
                                        <td >
                                            <strong class=""><?php echo $order->total_delivery; ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong class="pull-right">Net Total Amount :</strong></td>
                                        <td >
                                            <strong class=""><?php echo number_format($total_price,0,',','.');?></strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Main row -->
                </section><!-- /.content -->

        </div>
    </div>
</div>



<div class="row">
            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <table class="table" id="penjualan">
                        <thead class="thead-primary">
                            <tr class="text-center">
                                <th>Action</th>
                                <th>Status</th>
                                <th>No Order</th>
                                <th>Date Transaction</th>
                                <th>Payment</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($cart->result() as $row) { ?>    
                                <tr class="text-center">
                                    <td class="product-remove"><a href="<?php echo base_url('member/profil/order_preview/').$row->id_order; ?>"><span class="ion-ios-print"></span></a></td>
                                    
                                     <td class="status">
                                        <h3><?php echo $row->status; ?></h3>
                                    </td>
                                    <td class="no_order">
                                        <h3><?php echo $row->code_order; ?></h3>
                                    </td>

                                    <td class="tanggal" ><?php
                                        echo $row->date;
                                        ?>
                                       </td>
                                    <td class="payment" >Transfer Method
                                       </td>

                                    <td class="total" id='amount'><?php echo "Rp " . number_format($row->total_amount, 0, ',', '.'); ?>  <span id="price" class="hidden" hidden="" id='amounthide'><?php echo $row->total_amount; ?></td>
                                </tr><!-- END TR-->
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
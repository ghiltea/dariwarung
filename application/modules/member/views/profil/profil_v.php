<hr>
<section class="ftco-section ftco-cart" style="margin-top: -50px;">
    <div class="container">

        <div class="row justify-content-end">
            <div class="col-lg-3 cart-wrap ftco-animate">
                <div class="cart-total mb-3">
                    <h3>MAIN MENU</h3>
                    <hr>
                    <p><a href="<?php echo base_url('profil'); ?>"><img src="<?php echo base_url(); ?>assets/admin/img/user.png" style="width: 80px; height: 80px;"></span><sas style=" margin-left: 5px; color: grey">Hi,</sas> <?php echo $this->session->userdata('fullname'); ?></a></p>
                    <hr>
                    <h3> <a href="<?php echo base_url('profil'); ?>">My Profile</a></h3>
                    <h3> <a href="<?php echo base_url('profil/whislist'); ?>">Whislist</a></h3>
                    <h3> <a href="<?php echo base_url('profil/cart'); ?>">Cart</a></h3>
                    <h3> <a href="<?php echo base_url('profil/order'); ?>">History Order</a></h3>

                </div>
            </div>
            <div class="col-lg-9  cart-wrap ftco-animate">
                <div class="cart-total mb-3">

                    <h3 class="text-right"> 
                        <a href="<?php echo base_url('profil/cart');?>"><span class="icon-shopping_cart"></span> <span class="badge badge-danger" id="notifcart">0</span></a> &nbsp; &nbsp; |
                        <a href="<?php echo base_url('profil/order');?>"><span class="icon-bell"></span> <span class="badge badge-danger" id="notif count"><?php echo $notif; ?></span></a> 
                        &nbsp; &nbsp; | Profile > <?php echo $title; ?></h3>
                    <hr>
                    <?php echo $this->load->view($content_cild_view); ?>
                   
                </div>
            </div>

        </div>
    </div>
</section>


<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
    <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
            <div class="col-md-6">
                <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                <span>Get e-mail updates about our latest shops and special offers</span>
            </div>
            <div class="col-md-6 d-flex align-items-center">
                <form action="#" class="subscribe-form">
                    <div class="form-group d-flex">
                        <input type="text" name="email_subcribe" id="email" class="form-control" placeholder="Enter email address">
                        <input type="button" id="subcribe" value="Subscribe" class="submit px-3">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">


    $('#subcribe').on('click', function () {
        var email = $("input[name=email_subcribe]").val();
        $.ajax({
            url: "<?php echo base_url('admin/subcribe/r_insert') ?>",
            type: 'POST',
            data: {email: email},
            success: function (response) {
                //   alert(response);
                if (response == "success") {
                    alert("Success, Your mail Subcribe Newsletter !");
                    $("input[name=email_subcribe]").val("");
                }
            },
            error: function () {
                alert("Failed, Alerdy Exist mail Subcribe Newsletter !");
            }

        })
    });
</script>

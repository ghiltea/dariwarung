<style>
    #map-canvas {
        height: 500px;
        width: 100%;
        margin: 0px;
        padding: 0px;
    }
</style>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDF3ux1MY0FWAGxkaTh0LoTbOVFL1riU7o&sensor=false"></script>
<script>

    var Lat =<?php echo "-6.914744" ?>;
    var Lng =<?php echo "107.609810"; ?>;

    function initialize() {
        var myLatlng = new google.maps.LatLng(Lat, Lng);
        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Hello World!'
        });

    }
    google.maps.event.addDomListener(window, 'load', initialize);

</script>

<br>
<div class="text-center">
    <h1>Lokasi Dariwarung</h1>
    <p class="text-gray-44">Cari lokasi terdekat dengan tempat anda, Sehingga lebih cepat, mudah, murah untuk memenuhi segala kebutuhan anda</p>
</div>
<br>
<section class="ftco-section ftco-cart">
    <div class="container">
        <div id="map-canvas"></div>
    </div>
</section>
<br>
<br>


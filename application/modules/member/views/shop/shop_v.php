<!--<div class="hero-wrap hero-bread" style="background-image: url('<?php echo base_url('uploads/banner/') . $banner->image_banner; ?>');">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="<?php echo base_url('home'); ?>">Home</a></span> <span>Products</span></p>
                <h1 class="mb-0 bread">Products</h1>
            </div>
        </div>
    </div>
</div>-->

<div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-left" style="margin-top: 30px;">
            <h1 class="mb-0 bread" style="color: #00000;">Produk </h1>
            <p class="breadcrumbs" ><span class="mr-2"><a style="color: #00000;" href="<?php echo base_url(); ?>">Beranda > </a></span> <span style="color: #00000;">Produk</span></p>

        </div>
    </div>
</div>

<section class="ftco-section" style="margin-top: -50px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 mb-5 text-center">
                <ul class="product-category">
                    <!--<li><a href="<?php // echo base_url('shop');   ?>" class="active">All</a></li>-->
                    <li><a href="<?php echo base_url('shop'); ?>" class="active">All</a></li>
                    <?php foreach ($category->result() as $r) { ?>
                        <li><a  href="<?php echo base_url('shop/category/') . $r->id_category; ?>"><?php echo $r->title; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="row">
            <?php foreach ($product->result() as $row) { ?>
                <?php $hrg_dis = $row->price - $row->discount * $row->price / 100; ?>

                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="product">
                        <a rel="<?php echo $row->seo_canonical; ?>" href="<?php echo base_url('shop/detail/') . $row->id_product; ?>"  class="img-prod"><img class="img-fluid" src="<?php echo base_url('uploads/product/') . $row->image; ?>" alt="<?php echo $row->seo_image_alt; ?>">
                            <?php if ($row->discount > 0) { ?>
                                <span class="status"><?php echo $row->discount; ?>%</span>
                            <?php } ?>
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3 text-center">
                            <h3><a rel="<?php echo $row->seo_canonical; ?>" href="<?php echo base_url('shop/detail/') . $row->id_product; ?>"><?php echo $row->name; ?></a></h3>
                            <div class="d-flex">
                                <div class="pricing">
                                    <?php if ($row->discount > 0) { ?>
                                        <p class="price"><span class="mr-2 price-dc"><?php echo"Rp " . number_format($row->price, 0, ",", "."); ?></span><span class="price-sale"><?php echo"Rp " . number_format($hrg_dis, 0, ",", "."); ?></span></p>
                                    <?php } else { ?>
                                        <p class="price"><span class="price-sale"><?php echo"Rp " . number_format($row->price, 0, ",", "."); ?></span></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="bottom-area d-flex px-3">
                                <div class="m-auto d-flex">
                                    <a rel="<?php echo $row->seo_canonical; ?>" href="<?php echo base_url('shop/detail/') . $row->id_product; ?>" class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                        <span><i class="ion-ios-menu"></i></span>
                                    </a>
                                    <a id="add_cart" href="#" id_prod_cart="<?php echo $row->id_product; ?>" onclick="addcart(<?php echo $row->id_product; ?>);" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                        <span><i class="ion-ios-cart"></i></span>
                                    </a>
                                    <a id="add_whislist" href="#"  id_prod_whislist="<?php echo $row->id_product; ?>" onclick="addwhislist(<?php echo $row->id_product; ?>);" class="heart d-flex justify-content-center align-items-center ">
                                        <span><i class="ion-ios-heart"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>



        </div>

    </div>
</section>


<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
    <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
            <div class="col-md-6">
                <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                <span>Get e-mail updates about our latest shops and special offers</span>
            </div>
            <div class="col-md-6 d-flex align-items-center">
                <form action="#" class="subscribe-form">
                    <div class="form-group d-flex">
                        <input type="text" name="email_subcribe" id="email" class="form-control" placeholder="Enter email address">
                        <input type="button" id="subcribe" value="Subscribe" class="submit px-3">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">


    $('#subcribe').on('click', function () {
        var email = $("input[name=email_subcribe]").val();
        $.ajax({
            url: "<?php echo base_url('admin/subcribe/r_insert') ?>",
            type: 'POST',
            data: {email: email},
            success: function (response) {
                //   alert(response);
                if (response == "success") {
                    alert("Success, Your mail Subcribe Newsletter !");
                    $("input[name=email_subcribe]").val("");
                }
            },
            error: function () {
                alert("Failed, Alerdy Exist mail Subcribe Newsletter !");
            }

        })

    });
</script>
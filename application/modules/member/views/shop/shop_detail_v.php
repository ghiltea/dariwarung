<div class="hero-wrap hero-bread" style="background-image: url('<?php echo base_url('uploads/banner/').$banner->image_banner; ?>');">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="<?php echo base_url(); ?>">Home</a></span> <span class="mr-2"><a href="<?php echo base_url('shop'); ?>">Product</a></span> <span>Product Detail</span></p>
                <h1 class="mb-0 bread">Product Detail</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-5 ftco-animate">
                <a rel="<?php echo $detail->seo_canonical; ?>" href="<?php echo base_url('uploads/product/') . $detail->image; ?>" class="image-popup"><img src="<?php echo base_url('uploads/product/') . $detail->image; ?>" class="img-fluid" alt="<?php echo $detail->seo_image_alt; ?>"></a>
            </div>
            <div class="col-lg-6 product-details pl-md-5 ftco-animate">
                <h3><?php echo $detail->name; ?></h3>
                <?php if ($detail->discount > 0) { ?>
                    <div class="rating d-flex">
                        <p class="text-left mr-4">
                            <a href="#" class="mr-2">Discount : <?php echo $detail->discount; ?> % </a>
                        </p>
                        <p class="text-left">
                            <a href="#" class="mr-2" style="color: #000;"> <span style="color: #bbb;"> <s><?php echo number_format($detail->price, 0, ',', '.'); ?></s></span></a>
                        </p>

                    </div>
                    <?php $hrg_dis = $detail->price - $detail->discount * $detail->price / 100; ?>
                    <p class="price"><span><?php echo "Rp " . number_format($hrg_dis, 0, ',', '.') ?></span></p>
                <?php } else { ?>
                    <p class="price"><span><?php echo "Rp " . number_format($detail->price, 0, ',', '.') ?></span></p>

                <?php } ?>
                <p><?php echo $detail->description; ?>.
                </p>
                <div class="row mt-4">

                    <div class="w-100"></div>
                    <div class="input-group col-md-6 d-flex mb-3">
                        <span class="input-group-btn mr-2">
                            <button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
                                <i class="ion-ios-remove"></i>
                            </button>
                        </span>
                        <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                        <input hidden="" class="hidden" type="text" id="id_product" name="id_product" value="<?php echo $detail->id_product;?>">
                        <input hidden="" class="hidden" type="text" id="name" name="name" value="<?php echo $detail->name;?>">
                        <input hidden="" class="hidden" type="text" id="price" name="price" value="<?php echo $detail->price;?>" >
                        <input hidden="" class="hidden" type="text" id="discount" name="discount" value="<?php echo $detail->discount;?>">
                        <input hidden="" class="hidden" type="text" id="weight" name="weight" value="<?php echo $detail->weight;?>">
                        <span class="input-group-btn ml-2">
                            <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                                <i class="ion-ios-add"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <p><a style="color: #fff;" id="btn_add_cart" class="btn btn-black py-3 px-5">Add to Cart</a></p>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
            <div class="col-md-12 heading-section text-center ftco-animate">
                <span class="subheading">Products</span>
                <h2 class="mb-4">Related Products</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php foreach ($product->result() as $row) { ?>
                <?php $hrg_dis = $row->price - $row->discount * $row->price / 100; ?>

                <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="product">
                        <a rel="<?php echo $row->seo_canonical; ?>"href="<?php echo base_url('shop/detail/') . $row->id_product; ?>" class="img-prod"><img class="img-fluid" src="<?php echo base_url('uploads/product/') . $row->image; ?>" alt="<?php echo $row->seo_image_alt; ?>">
                            <?php if ($row->discount > 0) { ?>
                                <span class="status"><?php echo $row->discount; ?>%</span>
                            <?php } ?>
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3 text-center">
                            <h3><a href="<?php echo base_url('shop/detail/') . $row->id_product; ?>"><?php echo $row->name; ?></a></h3>
                            <div class="d-flex">
                                <div class="pricing">
                                    <?php if ($row->discount > 0) { ?>
                                        <p class="price"><span class="mr-2 price-dc"><?php echo"Rp " . number_format($row->price, 0, ",", "."); ?></span><span class="price-sale"><?php echo"Rp " . number_format($hrg_dis, 0, ",", "."); ?></span></p>
                                    <?php } else { ?>
                                        <p class="price"><span class="price-sale"><?php echo"Rp " . number_format($row->price, 0, ",", "."); ?></span></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="bottom-area d-flex px-3">
                                <div class="m-auto d-flex">
                                    <a href="<?php echo base_url('shop/detail/') . $row->id_product; ?>" class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                        <span><i class="ion-ios-menu"></i></span>
                                    </a>
                                    <a href="<?php echo base_url('member/product/cart') . $row->id_product; ?>" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                        <span><i class="ion-ios-cart"></i></span>
                                    </a>
                                    <a href="<?php echo base_url('member/product/favorit') . $row->id_product; ?>" class="heart d-flex justify-content-center align-items-center ">
                                        <span><i class="ion-ios-heart"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</section>

<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
    <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
            <div class="col-md-6">
                <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                <span>Get e-mail updates about our latest shops and special offers</span>
            </div>
            <div class="col-md-6 d-flex align-items-center">
                <form action="#" class="subscribe-form">
                    <div class="form-group d-flex">
                        <input type="text" class="form-control" placeholder="Enter email address">
                        <input type="submit" value="Subscribe" class="submit px-3">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script>
    $(document).ready(function () {




        var quantitiy = 0;
        $('.quantity-right-plus').click(function (e) {

            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            $('#quantity').val(quantity + 1);


            // Increment

        });

        $('.quantity-left-minus').click(function (e) {
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            // Increment
            if (quantity > 0) {
                $('#quantity').val(quantity - 1);
            }
        });




        $('#btn_add_cart').on('click', function () {
            var id_product = $("input[name=id_product]").val();
            var name   = $("input[name=name]").val();
            var price = $("input[name=price]").val();
            var discount = $("input[name=discount]").val();
            var weight = $("input[name=weight]").val();
            var quantity = $("input[name=quantity]").val();
            $.ajax({
                url: "<?php echo base_url('member/shop/r_insert') ?>",
                type: 'POST',
                data: {id_product: id_product, name:name, price:price, discount:discount, weight:weight, quantity:quantity},
                success: function (response) {
                    //   alert(response);
                    if (response == "success") {
                        alert("Success, Add To Cart !");
                        $("input[name=email_subcribe]").val("");
                    }
                },
                error: function () {
                    alert("Failed, Add To Cart !");
                }

            })
        });


    });
</script>

</body>
</html>

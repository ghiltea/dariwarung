
<br>
<br>
<div class="mx-xl-10">
    <div class="text-center">
        <h1>Lacak Pesanan</h1>
        <p class="text-gray-90 px-xl-10" >" Lacak Pesanan Kamu, Udah sampai mana ! "</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- Subscribe Form -->
            <form class="js-validate js-form-message" novalidate="novalidate">

                <div class="input-group input-group-pill">

                    <input type="email" class="form-control border-2 height-40" name="email_subcribe" id="subscribeSrEmail" placeholder="Masukan No Order " aria-label="Alamat Email" aria-describedby="subscribeButton" required="" data-msg="Silahkan masukan alamat email dengan benar !.">
                    <div class="input-group-append">
                        <button type="button" id="btn_subcribe" class="btn btn-dark btn-sm-wide height-40 py-2">Lacak</button>
                    </div>
                </div>
            </form>
            <!-- End Subscribe Form -->
        </div>

    </div>

</div>
<center style="margin-bottom: 150px; margin-top: 100px">
    <p class="text-gray-180 px-xl-10" >Hasil Lacak Pengiriman .... !</p>
</center>
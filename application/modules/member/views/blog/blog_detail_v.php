
<!--<div class="hero-wrap hero-bread" style="background-image: url('<?php echo base_url('uploads/banner/') . $banner->image_banner; ?>');">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="<?php echo base_url(); ?>">Home</a></span> <span>Blog</span></p>
                <h1 class="mb-0 bread">Blog</h1>
            </div>
        </div>
    </div>
</div>-->


<div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-left" style="margin-top: 30px;">
            <h1 class="mb-0 bread" style="color: #00000;">Blog </h1>
            <p class="breadcrumbs" ><span class="mr-2"><a style="color: #00000;" href="<?php echo base_url(); ?>">Beranda > </a></span> <span style="color: #00000;">Blog <?php echo $blog->title; ?></span></p>

        </div>
    </div>
</div>
<section class="ftco-section ftco-degree-bg" style="margin-top: -50px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ftco-animate">
                <h3 class="mb-3"><?php echo $blog->title; ?></h3>
                <div class="meta"><?php echo $blog->create_date; ?>, <?php echo $blog->create_by; ?></div>
                <p>
                    <img src="<?php echo base_url('uploads/blog/') . $blog->image; ?>" alt="" class="img-fluid">
                </p>
                <p><?php echo $blog->description; ?></p>




            </div> <!-- .col-md-8 -->
            <div class="col-lg-4 sidebar ftco-animate">
                <div class="sidebar-box">
                    <form action="<?php echo base_url('member/shop/search'); ?>" method="POST" class="search-form">
                        <div class="form-group">
                            <span class="icon ion-ios-search"></span>
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </form>
                </div>


                <div class="sidebar-box ftco-animate">
                    <h3 class="heading">Categories</h3>
                    <ul class="categories">
                        <?php foreach ($blog_category->result() as $row) { ?>
                            <li><a href="<?php echo base_url('blog/') . $row->id_category; ?>"><?php echo $row->title; ?> </a></li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="sidebar-box ftco-animate">
                    <h3 class="heading">Recent Latest Blog</h3>
                    <?php foreach ($latest_blog->result() as $row) { ?>
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4" style="background-image: url(<?php echo base_url('uploads/blog/') . $row->image; ?>);"></a>
                            <div class="text">
                                <h3 class="heading-1"><a href="<?php echo base_url('member/blog/detail/') . $row->id_blog ?>"><?php echo substr($row->description, 0, 50); ?></a></h3>
                                <div class="meta">
                                    <div><a href="<?php echo base_url('member/blog/detail/') . $row->id_blog ?>"><span class="icon-calendar"></span> <?php echo $row->create_date; ?></a></div>
                                    <div><a href="<?php echo base_url('member/blog/detail/') . $row->id_blog ?>"><span class="icon-person"></span> <?php echo $row->create_by; ?></a></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?> 


                </div>
            </div>

        </div>
    </div>
</section> <!-- .section -->
<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
    <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
            <div class="col-md-6">
                <h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
                <span>Get e-mail updates about our latest shops and special offers</span>
            </div>
            <div class="col-md-6 d-flex align-items-center">
                <form action="#" class="subscribe-form">
                    <div class="form-group d-flex">
                        <input type="text" name="email_subcribe" id="email" class="form-control" placeholder="Enter email address">
                        <input type="button" id="subcribe" value="Subscribe" class="submit px-3">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">


    $('#subcribe').on('click', function () {
        var email = $("input[name=email_subcribe]").val();
        $.ajax({
            url: "<?php echo base_url('admin/subcribe/r_insert') ?>",
            type: 'POST',
            data: {email: email},
            success: function (response) {
                //   alert(response);
                if (response == "success") {
                    alert("Success, Your mail Subcribe Newsletter !");
                    $("input[name=email_subcribe]").val("");
                }
            },
            error: function () {
                alert("Failed, Alerdy Exist mail Subcribe Newsletter !");
            }

        })

    });
</script>
<hr>
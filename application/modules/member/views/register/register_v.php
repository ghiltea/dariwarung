<div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-left" style="margin-top: 30px;">
            <h1 class="mb-0 bread" style="color: #00000;">Daftar </h1>
            <p class="breadcrumbs" ><span class="mr-2"><a style="color: #00000;" href="<?php echo base_url(); ?>">Beranda > </a></span> <span style="color: #00000;">Daftar</span></p>

        </div>
    </div>
</div>
<section class="ftco-section contact-section bg-light">
    <div class="container">
        <?php if ($this->session->flashdata('msg_error_register')) { ?>
            <div class="alert alert-danger alert-rounded"> <i class="ti-alert"></i> &nbsp;&nbsp;<?php echo $this->session->flashdata('msg_error_login'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
            </div>
        <?php } ?>
        <div class="row block-9">
            <div class="col-md-6 order-md-last d-flex">
                <form action="<?php echo base_url('member/register/r_insert') ?>" method="POST"class="bg-white p-5 contact-form">
                    <h4>Buat akun RedLine Anda</h4>
                    <div class="form-group">
                        <input type="text" name="fullname" class="form-control" placeholder="Your Name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="Your Email">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Register" class="btn btn-primary py-3 px-5">
                    </div>
                     <p>already have an account, <b style="color: #AA227F"><a href="<?php echo base_url('register'); ?>">Login here</a> </b></p>
                </form>

            </div>


            <div class="col-md-6 p-md-6 img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?php echo base_url('assets/public/') ?>images/about.png);">
                <a href="https://youtu.be/HaO14cpwUXg" class="icon popup-vimeo d-flex justify-content-center align-items-center">
                    <span class="icon-play"></span>
                </a>
            </div>
        </div>
</section>

